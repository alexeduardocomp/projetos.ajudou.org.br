<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<div class="row galeria">
    <?php
        $query = $this->db->get_where('video', array(
            'pagina' => $pagina
        )); 
        $videos = $query->result_array();
       foreach ($videos as $row) :
    ?>
    <div class="col-md-6 video">
        <h4 style="margin:20px 0px;margin-left: 10px;">
            <i class="entypo-folder"></i>
            <?php echo $row['titulo']; ?>
        </h4>
        <div class="videos">
            <?php echo $row['link']; ?>
        </div>
    </div>
    <?php
       endforeach; 
    ?>
</div>
<style>
.galeria{
    display: block;
    margin: 0 auto;
    width: 100%;
    background-color: white;
    text-align: center;
}
.videos{
    width: auto;
    position: relative;
    height: 300px;
    box-shadow: 0px 10px 30px 0px rgb(82 63 105 / 58%);
    border-radius: 20px;
}
.videos iframe{
    width: 100%;
    height: 100%;
    border-radius: 20px;
}
.video video{
    width: 100%;
}
.video h4{
    text-align: left;
}
</style>

<script type="text/javascript">
    $( "#running_year" ).change(function() {  
        var running_year = $("#running_year").val();
        console.log(running_year);
        var id = $("#id_turma").val();
        var conteudo_tabela = document.getElementById('tabela_alunos');
        conteudo_tabela.innerHTML = "";

        $.ajax('<?php echo site_url('admin/get_tabela_alunos_ano/'); ?>', {
            type: 'POST',  // http method
            data: { 
                running_year: running_year,
                id: id,                
            },  // data to submit
            success: function (response) {
                var tabela = response;
                var str = tabela.replace('"', '');
                var string = str.replace(/\\n/g, "")
                                .replace(/\\'/g, "\\'")
                                .replace(/\\"/g, '\\"')
                                .replace(/\\&/g, "\\&")
                                .replace(/\\r/g, "")
                                .replace(/\\/g, "")
                                .replace(/\\t/g, "\\t")
                                .replace(/\\b/g, "\\b")
                                .replace(/\\f/g, "\\f");
                conteudo_tabela.innerHTML = string  ;
            }
        });
    });

    jQuery(document).ready(function($) {
        $('.datatable').DataTable({
            "scrollX": true,
            "oLanguage": {
                "sProcessing": "Aguarde enquanto os dados são carregados ...",
                "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
                "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                "sInfoFiltered": "",
                "sSearch": "Procurar",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },


            dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
            buttons: [

                {
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'csv',
                    text: 'CSV'
                }
            ],
        });
    });
</script>