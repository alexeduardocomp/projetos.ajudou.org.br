<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once('phpmailer/class.phpmailer.php');
include_once('phpmailer/class.smtp.php');
include_once('phpmailer/PHPMailerAutoload.php');
class Email_model extends CI_Model {

	function __construct()
    {
        parent::__construct();
    }

	function account_opening_email($account_type = '' , $email = '', $password = '')
	{
		$system_name	=	$this->db->get_where('settings' , array('type' => 'system_name'))->row()->description;

		$email_msg		=	"Welcome to ".$system_name."<br />";
		$email_msg		.=	"Your account type : ".$account_type."<br />";
		$email_msg		.=	"Your login password : ". $password ."<br />";
		$email_msg		.=	"Login Here : ".base_url()."<br />";

		$email_sub		=	"Account opening email";
		$email_to		=	$email;

		//$this->do_email($email_msg , $email_sub , $email_to);
		$this->send_smtp_mail($email_msg , $email_sub , $email_to);
	}

	function password_reset_email($new_password = '' , $account_type = '' , $email = '')
	{
		$query	=	$this->db->get_where($account_type , array('email' => $email));
		if($query->num_rows() > 0)
		{
			$row = $query->row();
			$teste = $row->name; 
			$nome = explode(" ", $teste);
			$nome = $nome[0]. (isset($nome[1])?' '.$nome[1]:'');
			$email_msg = '<div>'.
							'<div class="img_suspenso" style="background-color: trasnparent;width: 80%;display: block; margin: 0 auto;margin-bottom: 2%;">'.
							'<a href="https://projetos.ajudou.org.br" target="_blank"><img style="object-fit: cover;display: block; margin: 0 auto;margin-top: 5%;" src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" alt="Ajudou"></a>'
							.'</div>'

							.'<div style="background-color: trasnparent;width: 80%;margin-left: 10%;margin-right: 10%;text-align: left;">
								<p style="font-size:15px;line-height:1;color:#000000; font-family: Ubuntu, sans-serif; text-align: left;">
								Caro(a) <span style="color:#000000;font-weight:600;">'.$nome.',</span> <br><br>
								Você solicitou a recuperação de senha de acesso ao sistema do Ajudôu, uma nova senha aleatória foi gerada. Para garantir a segurança da sua conta e evitar problemas com login novamente, pediimos que altere a nova senha
								assim que acessar o sistema. <span style="color:#000000;font-weight:600;">Sua nova senha é: </span></p>'
									
									.'<div  style="font-family: Ubuntu, sans-serif;border-radius: 10px;background-color: #e3051b;color: white;font-weight:600;padding: 10px;width: 25%;text-align: center;display: block; margin: 0 auto;">'.
									$new_password

									.'</div>'
								
							.'</div>' 
							.'<style>
							a.cta{background-color:#a058ae;top:0;padding:15px 40px;font-family: Ubuntu, sans-serif;text-decoration:none;line-height:1;color:#fff;position:relative;font-size:19px;font-weight:500;display:flex;width:auto;cursor:pointer;overflow:hidden;border:none;border-radius:25px;-webkit-box-shadow:0 4px 12px rgba(160,88,174,0.15);box-shadow:0 4px 12px rgba(160,88,174,0.15);margin-bottom:10px}
							</style>';
			/* $email_msg	=	"Your account type is : ".$account_type."<br />";
			$email_msg	.=	"Your password is : ".$new_password."<br />"; */
			$email_sub	=	"Sistema Ajudôu - Recuperação de Senha";
			$email_to	=	$email;
			//$this->do_email($email_msg , $email_sub , $email_to);
			$var = $this->send_email_mailer($email_msg , $email_sub , $email_to);
			return $var; 
		}
		else
		{
			return false;
		}
	}

	function contact_message_email($email_from, $email_to, $email_message) {
		$email_sub = "Message from School Website";
		//$this->do_email($email_message, $email_sub, $email_to, $email_from);
		$this->send_smtp_mail($email_message, $email_sub, $email_to, $email_from);
	}

    function personal_message_email($email_from, $email_to, $email_message) {
        $email_sub = "Message from School Website";
        //$this->do_email($email_message, $email_sub, $email_to, $email_from);
        $this->send_smtp_mail($email_message, $email_sub, $email_to, $email_from);
    }

    function request_book_email($student_id){
    	$student_name = $this->db->get_where('student', array('student_id', $student_id))->row('name');
    	$student_code = $this->db->get_where('student', array('student_id', $student_id))->row('student_code');
    	$email_message  = '<html><body><p>'.$student_name.' has been requested you, for the book.'.'</p><br><p>Student Code : '.$student_code.'</p></body></html>';
    	$email_sub		= 'New book issued';
    	$this->db->limit(1);
    	$librarians = $this->db->get('librarian')->result_array();
    	foreach($librarians as $librarian){
			$email_to = $librarian['email'];
    	}
    	$this->send_smtp_mail($email_message, $email_sub, $email_to);
    }

    // more stable function
	public function send_smtp_mail($msg=NULL, $sub=NULL, $to=NULL, $from=NULL) {
		//Load email library
		$this->load->library('email');

		if($from == NULL){
				$from		=	get_settings('system_email');
		}

		//SMTP & mail configuration
		$config = array(
			'protocol'  => get_settings('protocol'),
			'smtp_host' => get_settings('smtp_host'),
			'smtp_port' => get_settings('smtp_port'),
			'smtp_user' => get_settings('smtp_user'),
			'smtp_pass' => get_settings('smtp_pass'),
			'mailtype'  => 'html',
			'charset'   => 'utf-8',
			'smtp_timeout' => '30',
			'mailpath' => '/usr/sbin/sendmail',
			'wordwrap' => TRUE
		);
		$this->email->initialize($config);
		$this->email->set_mailtype("html");
		$this->email->set_newline("\r\n");

		$htmlContent = $msg;

		$this->email->to($to);
		$this->email->from($from, get_settings('website_title'));
		$this->email->subject($sub);
		$this->email->message($htmlContent);

		//Send email
		$this->email->send();
		$var = $this->email->print_debugger();
		return $var;
	}
	public function send_email_mailer($msg=NULL, $sub=NULL, $to=NULL, $from=NULL) {
		/* define( 'WPMS_SMTP_HOST', 'email-smtp.us-east-2.amazonaws.com' ); 
		define( 'WPMS_SMTP_USER', 'AKIARXQG3MEAMDVKWLVR' ); 
		define( 'WPMS_SMTP_PASS', 'BLC2YI68cHdKTQMNsBwksuhv0nsDTtPf9bgIf6fUtOFR' );
		define( 'WPMS_MAIL_FROM', 'naoresponda@mobsolutions.com.br' ); */
		$mail = new PHPMailer();
		$mail->IsSMTP();
		$mail->Host     = 'email-smtp.us-east-2.amazonaws.com';
		$mail->SMTPAuth = true;
		$mail->SMTPDebug = '';
		$mail->Debugoutput = 'html';
		$mail->SMTPKeepAlive = true; 
		$mail->SMTPSecure = 'tls'; 
		$mail->Username = 'AKIARXQG3MEAMDVKWLVR';
		$mail->Password = 'BLC2YI68cHdKTQMNsBwksuhv0nsDTtPf9bgIf6fUtOFR';
		$mail->Port = '587';
		$mail->Priority = 1;
		$mail->Encoding = 'base64';
		$mail->CharSet = "utf-8";
		$mail->ContentType = "text/html";
		$mail->SetFrom('naoresponda@mobsolutions.com.br', $name = get_settings('website_title'));
		/* if($email_reply_to != null){
			$mail->AddReplyTo($email_reply_to, $email_reply_to_name);
		} */

		/* Clear Mails */
		$mail->clearAddresses();
		$mail->clearCustomHeaders();
		$mail->clearAllRecipients();
		$mail->AddAddress($to, $name = get_settings('website_title'));
		$mail->Subject  =  $sub;
		$mail->Body = $msg;

		/* Send Error */
		if(!$mail->Send()){
			//return $mail->ErrorInfo;
			return false;
			//echo $mail->ErrorInfo;
		}else{
			return true;
			$enviado = 1;
			//echo $mail->ErrorInfo;
		}
	}
}
