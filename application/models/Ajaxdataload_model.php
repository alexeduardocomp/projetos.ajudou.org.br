<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ajaxdataload_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /*----------------------------- BOOKS -------------------------------*/

    function all_books_count()
    {
        $query = $this->db->get('book');
        return $query->num_rows();
    }

    function all_books($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('book');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_alunos($id_turma)
    {
        $query =

            $this->db->select("name")
            ->from("student")
            ->where('id_turma', $id_turma)->order_by("name", "ASC");



        $query = $this->db->get();


        return $query->result();
    }

    function get_tabela_alunos_ano($class_id, $running_year)
    {

        $ano = explode("-", $running_year);
        $students  =  $this->db->get_where('enroll', array(
            'class_id' => $class_id,
            'year(from_unixtime(date_added))' => $ano[0]
        ))->result_array();
        
        $empty = "";
        $tabela .= 
                "<table class='table table-bordered datatable display nowrap table-student'>
                    <thead>
                        <tr>
                            <th>
                                <div>Foto</div>".
                            "</th>".
                            "<th>
                                <div>Nome</div>".
                            "</th>".
                            "<th class='span3'>
                                <div>Telefone</div>".
                            "</th>".
                            "<th>
                                <div>".get_phrase('email') . '/ Nome de usuário'."</div>".
                            "</th>".
                            "<th>
                                <div>Projeto</div>".
                            "</th>".
                            "<th>
                                <div>Núcleo</div>".
                            "</th>".
                            "<th>
                                <div>Turma</div>".
                            "</th>".
                            "<th>
                                <div>Escola</div>".
                            "</th>".
                            "<th>
                                <div>Data de Matrícula</div>".
                            "</th>".
                            "<th>
                                <div>Opções</div>".
                            "</th>".
                        "</tr>".
                    "</thead>".
                    "<tbody>";
        foreach ($students as $row){
            $query =
                $this->db->select("enroll.*,class.projeto_id as projeto_id,class.nome_nucleo as nome_nucleo,class.name as nome_turma")
                ->from("enroll")
                ->join('class', "enroll.class_id = class.class_id")
                ->where('student_id', $row['student_id'])
                ->where('enroll.class_id', $class_id)

                ->order_by("date_added", "DESC");
            $query = $this->db->get();
            $ultima_matricula_aluno  = $query->row();

            $projeto  = $this->db->get_where('projeto', array('id' => $ultima_matricula_aluno->projeto_id))->row();

            //valores da tabela
            $nome = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;
            $telefone = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->phone;
            $email = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->email;
            $projeto_aluno = $projeto->nome;
            $nucleo =$ultima_matricula_aluno->nome_nucleo;
            $turma = $ultima_matricula_aluno->nome_turma;
            $escola = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->nome_escola;
            $data_matricula = date('d/m/Y', $ultima_matricula_aluno->date_added);
            
            $tabela .= "<tr>";
            $tabela .= 
                "<td><img src='".$this->crud_model->get_image_url('student', $row['student_id'])."' class='img-circle' width='30'></td>".
                "<td>".$nome."</td>".
                "<td>".$telefone."</td>".
                "<td>".$email."</td>".
                "<td>".$projeto_aluno."</td>".
                "<td>".$nucleo."</td>".
                "<td>".$turma."</td>".
                "<td>".$escola."</td>".
                "<td>".$data_matricula."</td>".
                "<td>".
                    "<div class='btn-group'>
                        <button type='button' class='btn btn-default btn-sm dropdown-toggle' data-toggle='dropdown'>
                            Ações <span class='caret'></span>
                        </button>
                        <ul class='dropdown-menu dropdown-default pull-right' role='menu'>
                            <li>
                                <a href='".site_url('teacher/student_profile/' . $row['student_id'])."'>
                                    <i class='entypo-user'></i>
                                    Perfil"
                                ."</a>".
                            "</li>".
                            "<li>
                                <a href='#' onclick='showAjaxModal(".'"'.site_url("modal/popup/modal_student_edit/" . $row['student_id']) . '/' . $class_id.'"'.") '>
                                    <i class='entypo-pencil'></i>
                                    Editar"
                                ."</a>".
                            "</li>".
                            '<li class="divider"></li>'.
                            "<li>
                                <a href='#' onclick='confirm_modal(".'"'.site_url('teacher/delete_student/' . $row['student_id'] . '/' . $class_id).'"'.") '>
                                    <i class='entypo-trash'></i>
                                    Deletar"
                                ."</a>".
                            "</li>".
                            "<li>
                                <a href='#' onclick='confirm_modal(".'"'.site_url('teacher/remover_aluno_turma/' . $row['student_id'] . '/' . $class_id).'"'.") '>
                                    <i class='entypo-cancel-squared'></i>
                                    Remover da turma"
                                ."</a>".
                            "</li>".
                        "</ul>".
                    "</div>".
                "</td>".
            "</tr>";
            $empty = "a";
        }
        if(!empty($empty)){
                $tabela .= 
                "</tbody>".
            "</table>";
        }else{
            $tabela .= 
                    "<tr><td align='center' colspan='10'>Nenhum registro correspondente ao criterio encontrado!</td></tr>".
                "</tbody>".
            "</table>";
        }

        return $tabela;
        
    }

    function get_students_turma($id_turma)
    {
        //SCRIPT get_students_turma
        /*SELECT student.*, enroll.date_added FROM `student` INNER JOIN enroll ON student.student_id = enroll.student_id WHERE enroll.class_id = 102 AND enroll.date_added <= '2021/08/16' ORDER BY name ASC */
        $query =

            $this->db->select("student.*,enroll.date_added")
            ->from("student")
            ->join("enroll", "student.student_id = enroll.student_id")
            ->where('enroll.class_id', $id_turma)
            ->where('enroll.date_added <=', time())
            ->order_by("name", "ASC");

        $query = $this->db->get();


        return $query->result();
    }

    function get_planos_data($class_id, $data)
    {

        $array = array('data_atividade' => $data, 'class_id' => $class_id);


        $query = $this->db->where($array)->get('class_routine');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_situacao_aluno($id)
    {
        $query =

            $this->db->select("situacao")
            ->from("student")
            ->where('student_id', $id);



        $query = $this->db->get();


        return $query->result();
    }

    function get_eventos_nucleo($id_nucleo)
    {

        $query =

            $this->db->select("notice_id, notice_title")
            ->from("noticeboard")
            ->where('id_nucleo', $id_nucleo);



        $query = $this->db->get();


        return $query->result();
    }

    function book_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('name', $search)
            ->or_like('author', $search)
            ->or_like('book_id', $search)
            ->or_like('price', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('book');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function book_search_count($search)
    {
        $query = $this
            ->db
            ->like('name', $search)
            ->or_like('book_id', $search)
            ->or_like('author', $search)
            ->or_like('price', $search)
            ->get('book');

        return $query->num_rows();
    }

    /*----------------------------- BOOKS -------------------------------*/

    function beneficiados($projeto, $ano)
    {

        $query  =  $this->db->query(' SELECT student.name as nome, modalidade.modalidade as modalidade,nome_turma as turma, cidade, situacao, estado, rua, bairro, numero, complemento, phone as telefone, data_nascimento FROM student, class, modalidade, enroll WHERE student.id_projeto = ' . $projeto . ' and class.class_id = student.id_turma and class.class_id = enroll.class_id and enroll.year = ' . $ano . ' and class.modalidade = modalidade.id group by student.student_id order by student.name ASC');






        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }




    function numero_instrutores()
    {

        $query  =  $this->db->query('SELECT * FROM teacher
         INNER JOIN class ON class.teacher_id = teacher.teacher_id GROUP BY teacher.teacher_id');






        if ($query->num_rows() > 0)
            return count($query->result());
        else
            return null;
    }


    /*----------------------------- TEACHERS -------------------------------*/

    function all_teachers_count()
    {
        $query = $this->db->get('teacher');
        return $query->num_rows();
    }


    function get_todos_instrutores()
    {


        $query = $this
            ->db->select("teacher_id, name")
            ->get('teacher');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function all_teachers($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('teacher');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_planos_data_periodo($class_id, $data, $data2)
    {

        $query = $this
            ->db
            ->where("class_id", $class_id)
            ->where("data_atividade BETWEEN '{$data}' AND '{$data2}'")
            ->order_by("data_atividade", "ASC")
            ->get('class_routine');

        //    $array = array('data_atividade' => $data, 'class_id' => $class_id);


        // $query =$this->db->where($array)->get('class_routine');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_planos($class_id)
    {
        $query = $this->db
            ->where("class_id", $class_id)
            ->order_by("data_atividade", "ASC")
            ->get('class_routine');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_datas($class_id)
    {

        $query = $this
            ->db
            ->select('data_atividade')
            ->where("class_id", $class_id)
            ->order_by("data_atividade", "ASC")
            ->group_by("data_atividade")
            ->get('class_routine');

        //    $array = array('data_atividade' => $data, 'class_id' => $class_id);


        // $query =$this->db->where($array)->get('class_routine');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_datas_periodo($class_id, $data, $data2)
    {

        $query = $this
            ->db
            ->select('data_atividade')
            ->where("class_id", $class_id)
            ->where("data_atividade BETWEEN '{$data}' AND '{$data2}'")
            ->order_by("data_atividade", "ASC")
            ->group_by("data_atividade")
            ->get('class_routine');

        //    $array = array('data_atividade' => $data, 'class_id' => $class_id);


        // $query =$this->db->where($array)->get('class_routine');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_turmas_projeto($id)
    {


        $query = $this
            ->db
            ->where("id_nucleo", $id)->get('class');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_turmas_teacher($id)
    {


        $query = $this
            ->db
            ->where("teacher_id", $id)->get('class');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_subject_teacher($id)
    {


        $query = $this
            ->db
            ->where("teacher_id", $id)->get('subject');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_nucleos_projeto($id)
    {


        $query = $this
            ->db
            ->where("id_projeto", $id)->get('nucleo');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function teacher_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('teacher_id', $search)
            ->or_like('name', $search)
            ->or_like('email', $search)
            ->or_like('phone', $search)
            ->or_like('cpf', $search)
            ->or_like('rg', $search)
            ->or_like('cargo', $search)
            ->or_like('telefone_2', $search)
            ->or_like('estado', $search)
            ->or_like('cidade', $search)
            ->or_like('cep', $search)
            ->or_like('rua', $search)
            ->or_like('bairro', $search)
            ->or_like('numero', $search)
            ->or_like('complemento', $search)
            ->or_like('carga_horaria', $search)
            ->or_like('data_contratacao', $search)
            ->or_like('conta_bancaria', $search)
            ->or_like('salario_bruto', $search)
            ->or_like('salario_liquido', $search)
            ->or_like('combustivel', $search)
            ->or_like('metas_qualitativas', $search)
            ->or_like('metas_quantitativas', $search)
            ->or_like('objetivos', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('teacher');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function teacher_search_count($search)
    {
        $query = $this
            ->db
            ->like('teacher_id', $search)
            ->or_like('name', $search)
            ->or_like('email', $search)
            ->or_like('phone', $search)
            ->or_like('cpf', $search)
            ->or_like('rg', $search)
            ->or_like('cargo', $search)
            ->or_like('telefone_2', $search)
            ->or_like('estado', $search)
            ->or_like('cidade', $search)
            ->or_like('cep', $search)
            ->or_like('rua', $search)
            ->or_like('bairro', $search)
            ->or_like('numero', $search)
            ->or_like('complemento', $search)
            ->or_like('carga_horaria', $search)
            ->or_like('data_contratacao', $search)
            ->or_like('conta_bancaria', $search)
            ->or_like('salario_bruto', $search)
            ->or_like('salario_liquido', $search)
            ->or_like('combustivel', $search)
            ->or_like('metas_qualitativas', $search)
            ->or_like('metas_quantitativas', $search)
            ->or_like('objetivos', $search)
            ->get('teacher');

        return $query->num_rows();
    }

    /*----------------------------- TEACHERS -------------------------------*/

    /*----------------------------- PATROCINADORES -------------------------------*/



    function get_patrocinador_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('patrocinador');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }



    function all_patrocinadores_count()
    {
        $query = $this->db->get('patrocinador');
        return $query->num_rows();
    }


    function all_patrocinadores($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('patrocinador');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function patrocinadores_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('patrocinador');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function patrocinador_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->get('patrocinador');

        return $query->num_rows();
    }

    /*----------------------------- PATROCINADORES -------------------------------*/


    /*----------------------------- PARENTS -------------------------------*/

    function all_parents_count()
    {
        $query = $this->db->get('parent');
        return $query->num_rows();
    }


    function all_parents($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('parent');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function parent_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('parent_id', $search)
            ->or_like('name', $search)
            ->or_like('email', $search)
            ->or_like('phone', $search)
            ->or_like('profession', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('parent');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function parent_search_count($search)
    {
        $query = $this
            ->db
            ->like('parent_id', $search)
            ->or_like('name', $search)
            ->or_like('email', $search)
            ->or_like('phone', $search)
            ->or_like('profession', $search)
            ->get('parent');

        return $query->num_rows();
    }

    /*----------------------------- PARENTS -------------------------------*/


    /*----------------------------- CIDADES -------------------------------*/



    function get_cidade_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('cidade');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }



    function all_cidades_count()
    {
        $query = $this->db->get('cidade');
        return $query->num_rows();
    }


    function all_cidades($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('cidade');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function cidades_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->or_like('estado', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('cidade');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function cidade_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->or_like('estado', $search)
            ->get('cidade');

        return $query->num_rows();
    }

    /*----------------------------- CIDADES -------------------------------*/





    /*----------------------------- AÇÕES -------------------------------*/



    function get_acao_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('acao');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }



    function all_acoes_count()
    {
        $query = $this->db->get('acao');
        return $query->num_rows();
    }


    function all_acoes($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('acao');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function acoes_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('acao', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('acao');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function acao_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('acao', $search)
            ->get('acao');

        return $query->num_rows();
    }

    /*----------------------------- AÇÕES -------------------------------*/



    /*----------------------------- NÚCLEO -------------------------------*/


    function get_local_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('local_execucao');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }





    function get_nucleo_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('nucleo');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }





    function get_instrutor_by_id($id)
    {

        $query = $this
            ->db
            ->where("teacher_id", $id)->get('teacher');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_escola_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('escola');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_turma_by_id($id)
    {

        $query = $this
            ->db
            ->where("class_id", $id)->get('class');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function all_nucleos_count()
    {
        $query = $this->db->get('nucleo');
        return $query->num_rows();
    }


    function all_nucleos($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function nucleos_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome_nucleo', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('meta_alunos', $search)
            ->or_like('patrocinador', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function nucleo_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome_nucleo', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('meta_alunos', $search)
            ->or_like('patrocinador', $search)
            ->get('nucleo');

        return $query->num_rows();
    }

    /*----------------------------- NÚCLEO -------------------------------*/



    /*----------------------------- MODALIDADE -------------------------------*/

    function all_modalidades_count()
    {
        $query = $this->db->get('modalidade');
        return $query->num_rows();
    }


    function all_modalidades($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('modalidade');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function modalidades_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('modalidade', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('modalidade');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function modalidade_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('modalidade', $search)
            ->get('modalidade');

        return $query->num_rows();
    }

    /*----------------------------- MODALIDADE -------------------------------*/


    /*----------------------------- RELACAO -------------------------------*/

    function all_relacoes_count()
    {
        $query = $this->db->get('instrutor_nucleo');
        return $query->num_rows();
    }


    function all_relacoes($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('instrutor_nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function relacoes_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_instrutor', $search)
            ->or_like('nome_nucleo', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('instrutor_nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function relacao_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_instrutor', $search)
            ->or_like('nome_nucleo', $search)
            ->get('instrutor_nucleo');

        return $query->num_rows();
    }


    function get_instrutores_nucleo2($id_nucleo)
    {

        $query =

            $this->db
            ->select("id_instrutor, nome_instrutor")
            ->from("instrutor_nucleo")

            ->where('id_nucleo', $id_nucleo);

        $query = $this->db->get();

        return $query->result();
    }

    function get_secoes_turma2($id_turma)
    {

        /*$query = 

$this->db
  ->select("section.name, section.section_id")
  ->from("section")
   -> join('enroll', 'section.section_id = enroll.section_id')
   ->where('enroll.class_id', $id_turma);*/

        $query =

            $this->db->distinct()->select("section_id, name")
            ->from("section")
            ->where('class_id', $id_turma);

        $query = $this->db->get();

        return $query->result();
    }



    function get_instrutores_nucleo($id_nucleo)
    {

        $query =

            $this->db->distinct()
            ->select("teacher_id, name")
            ->from("teacher, instrutor_nucleo")

            ->where('teacher_id not in (select teacher_id from teacher INNER JOIN instrutor_nucleo on instrutor_nucleo.id_instrutor = teacher_id and instrutor_nucleo.id_nucleo = "' . $id_nucleo . '")', null);



        $query = $this->db->get();


        $query2 =

            $this->db
            ->select("teacher_id, name")
            ->from("teacher");

        $query2 = $this->db->get();


        // verificar se na tabela eu tenho um esse id nucleo cadastrado

        $query1 = $this->db->get_where('instrutor_nucleo', array('id_nucleo' => $id_nucleo));
        $result = $query1->result_array();
        if (count($result) > 0) {

            return $query->result();
        } else {

            return $query2->result();
        }
    }



    function get_escolas_nucleo($id_nucleo)
    {

        $query =

            $this->db->distinct()
            ->select("id, nome")
            ->from("escola, escola_nucleo")

            ->where('id not in (select id from escola INNER JOIN escola_nucleo on escola_nucleo.id_escola = id and escola_nucleo.id_nucleo = "' . $id_nucleo . '")', null);



        $query = $this->db->get();


        $query2 =

            $this->db
            ->select("id, nome")
            ->from("escola");

        $query2 = $this->db->get();


        // verificar se na tabela eu tenho um esse id nucleo cadastrado

        $query1 = $this->db->get_where('escola_nucleo', array('id_nucleo' => $id_nucleo));
        $result = $query1->result_array();
        if (count($result) > 0) {

            return $query->result();
        } else {

            return $query2->result();
        }
    }


    function get_escolas_projeto($id_projeto)
    {
        $query =
            $this->db->distinct()
            ->select("id, nome")
            ->from("escola")
            ->join('escola_nucleo', "escola.id = escola_nucleo.id_escola")
            ->where('id_projeto', $id_projeto)
            ->order_by('escola.nome', 'ASC');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }


    function get_local_execucao_projeto($id_projeto)
    {
        $query =
            $this->db->distinct()
            ->select("id, local")
            ->from("local_execucao")
            ->where('id_projeto', $id_projeto)
            ->order_by('local_execucao.local', 'ASC');
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    function get_local_execucao_nucleo($id_nucleo)
    {
        $query =
            $this->db->distinct()
            ->select("id, local")
            ->from("local_execucao")
            ->where('id_nucleo', $id_nucleo);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return null;
        }
    }

    function get_escolas_nucleo2($id_nucleo)
    {

        $query =
            $this->db->select("id, nome")
            ->from("escola")
            ->join('escola_nucleo', 'escola.id = escola_nucleo.id_escola')
            ->where('escola_nucleo.id_nucleo', $id_nucleo);

        $query = $this->db->get();

        return $query->result();
    }


    function get_turmas_nucleo($id_nucleo)
    {
        $query =
            $this->db->select("class_id, name")
            ->from("class")
            ->where('id_nucleo', $id_nucleo);

        $query = $this->db->get();
        return $query->result();
    }

    function get_turmas_nucleo_modalidade($id_nucleo, $id_modalidade)
    {
        $query =
            $this->db->select("class_id, name")
            ->from("class")
            ->where('id_nucleo', $id_nucleo)
            ->where('modalidade', $id_modalidade)
            ->order_by("id_nucleo", "ASC");

        $query = $this->db->get();
        return $query->result();
    }


    function pegar_turma_projeto_instrutor($id_projeto, $id_instrutor)
    {
        $query =
            $this->db->select("class_id, name")
            ->from("class")
            ->join('instrutor_nucleo', 'instrutor_nucleo.id_nucleo = class.id_nucleo')
            ->where('instrutor_nucleo.id_instrutor', $id_instrutor)
            ->where('projeto_id', $id_projeto)
            ->where('teacher_id', $id_instrutor);

        $query = $this->db->get();
        return $query->result();
    }

    function pegar_turma_nucleo_instrutor($id_nucleo, $id_instrutor)
    {
        $query =
            $this->db->select("class_id, name")
            ->from("class")
            ->where('id_nucleo', $id_nucleo)
            ->where('teacher_id', $id_instrutor);

        $query = $this->db->get();
        return $query->result();
    }

    function get_secoes_turma($id_turma)
    {
        $query =
            $this->db->select("section_id, name")
            ->from("section")
            ->where('class_id', $id_turma);

        $query = $this->db->get();

        return $query->result();
    }

    function get_acoes_nucleo($id_nucleo)
    {
        $query =
            $this->db->distinct()
            ->select("id, acao")
            ->from("acao, acao_nucleo")
            ->where('id not in (select id from acao INNER JOIN acao_nucleo on acao_nucleo.id_acao = id and acao_nucleo.id_nucleo = "' . $id_nucleo . '")', null);

        $query = $this->db->get();

        $query2 =
            $this->db
            ->select("id, acao")
            ->from("acao");

        $query2 = $this->db->get();

        // verificar se na tabela eu tenho um esse id nucleo cadastrado
        $query1 = $this->db->get_where('acao_nucleo', array('id_nucleo' => $id_nucleo));
        $result = $query1->result_array();
        if (count($result) > 0) {
            return $query->result();
        } else {
            return $query2->result();
        }
    }


    /*----------------------------- RELACAO -------------------------------*/





    /*----------------------------- RELACAO ESCOLA-------------------------------*/

    function all_relacoes_escola_count()
    {
        $query = $this->db->get('escola_nucleo');
        return $query->num_rows();
    }


    function all_relacoes_escola($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('escola_nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function relacoes_escola_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id_relacao', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_escola', $search)
            ->or_like('nome_nucleo', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('escola_nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function relacao_escola_search_count($search)
    {
        $query = $this
            ->db
            ->like('id_relacao', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_escola', $search)
            ->or_like('nome_nucleo', $search)
            ->get('escola_nucleo');

        return $query->num_rows();
    }







    /*----------------------------- RELACAO ESCOLA -------------------------------*/




    /*----------------------------- RELACAO AÇAO-------------------------------*/

    function all_relacoes_acao_count()
    {
        $query = $this->db->get('acao_nucleo');
        return $query->num_rows();
    }


    function all_relacoes_patrocinador_count()
    {
        $query = $this->db->get('patrocinador_projeto');
        return $query->num_rows();
    }


    function all_relacoes_acao($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('acao_nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function all_relacoes_patrocinador($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('patrocinador_projeto');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function relacoes_acao_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id_relacao', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_acao', $search)
            ->or_like('nome_nucleo', $search)
            ->or_like('quantidade', $search)
            ->or_like('numeracao', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('acao_nucleo');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function relacoes_patrocinador_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id_relacao', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_patrocinador', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('patrocinador_projeto');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function relacao_acao_search_count($search)
    {
        $query = $this
            ->db
            ->like('id_relacao', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_acao', $search)
            ->or_like('nome_nucleo', $search)
            ->or_like('quantidade', $search)
            ->or_like('numeracao', $search)
            ->get('acao_nucleo');

        return $query->num_rows();
    }


    function relacao_patrocinador_search_count($search)
    {
        $query = $this
            ->db
            ->like('id_relacao', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_patrocinador', $search)
            ->get('patrocinador_projeto');

        return $query->num_rows();
    }






    /*----------------------------- RELACAO AÇÃO -------------------------------*/





    /*----------------------------- LOCAL EXECUÇÃO -------------------------------*/

    function all_locais_count()
    {
        $query = $this->db->get('local_execucao');
        return $query->num_rows();
    }


    function all_locais($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('local_execucao');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function locais_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_nucleo', $search)
            ->or_like('local', $search)
            ->or_like('estado', $search)
            ->or_like('cidade', $search)
            ->or_like('cep', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('local_execucao');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function local_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome_projeto', $search)
            ->or_like('nome_nucleo', $search)
            ->or_like('local', $search)
            ->or_like('estado', $search)
            ->or_like('cidade', $search)
            ->or_like('cep', $search)
            ->get('local_execucao');

        return $query->num_rows();
    }

    /*----------------------------- LOCAL EXECUÇÃO -------------------------------*/



    /*----------------------------- GET NUCLEO PROJETO -------------------------------*/



    function get_n_p($id)
    {


        $query = $this
            ->db
            ->where("id_projeto", $id)
            ->order_by('nucleo.nome_nucleo', 'ASC')
            ->get('nucleo');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_teacher_projeto($id)
    {
        $query = $this
            ->db
            ->select("teacher.teacher_id as id,teacher.name as name")
            ->join("teacher", "class.teacher_id = teacher.teacher_id")
            ->group_by("teacher.teacher_id")
            ->where("class.projeto_id", $id)
            ->order_by('teacher.name', 'ASC')
            ->get('class');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_modalidade_projeto($id)
    {
        $query = $this
            ->db
            ->select("modalidade.id as id,modalidade.modalidade as name")
            ->join("modalidade", "class.modalidade = modalidade.id")
            ->group_by("modalidade.id")
            ->where("class.projeto_id", $id)
            ->order_by('modalidade.modalidade', 'ASC')
            ->get('class');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_teacher_nucleo($id)
    {
        $query = $this
            ->db
            ->select("teacher.teacher_id as id,teacher.name as name")
            ->join("teacher", "class.teacher_id = teacher.teacher_id")
            ->group_by("teacher.teacher_id")
            ->where("class.id_nucleo", $id)->get('class');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_teacher_modalidade($id)
    {
        $query = $this
            ->db
            ->select("teacher.teacher_id as id,teacher.name as name")
            ->join("teacher", "class.teacher_id = teacher.teacher_id")
            ->group_by("teacher.teacher_id")
            ->where("class.modalidade", $id)->get('class');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }



    function get_p_p($id) 
    {
        $query =
            $this->db->distinct()
            ->select("id, nome")
            ->from("patrocinador, patrocinador_projeto")
            ->where('id not in (select id from patrocinador INNER JOIN patrocinador_projeto on patrocinador_projeto.id_patrocinador = id and patrocinador_projeto.id_projeto = "' . $id . '")', null);

        $query = $this->db->get();


        $query2 =

            $this->db
            ->select("id, nome")
            ->from("patrocinador");

        $query2 = $this->db->get();


        $query1 = $this->db->get_where('patrocinador_projeto', array('id_projeto' => $id));
        $result = $query1->result_array();
        if (count($result) > 0) {

            return $query->result();
        } else {

            return $query2->result();
        }
    }

    function get_c_e($estado)
    {
        $query = $this
            ->db
            ->or_like('estado', $estado)
            ->get('cidade');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function get_l_n($id)
    {
        $query = $this
            ->db
            ->where("id_nucleo", $id)->get('local_execucao');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }



    /*----------------------------- GET NUCLEO PROJETO -------------------------------*/





    /*----------------------------- ESCOLAS -------------------------------*/

    function all_escolas_count()
    {
        $query = $this->db->get('escola');
        return $query->num_rows();
    }


    function all_escolas($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('escola');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function escolas_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->or_like('tipo_escola', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('escola');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function escola_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->or_like('tipo_escola', $search)
            ->get('escola');

        return $query->num_rows();
    }

    /*----------------------------- ESCOLAS -------------------------------*/


    /*----------------------------- PROJETOS -------------------------------*/


    function get_modalidade_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('modalidade');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function get_projeto_by_id($id)
    {

        $query = $this
            ->db
            ->where("id", $id)->get('projeto');


        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }


    function all_projetos_count()
    {
        $query = $this->db->get('projeto');
        return $query->num_rows();
    }


    function all_projetos($limit, $start, $col, $dir)
    {
        $query = $this
            ->db
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('projeto'); 

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function projetos_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->or_like('tipo_projeto', $search)
            ->or_like('numero', $search)
            ->or_like('patrocinador', $search)
            ->or_like('manifestacao_esportiva', $search)
            ->or_like('proponente', $search)
            ->or_like('metas', $search)
            ->or_like('objetivo', $search)
            ->or_like('data_inicio', $search)
            ->or_like('data_final', $search)
            ->or_like('situacao', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('projeto');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function projeto_search_count($search)
    {
        $query = $this
            ->db
            ->like('id', $search)
            ->or_like('nome', $search)
            ->or_like('tipo_projeto', $search)
            ->or_like('numero', $search)
            ->or_like('patrocinador', $search)
            ->or_like('manifestacao_esportiva', $search)
            ->or_like('proponente', $search)
            ->or_like('metas', $search)
            ->or_like('objetivo', $search)
            ->or_like('data_inicio', $search)
            ->or_like('data_final', $search)
            ->or_like('situacao', $search)
            ->get('projeto');

        return $query->num_rows();
    }

    /*----------------------------- PROJETOS -------------------------------*/

    /*----------------------------- EXPENSES -------------------------------*/

    function all_expenses_count()
    {
        $array = array('payment_type' => 'expense', 'year' => get_settings('running_year'));
        $query = $this
            ->db
            ->where($array)
            ->get('payment');
        return $query->num_rows();
    }

    function all_expenses($limit, $start, $col, $dir)
    {
        $array = array('payment_type' => 'expense', 'year' => get_settings('running_year'));
        $query = $this
            ->db
            ->where($array)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('payment');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function expense_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('payment_id', $search)
            ->or_like('title', $search)
            ->or_like('amount', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('payment');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function expense_search_count($search)
    {
        $query = $this
            ->db
            ->like('payment_id', $search)
            ->or_like('title', $search)
            ->or_like('amount', $search)
            ->get('payment');

        return $query->num_rows();
    }

    /*----------------------------- EXPENSES -------------------------------*/


    /*----------------------------- INVOICES -------------------------------*/

    function all_invoices_count()
    {
        $array = array('year' => get_settings('running_year'));
        $query = $this
            ->db
            ->where($array)
            ->get('invoice');
        return $query->num_rows();
    }

    function all_invoices($limit, $start, $col, $dir)
    {
        $array = array('year' => get_settings('running_year'));
        $query = $this
            ->db
            ->where($array)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('invoice');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function invoice_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('invoice_id', $search)
            ->or_like('title', $search)
            ->or_like('amount', $search)
            ->or_like('status', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('invoice');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function invoice_search_count($search)
    {
        $query = $this
            ->db
            ->like('invoice_id', $search)
            ->or_like('title', $search)
            ->or_like('amount', $search)
            ->or_like('status', $search)
            ->get('invoice');

        return $query->num_rows();
    }

    /*----------------------------- INVOICES -------------------------------*/


    /*----------------------------- PAYMENTS -------------------------------*/

    function all_payments_count()
    {
        $array = array('payment_type' => 'income', 'year' => get_settings('running_year'));
        $query = $this
            ->db
            ->where($array)
            ->get('payment');
        return $query->num_rows();
    }

    function all_payments($limit, $start, $col, $dir)
    {
        $array = array('payment_type' => 'income', 'year' => get_settings('running_year'));
        $query = $this
            ->db
            ->where($array)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('payment');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function payment_search($limit, $start, $search, $col, $dir)
    {
        $query = $this
            ->db
            ->like('payment_id', $search)
            ->or_like('title', $search)
            ->or_like('amount', $search)
            ->limit($limit, $start)
            ->order_by($col, $dir)
            ->get('payment');

        if ($query->num_rows() > 0)
            return $query->result();
        else
            return null;
    }

    function payment_search_count($search)
    {
        $query = $this
            ->db
            ->like('payment_id', $search)
            ->or_like('title', $search)
            ->or_like('amount', $search)
            ->get('payment');

        return $query->num_rows();
    }

    /*----------------------------- PAYMENTS -------------------------------*/
}
