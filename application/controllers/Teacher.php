<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
 *  @author   : Creativeitem
 *  date    : 14 september, 2017
 *  Ekattor School Management System Pro
 *  http://codecanyon.net/user/Creativeitem
 *  http://support.creativeitem.com
 */

class Teacher extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model(array('Ajaxdataload_model' => 'ajaxload'));
        /*cache control*/
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    }

    /***default functin, redirects to login page if no teacher logged in yet***/
    public function index()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($this->session->userdata('teacher_login') == 1)
            redirect(site_url('teacher/dashboard'), 'refresh');
    }

    /***TEACHER DASHBOARD***/
    function dashboard()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name']  = 'dashboard';
        $page_data['page_title'] = 'Painel do Instrutor';
        $this->load->view('backend/index', $page_data);
    }


    /*ENTRY OF A NEW STUDENT*/



    /****** CADASTRO DE ALUNOS *****/

    function student($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');

        $running_year = $this->db->get_where('settings', array(
            'type' => 'running_year'
        ))->row()->description;

        if ($param1 == 'create') {
            $data['name']         = strtoupper(html_escape($this->input->post('name')));
            if (html_escape($this->input->post('birthday')) != null) {
                $data['birthday']     = html_escape($this->input->post('birthday'));
            }
            if ($this->input->post('sex') != null) {
                $data['sex']          = $this->input->post('sex');
            }
            if (html_escape($this->input->post('address')) != null) {
                $data['address']      = html_escape($this->input->post('address'));
            }
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone']        = html_escape($this->input->post('phone'));
            }
            if (html_escape($this->input->post('student_code')) != null) {
                $data['student_code'] = html_escape($this->input->post('student_code'));
                $code_validation = code_validation_insert(html_escape($data['student_code']));
                if (!$code_validation) {
                    $this->session->set_flashdata('error_message', get_phrase('this_id_no_is_not_available'));
                    redirect(site_url('teacher/student_add'), 'refresh');
                }
            }

            $data['email']  =  $data['student_code'] . '@ajudou.com';

            $data['password'] = "123456";

            $data['id_projeto']  = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']  = html_escape($this->input->post('id_nucleo'));
            $data['id_turma']  = html_escape($this->input->post('class_id'));
            $data['id_escola']  = html_escape($this->input->post('id_escola'));
            $data['situacao']  = html_escape($this->input->post('situacao'));
            $data['data_nascimento']  = html_escape($this->input->post('data_nascimento'));
            $data['nome_pai']  = html_escape($this->input->post('nome_pai'));
            $data['nome_mae']  = html_escape($this->input->post('nome_mae'));
            $data['cpf_aluno']  = html_escape($this->input->post('cpf_aluno'));
            $data['cpf_responsavel']  = html_escape($this->input->post('cpf_responsavel'));
            $data['rg_aluno']  = strtoupper(html_escape($this->input->post('rg_aluno')));
            $data['rg_responsavel']  = strtoupper(html_escape($this->input->post('rg_responsavel')));
            $data['telefone_2']  = html_escape($this->input->post('telefone_2'));
            $data['cep']  = html_escape($this->input->post('cep'));
            $data['cidade']  = html_escape($this->input->post('cidade'));
            $data['estado']  = html_escape($this->input->post('estado'));
            $data['bairro']  = html_escape($this->input->post('bairro'));
            $data['rua']  = html_escape($this->input->post('rua'));
            $data['numero']  = html_escape($this->input->post('numero'));
            $data['complemento']  = html_escape($this->input->post('complemento'));
            $data['problema_saude']  = html_escape($this->input->post('problema_saude'));
            $data['descricao_problema']  = html_escape($this->input->post('descricao_problema'));
            $data['uniforme']  = html_escape($this->input->post('uniforme'));
            $data['numero_tenis']  = html_escape($this->input->post('numero_tenis'));
            $data['numero_pessoas_domicilio']  = html_escape($this->input->post('numero_pessoas_domicilio'));
            $data['responsavel_sustento']  = html_escape($this->input->post('responsavel_sustento'));
            $data['renda_familiar']  = html_escape($this->input->post('renda_familiar'));
            $data['parente_trabalha_patrocinadora']  = html_escape($this->input->post('parente_trabalha_patrocinadora'));
            $data['data_aluno_projeto']  = html_escape($this->input->post('data_aluno_projeto'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;

            $turma_selecionada = $this->ajaxload->get_turma_by_id($data['id_turma']);

            foreach ($turma_selecionada as $turmax) {
                $nome_turma = $turmax->name;
            }

            $data['nome_turma'] = $nome_turma;

            $escola_selecionada = $this->ajaxload->get_escola_by_id($data['id_escola']);

            foreach ($escola_selecionada as $escolax) {
                $nome_escola = $escolax->nome;
            }

            $data['nome_escola'] = $nome_escola;

            if ($this->input->post('parent_id') != null) {
                $data['parent_id']    = $this->input->post('parent_id');
            }
            if ($this->input->post('dormitory_id') != null) {
                $data['dormitory_id'] = $this->input->post('dormitory_id');
            }
            if ($this->input->post('transport_id') != null) {
                $data['transport_id'] = $this->input->post('transport_id');
            }
            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('student', $data);
                $student_id = $this->db->insert_id();

                $data2['student_id']     = $student_id;
                $data2['enroll_code']    = substr(md5(rand(0, 1000000)), 0, 7);

                if ($this->input->post('class_id') != null) {
                    $data2['class_id']       = $this->input->post('class_id');
                }
                if ($this->input->post('section_id') != '') {
                    $data2['section_id'] = $this->input->post('section_id');
                }
                if (html_escape($this->input->post('roll')) != '') {
                    $data2['roll']           = html_escape($this->input->post('roll'));
                }
                $data2['date_added']     = strtotime(html_escape($this->input->post('data_aluno_projeto')));
                $data2['year']           = $running_year;
                $this->db->insert('enroll', $data2);
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $student_id . '.jpg');

                $this->session->set_flashdata('flash_message', 'Aluno adicionado com sucesso');
                $this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', 'Este email não é válido');
            }
            
            redirect(site_url('teacher/student_add'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']           = strtoupper(html_escape($this->input->post('name')));
            $data['email']          = html_escape($this->input->post('email'));
            $data['parent_id']      = $this->input->post('parent_id');

            // $data['id_projeto']  = html_escape($this->input->post('id_projeto'));
            //  $data['id_nucleo']  = html_escape($this->input->post('id_nucleo'));
            //  $data['id_turma']  = html_escape($this->input->post('class_id'));
            //  $data2['class_id']  = html_escape($this->input->post('class_id'));
            $data['id_escola']  = html_escape($this->input->post('id_escola'));
            $data['situacao']  = html_escape($this->input->post('situacao'));
            $data['data_nascimento']  = html_escape($this->input->post('data_nascimento'));
            $data['nome_pai']  = html_escape($this->input->post('nome_pai'));
            $data['nome_mae']  = html_escape($this->input->post('nome_mae'));
            $data['cpf_aluno']  = html_escape($this->input->post('cpf_aluno'));
            $data['cpf_responsavel']  = html_escape($this->input->post('cpf_responsavel'));
            $data['rg_aluno']  = html_escape($this->input->post('rg_aluno'));
            $data['rg_responsavel']  = html_escape($this->input->post('rg_responsavel'));
            $data['telefone_2']  = html_escape($this->input->post('telefone_2'));
            $data['cep']  = html_escape($this->input->post('cep'));
            $data['cidade']  = html_escape($this->input->post('cidade'));
            $data['estado']  = html_escape($this->input->post('estado'));
            $data['bairro']  = html_escape($this->input->post('bairro'));
            $data['rua']  = html_escape($this->input->post('rua'));
            $data['numero']  = html_escape($this->input->post('numero'));
            $data['complemento']  = html_escape($this->input->post('complemento'));
            $data['problema_saude']  = html_escape($this->input->post('problema_saude'));
            $data['descricao_problema']  = html_escape($this->input->post('descricao_problema'));
            $data['uniforme']  = html_escape($this->input->post('uniforme'));
            $data['numero_tenis']  = html_escape($this->input->post('numero_tenis'));
            $data['numero_pessoas_domicilio']  = html_escape($this->input->post('numero_pessoas_domicilio'));
            $data['responsavel_sustento']  = html_escape($this->input->post('responsavel_sustento'));
            $data['renda_familiar']  = html_escape($this->input->post('renda_familiar'));
            $data['parente_trabalha_patrocinadora']  = html_escape($this->input->post('parente_trabalha_patrocinadora'));
            $data['data_aluno_projeto']  = html_escape($this->input->post('data_aluno_projeto'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;

            $turma_selecionada = $this->ajaxload->get_turma_by_id($data['id_turma']);

            foreach ($turma_selecionada as $turmax) {
                $nome_turma = $turmax->name;
            }

            $data['nome_turma'] = $nome_turma;

            $escola_selecionada = $this->ajaxload->get_escola_by_id($data['id_escola']);

            foreach ($escola_selecionada as $escolax) {
                $nome_escola = $escolax->nome;
            }

            $data['nome_escola'] = $nome_escola;

            if (html_escape($this->input->post('birthday')) != null) {
                $data['birthday']   = html_escape($this->input->post('birthday'));
            }
            if ($this->input->post('sex') != null) {
                $data['sex']            = $this->input->post('sex');
            }
            if (html_escape($this->input->post('address')) != null) {
                $data['address']        = html_escape($this->input->post('address'));
            }
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone']          = html_escape($this->input->post('phone'));
            }
            if ($this->input->post('dormitory_id') != null) {
                $data['dormitory_id']   = $this->input->post('dormitory_id');
            }
            if ($this->input->post('transport_id') != null) {
                $data['transport_id']   = $this->input->post('transport_id');
            }

            //student id
            if (html_escape($this->input->post('student_code')) != null) {
                $data['student_code'] = html_escape($this->input->post('student_code'));
                $code_validation = code_validation_update($data['student_code'], $param2);
                if (!$code_validation) {
                    $this->session->set_flashdata('error_message', get_phrase('this_id_no_is_not_available'));
                    redirect(site_url('teacher/student_information/' . $param3), 'refresh');
                }
            }         
            $validation = email_validation_for_edit($data['email'], $param2, 'student');
            if ($validation == 1) {
                $this->db->where('student_id', $param2);
                $this->db->update('student', $data);

                $data2['section_id'] = $this->input->post('section_id');

                if (html_escape($this->input->post('roll')) != null) {
                    $data2['roll'] = html_escape($this->input->post('roll'));
                } else {
                    $data2['roll'] = null;
                }
                $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

                $data_matricula_turma  = strtotime($this->input->post('data_matricula_turma'));

                //  echo  $data_matricula_turma;
                // echo"<br>";
                $enroll_id  = $this->input->post('enroll_id');

                //  echo  $enroll_id;
                //echo"<br>";
                $this->db->where('student_id', $param2);
                $this->db->where('enroll_id', $enroll_id);
                $this->db->update('enroll', array(
                    'date_added' => $data_matricula_turma
                ));

                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $param2 . '.jpg');
                $this->crud_model->clear_cache();
                $this->session->set_flashdata('flash_message', 'Aluno atualizado');
            } else {
                $this->session->set_flashdata('error_message', 'Este email não é válido');
            } 
            
            redirect(site_url('teacher/student_information/' . $param3), 'refresh');
        }
    }



    function get_cidade_estado()
    {

        $estado = $this->input->post("dados");

        $cidades_estado = $this->ajaxload->get_c_e($estado);


        $cont = 0;
        $cidades = array();

        foreach ($cidades_estado as $cid) {
            $cidades[$cont]["nome_cidade"] = $cid->nome;
            $cidades[$cont]["id"] = $cid->id;
            $cont++;
        }

        echo json_encode($cidades);
    }
 
    function get_turma_projeto_instrutor()
    {
        $id = $this->input->post("dados");
        $id_teacher = $this->session->userdata('login_user_id');

        $retorno = $this->ajaxload->pegar_turma_projeto_instrutor($id, $id_teacher);
        
        $cont = 0;
        $turmas = array();

        foreach ($retorno as $ret) {
            $turmas[$cont]["id_turma"] = $ret->class_id;
            $turmas[$cont]["nome_turma"] = $ret->name;
            $cont++;
        }

        echo json_encode($turmas);
    }

    function get_turma_nucleo_instrutor()
    {



        $id = $this->input->post("dados");
        $id_teacher = $this->session->userdata('login_user_id');




        $retorno = $this->ajaxload->pegar_turma_nucleo_instrutor($id, $id_teacher);



        $cont = 0;
        $turmas = array();

        foreach ($retorno as $ret) {
            $turmas[$cont]["id_turma"] = $ret->class_id;
            $turmas[$cont]["nome_turma"] = $ret->name;
            $cont++;
        }

        echo json_encode($turmas);
    }


    function delete_student($student_id = '', $class_id = '')
    {
        $this->crud_model->delete_student($student_id);
        $this->db->where('student_id', $student_id);
        $this->db->delete('enroll');
        $this->session->set_flashdata('flash_message', 'Aluno deletado');
        redirect(site_url('teacher/student_information/' . $class_id), 'refresh');
    }

    function remover_aluno_turma($student_id = '', $class_id = '')
    {
        $this->db->where('student_id', $student_id);
        $this->db->where('class_id', $class_id);
        $this->db->delete('enroll');
        $this->session->set_flashdata('flash_message', 'Aluno removido da turma');
        redirect(site_url('teacher/student_information/' . $class_id), 'refresh');
    }

    function get_turma_nucleo()
    {

        $id = $this->input->post("dados");


        $retorno = $this->ajaxload->get_turmas_nucleo($id);









        $cont = 0;
        $turmas = array();

        foreach ($retorno as $ret) {
            $turmas[$cont]["id_turma"] = $ret->class_id;
            $turmas[$cont]["nome_turma"] = $ret->name;
            $cont++;
        }

        echo json_encode($turmas);
    }



    function get_escola_nucleo2()
    {

        $id = $this->input->post("dados");


        $retorno = $this->ajaxload->get_escolas_nucleo2($id);









        $cont = 0;
        $escolas = array();

        foreach ($retorno as $ret) {
            $escolas[$cont]["id_escola"] = $ret->id;
            $escolas[$cont]["nome_escola"] = $ret->nome;
            $cont++;
        }

        echo json_encode($escolas);
    }

    function get_secao_turma2()
    {

        $id = $this->input->post("dados");

        $retorno = $this->ajaxload->get_secoes_turma2($id);


        $cont = 0;
        $secoes = array();

        foreach ($retorno as $ret) {
            $secoes[$cont]["id"] = $ret->section_id;
            $secoes[$cont]["nome_secao"] = $ret->name;
            $cont++;
        }

        echo json_encode($secoes);
    }


    function get_tabela_alunos_ano()
    {
        $id = $this->input->post("id");
        $running_year = $this->input->post("running_year");

        $tabela = $this->ajaxload->get_tabela_alunos_ano($id, $running_year);

      
        echo json_encode($tabela, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
    }


    function get_nucleo_projeto()
    {

        $id = $this->input->post("dados");

        $nucleos_selecionados = $this->ajaxload->get_n_p($id);


        $cont = 0;
        $nucleos = array();

        foreach ($nucleos_selecionados as $nuc) {
            $nucleo[$cont]["nome_nucleo"] = $nuc->nome_nucleo;
            $nucleo[$cont]["id_nucleo"] = $nuc->id;
            $cont++;
        }

        echo json_encode($nucleo);
    }


    function student_add()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  = 'student_add';
        $page_data['page_title'] = 'Cadastrar Alunos';
        $this->load->view('backend/index', $page_data);
    }


    /****MANAGE STUDENTS CLASSWISE*****/

    function student_information($class_id = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect('login', 'refresh');

        $page_data['page_name']      = 'student_information';
        $page_data['page_title']     = "Informação do Aluno - Turma : " .
            $this->crud_model->get_class_name($class_id);
        $page_data['class_id']     = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function student_profile($student_id)
    {
        if ($this->session->userdata('teacher_login') != 1) {
            redirect(base_url(), 'refresh');
        }
        $page_data['page_name']  = 'student_profile';
        $page_data['page_title'] = 'Perfil do Aluno';
        $page_data['student_id']  = $student_id;
        $this->load->view('backend/index', $page_data);
    }

    function student_marksheet($student_id = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect('login', 'refresh');
        $class_id     = $this->db->get_where('enroll', array(
            'student_id' => $student_id, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->row()->class_id;
        $student_name = $this->db->get_where('student', array('student_id' => $student_id))->row()->name;
        $class_name   = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
        $page_data['page_name']  =   'student_marksheet';
        $page_data['page_title'] =   get_phrase('marksheet_for') . ' ' . $student_name . ' (' . get_phrase('class') . ' ' . $class_name . ')';
        $page_data['student_id'] =   $student_id;
        $page_data['class_id']   =   $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function student_marksheet_print_view($student_id, $exam_id)
    { 
        if ($this->session->userdata('teacher_login') != 1)
            redirect('login', 'refresh');
        $class_id     = $this->db->get_where('enroll', array(
            'student_id' => $student_id, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->row()->class_id;
        $class_name   = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;

        $page_data['student_id'] =   $student_id;
        $page_data['class_id']   =   $class_id;
        $page_data['exam_id']    =   $exam_id;
        $this->load->view('backend/teacher/student_marksheet_print_view', $page_data);
    }

    function get_class_section_subject($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/teacher/class_routine_section_subject_selector', $page_data);
    }



    function get_class_section($class_id)
    {
        $sections = $this->db->get_where('section', array(
            'class_id' => $class_id
        ))->result_array();
        foreach ($sections as $row) {
            echo '<option value="' . $row['section_id'] . '">' . $row['name'] . '</option>';
        }
    }

    function get_class_subject($class_id)
    {
        $subject = $this->db->get_where('subject', array(
            'class_id' => $class_id, 'teacher_id' => $this->session->userdata('teacher_id')
        ))->result_array();
        foreach ($subject as $row) {
            echo '<option value="' . $row['subject_id'] . '">' . $row['name'] . '</option>';
        }
    }
    /****MANAGE TEACHERS*****/
    function teacher_list($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'personal_profile') {
            $page_data['personal_profile']   = true;
            $page_data['current_teacher_id'] = $param2;
        }
        $page_data['teachers']   = $this->db->get('teacher')->result_array();
        $page_data['page_name']  = 'teacher';
        $page_data['page_title'] = 'Lista de Instrutores';
        $this->load->view('backend/index', $page_data);
    }


    //***** RELATORIO PRESENÇA *****//




    function get_section($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/teacher/manage_attendance_section_holder', $page_data);
    }


    ///////ATTENDANCE REPORT /////
    function attendance_report()
    {
        $page_data['month']        = date('m');
        $page_data['page_name']    = 'attendance_report';
        $page_data['page_title']   = 'Relatório de Frequência';
        $this->load->view('backend/index', $page_data);
    }

    // Apagar frequência
    function apagar_frequencia()
    {
        $page_data['month']        = date('m');
        $page_data['page_name']    = 'apagar_frequencia';
        $page_data['page_title']   = 'Apagar frequência';
        $this->load->view('backend/index', $page_data);
    }


    // Apagar frequência
    function apagar_frequencia_post()
    {
        $class_id       = $this->input->post('class_id');
        $data     = $this->input->post('timestamp');
        $timestamp = strtotime($data);

        //  echo $class_id;
        //  echo $timestamp;

        $this->db->where('class_id', $class_id);
        $this->db->where('timestamp', $timestamp);
        $this->db->delete('attendance');
        $this->session->set_flashdata('flash_message', 'Frequências apagadas com sucesso!');
        redirect(site_url('teacher/apagar_frequencia/'), 'refresh');
    }

    function attendance_report_view($class_id = '', $section_id = '', $month = '', $sessional_year = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        $class_name                     = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
        $section_name                   = $this->db->get_where('section', array('section_id' => $section_id))->row()->name;
        $page_data['class_id']          = $class_id;
        $page_data['section_id']        = $section_id;
        $page_data['month']             = $month;
        $page_data['sessional_year']    = $sessional_year;
        $page_data['page_name']         = 'attendance_report_view';
        $page_data['page_title']        =   'Relatório de Frequência da Turma ' . $class_name;
        $this->load->view('backend/index', $page_data);
    }
    function attendance_report_print_view($class_id = '', $section_id = '', $month = '', $sessional_year = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['class_id']          = $class_id;
        $page_data['section_id']        = $section_id;
        $page_data['month']             = $month;
        $page_data['sessional_year']    = $sessional_year;
        $this->load->view('backend/teacher/attendance_report_print_view', $page_data);
    }

    function attendance_report_selector()
    {
        if ($this->input->post('class_id') == '' || $this->input->post('sessional_year') == '') {
            $this->session->set_flashdata('error_message', get_phrase('please_make_sure_class_and_sessional_year_are_selected'));
            redirect(site_url('teacher/attendance_report'), 'refresh');
        }
        $data['class_id']       = $this->input->post('class_id');
        $data['section_id']     = $this->input->post('section_id');
        $data['month']          = $this->input->post('month');
        $data['sessional_year'] = $this->input->post('sessional_year');
        redirect(site_url('teacher/attendance_report_view/' . $data['class_id'] . '/' . $data['section_id'] . '/' . $data['month'] . '/' . $data['sessional_year']), 'refresh');
    }


    //***** /RELATORIO *****//   


    /****MANAGE SUBJECTS*****/
    function subject($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        if ($param1 == 'create') {
            $data['name']       = html_escape($this->input->post('name'));
            //$data['class_id']   = $this->input->post('class_id');
            $data['descricao'] = html_escape($this->input->post('descricao'));

            if ($this->input->post('teacher_id') != null) {
                $data['teacher_id'] = $this->input->post('teacher_id');
            }
            $data['year']       = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

            if (($data['name'] != '') && ($data['descricao'] != '') && ($data['teacher_id'] != '')) {
                $this->db->insert('subject', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('select_class'));
            }

            redirect(site_url('teacher/subject/'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']       = html_escape($this->input->post('name'));
            //$data['class_id']   = $this->input->post('class_id');
            $data['descricao'] = html_escape($this->input->post('descricao'));
            if ($this->input->post('teacher_id') != null) {
                $data['teacher_id'] = $this->input->post('teacher_id');
            } else {
                $data['teacher_id'] = null;
            }
            $data['year']       = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if (($data['name'] != '') && ($data['descricao'] != '') && ($data['teacher_id'] != '')) {
                $this->db->where('subject_id', $param2);
                $this->db->update('subject', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('select_class'));
            }

            redirect(site_url('teacher/subject/'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('subject', array(
                'subject_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('subject_id', $param2);
            $this->db->delete('subject');
            redirect(site_url('teacher/subject/' . $param3), 'refresh');
        }
        //$page_data['class_id']   = $param1;
        $page_data['subjects']   = $this->db->get_where('subject', array(
            'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->result_array();
        $page_data['page_name']  = 'subject';
        $page_data['page_title'] = 'Gerenciar Atividades';
        $this->load->view('backend/index', $page_data);
    }



    /****MANAGE EXAM MARKS*****/
    function marks_manage()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name']  =   'marks_manage';
        $page_data['page_title'] = get_phrase('manage_exam_marks');
        $this->load->view('backend/index', $page_data);
    }

    function marks_manage_view($exam_id = '', $class_id = '', $section_id = '', $subject_id = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['exam_id']    =   $exam_id;
        $page_data['class_id']   =   $class_id;
        $page_data['subject_id'] =   $subject_id;
        $page_data['section_id'] =   $section_id;
        $page_data['page_name']  =   'marks_manage_view';
        $page_data['page_title'] = get_phrase('manage_exam_marks');
        $this->load->view('backend/index', $page_data);
    }

    function marks_selector()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        $data['exam_id']    = $this->input->post('exam_id');
        $data['class_id']   = $this->input->post('class_id');
        $data['section_id'] = $this->input->post('section_id');
        $data['subject_id'] = $this->input->post('subject_id');
        $data['year']       = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        if ($data['class_id'] != '' && $data['exam_id'] != '') {
            $query = $this->db->get_where('mark', array(
                'exam_id' => $data['exam_id'],
                'class_id' => $data['class_id'],
                'section_id' => $data['section_id'],
                'subject_id' => $data['subject_id'],
                'year' => $data['year']
            ));
            if ($query->num_rows() < 1) {
                $students = $this->db->get_where('enroll', array(
                    'class_id' => $data['class_id'], 'section_id' => $data['section_id'], 'year' => $data['year']
                ))->result_array();
                foreach ($students as $row) {
                    $data['student_id'] = $row['student_id'];
                    $this->db->insert('mark', $data);
                }
            }
            redirect(site_url('teacher/marks_manage_view/' . $data['exam_id'] . '/' . $data['class_id'] . '/' . $data['section_id'] . '/' . $data['subject_id']), 'refresh');
        } else {
            $this->session->set_flashdata('error_message', get_phrase('select_all_the_fields'));
            $page_data['page_name']  =   'marks_manage';
            $page_data['page_title'] = get_phrase('manage_exam_marks');
            $this->load->view('backend/index', $page_data);
        }
    }
    function marks_update($exam_id = '', $class_id = '', $section_id = '', $subject_id = '')
    {
        $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        if ($class_id != '' && $exam_id != '') {
            $marks_of_students = $this->db->get_where('mark', array(
                'exam_id' => $exam_id,
                'class_id' => $class_id,
                'section_id' => $section_id,
                'year' => $running_year,
                'subject_id' => $subject_id
            ))->result_array();
            foreach ($marks_of_students as $row) {
                $obtained_marks = html_escape($this->input->post('marks_obtained_' . $row['mark_id']));
                $comment = html_escape($this->input->post('comment_' . $row['mark_id']));
                $this->db->where('mark_id', $row['mark_id']);
                $this->db->update('mark', array('mark_obtained' => $obtained_marks, 'comment' => $comment));
            }
            $this->session->set_flashdata('flash_message', get_phrase('marks_updated'));
            redirect(site_url('teacher/marks_manage_view/' . $exam_id . '/' . $class_id . '/' . $section_id . '/' . $subject_id), 'refresh');
        } else {
            $this->session->set_flashdata('error_message', get_phrase('select_all_the_fields'));
            $page_data['page_name']  =   'marks_manage';
            $page_data['page_title'] = get_phrase('manage_exam_marks');
            $this->load->view('backend/index', $page_data);
        }
    }

    function marks_get_subject($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/teacher/marks_get_subject', $page_data);
    }


    // ACADEMIC SYLLABUS
    function academic_syllabus($class_id = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        // detect the first class
        if ($class_id == '')
            $class_id           =   $this->db->get('class')->first_row()->class_id;

        $page_data['page_name']  = 'academic_syllabus';
        $page_data['page_title'] = 'Programa Acadêmico';
        $page_data['class_id']   = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function upload_academic_syllabus()
    {
        $data['academic_syllabus_code'] =   substr(md5(rand(0, 1000000)), 0, 7);
        $data['title']                  =   html_escape($this->input->post('title'));
        $data['description']            =   html_escape($this->input->post('description'));
        $data['class_id']               =   html_escape($this->input->post('class_id'));
        if ($this->input->post('subject_id') != null) {
            $data['subject_id']          =   $this->input->post('subject_id');
        }
        $data['uploader_type']          =   $this->session->userdata('login_type');
        $data['uploader_id']            =   $this->session->userdata('login_user_id');
        $data['year']                   =   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        $data['timestamp']              =   strtotime(date("Y-m-d H:i:s"));
        //uploading file using codeigniter upload library
        $files = $_FILES['file_name'];
        $this->load->library('upload');
        $config['upload_path']   =  'uploads/syllabus/';
        $config['allowed_types'] =  '*';
        $_FILES['file_name']['name']     = $files['name'];
        $_FILES['file_name']['type']     = $files['type'];
        $_FILES['file_name']['tmp_name'] = $files['tmp_name'];
        $_FILES['file_name']['size']     = $files['size'];
        $this->upload->initialize($config);
        $this->upload->do_upload('file_name');

        $data['file_name'] = $_FILES['file_name']['name'];

        $this->db->insert('academic_syllabus', $data);
        $this->session->set_flashdata('flash_message', get_phrase('syllabus_uploaded'));
        redirect(site_url('teacher/academic_syllabus/' . $data['class_id']), 'refresh');
    }

    function download_academic_syllabus($academic_syllabus_code)
    {
        $file_name = $this->db->get_where('academic_syllabus', array(
            'academic_syllabus_code' => $academic_syllabus_code
        ))->row()->file_name;
        $this->load->helper('download');
        $data = file_get_contents("uploads/syllabus/" . $file_name);
        $name = $file_name;

        force_download($name, $data);
    }

    /*****BACKUP / RESTORE / DELETE DATA PAGE**********/
    function backup_restore($operation = '', $type = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        if ($operation == 'create') {
            $this->crud_model->create_backup($type);
        }
        if ($operation == 'restore') {
            $this->crud_model->restore_backup();
            $this->session->set_flashdata('backup_message', 'Backup Restored');
            redirect(site_url('teacher/backup_restore'), 'refresh');
        }
        if ($operation == 'delete') {
            $this->crud_model->truncate($type);
            $this->session->set_flashdata('backup_message', 'Data removed');
            redirect(site_url('teacher/backup_restore'), 'refresh');
        }

        $page_data['page_info']  = 'Create backup / restore from backup';
        $page_data['page_name']  = 'backup_restore';
        $page_data['page_title'] = get_phrase('manage_backup_restore');
        $this->load->view('backend/index', $page_data);
    }

    /******MANAGE OWN PROFILE AND CHANGE PASSWORD***/
    function manage_profile($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'update_profile_info') {
            $data['name']        = html_escape($this->input->post('name'));
            $data['email']       = html_escape($this->input->post('email'));
            $validation = email_validation_for_edit($data['email'], $this->session->userdata('teacher_id'), 'teacher');
            if ($validation == 1) {
                $this->db->where('teacher_id', $this->session->userdata('teacher_id'));
                $this->db->update('teacher', $data);
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $this->session->userdata('teacher_id') . '.jpg');
                $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }
            redirect(site_url('teacher/manage_profile/'), 'refresh');
        }
        if ($param1 == 'change_password') {
            $data['password']             = sha1($this->input->post('password'));
            $data['new_password']         = sha1($this->input->post('new_password'));
            $data['confirm_new_password'] = sha1($this->input->post('confirm_new_password'));

            $current_password = $this->db->get_where('teacher', array(
                'teacher_id' => $this->session->userdata('teacher_id')
            ))->row()->password;
            if ($current_password == $data['password'] && $data['new_password'] == $data['confirm_new_password']) {
                $this->db->where('teacher_id', $this->session->userdata('teacher_id'));
                $this->db->update('teacher', array(
                    'password' => $data['new_password']
                ));
                $this->session->set_flashdata('flash_message', 'Senha atualizada!'); 
            } else {
                $this->session->set_flashdata('error_message', 'Senha incorreta!');
            }
            redirect(site_url('teacher/manage_profile/'), 'refresh');
        }
        $page_data['page_name']  = 'manage_profile';
        $page_data['page_title'] = 'Gerenciar Perfil';
        $page_data['edit_data']  = $this->db->get_where('teacher', array(
            'teacher_id' => $this->session->userdata('teacher_id')
        ))->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /**********MANAGING CLASS ROUTINE******************/
    /*function class_routine($class_id)
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        $page_data['page_name']  = 'class_routine';
        $page_data['class_id']  =   $class_id;
        $page_data['page_title'] = "Plano de Aula";
        $this->load->view('backend/index', $page_data);
    }*/


    function class_routine($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {



            //$data['subject_id']     = $this->input->post('subject_id');

            $data1 = $this->input->post('data');

            $data_at = str_replace("/", "-",  $data1);
            $data['data_atividade'] = date('Y-m-d', strtotime($data_at));



            //$data['day']            = $this->input->post('day');
            $data['year']           = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            // checking duplication
            $array = array(
                'class_id'      => $data['class_id'],
                'year'          => $data['year']
            );


            $turmas = html_escape($this->input->post('class_id'));
            $atividades = html_escape($this->input->post('subject_id'));



            if ((!empty($atividades)) && (!empty($turmas))) {


                foreach ($turmas as $tur) {

                    $data['class_id'] = $tur;

                    $data['time_start'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start;
                    $data['time_end'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end;
                    $data['time_end_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end_min;
                    $data['time_start_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start_min;


                    foreach ($atividades as $ati) {
                        $id_ativ = $ati;
                        $data['subject_id'] = $id_ativ;
                        $this->db->insert('class_routine', $data);
                    }

                    $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos.');
            }

            redirect(site_url('teacher/class_routine_add'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['class_id']       = $this->input->post('class_id');

            $data1 = $this->input->post('data');

            $data_at = str_replace("/", "-",  $data1);
            $data['data_atividade'] = date('Y-m-d', strtotime($data_at));

            $data['time_start'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start;
            $data['time_end'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end;
            $data['time_end_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end_min;
            $data['time_start_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start_min;


            /*if($this->input->post('section_id') != '') {
                $data['section_id'] = $this->input->post('section_id');
            }*/

            $data['subject_id']     = $this->input->post('subject_id');


            //$data['day']            = $this->input->post('day');
            $data['year']           = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($data['subject_id'] != '') {
                // checking duplication
                $array = array(
                    'class_id'      => $data['class_id'],
                    'year'          => $data['year']
                );
                //$validation = duplication_of_class_routine_on_edit($array, $param2);
                $validation = 1;

                if ($validation == 1) {
                    $this->db->where('class_routine_id', $param2);
                    $this->db->update('class_routine', $data);
                    $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
                } else {
                    $this->session->set_flashdata('error_message', get_phrase('time_conflicts'));
                }
            } else {
                $this->session->set_flashdata('error_message', get_phrase('subject_is_not_found'));
            }

            redirect(site_url('teacher/class_routine_view/' . $data['class_id']), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('class_routine', array(
                'class_routine_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $class_id = $this->db->get_where('class_routine', array('class_routine_id' => $param2))->row()->class_id;
            $this->db->where('class_routine_id', $param2);
            $this->db->delete('class_routine');
            $this->session->set_flashdata('flash_message', 'Deletado com sucesso.');
            redirect(site_url('teacher/class_routine_view/' . $class_id), 'refresh');
        }
    }



    function class_routine_view($class_id)
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  = 'class_routine_view';
        $page_data['class_id']  =   $class_id;
        $page_data['page_title'] = 'Plano de Aula';
        $this->load->view('backend/index', $page_data);
    }

    function class_routine_add()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  = 'class_routine_add';
        $page_data['page_title'] = 'Adicionar Plano de Aula';
        $this->load->view('backend/index', $page_data);
    }

    function class_routine_print_view($class_id)
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect('login', 'refresh');
        $page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/teacher/class_routine_print_view', $page_data);
    }

    function section_subject_edit($class_id, $class_routine_id)
    {
        $page_data['class_id']          =   $class_id;
        $page_data['class_routine_id']  =   $class_routine_id;
        $this->load->view('backend/teacher/class_routine_section_subject_edit', $page_data);
    }

    /****** DAILY ATTENDANCE *****************/
    function manage_attendance($class_id)
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        $class_name = $this->db->get_where('class', array(
            'class_id' => $class_id
        ))->row()->name;
        $page_data['page_name']  =  'manage_attendance';
        $page_data['class_id']   =  $class_id;
        $page_data['page_title'] =   'Gerenciar Frequência da Turma ' . $class_name;
        $this->load->view('backend/index', $page_data);
    }

    function manage_attendance_view($class_id = '', $section_id = '', $timestamp = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        $class_name = $this->db->get_where('class', array(
            'class_id' => $class_id
        ))->row()->name;
        $page_data['class_id'] = $class_id;
        $page_data['timestamp'] = $timestamp;
        $page_data['page_name'] = 'manage_attendance_view';
        $section_name = $this->db->get_where('section', array(
            'section_id' => $section_id
        ))->row()->name; 
        $page_data['section_id'] = $section_id;
        $page_data['page_title'] = 'Gerenciar Frequência da Turma ' . $class_name;
        $this->load->view('backend/index', $page_data);
    }

    function attendance_selector()
    {
        $data['class_id']   = $this->input->post('class_id');
        $data['year']       = $this->input->post('year');
        $data['timestamp']  = strtotime($this->input->post('timestamp'));

        $data['data2']  = date("d/m/Y", strtotime($this->input->post('timestamp')));
        $data['data']  = date("Y-m-d", strtotime($this->input->post('timestamp')));

        $data['section_id'] = $this->input->post('section_id');
        $query = $this->db->get_where('attendance', array(
            'class_id' => $data['class_id'],
            //'section_id' => $data['section_id'],
            //'year' => $data['year'],
            'timestamp' => $data['timestamp']
        ));

        $sql = "SELECT attendance.* FROM attendance INNER JOIN student ON student.student_id = attendance.student_id WHERE class_id=" . $data['class_id'] . " AND `timestamp` = '" . $data['timestamp'] . "' ORDER BY student.name";

        //   echo $sql;
        $query = $this->db->query($sql);



        //echo $this->input->post('timestamp');DATE_FORMAT(FROM_UNIXTIME(`date_added`), '%Y-%m-%d')
        //   echo "<br>";
        //  echo $data['data'];

        //  if ($query->num_rows() < 1) {
        $sql = "SELECT student.*,enroll.class_id,enroll.date_added FROM `enroll` INNER JOIN student ON enroll.student_id = student.student_id WHERE enroll.class_id=" . $data['class_id'] . " AND ADDDATE(DATE_FORMAT(FROM_UNIXTIME(`date_added`), '%Y-%m-%d'), INTERVAL 1 DAY) <= '" . $data['data'] . "' GROUP BY student.student_id order by date_added DESC,student.name";

        // echo $sql;
        //  echo "<br>";
        $query = $this->db->query($sql);

        $students = $query->result_array();

        foreach ($students as $row) {
            //   echo "<br>";
            // $sql = "SELECT enroll.* FROM `enroll` WHERE enroll.class_id<>" . $data['class_id'] . " AND DATE_FORMAT(FROM_UNIXTIME(`date_added`), '%Y-%m-%d') > DATE_FORMAT(FROM_UNIXTIME('" . $row['date_added'] . "'), '%Y-%m-%d') AND enroll.student_id=" . $row['student_id'] . " AND  DATE_FORMAT(FROM_UNIXTIME(`date_added`), '%Y-%m-%d') <= '" . $data['data'] . "'";
            $sql = "SELECT attendance.* FROM attendance WHERE class_id=" . $data['class_id'] . " AND `timestamp` = '" . $data['timestamp'] . "' AND `student_id` = '" . $row['student_id'] . "' ";

            //  echo $sql;
            //  echo "<br>";
            $query = $this->db->query($sql);

            if ($query->num_rows() < 1) {
                //   echo "query->num_rows() < 1";
                //    echo "<br>";
                $attn_data['class_id']   = $data['class_id'];
                $attn_data['year']       = $data['year'];
                $attn_data['timestamp']  = $data['timestamp'];
                //$attn_data['section_id'] = $data['section_id'];
                $attn_data['student_id'] = $row['student_id'];
                $this->db->insert('attendance', $attn_data);
            }
        }
        //   }
        redirect(site_url('teacher/manage_attendance_view/' . $data['class_id'] . '/' . $data['section_id'] . '/' . $data['timestamp']), 'refresh');
    }

    function attendance_update($class_id = '', $section_id = '', $timestamp = '')
    {
        $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        $active_sms_service = $this->db->get_where('settings', array('type' => 'active_sms_service'))->row()->description;
        $attendance_of_students = $this->db->get_where('attendance', array(
            'class_id' => $class_id, 'timestamp' => $timestamp
        ))->result_array();
        foreach ($attendance_of_students as $row) {
            $attendance_status = html_escape($this->input->post('status_' . $row['attendance_id']));
            // echo $row['attendance_id'] . " - " . $attendance_status;
            //   echo "<br>";

            $this->db->where('attendance_id', $row['attendance_id']);
            $this->db->update('attendance', array('status' => $attendance_status));

            if ($attendance_status == 2) {

                if ($active_sms_service != '' || $active_sms_service != 'disabled') {
                    $student_name   = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;
                    $parent_id      = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->parent_id;
                    $message        = 'Your child' . ' ' . $student_name . 'is absent today.';
                    if ($parent_id != null && $parent_id != 0) {
                        $receiver_phone = $this->db->get_where('parent', array('parent_id' => $parent_id))->row()->phone;
                        if ($receiver_phone != '' || $receiver_phone != null) {
                            //$this->sms_model->send_sms($message,$receiver_phone);
                        } else {
                            //$this->session->set_flashdata('error_message' , get_phrase('parent_phone_number_is_not_found'));
                        }
                    } else {
                        //$this->session->set_flashdata('error_message' , get_phrase('parent_phone_number_is_not_found'));
                    }
                }
            }
        }
        $this->session->set_flashdata('flash_message', 'Frequências atualizadas com sucesso.');
        redirect(site_url('teacher/manage_attendance_view/' . $class_id . '/' . $section_id . '/' . $timestamp), 'refresh');
    }


    /**********MANAGE LIBRARY / BOOKS********************/
    function book($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect('login', 'refresh');

        $page_data['books']      = $this->db->get('book')->result_array();
        $page_data['page_name']  = 'book';
        $page_data['page_title'] = get_phrase('manage_library_books');
        $this->load->view('backend/index', $page_data);
    }
    /**********MANAGE TRANSPORT / VEHICLES / ROUTES********************/
    function transport($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect('login', 'refresh');

        $page_data['transports'] = $this->db->get('transport')->result_array();
        $page_data['page_name']  = 'transport';
        $page_data['page_title'] = get_phrase('manage_transport');
        $this->load->view('backend/index', $page_data);
    }

    /***MANAGE EVENT / NOTICEBOARD, WILL BE SEEN BY ALL ACCOUNTS DASHBOARD**/
    function noticeboard($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        if ($param1 == 'create') {
            $data['notice_title']     = html_escape($this->input->post('notice_title'));
            $data['notice']           = html_escape($this->input->post('notice'));
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            $this->db->insert('noticeboard', $data);
            redirect(site_url('teacher/noticeboard/'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['notice_title']     = html_escape($this->input->post('notice_title'));
            $data['notice']           = html_escape($this->input->post('notice'));
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            $this->db->where('notice_id', $param2);
            $this->db->update('noticeboard', $data);
            $this->session->set_flashdata('flash_message', get_phrase('notice_updated'));
            redirect(site_url('teacher/noticeboard/'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('noticeboard', array(
                'notice_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('notice_id', $param2);
            $this->db->delete('noticeboard');
            redirect(site_url('teacher/noticeboard/'), 'refresh');
        }
        $page_data['page_name']  = 'noticeboard';
        $page_data['page_title'] = 'Gerenciar Quadro de Eventos';
        $page_data['notices']    = $this->db->get_where('noticeboard', array('status' => 1))->result_array();
        $this->load->view('backend/index', $page_data);
    }


    /**********MANAGE DOCUMENT / home work FOR A SPECIFIC CLASS or ALL*******************/
    function document($do = '', $document_id = '')
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect('login', 'refresh');
        if ($do == 'upload') {
            move_uploaded_file($_FILES["userfile"]["tmp_name"], "uploads/document/" . $_FILES["userfile"]["name"]);
            $data['document_name'] = html_escape($this->input->post('document_name'));
            $data['file_name']     = $_FILES["userfile"]["name"];
            $data['file_size']     = $_FILES["userfile"]["size"];
            $this->db->insert('document', $data);
            redirect(site_url('teacher/manage_document'), 'refresh');
        }
        if ($do == 'delete') {
            $this->db->where('document_id', $document_id);
            $this->db->delete('document');
            redirect(site_url('teacher/manage_document'), 'refresh');
        }
        $page_data['page_name']  = 'manage_document';
        $page_data['page_title'] = get_phrase('manage_documents');
        $page_data['documents']  = $this->db->get('document')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    /*********MANAGE STUDY MATERIAL************/
    function study_material($task = "", $document_id = "")
    {
        if ($this->session->userdata('teacher_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($task == "create") {
            $this->crud_model->save_study_material_info();
            $this->session->set_flashdata('flash_message', get_phrase('study_material_info_saved_successfuly'));
            redirect(site_url('teacher/study_material/'), 'refresh');
        }

        if ($task == "update") {
            $this->crud_model->update_study_material_info($document_id);
            $this->session->set_flashdata('flash_message', get_phrase('study_material_info_updated_successfuly'));
            redirect(site_url('teacher/study_material/'), 'refresh');
        }

        if ($task == "delete") {
            $this->crud_model->delete_study_material_info($document_id);
            redirect(site_url('teacher/study_material/'), 'refresh');
        }

        $data['study_material_info']    = $this->crud_model->select_study_material_info_for_teacher();
        $data['page_name']              = 'study_material';
        $data['page_title']             = 'Material de Estudo';
        $this->load->view('backend/index', $data);
    }





    function get_class_routine_data()
    {

        $class_id = $this->input->post("class_id");
        //$data = $this->input->post("");


        $data1 = $this->input->post('data_atividade');

        $data_at = str_replace("/", "-",  $data1);
        $data = date('Y-m-d', strtotime($data_at));

        // echo $class_id;
        //echo $data;

        $planos_selecionadas = $this->ajaxload->get_planos_data($class_id, $data);

        if (count($planos_selecionadas) > 0) {
            //var_dump($planos_selecionadas);
            //echo json_encode($planos_selecionadas);


            foreach ($planos_selecionadas as $row2) {



                echo '<div class="btn-group" style="margin-right:3px;">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
                echo $this->crud_model->get_subject_name_by_id($row2->subject_id);

                if ($row2->time_start_min == 0 && $row2->time_end_min == 0)
                    echo ' (' . $row2->time_start . '-' . $row2->time_end . ') ';
                if ($row2->time_start_min != 0 || $row2->time_end_min != 0)
                    echo ' (' . $row2->time_start . ':' . $row2->time_start_min . '-' . $row2->time_end . ':' . $row2->time_end_min . ') ';

                echo '<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                        <a href="#" onclick="showAjaxModal(';
                echo "'";
                echo site_url('modal/popup/modal_edit_class_routine/' . $row2->class_routine_id);
                echo "'";
                echo ');">
                                            <i class="entypo-pencil"></i>
                                               Editar
                                                        </a>
                                 </li>';

                echo '<li>
                                    <a href="#" onclick="confirm_modal(';
                echo "'";
                echo site_url('teacher/class_routine/delete/' . $row2->class_routine_id);
                echo "'";
                echo ');">
                                        <i class="entypo-trash"></i>
                                            Deletar
                                        </a>
                                    </li>
                                    </ul>
                                </div>';
            }
        } else {
        }

        //echo json_encode($planos_selecionadas);



        /*$cont = 0;
$planos = array();

               foreach ($planos_selecionadas as $plan) {
                  $planos[$cont]["nome"] = $plan->name;
                  $planos[$cont]["id"] = $plan->class_id;
                  $cont++;
               }

               echo json_encode($turmas);*/
    }




    /* private messaging */


    function message($param1 = 'message_home', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('teacher_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        $max_size = 2097152;
        if ($param1 == 'send_new') {

            // Folder creation
            if (!file_exists('uploads/private_messaging_attached_file/')) {
                $oldmask = umask(0);  // helpful when used in linux server
                mkdir('uploads/private_messaging_attached_file/', 0777);
            }
            if ($_FILES['attached_file_on_messaging']['name'] != "") {
                if ($_FILES['attached_file_on_messaging']['size'] > $max_size) {
                    $this->session->set_flashdata('error_message', get_phrase('file_size_can_not_be_larger_that_2_Megabyte'));
                    redirect(site_url('teacher/message/message_new/'), 'refresh');
                } else {
                    $file_path = 'uploads/private_messaging_attached_file/' . $_FILES['attached_file_on_messaging']['name'];
                    move_uploaded_file($_FILES['attached_file_on_messaging']['tmp_name'], $file_path);
                }
            }
            $message_thread_code = $this->crud_model->send_new_private_message();
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(site_url('teacher/message/message_read/' . $message_thread_code), 'refresh');
        }

        if ($param1 == 'send_reply') {

            if (!file_exists('uploads/private_messaging_attached_file/')) {
                $oldmask = umask(0);  // helpful when used in linux server
                mkdir('uploads/private_messaging_attached_file/', 0777);
            }
            if ($_FILES['attached_file_on_messaging']['name'] != "") {
                if ($_FILES['attached_file_on_messaging']['size'] > $max_size) {
                    $this->session->set_flashdata('error_message', get_phrase('file_size_can_not_be_larger_that_2_Megabyte'));
                    redirect(site_url('teacher/message/message_read/' . $param2), 'refresh');
                } else {
                    $file_path = 'uploads/private_messaging_attached_file/' . $_FILES['attached_file_on_messaging']['name'];
                    move_uploaded_file($_FILES['attached_file_on_messaging']['tmp_name'], $file_path);
                }
            }
            $this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(site_url('teacher/message/message_read/' . $param2), 'refresh');
        }

        if ($param1 == 'message_read') {
            $page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code
            $this->crud_model->mark_thread_messages_read($param2);
        }

        $page_data['message_inner_page_name']   = $param1;
        $page_data['page_name']                 = 'message';
        $page_data['page_title']                = get_phrase('private_messaging');
        $this->load->view('backend/index', $page_data);
    }

    //GROUP MESSAGE
    function group_message($param1 = "group_message_home", $param2 = "")
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');
        $max_size = 2097152;

        if ($param1 == 'group_message_read') {
            $page_data['current_message_thread_code'] = $param2;
        } else if ($param1 == 'send_reply') {
            if (!file_exists('uploads/group_messaging_attached_file/')) {
                $oldmask = umask(0);  // helpful when used in linux server
                mkdir('uploads/group_messaging_attached_file/', 0777);
            }
            if ($_FILES['attached_file_on_messaging']['name'] != "") {
                if ($_FILES['attached_file_on_messaging']['size'] > $max_size) {
                    $this->session->set_flashdata('error_message', get_phrase('file_size_can_not_be_larger_that_2_Megabyte'));
                    redirect(site_url('teacher/group_message/group_message_read/' . $param2), 'refresh');
                } else {
                    $file_path = 'uploads/group_messaging_attached_file/' . $_FILES['attached_file_on_messaging']['name'];
                    move_uploaded_file($_FILES['attached_file_on_messaging']['tmp_name'], $file_path);
                }
            }

            $this->crud_model->send_reply_group_message($param2);  //$param2 = message_thread_code
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(site_url('teacher/group_message/group_message_read/' . $param2), 'refresh');
        }
        $page_data['message_inner_page_name']   = $param1;
        $page_data['page_name']                 = 'group_message';
        $page_data['page_title']                = get_phrase('group_messaging');
        $this->load->view('backend/index', $page_data);
    }

    // MANAGE QUESTION PAPERS
    function question_paper($param1 = "", $param2 = "")
    {
        if ($this->session->userdata('teacher_login') != 1) {
            $this->session->set_userdata('last_page', current_url());
            redirect(base_url(), 'refresh');
        }

        if ($param1 == "create") {
            $this->crud_model->create_question_paper();
            $this->session->set_flashdata('flash_message', get_phrase('data_created_successfully'));
            redirect(site_url('teacher/question_paper'), 'refresh');
        }

        if ($param1 == "update") {
            $this->crud_model->update_question_paper($param2);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated_successfully'));
            redirect(site_url('teacher/question_paper'), 'refresh');
        }

        if ($param1 == "delete") {
            $this->crud_model->delete_question_paper($param2);
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted_successfully'));
            redirect(site_url('teacher/question_paper'), 'refresh');
        }

        $data['page_name']  = 'question_paper';
        $data['page_title'] = get_phrase('question_paper');
        $this->load->view('backend/index', $data);
    }

    // Details of searched student
    function student_details()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(base_url(), 'refresh');

        $student_identifier = html_escape($this->input->post('student_identifier'));
        $query_by_code = $this->db->get_where('student', array('student_code' => $student_identifier));

        if ($query_by_code->num_rows() == 0) {
            $this->db->like('name', $student_identifier);
            $query_by_name = $this->db->get('student');
            if ($query_by_name->num_rows() == 0) {
                $this->session->set_flashdata('error_message', get_phrase('no_student_found'));
                redirect(site_url('teacher/dashboard'), 'refresh');
            } else {
                $page_data['student_information'] = $query_by_name->result_array();
            }
        } else {
            $page_data['student_information'] = $query_by_code->result_array();
        }
        $page_data['page_name']      = 'search_result';
        $page_data['page_title']     = get_phrase('search_result');
        $this->load->view('backend/index', $page_data);
    }











    function get_class_routine_data_periodo()
    {



        $class_id = $this->input->post("class_id");
        //$data = $this->input->post("");


        $data1 = $this->input->post('data_atividade');
        $data2 = $this->input->post('data_atividade_fim');

        $data_at = str_replace("/", "-",  $data1);
        $data_at2 = str_replace("/", "-",  $data2);
        $data = date('Y-m-d', strtotime($data_at));
        $data22 = date('Y-m-d', strtotime($data_at2));

        // echo $class_id;
        //  echo $data;
        //  echo $data22;

        $planos_selecionadas = $this->ajaxload->get_planos_data_periodo($class_id, $data, $data22);
        $datas_selecionadas = $this->ajaxload->get_datas_periodo($class_id, $data, $data22);

        // var_dump($datas_selecionadas);




        if (count($datas_selecionadas) > 0) {

            foreach ($datas_selecionadas as $dt) {
                echo '<tr>';
                echo '<td style="padding-left:10px; width:10%;"><b>';
                $data = date('d-m-Y', strtotime($dt->data_atividade));
                echo $data;
                echo " : </b></td>";
                echo '<td style="padding:20px; width:100%;">';
                foreach ($planos_selecionadas as $plan) {
                    if ($plan->data_atividade == $dt->data_atividade) {



                        echo '<div class="btn-group" style="margin-right:3px;">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
                        echo $this->crud_model->get_subject_name_by_id($plan->subject_id);

                        if ($plan->time_start_min == 0 && $plan->time_end_min == 0)
                            echo ' (' . $plan->time_start . '-' . $plan->time_end . ') ';
                        if ($plan->time_start_min != 0 || $plan->time_end_min != 0)
                            echo ' (' . $plan->time_start . ':' . $plan->time_start_min . '-' . $plan->time_end . ':' . $plan->time_end_min . ') ';

                        echo '<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                        <a href="#" onclick="showAjaxModal(';
                        echo "'";
                        echo site_url('modal/popup/modal_edit_class_routine/' . $plan->class_routine_id);
                        echo "'";
                        echo ');">
                                            <i class="entypo-pencil"></i>
                                               Editar
                                                        </a>
                                 </li>';

                        echo '<li> 
                                    <a href="#" onclick="confirm_modal(';
                        echo "'";
                        echo site_url('teacher/class_routine/delete/' . $plan->class_routine_id);
                        echo "'";
                        echo ');">
                                        <i class="entypo-trash"></i>
                                            Deletar
                                        </a>
                                    </li>
                                    </ul>
                                </div>';
                    }
                }
                echo "</td>";
                echo '</tr>';
            }
        }
    }






    function get_teachers()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'teacher_id',
            1 => 'photo',
            2 => 'name',
            3 => 'email',
            4 => 'phone',
            5 => 'teacher_id'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_teachers_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $teachers = $this->ajaxload->all_teachers($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $teachers =  $this->ajaxload->teacher_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->teacher_search_count($search);
        }

        $data = array();
        if (!empty($teachers)) {
            foreach ($teachers as $row) {

                $photo = '<img src="' . $this->crud_model->get_image_url('teacher', $row->teacher_id) . '" class="img-circle" width="30" />';

                $nestedData['teacher_id'] = $row->teacher_id;
                $nestedData['photo'] = $photo;
                $nestedData['name'] = $row->name;
                $nestedData['email'] = $row->email;
                $nestedData['phone'] = $row->phone;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    function get_books()
    {
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'book_id',
            1 => 'name',
            2 => 'author',
            3 => 'description',
            4 => 'price',
            5 => 'class',
            6 => 'download',
            7 => 'book_id'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_books_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $books = $this->ajaxload->all_books($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $books =  $this->ajaxload->book_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->book_search_count($search);
        }

        $data = array();
        if (!empty($books)) {
            foreach ($books as $row) {
                if ($row->file_name == null)
                    $download = '';
                else
                    $download = '<a href="' . site_url("uploads/document/$row->file_name") . '" class="btn btn-blue btn-icon icon-left"><i class="entypo-download"></i>' . get_phrase('download') . '</a>';

                $nestedData['book_id'] = $row->book_id;
                $nestedData['name'] = $row->name;
                $nestedData['author'] = $row->author;
                $nestedData['description'] = $row->description;
                $nestedData['price'] = $row->price;
                $nestedData['class'] = $this->db->get_where('class', array('class_id' => $row->class_id))->row()->name;
                $nestedData['download'] = $download;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    // STUDENT PROMOTION
    function trocar_aluno_turma($param1 = '', $param2 = '')
    { 
        if ($this->session->userdata('teacher_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'promote') {
            $student_id = $this->input->post('student');

            //validar datas de matriculas
            $sql = "SELECT enroll.* FROM `enroll` WHERE `date_added` >= ". strtotime($this->input->post('data_matricula')) ." AND enroll.student_id=".$student_id. " ORDER BY date_added ASC";
            
            $query = $this->db->query($sql); 

            if ($query->num_rows() < 1) {
                $row = $query->row();
                $sections = $this->db->get_where('section', array('class_id' => $this->input->post('promotion_to_class_id')))->row_array();
                $enroll_data['enroll_code']     =   substr(md5(rand(0, 1000000)), 0, 7);
                $enroll_data['student_id']      =   $student_id;
                $enroll_data['class_id']        =   $this->input->post('promotion_to_class_id');
                $enroll_data['section_id']      =   $sections['section_id'];
                $enroll_data['year']            =   $this->input->post('promotion_year');
                $enroll_data['date_added']      =   strtotime(date("Y-m-d", strtotime($this->input->post('data_matricula'))));
                $this->db->insert('enroll', $enroll_data);
                // var_dump($enroll_data);

                $this->session->set_flashdata('flash_message', "Troca de turma realizada com sucesso!");
                redirect(site_url('teacher/trocar_aluno_turma'), 'refresh');
            }else{
                $row = $query->row();
                $this->session->set_flashdata('error_message', "Erro ao fazer a troca! A data de matrícula deve ser maior que última matrícula do aluno, feita no dia ".date("d/m/Y",$row->date_added)).'.';
                redirect(site_url('teacher/trocar_aluno_turma'), 'refresh');
            }
        }

        $page_data['page_title']    = 'Trocar Aluno de Turma';
        $page_data['page_name']  = 'trocar_aluno_turma';

        $this->load->view('backend/index', $page_data);
    }
}
