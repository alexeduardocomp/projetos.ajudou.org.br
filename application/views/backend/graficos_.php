<?php
$ano = $_GET['ano'];

$projeto_id = $_GET['projeto'];
$nome_projeto = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->nome;
$tipo_projeto = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->tipo_projeto;

$proponente = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->proponente;

$nucleo_id = $_GET['nucleo'];
$nome_nucleo = $this->db->get_where('nucleo', array('id' => $nucleo_id))->row()->nome_nucleo;

$this->db->select("count(*) as soma, parente_trabalha_patrocinadora");
$this->db->where("parente_trabalha_patrocinadora<>", "");
$this->db->where("id_projeto", $projeto_id);
$this->db->where("id_nucleo", $nucleo_id);
$this->db->where("year(data_aluno_projeto)", $ano);
$this->db->group_by("parente_trabalha_patrocinadora");
$dados = $this->db->get("student")->result();
?>

<html>

<head>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <script type="text/javascript">
    google.charts.load("current", {
      packages: ["corechart"],
      language: 'pt'
    });
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = google.visualization.arrayToDataTable([
        ['Parente trabalha para Patrocinador', 'Quantidade'],
        <?php
        foreach ($dados as $d) {
          echo "['" . $d->parente_trabalha_patrocinadora . "', " . $d->soma . "],";
        }
        ?>
      ]);

      var options = {
        // title: 'My Daily Activities'
      };

      var chart = new google.visualization.PieChart(document.getElementById('piechart'));

      chart.draw(data, options);
    }
  </script>

  <script>
    var is_chrome = function() {
      return Boolean(window.chrome);
    }
    if (is_chrome) {
      window.print();
      setTimeout(function() {
        window.close();
      }, 10000);
      //give them 10 seconds to print, then close
    } else {
      window.print();
      window.close();
    }
  </script>
</head>

<body onLoad="loadHandler();">

  <?php

  $logo = "";
  if ($tipo_projeto == "FEDERAL") {
    $logo =  '<td ROWSPAN="2" width="120" style="text-align:center;">
    <div><img src="' . base_url() . 'uploads/logos/federal.jpg " width="100" style="padding:5px 0px;"></div>
  </td>';
  } elseif ($tipo_projeto == "ESTADUAL") {
    $logo = '<td ROWSPAN="2" width="120" style="text-align:center;">
    <div><img src="' . base_url() . 'uploads/logos/estadual.jpg " width="100" style="padding:5px 0px;"></div>
  </td>';
  }

  $cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
        <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE PARENTE TRABALHA PARA PATROCINADOR</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Ano: </b> ' . $ano . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>';
  echo $cabecalho;
  ?>

  <div id="piechart" style="width: 900px; height: 500px;"></div>

  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <th>Parente trabalha para Patrocinador</th>
      <th>Quantidade</th>
    </tr>
    <?php
    foreach ($dados as $dado) {
    ?>
      <tr>
        <td><?php echo $dado->parente_trabalha_patrocinadora; ?></td>
        <td><?php echo $dado->soma; ?></td>
      </tr>
    <?php
    }
    ?>
  </table>
</body>

</html>