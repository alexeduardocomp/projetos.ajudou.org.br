<?php 
    $query = $this->db->get('projeto');
    $projetos = $query->result();

        $query2 = $this->db->get('nucleo');
    $nucleos = $query2->result();


        $query3 = $this->db->get('modalidade');
    $modalidades = $query3->result();

?>


  <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_turma_add/');?>');"
                class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
               Adicionar nova turma
 
                </a>
                <br/><br/>

<hr />
<div class="row">
	<div class="col-md-12">
    
    	<!------CONTROL TABS START------>
		<ul class="nav nav-tabs bordered">
			<li class="active">
            	<a href="#list" data-toggle="tab"><i class="entypo-menu"></i> 
					Lista de turmas
                    	</a></li>
			<!--<li>
            	<a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
					Adicionar turma
                    	</a></li>-->
		</ul>
    	<!------CONTROL TABS END------>
        
		<div class="tab-content">
        <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
				
                <table class="table table-bordered datatable" id="table_export">
                	<thead>
                		<tr>
                    		<th><div>Id turma</div></th>
                    		<th><div>Nome turma</div></th>
                    		<th><div>Instrutor</div></th>
                            <th><div>Projeto</div></th>
                            <th><div>Núcleo</div></th>
                            <th><div>Local</div></th>
                            <th><div>Modalidade</div></th>
                            <th><div>Opções</div></th>
						</tr>
					</thead>
                    <tbody>
                    	<?php $count = 1;foreach($classes as $row):?>
                        <tr>
                            <td><?php echo $row['class_id']?></td>
							<td><?php echo $row['name'];?></td>
                            <td><?php echo $row['nome_instrutor'];?></td>
                            <td><?php echo $row['nome_projeto'];?></td>
                            <td><?php echo $row['nome_nucleo'];?></td>
                            <td><?php echo $row['local'];?></td>
                            <td>


                              
                                

                                <?php

                                   $modalidade_selecionado = $this->ajaxload->get_modalidade_by_id($row['modalidade']);

                 foreach ($modalidade_selecionado as $instx) {
                            $nome_modalidade = $instx->modalidade;
                        }

                   echo $nome_modalidade;

                                 ?>


                              </td>
							<td>
                            <div class="btn-group">
                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                    
                                    <!-- EDITING LINK -->
                                    <li>
                                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_edit_class/'.$row['class_id']);?>');">
                                            <i class="entypo-pencil"></i>
                                                Editar
                                            </a>
                                                    </li>
                                    <li class="divider"></li>
                                    
                                    <!-- DELETION LINK -->
                                    <li>
                                        <a href="#" onclick="confirm_modal('<?php echo site_url('admin/classes/delete/'.$row['class_id']);?>');">
                                            <i class="entypo-trash"></i>
                                                Deletar
                                            </a>
                                                    </li>
                                </ul>
                            </div>
        					</td>
                        </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
			</div>
            <!----TABLE LISTING ENDS--->
            
            
			<!----CREATION FORM STARTS---->
			<!--<div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                	<?php echo form_open(site_url('admin/classes/create') , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                        <div class="padded">




                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                                  <div class="col-sm-5">
                                    <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

                                        <option selected disabled>Selecione</option>
                                    <?php

                                    $tama = count($projetos);
                                            for ($i=0; $i < $tama ; $i++) { 
                        
                                                echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

                                            }

                                     ?>
                                      
                                    </select>
                                </div>

                                </div>


                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                                  <div class="col-sm-5">
                                    <select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                            <option disabled selected>Selecione um projeto</option>

                                      
                                    </select>
                                </div>

                                </div>

                                <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Local execução</label>
                                  <div class="col-sm-5">
                                    <select name="id_local" class="form-control" id="loc" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                            <option disabled selected>Selecione um núcleo</option>

                                      
                                    </select>
                                </div>

                                </div>


                                 <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Instrutor</label>
                                  <div class="col-sm-5">
                                    <select name="id_istrutor" class="form-control" id="ins" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                            <option disabled selected>Selecione um núcleo</option>

                                      
                                    </select>
                                </div>

                                </div>





                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Modalidade</label>
                                  <div class="col-sm-5">
                                    <select name="modalidade" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

                                        <option selected disabled>Selecione</option>
                                    <?php

                                    $tama = count($modalidades);
                                            for ($i=0; $i < $tama ; $i++) { 
                        
                                                echo '<option value='.$modalidades[$i]->id.'>'.$modalidades[$i]->modalidade.'</option>';

                                            }

                                     ?>
                                      
                                    </select>
                                </div>

                                </div>






                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nome</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Categoria</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="categoria" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Intervalo de idades</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="intervalo_idades" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>


                             <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Sexo</label>
                                  <div class="col-sm-5">
                                    <select name="sexo" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

                                        <option selected value='MASCULINO'>MASCULINO</option>
                                         <option value='FEMININO'>FEMININO</option>
                                    
                                      
                                    </select>
                                </div>

                                </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">Número de Atletas</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="numero_atletas" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-group">
                              <div class="col-sm-offset-3 col-sm-5">
                                  <button type="submit" class="btn btn-info">Adicionar turma</button>
                              </div>
							</div>
                    </form>                
                </div>                
			</div>-->
			<!----CREATION FORM ENDS-->
		</div>
	</div>
</div>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->                      
<script type="text/javascript">

    jQuery(document).ready(function($){


    $('#table_export').DataTable({
    columnDefs: [
            { orderable: false, targets: 7 }
    ],
                "scrollX": true,
            "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 },

 
                  dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
  buttons: [

  { extend: 'copy', text: 'Copiar' },
  { extend: 'excel', text: 'Excel' },
  { extend: 'pdf', text: 'PDF' },
  { extend: 'csv', text: 'CSV' }
  ],  
        });
    
        

        var datatable = $("#table_export").dataTable();
        
        $(".dataTables_wrapper select").select2({
            minimumResultsForSearch: -1
        });
    });
        
</script>


