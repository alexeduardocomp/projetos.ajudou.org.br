<?php
$this->db->where('tipo_projeto', 'FEDERAL');
$query = $this->db->get('projeto');
$projetos = $query->result();
$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();
?>
<style type="text/css">
    .page-body .selectboxit-container {
        margin-left: 15px;
    }
</style>
<meta charset="utf-8">
<div class="panel panel-primary">
    <div class="panel-heading">
        Frequência da
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                    <div class="col-sm-12">
                        <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <?php
                            $tama = count($projetos);
                            for ($i = 0; $i < $tama; $i++) {
                                echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                    <div class="col-sm-12">
                        <select name="id_nucleo" class="form-control js-example-basic-multiple" multiple="multiple" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled>Selecione um projeto</option>
                        </select>
                    </div>
                </div>
            </div>
            <input name="nucleos" id="nucleos" type="hidden"></input>

            <div class="col-md-5">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-12">
                        <select name="class_id" class="form-control js-example-basic-multiple" multiple="multiple" id="turm" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" onchange="return get_class_sections(this.value)">
                            <option disabled>Selecione um núcleo</option>
                        </select>
                    </div>
                </div>
            </div>
            <input name="turmas" id="turmas" type="hidden"></input>

            <div id="section_holder" style="display: none;">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label" style="margin-bottom: 5px;">Sessão</label>
                        <select class="form-control selectboxit" name="section_id">
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">


            <div class="col-md-2">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Ano</label>
                    <select class="form-control selectboxit" name="sessional_year" id="ano" style="margin-left:12px;">
                        <?php
                        $sessional_year_options = explode('-', $running_year); ?>
                        <option value="<?php echo $sessional_year_options[0]; ?>"><?php echo $sessional_year_options[0]; ?></option>
                        <option value="<?php echo $sessional_year_options[1]; ?>"><?php echo $sessional_year_options[1]; ?></option>
                    </select>
                </div>
            </div>

            <div class="col-md-4">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Mês</label>
                    <div class="col-sm-12">

                        <select name="month" class="form-control selectboxit js-example-basic-multiple" multiple="multiple" id="mes">
                            <?php
                            for ($i = 1; $i <= 12; $i++) :
                                if ($i == 1)
                                    $m = 'Janeiro';
                                else if ($i == 2)
                                    $m = 'Fevereiro';
                                else if ($i == 3)
                                    $m = 'Março';
                                else if ($i == 4)
                                    $m = 'Abril';
                                else if ($i == 5)
                                    $m = 'Maio';
                                else if ($i == 6)
                                    $m = 'Junho';
                                else if ($i == 7)
                                    $m = 'Julho';
                                else if ($i == 8)
                                    $m = 'Agosto';
                                else if ($i == 9)
                                    $m = 'Setembro';
                                else if ($i == 10)
                                    $m = 'Outubro';
                                else if ($i == 11)
                                    $m = 'Novembro';
                                else if ($i == 12)
                                    $m = 'Dezembro';
                            ?>
                                <option value="<?php echo $i; ?>" <?php if ($month == $i) echo 'selected'; ?>>
                                    <?php echo $m; ?>
                                </option>
                            <?php
                            endfor;
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <input type="hidden" name="year" value="<?php echo $running_year; ?>">
            <div class="col-md-2" style="margin-top: 25px; margin-left: 30px;">
                <a id="link_relatorio" href="<?php echo site_url('admin/relatorio_frequencia_print_view/'); ?>" target="_blank">
                    <button id="submit" class="btn btn-info">Gerar Relatório PDF</button>
                </a>
            </div>
            <div class="col-md-2" style="margin-top: 25px; margin-left: 0px;">
                <div class="botoes" style="">
                    <a id='link_excel' target="_blank" href="http://projetos.ajudou.org.br/index.php/admin/excel/">
                        <button class="btn btn-success">Gerar Relatório Excel</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript">
    // Change Projeto
    $("#pro").change(function() {

        $("#nuc").val("");
        $("#nuc").empty();
        $("#nuc").select2("val", "");

        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);

                for (var i = 0; i < data.length; i++) {
                    $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                }
            }
        });
    });

    // Change Núcleo
    $("#nuc").change(function() {

        var nucleos = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        $('#assigned_to').empty();
        var clients = $('#clients_select').val();
        var groups = $('#groups_select').val();

        $("#turm").val("");
        $("#turm").empty();
        $("#turm").select2("val", "");

        var url = "<?php echo site_url('admin/get_turma_nucleo_multiple'); ?>" + "?nucleos=" + nucleos + "";
        $.ajaxSetup({
            async: false
        });
        $.get(url, function(data) {
            const json = data;
            const obj = JSON.parse(json);
            $.each(obj, function(key, item) {
                var select = "<option value='" + item.class_id + "'>" + item.name + " - " + item.nome_nucleo + "</option>";
                $('#turm').append(select);
            });
        });
        $.ajaxSetup({
            async: true
        });
        var str = '<?php echo site_url("admin/relatorio_frequencia_print_view/?projeto="); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano;        
        var str = str.replaceAll(",", ".");
        $('#link_relatorio').attr('href', str);
        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano);

        if ((id_turma != null) & (id_projeto != null) & (nucleos != null)) {

            $('#submit').removeAttr('disabled');
        }


        /*
                var id_nucleo = $("#nuc").val();
                var id_projeto = $("#pro").val();

                //alert(id_nucleo);
                $.ajax('<?php echo site_url('admin/get_turma_nucleo_multiple/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: id_nucleo
                    }, // data to submit
                    success: function(data2, status, xhr) {
                        //alert(data2);
                        $("#turm").empty();
                        data2 = $.parseJSON(data2);

                        if ((data2 == null) | (data2 == '')) {
                            $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                        } else {
                            for (var i = 0; i < data2.length; i++) {

                                $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                            }
                        }

                        var id_turma = $("#turm").val();

                        var mes = $("#mes").val();
                        var ano = $("#ano").val();

                        $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_frequencia_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano);
                        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano);

                        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {

                            $('#submit').removeAttr('disabled');
                        }

                        $.ajax({
                            url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
                            success: function(response) {
                                jQuery('#section_selector_holder').html(response);
                            }
                        });
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                    }
                });
                */
    });

    // Change Turma
    $("#turm").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var mes = $("#mes").val();
        var ano = $("#ano").val();
        var str = '<?php echo site_url("admin/relatorio_frequencia_print_view/?projeto="); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano;        
        var str = str.replaceAll(",", ".");
     
        $('#link_relatorio').attr('href', str);
        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano);

        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });


    // Change Mês
    $("#mes").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var mes = $("#mes").val();
        var ano = $("#ano").val();

        var str = '<?php echo site_url("admin/relatorio_frequencia_print_view/?projeto="); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano;        
        var str = str.replaceAll(",", ".");
        $('#link_relatorio').attr('href', str);
        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano);
       
        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {

            $('#submit').removeAttr('disabled');
        }

    });

    // Change Ano
    $("#ano").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var mes = $("#mes").val();
        var ano = $("#ano").val();

        var str = '<?php echo site_url("admin/relatorio_frequencia_print_view/?projeto="); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano;        
        var str = str.replaceAll(",", ".");
        $('#link_relatorio').attr('href', str);
        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&mes=' + mes + '&ano=' + ano);
  
        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });

    var class_selection = "";
    jQuery(document).ready(function($) {
        $('#submit').attr('disabled', 'disabled');
    });

    function select_section(class_id) {
        if (class_id !== '') {
            $.ajax({
                url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
                success: function(response) {
                    jQuery('#section_holder').html(response);
                }
            });
        }
    }

    function check_validation() {
        if (class_selection !== '') {
            $('#submit').removeAttr('disabled')
        } else {
            $('#submit').attr('disabled', 'disabled');
        }
    }

    $('#class_selection').change(function() {
        class_selection = $('#class_selection').val();
        check_validation();
    });

    // $( "#pro" ).change(function() {
    // var id_projeto = $("#pro").val();

    //  $.ajax('<?php echo site_url('admin/get_turma_projeto/'); ?>', {
    //     type: 'POST',  // http method
    //     data: { dados: id_projeto },  // data to submit
    //     success: function (data, status, xhr) {

    //     	//alert(data);

    //     		  $('#class_selection').empty();
    //     	 data = $.parseJSON(data);

    //     	 if(data.length > 1){
    //     	 	     for (var i = 0; i < data.length; i++) {

    //     	 	     	if(i == 0){
    //     	 	     		$('#class_selection').append('<option selected value='+data[i]['id']+'>'+data[i]['nome']+'</option>');
    //     	 	     	}else{
    //     	 	     		$('#class_selection').append('<option value='+data[i]['id']+'>'+data[i]['nome']+'</option>');
    //     	 	     	}


    // }
    // class_selection = 'selecionado';
    // check_validation();
    // var id_class = $("#class_selection").val();
    // select_section(id_class);
    //     	 }else{
    //     	 	class_selection = '';
    //     	 	check_validation();
    //     $('#class_selection').append('<option disabled selected>Nenhuma turma encontrada</option>');
    //     	 }   




    //     },
    //     error: function (jqXhr, textStatus, errorMessage) {
    //            alert(errorMessage);
    //     }
    // });

    // });

    $('#class_selection').change(function() {
        var id_class = $("#class_selection").val();
        select_section(id_class);
    });

    //Select2
    $(document).ready(function() {
        $('#nuc').select2();
    });
    $(document).ready(function() {
        $('#turm').select2();
    });
    $(document).ready(function() {
        $('#mes').select2();
    });
</script>