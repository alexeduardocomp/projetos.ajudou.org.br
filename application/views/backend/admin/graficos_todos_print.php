<?php

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(1);
ini_set('display_errors', 1); 

require_once 'phplot.php'; 


$mpdf = new \Mpdf\Mpdf([
  'margin_top' => '0',
  'debug' => true,
  'setAutoTopMargin' => 'pad',
  'pagenumPrefix' => 'Página ',
  'pagenumSuffix' => ' - ',
  'nbpgPrefix' => ' de ',
  'nbpgSuffix' => ' páginas / '
]);


$data_inicial = $_POST['data_inicial'];
$data_final = $_POST['data_final'];
$graficos_array = json_decode($_POST['graficos']);

$projeto_id = $_POST['id_projeto'];
$nome_projeto = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->nome;
$tipo_projeto = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->tipo_projeto;

$proponente = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->proponente;

$nucleo_id = $_POST['id_nucleo'];
if ($nucleo_id == "todos") {
  $nome_nucleo = "TODOS OS NÚCLEOS";
} else {
  $nome_nucleo = $this->db->get_where('nucleo', array('id' => $nucleo_id))->row()->nome_nucleo;
}

/* echo '<pre>'; 
echo print_r($graficos_array);
echo '</pre>'; */

/******************************************************************************************************************
 * Gráfico Escolas
 */

$this->db->select("*, count(*) as soma");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("nome_escola<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->order_by('nome_escola', 'ASC');
$this->db->group_by("id_escola");
$dados = $this->db->get("student")->result();

$logo = "";
if ($tipo_projeto == "FEDERAL") {
  $logo =  '<td ROWSPAN="2" width="120" style="text-align:center;">
    <div><img src="' . base_url() . 'uploads/logos/federal.jpg " width="100" style="padding:5px 0px;"></div>
  </td>';
} elseif ($tipo_projeto == "ESTADUAL") {
  $logo = '<td ROWSPAN="2" width="120" style="text-align:center;">
    <div><img src="' . base_url() . 'uploads/logos/estadual.jpg " width="100" style="padding:5px 0px;"></div>
  </td>';
}

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
        <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE ESCOLAS</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->nome_escola, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph(); */
$total .= "<img src='" . $graficos_array->escolas . "'>";


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Escola</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->nome_escola . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}
$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->WriteHTML($total);


/******************************************************************************************************************
 * Gráfico Faixa Etária
 */

$this->db->select("count(*) as soma, TIMESTAMPDIFF(YEAR, data_nascimento, NOW()) AS idade,id_nucleo,id_projeto");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("data_nascimento<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->order_by('idade', 'ASC');
$this->db->group_by("idade");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE FAIXA ETÁRIA</div>
      </td>
      ' . $logo . '
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->idade, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(' anos: ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->faixa_etaria . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Idade</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->idade . " anos</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}
$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->shrink_tables_to_fit = 1.4;
$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total);

/******************************************************************************************************************
 * Gráfico Moradores a Domicílio
 */

$this->db->select("count(*) as soma, numero_pessoas_domicilio");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("numero_pessoas_domicilio<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->order_by("numero_pessoas_domicilio", "ASC");
$this->db->group_by("numero_pessoas_domicilio");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE MORADORES A DOMICÍLIO</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {
  $data[] = array($d->numero_pessoas_domicilio, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(' pessoas: ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->moradores . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Número de pessoas moradores a domicílio</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->numero_pessoas_domicilio . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}
$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->shrink_tables_to_fit = 1.4;
$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total);


/******************************************************************************************************************
 * Gráfico Problemas de Saúde
 */

$this->db->select("count(*) as soma, problema_saude");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("problema_saude<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("problema_saude");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE PROBLEMAS DE SAÚDE</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->problema_saude, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->problemas_saude . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Problemas de Saúde</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->problema_saude . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}
$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total);


/******************************************************************************************************************
 * Gráfico Renda Familiar
 */

$this->db->select("count(*) as soma, renda_familiar");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("renda_familiar<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("renda_familiar");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE RENDA</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->renda_familiar, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->renda  . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Renda</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->renda_familiar . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}
$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total);


/******************************************************************************************************************
 * Gráfico Gênero
 */

$this->db->select("count(*) as soma, sex");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("sex<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("sex");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE GÊNERO</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->sex, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
 /*  $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->genero  . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Gênero</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->sex . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}
$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->AddPage('P');
$mpdf->WriteHTML($total);


/******************************************************************************************************************
 * Gráfico Responsável pelo Sustento
 */

$this->db->select("count(*) as soma, responsavel_sustento");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("responsavel_sustento<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("responsavel_sustento");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE RESPONSÁVEIS PELO SUSTENTO</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->responsavel_sustento, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
 /*  $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->responsavel_sustento . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Responsável pelo Sustento</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->responsavel_sustento . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}

$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->shrink_tables_to_fit = 1.4;
$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total, \Mpdf\HTMLParserMode::HTML_BODY);

/******************************************************************************************************************
 * Gráfico Situação
 */

$this->db->select("count(*) as soma, situacao");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("situacao<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("situacao");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
        <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE SITUAÇÃO</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->situacao, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->situacao  . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Situação</th>
    <th>Quantidade</th>
  </tr>';
  $soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->situacao . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}

$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->AddPage('P');

$mpdf->WriteHTML($total);


/******************************************************************************************************************
 * Gráfico Tipo Escola
 */

$this->db->select("count(*) as soma, tipo_escola");
$this->db->join("escola", "escola.id = student.id_escola");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("tipo_escola<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("tipo_escola");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE TIPO DE ESCOLA</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->tipo_escola, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->tipo_escola . "'>"; 


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Tipo de Escola</th>
    <th>Quantidade</th>
  </tr>';
$soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->tipo_escola . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}
$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";

$tabela .= "</table>";
$total .= $tabela;

$mpdf->shrink_tables_to_fit = 1.4;
$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total);




/******************************************************************************************************************
 * Gráfico Parente Trabalha Patrocinador
 */

$this->db->select("count(*) as soma, parente_trabalha_patrocinadora");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("parente_trabalha_patrocinadora<>", "");
$this->db->where("id_projeto", $projeto_id);
if ($nucleo_id != "todos") {
  $this->db->where("student.id_nucleo", $nucleo_id);
}
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("parente_trabalha_patrocinadora");
$dados = $this->db->get("student")->result();

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE PARENTE TRABALHA PARA PATROCINADOR</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->parente_trabalha_patrocinadora, $d->soma);
}

/* $plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0); */

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  /* $plot->SetLegend($legenda); */
}


# Place the legend in the upper left corner:
/* $plot->SetLegendPixels(5, 5);

$plot->DrawGraph();*/
$total .= "<img src='" . $graficos_array->trabalha_patrocinador . "'>"; 

 
$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-top: 20px;">
  <tr>
    <th>Parente trabalha para Patrocinador</th>
    <th>Quantidade</th>
  </tr>';

$soma_total = 0;
foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td style='text-align: center;'>" . $dado->parente_trabalha_patrocinadora . "</td>
      <td style='text-align: center;'>" . $dado->soma . "</td>
    </tr>
  ";
  $soma_total = $soma_total + $dado->soma;
}

$tabela .= "<tr>
              <td style='text-align: center;'><b>Total</b></td>".
              "<td style='text-align: center;'>".$soma_total."</td>"
            ."</tr>";
            

$tabela .= "</table>";
$total .= $tabela;

$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total);
ob_clean();
$mpdf->output();
