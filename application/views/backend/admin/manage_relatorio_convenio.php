<?php
$query = $this
->db
->where('tipo_projeto', 'CONVÊNIO')
->order_by('projeto.nome', 'ASC')
->get('projeto');

$projetos = $query->result();


$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();

$query3 = $this->db->get('modalidade');
$modalidades = $query3->result();

$instrutores = $this->db->get('teacher')->result();

$escolas = $this->db->get('escola')->result();

$turmas = $this->db->get('class')->result();

$query5 = $this->db->get('local_execucao');
$locais_execucao = $query5->result();

?>
<style type="text/css">
	.page-body .selectboxit-container {
		/*margin-left: 15px;*/
	}
</style>


<meta charset="utf-8">
<div class="panel panel-primary">
	<div class="panel-heading">
		Filtrar beneficiados
	</div>
	<div class="panel-body">


		<?php echo form_open(site_url('admin/relatorio_projeto_print_view/'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data', 'id' => 'formulario_beneficiados', 'target' => '_blank')); ?>

		<div class="row">

			<div class="col-md-3">

				<div class="form-group">
					<div class="col-sm-12">
						<label for="field-1" class=" control-label">Projeto</label>

						<select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option selected disabled>Selecione</option>
							<?php
							$tama = count($projetos);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<div class="col-sm-12">
						<label for="field-1" class=" control-label" style="text-align: left;">Data inicial</label>
						<input type="date" class="form-control" name="data_inicial" id="data_inicial" autofocus value="">
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<div class="col-sm-12">
						<label for="field-1" class="control-label" style="text-align: left;">Data final</label>
						<input type="date" class="form-control" name="data_final" id="data_final" autofocus value="">
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<div class="col-sm-12">
						<label for="field-1" class="control-label">Núcleos</label>
						<select name="id_nucleo[]" class="form-control js-example-basic-multiple" multiple="multiple" id="nuc">
							<?php
							$tama = count($nucleos);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $nucleos[$i]->id . '>' . $nucleos[$i]->nome_nucleo . '</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Local de execução</label>
					<div class="col-sm-12">
						<select name="local_execucao[]" class="form-control" id="local_execucao" multiple>
							<?php
							$tama = count($locais_execucao);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $locais_execucao[$i]->id . '>' . $locais_execucao[$i]->local . '</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Modalidades</label>
					<div class="col-sm-12">
						<select name="modalidade_id[]" class="form-control" id="modalidade_id" multiple>
							<?php
							$tama = count($modalidades);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $modalidades[$i]->id . '>' . $modalidades[$i]->modalidade . '</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Intrutores</label>
					<div class="col-sm-12">
						<select name="instrutores[]" class="form-control" id="instrutores" multiple>
							<?php
							$tama = count($instrutores);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $instrutores[$i]->id . '>' . $instrutores[$i]->name . '</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Turmas</label>
					<div class="col-sm-12">
						<select name="class_id[]" class="form-control" id="turm" multiple>
							<?php
							$tama = count($turmas);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $turmas[$i]->class_id . '>' . $turmas[$i]->name . '</option>';
							}
							?> </select>
					</div>
				</div>
			</div>
		</div>

		<div class="row" style="margin-top:0px;" id="linha_02">

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Tipo Escola</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="tipo_escola" id="tipo_escola">
							<option value="AMBOS" selected>AMBOS</option>
							<option value="PÚBLICA">PÚBLICA
							</option>
							<option value="PARTICULAR">PARTICULAR
							</option>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label" style="text-align: left;">Escolas</label>
					<div class="col-sm-12">
						<select name="escola_id[]" class="form-control" id="esc" onchange="return get_class_sections(this.value)" multiple>
							<?php
							$tama = count($escolas);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $escolas[$i]->id . '>' . $escolas[$i]->nome . '</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Sexo</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="sexo" id="sexo">
							<option value="AMBOS">AMBOS</option>
							<option value="MASCULINO">MASCULINO
							</option>
							<option value="FEMININO">FEMININO
							</option>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label" style="text-align: left;">Situação</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="situacao" id="situacao">
							<option value="AMBOS" selected>AMBAS</option>
							<option value="ATIVO">ATIVO
							</option>
							<option value="INATIVO">INATIVO
							</option>
						</select>
					</div>
				</div>
			</div>
		</div>

		<div class="row" style="margin-top:0px;" id="linha_03">

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Tipo</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="tipo" id="tipo">
							<option value="PARCIAL" selected>PARCIAL</option>
							<option value="FINAL">FINAL</option>
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Idade Inicial</label>
					<div class="col-sm-12">
						<input type="number" name="idade_inicial" id="idade_inicial" value="0" class="form-control">
					</div>
				</div>
			</div>

			<div class="col-md-3 aparecer">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Idade Final</label>
					<div class="col-sm-12">
						<input type="number" name="idade_final" id="idade_final" value="100" class="form-control">
					</div>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-md-2">
				<button type="submit" class="btn btn-info" id="submit" target="_blank">Gerar Relatório PDF</button>
			</div>

			<div class="col-md-2">

				<a id='link_excel' target="_blank" class="btn btn-success">
					Gerar Relatório Excel
				</a>

			</div>
			<input type="hidden" name="year" value="<?php echo $running_year; ?>">

			<?php echo form_close(); ?>
		</div>

		<br><br>
	</div>
</div>
<div class="panel panel-primary">
	<div class="panel-heading">
		Resultados da Consulta
	</div>
	<div class="panel-body">


		<div id="qtd_result" style="font-weight:bold; font-size:15px;">

		</div>
		<table class="table table-bordered" id="beneficiados">
			<thead>
				<tr>
					<th>
						<div>Nome</div>
					</th>
					<th>
						<div>Modalidade</div>
					</th>
					<th>
						<div>Turma</div>
					</th>
					<th>
						<div>Endereço</div>
					</th>
					<th>
						<div>Telefone</div>
					</th>
					<th>
						<div>Data de nascimento</div>
					</th>
				</tr>
			</thead>
		</table>

		<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
		<script type="text/javascript">
			$("#pro,#local_execucao, #modalidade_id, #data_inicial,#data_final,#nuc,#modalidade_id,#instrutores,#turm,#tipo_escola,#esc,#sexo,#situacao,#idade_inicial,#idade_final").change(function() {

				var url = "<?php echo site_url('admin/excel_beneficiados'); ?>" + "?id_projeto=" + $("#pro").val() +
					"&id_nucleo=" + $("#nuc").val() +
					"&tipo_escola=" + $("#tipo_escola").val() +
					"&modalidade_id=" + $("#modalidade_id").val() +
					"&class_id=" + $("#turm").val() +
					"&sexo=" + $("#sexo").val() +
					"&situacao=" + $("#situacao").val() +
					"&idade_inicial=" + $("#idade_inicial").val() +
					"&idade_final=" + $("#idade_final").val() +
					"&escolas=" + $("#esc").val() +
					"&instrutores=" + $("#instrutores").val() +
					"&local_execucao=" + $("#local_execucao").val() +
					"&data_inicial=" + $("#data_inicial").val() +
					"&data_final=" + $("#data_final").val();

				$("#link_excel").attr("href", url);

				$.ajax('<?php echo site_url('admin/get_beneficiados'); ?>', {
					type: 'POST', // http method
					data: {
						"id_projeto": $("#pro").val(),
						"id_nucleo": $("#nuc").val(),
						"tipo_escola": $("#tipo_escola").val(),
						"modalidade_id": $("#modalidade_id").val(),
						"class_id": $("#turm").val(),
						"sexo": $("#sexo").val(),
						"situacao": $("#situacao").val(),
						"idade_inicial": $("#idade_inicial").val(),
						"idade_final": $("#idade_final").val(),
						"escolas": $("#esc").val(),
						"local_execucao": $("#local_execucao").val(),
						"instrutores": $("#instrutores").val(),
						"data_inicial": $("#data_inicial").val(),
						"data_final": $("#data_final").val(),
					},
					success: function(data2, status, xhr) {
						data = $.parseJSON(data2);
						$("#qtd_result").html("");

						$("#qtd_result").text("Total de beneficiados: " + data["recordsTotal"]);
					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});

				$('#beneficiados').DataTable().clear();
				$('#beneficiados').DataTable().destroy();

				$('#beneficiados').DataTable({
					"processing": true,
					"serverSide": true,
					"searching": false,
					"scrollX": true,
					"paging": false,
					"info": false,
					"ordering": false,
					"ajax": {
						"url": "<?php echo site_url('admin/get_beneficiados') ?>",
						"dataType": "json",
						"type": "POST",
						"data": {
							"id_projeto": $("#pro").val(),
							"id_nucleo": $("#nuc").val(),
							"tipo_escola": $("#tipo_escola").val(),
							"modalidade_id": $("#modalidade_id").val(),
							"class_id": $("#turm").val(),
							"sexo": $("#sexo").val(),
							"situacao": $("#situacao").val(),
							"idade_inicial": $("#idade_inicial").val(),
							"idade_final": $("#idade_final").val(),
							"escolas": $("#esc").val(),
							"local_execucao": $("#local_execucao").val(),
							"instrutores": $("#instrutores").val(),
							"data_inicial": $("#data_inicial").val(),
							"data_final": $("#data_final").val(),
						}
					},
					"columns": [{
							"data": "nome"
						},
						{
							"data": "modalidade"
						},
						{
							"data": "turma"
						},
						{
							"data": "endereco"
						},
						{
							"data": "telefone"
						},
						{
							"data": "data_nascimento"
						}
					],
					dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-12.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
					buttons: [{
							extend: 'excel',
							text: 'Exportar tabela em Excel'
						},
						{
							extend: 'pdf',
							text: 'Exportar tabela em PDF'
						},
						{
							extend: 'csv',
							text: 'Exportar tabela em CSV'
						}
					],

					"oLanguage": {
						"sProcessing": "Aguarde enquanto os dados são carregados ...",
						"sLengthMenu": "Mostrar _MENU_ registros por pagina",
						"sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
						"sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
						"sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
						"sInfoFiltered": "",
						"sSearch": "Procurar",
						"oPaginate": {
							"sFirst": "Primeiro",
							"sPrevious": "Anterior",
							"sNext": "Próximo",
							"sLast": "Último"
						}
					}
				});
			});
		</script>

	</div>

</div>
</div>


<script type="text/javascript">
	$('#add_filtro').click(function() {
		var action = $('#formulario_beneficiados').attr('action');

		if (action == '<?php echo site_url('admin/relatorio_projeto_beneficiario_original/'); ?>') {
			$('#formulario_beneficiados').attr('action', '<?php echo site_url('admin/relatorio_projeto_print_view/'); ?>');
		} else {
			$('#formulario_beneficiados').attr('action', '<?php echo site_url('admin/relatorio_projeto_beneficiario_original/'); ?>');
		}

		var estilo = $('.aparecer').css('display');

		if (estilo == 'none') {
			$('.aparecer').css('display', 'block');
			$('#add_filtro').html('Remover Filtros');
		} else {
			$('.aparecer').css('display', 'none');
			$('#add_filtro').html('Adicionar Filtros');
		}
	})

	// Change Projeto
	$("#pro").change(function() {

		// Busca núcleos
		$("#nuc").val("");
		$("#nuc").empty();
		$("#nuc").select2("val", "");

		var id_projeto = $("#pro").val();

		$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {
				data = $.parseJSON(data);

				for (var i = 0; i < data.length; i++) {
					$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
				}
			}
		});

		// Busca instrutores
		$("#instrutores").val("");
		$("#instrutores").empty();
		$("#instrutores").select2("val", "");

		$.ajax('<?php echo site_url('admin/get_teacher_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {
				data = $.parseJSON(data);

				for (var i = 0; i < data.length; i++) {
					$("#instrutores").append('<option value=' + data[i]['id_instrutor'] + '>' + data[i]['nome_instrutor'] + '</option>');
				}
			}
		});

		// Busca modalidade
		$("#modalidade_id").val("");
		$("#modalidade_id").empty();
		$("#modalidade_id").select2("val", "");

		$.ajax('<?php echo site_url('admin/get_modalidade_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {
				data = $.parseJSON(data);
				for (var i = 0; i < data.length; i++) {
					$("#modalidade_id").append('<option value=' + data[i]['id'] + '>' + data[i]['name'] + '</option>');
				}
			}
		});

		// Busca escola
		$("#esc").val("");
		$("#esc").empty();
		$("#esc").select2("val", "");

		$.ajax('<?php echo site_url('admin/get_escolas_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {
				data = $.parseJSON(data);
				for (var i = 0; i < data.length; i++) {
					$("#esc").append('<option value=' + data[i]['id_escola'] + '>' + data[i]['nome_escola'] + '</option>');
				}
			}
		});

		// Busca local de execucao
		$("#local_execucao").val("");
		$("#local_execucao").empty();
		$("#local_execucao").select2("val", "");

		$.ajax('<?php echo site_url('admin/get_local_execucao_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {
				data = $.parseJSON(data);
				//console.log(data);
				for (var i = 0; i < data.length; i++) {
					//console.log(data[i]['local']);
					$("#local_execucao").append('<option value=' + data[i]['id'] + '>' + data[i]['local'] + '</option>');
				}
			}
		});

	});

	// Change Modalidade
	$('#modalidade_id').change(function() {
		//array de nucleos
		var id_nucleo = $("#nuc").val();

		$("#turm").val("");
		$("#turm").empty();
		$("#turm").select2("val", "");

		//array de modalidades
		var valor = $('#modalidade_id').val();

		if ((valor == null) | (valor == '')) {
			$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
		}

		for (var j = 0; j < id_nucleo.length; j++) {
			for (var i = 0; i < valor.length; i++) {
				var id_nuc = id_nucleo[j];
				var id_modalidade = valor[i];

				$.ajax('<?php echo site_url('admin/get_turma_nucleo_modalidade/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: id_nuc,
						id_modalidade: id_modalidade
					}, // data to submit
					success: function(data2, status, xhr) {

						data2 = $.parseJSON(data2);

						if ((data2 == null) | (data2 == '')) {
							//$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
						} else {
							for (var i = 0; i < data2.length; i++) {
								$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
							}
						}

						var id_turma = $("#turm").val();

						$.ajax({
							url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
							success: function(response) {
								jQuery('#section_selector_holder').html(response);
							}
						});
					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});
			}
		}
	});

	// Change Núcleo
	$("#nuc").change(function() {
		var id_nucleo = $("#nuc").val();

		if (id_nucleo) {
			var tipo_escola = $("#tipo_escola").val();
			// Busca escolas
			$.ajax('<?php echo site_url('admin/get_escola_nucleo_tipo/'); ?>', {
				type: 'POST', // http method
				data: {
					dados: id_nucleo,
					tipo: tipo_escola
				}, // data to submit
				success: function(data2, status, xhr) {

					$("#esc").val("");
					$("#esc").empty();
					$("#esc").select2("val", "");

					data2 = $.parseJSON(data2);

					if ((data2 == null) | (data2 == '')) {
						$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
					} else {
						for (var i = 0; i < data2.length; i++) {
							$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
						}
					}
				},
				error: function(jqXhr, textStatus, errorMessage) {
					alert(errorMessage);
				}
			});


			// Busca local de execucao
			$("#local_execucao").val("");
			$("#local_execucao").empty();
			$("#local_execucao").select2("val", "");

			$.ajax('<?php echo site_url('admin/get_local_execucao_nucleo_multiple/'); ?>', {
				type: 'POST', // http method
				data: {
					nucleos: id_nucleo
				}, // data to submit
				success: function(data, status, xhr) {
					data = $.parseJSON(data);
					//	console.log(data);
					for (var i = 0; i < data.length; i++) {
						//	console.log(data[i]['local']);
						$("#local_execucao").append('<option value=' + data[i]['id'] + '>' + data[i]['local'] + '</option>');
					}
				}
			});

			// Busca instrutores
			$("#instrutores").val("");
			$("#instrutores").empty();
			$("#instrutores").select2("val", "");

			$.ajax('<?php echo site_url('admin/get_teacher_nucleo_multiple/'); ?>', {
				type: 'POST', // http method
				data: {
					nucleos: id_nucleo
				}, // data to submit
				success: function(data, status, xhr) {
					data = $.parseJSON(data);
					teste = data;
					for (var i = 0; i < data.length; i++) {
						$("#instrutores").append('<option value=' + data[i]['id_instrutor'] + '>' + data[i]['nome_instrutor'] + '</option>');
					}
				}
			});

			// Busca turmas
			var valor = $('#modalidade_id').val();

			if ((valor != null) | (valor != " ")) {

				$("#turm").val("");
				$("#turm").empty();
				$("#turm").select2("val", "");

				for (var j = 0; j < id_nucleo.length; j++) {
					for (var i = 0; i < valor.length; i++) {

						var id_nuc = id_nucleo[j];
						var id_modalidade = valor[i];

						$.ajax('<?php echo site_url('admin/get_turma_nucleo_modalidade/'); ?>', {
							type: 'POST', // http method
							data: {
								dados: id_nuc,
								id_modalidade: id_modalidade
							}, // data to submit
							success: function(data2, status, xhr) {

								data2 = $.parseJSON(data2);

								if ((data2 == null) | (data2 == '')) {
									//$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
								} else {
									for (var i = 0; i < data2.length; i++) {
										$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
									}
								}

								var id_turma = $("#turm").val();

								$.ajax({
									url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
									success: function(response) {
										jQuery('#section_selector_holder').html(response);
									}
								});
							},
							error: function(jqXhr, textStatus, errorMessage) {
								alert(errorMessage);
							}
						});
					}
				}
			}

		}

	});

	//Change Instrutor
	$("#instrutores").change(function() {
		var instrutores = $("#instrutores").val();

		// Busca instrutores
		$("#turm").val("");
		$("#turm").empty();
		$("#turm").select2("val", "");

		$.ajax('<?php echo site_url('admin/get_turma_teacher_multiple/'); ?>', {
			type: 'POST', // http method
			data: {
				instrutores: instrutores
			}, // data to submit
			success: function(data, status, xhr) {
				data = $.parseJSON(data);
				for (var i = 0; i < data.length; i++) {
					$("#turm").append('<option value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
				}
			}
		});
	});

	$("#ano").change(function() {
		var id_projeto = $("#pro").val();
		var ano = $("#ano").val();

		if ((id_projeto != null) & (id_projeto != "")) {
			//$('#submit').removeAttr('disabled');
			$('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_projeto_print_view/?projeto='); ?>' + id_projeto + '&ano=' + ano);
		}
	});

	var class_selection = "";
	jQuery(document).ready(function($) {
		//$('#submit').attr('disabled', 'disabled');
	});

	function select_section(class_id) {
		if (class_id !== '') {
			$.ajax({
				url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
				success: function(response) {
					jQuery('#section_holder').html(response);
				}
			});
		}
	}

	function check_validation() {
		if (class_selection !== '') {
			//$('#submit').removeAttr('disabled')
		} else {
			//$('#submit').attr('disabled', 'disabled');
		}
	}

	$('#class_selection').change(function() {
		class_selection = $('#class_selection').val();
		check_validation();
	});


	$('#class_selection').change(function() {
		var id_class = $("#class_selection").val();
		select_section(id_class);
	});

	// Change Tipo Escola
	$('#tipo_escola').change(function() {

		var id_nucleo = $("#nuc").val();
		var tipo_escola = $("#tipo_escola").val();

		$.ajax('<?php echo site_url('admin/get_escola_nucleo_tipo/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo,
				tipo: tipo_escola
			}, // data to submit
			success: function(data2, status, xhr) {

				$("#esc").val("");
				$("#esc").empty();
				$("#esc").select2("val", "");
				data2 = $.parseJSON(data2);

				if ((data2 == null) | (data2 == '')) {
					$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
				} else {
					for (var i = 0; i < data2.length; i++) {
						$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
					}
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});
	});


	//Select2
	$(document).ready(function() {
		$('#nuc').select2();
	});
	$(document).ready(function() {
		$('#turm').select2();
	});
	$(document).ready(function() {
		$('#modalidade_id').select2();
	});
	$(document).ready(function() {
		$('#esc').select2();
	});
	$(document).ready(function() {
		$('#instrutores').select2();
	});
	$(document).ready(function() {
		$('#local_execucao').select2();
	});
</script>