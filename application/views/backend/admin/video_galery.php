<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>



<div class="row galeria">
        <div class="col-md-6 video">
            <h3 style="margin:20px 0px;">
                <i class="entypo-folder"></i>
                Video Teste 1
            </h3>
            <video  controls>
                <source src="https://www.w3schools.com/html/mov_bbb.mp4">
                Your browser does not support HTML video.
            </video>
        </div>
        <div class="col-md-6 video">
            <h3 style="margin:20px 0px;">
                <i class="entypo-folder"></i>
                Video Teste 2
            </h3>
            <video  controls>
                <source src="https://www.w3schools.com/html/mov_bbb.mp4">
                Your browser does not support HTML video.
            </video>
        </div>
        <div class="col-md-6 video">
            <h3 style="margin:20px 0px;">
                <i class="entypo-folder"></i>
                Video Teste 3
            </h3>
            <video  controls>
                <source src="https://www.w3schools.com/html/mov_bbb.mp4">
                Your browser does not support HTML video.
            </video>
        </div>
        <div class="col-md-6 video">
            <h3 style="margin:20px 0px;">
                <i class="entypo-folder"></i>
                Video Teste 4
            </h3>
            <video  controls>
                <source src="https://www.w3schools.com/html/mov_bbb.mp4">
                Your browser does not support HTML video.
            </video>
        </div> 
</div>
</div>
<style>
.galeria{
    display: block;
    margin: 0 auto;
    width: auto;
    background-color: #ffffff;
    text-align: center;
}
.video{
    margin-bottom: 10px;
    padding: 20px;
}
.video video{
    width: 100%;
}
.video h3{
    text-align: left;
}
</style>

<script type="text/javascript">
    $( "#running_year" ).change(function() {  
        var running_year = $("#running_year").val();
        var id = $("#id_turma").val();
        var conteudo_tabela = document.getElementById('tabela_alunos');
        conteudo_tabela.innerHTML = "";

        $.ajax('<?php echo site_url('admin/get_tabela_alunos_ano/'); ?>', {
            type: 'POST',  // http method
            data: { 
                running_year: running_year,
                id: id,                
            },  // data to submit
            success: function (response) {
                var tabela = response;
                var str = tabela.replace('"', '');
                var string = str.replace(/\\n/g, "")
                                .replace(/\\'/g, "\\'")
                                .replace(/\\"/g, '\\"')
                                .replace(/\\&/g, "\\&")
                                .replace(/\\r/g, "")
                                .replace(/\\/g, "")
                                .replace(/\\t/g, "\\t")
                                .replace(/\\b/g, "\\b")
                                .replace(/\\f/g, "\\f");
                conteudo_tabela.innerHTML = string  ;
            }
        });
    });

    jQuery(document).ready(function($) {
        $('.datatable').DataTable({
            "scrollX": true,
            "oLanguage": {
                "sProcessing": "Aguarde enquanto os dados são carregados ...",
                "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
                "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                "sInfoFiltered": "",
                "sSearch": "Procurar",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },


            dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
            buttons: [

                {
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'csv',
                    text: 'CSV'
                }
            ],
        });
    });
</script>