
            <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_nucleo_add/');?>');"
                class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
                Adicionar novo núcleo

                </a>
                <br><br>
               <table class="table table-bordered" id="nucleos">
                    <thead>
                        <tr>
                            <th width="60"><div>Id núcleo</div></th>
                            <th><div>Nome</div></th>
                            <th><div>Nome do projeto</div></th>
                            <th><div>Meta de alunos</div></th>
                            <th><div>Patrocinador</div></th>
                            <!--<th><div>Manifestação esportiva</div></th>
                            <th><div>Proponente</div></th>
                            <th><div>Metas</div></th>
                            <th><div>Objetivo</div></th>-->
                              <th><div>Opções</div></th>
                        </tr>
                    </thead>
                </table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#nucleos').DataTable({
            "processing": true,
            "scrollX": true,
            "serverSide": true,
            "ajax":{
                "url": "<?php echo site_url('admin/get_nucleos') ?>",
                "dataType": "json",
                "type": "POST",
            },
            "columns": [
                { "data": "id" },
                { "data": "nome_nucleo" },
                { "data": "nome_projeto" },
                 { "data": "meta_alunos" },
                 { "data": "patrocinador" },
                { "data": "options",
                    "orderable": false 
                },
            ],

            
                  dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
  buttons: [

  { extend: 'copy', text: 'Copiar' },
  { extend: 'excel', text: 'Excel' },
  { extend: 'pdf', text: 'PDF' },
  { extend: 'csv', text: 'CSV' }
  ],

             "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 }                              
        });
    });

    function nucleo_edit_modal(nucleo_id) {
        showAjaxModal('<?php echo site_url('modal/popup/modal_nucleo_edit/');?>' + nucleo_id);
    }

    function nucleo_delete_confirm(nucleo_id) {
        confirm_modal('<?php echo site_url('admin/nucleo/delete/');?>' + nucleo_id);
    }

</script>

<style type="text/css">
    
textarea.form-control {
    min-height: 100px !important;
}

</style>
