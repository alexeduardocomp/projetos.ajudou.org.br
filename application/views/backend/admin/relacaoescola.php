
            <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_relacaoescola_add/');?>');"
                class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
                Relacionar escola ao núcleo

                </a>
                <br><br>
               <table class="table table-bordered" id="relacaoes">
                    <thead>
                        <tr>
                            <th width="60"><div>Id Relação</div></th>
                            <th><div>Escola</div></th>
                            <th><div>Núcleo</div></th>
                            <th><div>Projeto</div></th>

                              <th><div>Opções</div></th>
                        </tr>
                    </thead>
                </table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#relacaoes').DataTable({
            "processing": true,
            "scrollX": true,
            "serverSide": true,
            "ajax":{
                "url": "<?php echo site_url('admin/get_relacao_escola') ?>",
                "dataType": "json",
                "type": "POST",
            },
            "columns": [
                { "data": "id_relacao" },
                { "data": "nome_escola" },
                 { "data": "nome_nucleo" },
                  { "data": "nome_projeto" },
                { "data": "options",
                    "orderable": false },
            ],

            
                  dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
  buttons: [

  { extend: 'copy', text: 'Copiar' },
  { extend: 'excel', text: 'Excel' },
  { extend: 'pdf', text: 'PDF' },
  { extend: 'csv', text: 'CSV' }
  ],

             "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 }                              
        });
    });

   

    function relacaoescola_delete_confirm(relacaoescola_id) {
        confirm_modal('<?php echo site_url('admin/relacaoescola/delete/');?>' + relacaoescola_id);
    }

</script>

<style type="text/css">
    
textarea.form-control {
    min-height: 100px !important;
}

</style>
