<?php 
$edit_data		=	$this->db->get_where('class_routine' , array('class_routine_id' => $param2) )->result_array();
?>
<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">
        <?php foreach($edit_data as $row):?>
        <?php echo form_open(site_url('admin/class_routine/do_update/'.$row['class_routine_id'])  , array('class' => 'form-horizontal validatable','target'=>'_top'));?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-5">
                        <select id="class_id" name="class_id" class="form-control selectboxit" onchange="section_subject_select(this.value , <?php echo $param2;?>)">
                            <?php 
                            $classes = $this->db->get('class')->result_array();
                            foreach($classes as $row2):
                            ?>
                                <option value="<?php echo $row2['class_id'];?>" <?php if($row['class_id']==$row2['class_id'])echo 'selected';?>>
                                    <?php echo $row2['name'];?></option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div id="section_subject_edit_holder"></div>
                <!--<div class="form-group">
                    <label class="col-sm-3 control-label">Dia</label>
                    <div class="col-sm-5">
                        <select name="day" class="form-control selectboxit">
                            <option value="saturday" 	<?php if($row['day']=='saturday')echo 'selected="selected"';?>>Sábado</option>
                            <option value="sunday" 		<?php if($row['day']=='sunday')echo 'selected="selected"';?>>Domingo</option>
                            <option value="monday" 		<?php if($row['day']=='monday')echo 'selected="selected"';?>>Segunda</option>
                            <option value="tuesday" 	<?php if($row['day']=='tuesday')echo 'selected="selected"';?>>Terça</option>
                            <option value="wednesday" 	<?php if($row['day']=='wednesday')echo 'selected="selected"';?>>Quarta</option>
                            <option value="thursday" 	<?php if($row['day']=='thursday')echo 'selected="selected"';?>>Quinta</option>
                            <option value="friday" 		<?php if($row['day']=='friday')echo 'selected="selected"';?>>Sexta</option>
                        </select>
                    </div>
                </div>-->

                               <div class="form-group">
    <label class="col-sm-3 control-label">Atividade</label>
    <div class="col-sm-5">
        <select name="subject_id" class="form-control selectboxit" style="width:100%;" required="" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"> 
          
        <?php
        $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            //$page_data['class_id']   = $param1;
        $subjects  = $this->db->get_where('subject' , array('year' => $running_year))->result_array();
        //$subjects = $this->db->get('subject')->result_array();
            //$subjects = $this->db->get_where('subject' , array('class_id' => $class_id))->result_array();
             foreach($subjects as $row2):
                            ?>
                                <option value="<?php echo $row2['subject_id'];?>" <?php if($row['subject_id']==$row2['subject_id'])echo 'selected';?>>
                                    <?php echo $row2['name'];?></option>
                            <?php
                            endforeach;
?>
        </select>
    </div>
</div>


<div class="form-group">
 <label class="col-sm-3 control-label">Data da atividade</label>
    <div class="col-sm-5">
        <?php
             $data_at = str_replace("/", "-",  $row['data_atividade']);
              $data_certa = date('d-m-Y', strtotime($data_at));

         ?>

<input type="text" id="data" class="form-control datepicker" name="data" data-format="dd-mm-yyyy"
                        value="<?php echo $data_certa;?>" placeholder="Selecione uma data" required="" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"/>
    </div>
    </div>
                <!--<div class="form-group">
                    <label class="col-sm-3 control-label">Hora de começo</label>
                    <div class="col-sm-9">
                        <?php 
                            if($row['time_start'] < 13)
                            {
                                $time_start		=	$row['time_start'];
                                $time_start_min =   $row['time_start_min'];
                                $starting_ampm	=	1;
                            }
                            else if($row['time_start'] > 12)
                            {
                                $time_start		=	$row['time_start'] - 12;
                                $time_start_min =   $row['time_start_min'];
                                $starting_ampm	=	2;
                            }
                            
                        ?>
                        <div class="col-md-3">
                            <select name="time_start" class="form-control" required>
                            <option value="">Hora</option>
                                <?php for($i = 0; $i <= 12 ; $i++):?>
                                    <option value="<?php echo $i;?>" <?php if($i ==$time_start)echo 'selected="selected"';?>>
                                        <?php echo $i;?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="time_start_min" class="form-control" required>
                            <option value="">Minutos</option>
                                <?php for($i = 0; $i <= 11 ; $i++):?>
                                    <option value="<?php echo $i * 5;?>" <?php if (($i * 5) == $time_start_min) echo 'selected';?>><?php echo $i * 5;?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="starting_ampm" class="form-control selectboxit">
                                <option value="1" <?php if($starting_ampm	==	'1')echo 'selected="selected"';?>>am</option>
                                <option value="2" <?php if($starting_ampm	==	'2')echo 'selected="selected"';?>>pm</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hora do término</label>
                    <div class="col-sm-9">
                        
                        
                        <?php 
                            if($row['time_end'] < 13)
                            {
                                $time_end		=	$row['time_end'];
                                $time_end_min   =   $row['time_end_min'];
                                $ending_ampm	=	1;
                            }
                            else if($row['time_end'] > 12)
                            {
                                $time_end		=	$row['time_end'] - 12;
                                $time_end_min   =   $row['time_end_min'];
                                $ending_ampm	=	2;
                            }
                            
                        ?>
                        <div class="col-md-3">
                            <select name="time_end" class="form-control" required>
                            <option value="">Hora</option>
                                <?php for($i = 0; $i <= 12 ; $i++):?>
                                    <option value="<?php echo $i;?>" <?php if($i ==$time_end)echo 'selected="selected"';?>>
                                        <?php echo $i;?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="time_end_min" class="form-control" required>
                            <option value="">Minutos</option>
                                <?php for($i = 0; $i <= 11 ; $i++):?>
                                    <option value="<?php echo $i * 5;?>" <?php if (($i * 5) == $time_end_min) echo 'selected';?>><?php echo $i * 5;?></option>
                                <?php endfor;?>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <select name="ending_ampm" class="form-control selectboxit">
                                <option value="1" <?php if($ending_ampm	==	'1')echo 'selected="selected"';?>>am</option>
                                <option value="2" <?php if($ending_ampm	==	'2')echo 'selected="selected"';?>>pm</option>
                            </select>
                        </div>
                    </div>
                </div>-->
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-5">
                      <button id="add_class_routine" type="submit" class="btn btn-info">Atualizar rotina da turma</button>
                  </div>
                </div>
        </form>
        <?php endforeach;?>
    </div>
</div>

<script type="text/javascript">
    function section_subject_select(class_id , class_routine_id) {
        $.ajax({
            url: '<?php echo site_url('admin/section_subject_edit/');?>' + class_id + '/' + class_routine_id ,
            success: function(response)
            {
                jQuery('#section_subject_edit_holder').html(response);
            }
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var class_id = $('#class_id').val();
        var class_routine_id = '<?php echo $param2;?>';
        section_subject_select(class_id,class_routine_id);
        
    }); 


    
     $('#data').change(function(){


        var data = $('#data').val();
        //data2 = String(data);
        //var date = new Date(data2);
        //alert(data2);

        split = data.split('-');
novadata = split[1] + "/" +split[0]+"/"+split[2];
data_input = new Date(novadata);


        const atual = new Date();
        atual.setHours(0);
        atual.setMinutes(0);
        atual.setMilliseconds(0);
        atual.setSeconds(0);
          

          var ano = data_input.getFullYear();


        

        /*if (data_input.getTime() === atual.getTime()) {
        
      $('#add_class_routine').removeAttr('disabled');
}
else if (data_input.getTime() > atual.getTime()) {

    // Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
    //alert(data_input.toString() + ' maior que ' + atual.toString());
   $('#add_class_routine').removeAttr('disabled');

}
else {

 $('#add_class_routine').attr('disabled', 'disabled');
      
    //alert(data_input.toString() + ' menor que ' + atual.toString());
}
*/






    });
</script>

