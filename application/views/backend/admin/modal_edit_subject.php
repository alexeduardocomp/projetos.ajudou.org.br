<?php
$edit_data		=	$this->db->get_where('subject' , array('subject_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row"> 
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					Editar Atividade
            	</div>
            </div>
			<div class="panel-body">
                <?php echo form_open(site_url('admin/subject/do_update/'.$row['subject_id']) , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>" required/>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-5 controls">
                        <select name="class_id" class="form-control">
                            <?php
                            $classes = $this->db->get('class')->result_array();
                            foreach($classes as $row2):
                            ?>
                                <option value="<?php echo $row2['class_id'];?>"
                                    <?php if($row['class_id'] == $row2['class_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>-->
                <div class="form-group">
                    <label class="col-sm-3 control-label">Instrutor</label>
                    <div class="col-sm-5 controls">
                        <select name="teacher_id" class="form-control">
                            <?php
                            $teachers = $this->db->get('teacher')->result_array();
                            foreach($teachers as $row2):
                            ?>
                                <option value="<?php echo $row2['teacher_id'];?>"
                                    <?php if($row['teacher_id'] == $row2['teacher_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>

                 <div class="form-group">
                    <label for="field-2" class="col-sm-3 control-label">Descrição</label>
                    <?php 
                        $descricao_edit = stripcslashes(nl2br($row['descricao']));
                    ?>
                    <div class="col-sm-12">
                        <textarea name="descricao" id="text-editor" class="form-control text-editor"><?php echo $descricao_edit;?> </textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info">Editar Atividade</button>
                    </div>
                 </div>
        		</form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>
<link rel="stylesheet" href="<?php echo base_url('assets/simditor/styles/simditor.css');?>">

<!-- Bottom Scripts -->
<script src="<?php echo base_url('assets/simditor/scripts/mobilecheck.js');?>"></script>
<script src="<?php echo base_url('assets/simditor/scripts/module.js');?>"></script>
<script src="<?php echo base_url('assets/simditor/scripts/uploader.js');?>"></script>
<script src="<?php echo base_url('assets/simditor/scripts/hotkeys.js');?>"></script>
<script src="<?php echo base_url('assets/simditor/scripts/simditor.js');?>"></script>

<script>
    (function () {
        $(function () {
            var $preview, editor, mobileToolbar, toolbar, allowedTags;
            Simditor.locale = 'en-US';
            toolbar = ['title', 'bold','italic','underline','|','ol','ul','blockquote','table','link','|','image','hr','indent','outdent','alignment'];
            mobileToolbar = ["bold", "italic", "underline", "ul", "ol"];
            if (mobilecheck()) {
                toolbar = mobileToolbar;
            }
            allowedTags = ['br', 'span', 'a', 'img', 'b', 'strong', 'i', 'strike', 'u', 'font', 'p', 'ul', 'ol', 'li', 'blockquote', 'pre',  'h2', 'h3', 'h4', 'hr', 'table'];
            editor = new Simditor({
                textarea: $('#text-editor') ,
                placeholder: '',
                toolbar: toolbar,
                pasteImage: false,
                toolbarFloat: false,
                defaultImage: "<?php echo base_url('assets/simditor/images/image.png'); ?>",
                upload: false,
                allowedTags: allowedTags
            });
            $preview = $('#preview');
            if ($preview.length > 0) {
                return editor.on('valuechanged', function (e) {
                    return $preview.html(editor.getValue());
                });
            }
            editor.setValue("<?php echo $descricao_edit;?> ");
            console.log(editor);
            //$('.text-editor').val('Hello world');

        });
    }).call(this);
</script>

<script type="text/javascript">
    // ajax form plugin calls at each modal loading,
    $(document).ready(function() { 

        // SelectBoxIt Dropdown replacement
        if($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function(i, el)
            {
                var $this = $(el),
                    opts = {
                        showFirstOption: attrDefault($this, 'first-option', true),
                        'native': attrDefault($this, 'native', false),
                        defaultText: attrDefault($this, 'text', ''),
                    };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }
    });

</script>
