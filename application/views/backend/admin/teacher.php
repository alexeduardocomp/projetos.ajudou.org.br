

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>


            <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_teacher_add/');?>');"
            	class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
            	Adicionar novo instrutor
               
                </a>

                <br><br>
               <table class="table table-bordered datatable" id="teachers">
                    <thead> 
                        <tr>
                            <th width="60"><div><?php echo get_phrase('id');?></div></th>
                            <th width="80"><div>Foto</div></th>
                            <th><div>Nome</div></th>
                            <th><div><?php echo get_phrase('email').'/ Nome de usuário';?></div></th>
                            <th><div>Telefone</div></th>
                              <!--<th><div>CPF</div></th>
                              <th><div>RG</div></th>-->
                              <th><div>Cargo</div></th>
                              <th><div>Telefone 2</div></th>
                              <!--<th><div>Estado</div></th>
                              <th><div>Cidade</div></th>
                              <th><div>CEP</div></th>
                              <th><div>Rua</div></th>
                              <th><div>Bairro</div></th>
                              <th><div>Número</div></th>
                              <th><div>Complemento</div></th>-->
                              <th><div>Carga horária</div></th>
                              <th><div>Data contratação</div></th>
                              <!--<th><div>Conta bancaria</div></th>
                              <th><div>Salário bruto</div></th>
                              <th><div>Salário líquido</div></th>
                              <th><div>Combustível</div></th>
                              <th><div>Metas qualitativas</div></th>
                              <th><div>Metas quantitavas</div></th>
                              <th><div>Objetivos</div></th>-->

                            <th><div>Opções</div></th>
                        </tr>
                    </thead>
                </table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

	jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#teachers').DataTable({

            "processing": true,
            "scrollX": true,
            "serverSide": true,
            "ajax":{
                "url": "<?php echo site_url('admin/get_teachers') ?>",
                "dataType": "json",
                "type": "POST",
            },
            "columns": [
                { "data": "teacher_id" },
                { "data": "photo" },
                { "data": "name" },
                { "data": "email" },
                { "data": "phone" },
                //{ "data": "CPF" },
                //{ "data": "RG" },
                { "data": "cargo" },
                { "data": "telefone_2" },
                //{ "data": "estado" },
                //{ "data": "cidade" },
                //{ "data": "CEP" },
                //{ "data": "rua" },
               // { "data": "bairro" },
                //{ "data": "numero" },
               // { "data": "complemento" },
                { "data": "carga_horaria" },
                { "data": "data_contratacao" },
                //{ "data": "conta_bancaria" },
                //{ "data": "salario_bruto" },
                //{ "data": "salario_liquido" },
                //{ "data": "combustivel" },
               // { "data": "metas_qualitativas" },
                //{ "data": "metas_quantitativas" },
                //{ "data": "objetivos" },
                { "data": "options" },
            ],
            "columnDefs": [
                {
                    "targets": [1,9],
                    "orderable": false
                },
            ],

             "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 },

 
                  dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
  buttons: [

  { extend: 'copy', text: 'Copiar' },
  { extend: 'excel', text: 'Excel' },
  { extend: 'pdf', text: 'PDF' },
  { extend: 'csv', text: 'CSV' }
  ],                              
        });
	});

    function teacher_edit_modal(teacher_id) {
        showAjaxModal('<?php echo site_url('modal/popup/modal_teacher_edit/');?>' + teacher_id);
    }

    function teacher_delete_confirm(teacher_id) {
        confirm_modal('<?php echo site_url('admin/teacher/delete/');?>' + teacher_id);
    }

</script>
<style type="text/css">
    
textarea.form-control {
    min-height: 100px;
}



</style>
