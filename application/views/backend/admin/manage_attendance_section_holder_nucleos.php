<div class="col-md-3">
	<div class="form-group">
	<label class="control-label" style="margin-bottom: 5px;"><?php echo get_phrase('section');?></label>
		<select name="section_id2" id="section_id2" class="form-control selectboxit">
			<?php 
				$sections = $this->db->get_where('section' , array(
					'id' => $nucleo_id 
				))->result_array();
				foreach($sections as $row):
			?>
			<option value="<?php echo $row['section_id2'];?>"><?php echo $row['nome_nucleo'];?></option>
			<?php endforeach;?>
		</select>
	</div>
</div>

<script type="text/javascript">
   
    $(document).ready(function () {

        // SelectBoxIt Dropdown replacement
        if ($.isFunction($.fn.selectBoxIt))
        {
            $("select.selectboxit").each(function (i, el)
            {
                var $this = $(el),
                        opts = {
                            showFirstOption: attrDefault($this, 'first-option', true),
                            'native': attrDefault($this, 'native', false),
                            defaultText: attrDefault($this, 'text', ''),
                        };

                $this.addClass('visible');
                $this.selectBoxIt(opts);
            });
        }

    });

</script>