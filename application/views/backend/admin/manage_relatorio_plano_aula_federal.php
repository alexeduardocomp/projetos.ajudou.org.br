<?php
$this->db->where('tipo_projeto', 'FEDERAL');
$query = $this->db
->order_by('projeto.nome', 'ASC')->get('projeto');
$projetos = $query->result();
$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();
?>
<style type="text/css">
    .page-body .selectboxit-container {
        margin-left: 15px;
    }
</style>
<meta charset="utf-8">
<div class="panel panel-primary">
    <div class="panel-heading">
        Plano de Aula da
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                    <div class="col-sm-12">
                        <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <?php
                            $tama = count($projetos);
                            for ($i = 0; $i < $tama; $i++) {
                                echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                    <div class="col-sm-12">
                        <select name="id_nucleo" class="form-control js-example-basic-multiple" id="nuc" multiple="multiple" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled>Selecione um projeto</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="form-group">
                    <label for="field-1" class="col-sm-5 control-label">Local de Execução</label>
                    <div class="col-sm-12">
                        <select id="local_execucao" name="local_execucao" class="form-control js-example-basic-multiple" multiple="multiple" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            
                        </select>
                    </div>
                </div>
            </div>              
            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-12">
                        <select name="class_id" class="form-control js-example-basic-multiple" multiple="multiple" id="turm" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" onchange="return get_class_sections(this.value)">
                            <option  disabled>Selecione um local</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Prestação</label>
                    <div class="col-sm-12">
                        <select name="prestacao" class="form-control" id="prest" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected value='PARCIAL'>PARCIAL</option>
                            <option value='TOTAL'>TOTAL</option>
                        </select>
                    </div>
                </div>
            </div>                
        </div>
        <div class="row">
            <input type="hidden" name="year" value="<?php echo $running_year; ?>">
            <div class="col-md-2" style="margin-top: 25px; margin-left: 30px;">
                <a id="link_relatorio" target="_blank"><button id="submit" class="btn btn-info">Gerar Relatório PDF</button></a>
            </div>

            <div class="col-md-2" style="margin-top: 25px; margin-left: 30px;">
                <a id="link_relatorio_word" target="_blank"><button id="submit" class="btn btn-default">Gerar Relatório Word</button></a>
            </div>
        </div>
    </div> 
</div>

<script type="text/javascript">
    $("#pro").change(function() {

        $("#nuc").val("");
        $("#nuc").empty();
        $("#nuc").select2("val", "");

        $("#local_execucao").val("");
        $("#local_execucao").empty();
        $("#local_execucao").select2("val", "");

        $("#turm").val("");
        $("#turm").empty();
        $("#turm").select2("val", "");

        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);

                for (var i = 0; i < data.length; i++) {
                    $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                }
            }
        });
    });

    $("#prest").change(function() {

        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                $("#nuc").empty();
                data = $.parseJSON(data);

                for (var i = 0; i < data.length; i++) {

                    $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                }

                var id_nucleo = $("#nuc").val();

            }
        });
    });

    $("#turm").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();


        var prestacao = $("#prest").val();


        $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);
        $('#link_relatorio_word').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view_word/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);
        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);

        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });

    $("#local_execucao").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var locais = $("#local_execucao").val();

        $("#turm").val("");
        $("#turm").empty();
        $("#turm").select2("val", "");

        $.ajax('<?php echo site_url('admin/get_turmas_local_multiple/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto,
                id_nucleo: id_nucleo,
                locais: locais
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);
                for (var i = 0; i < data.length; i++) {
                    $("#turm").append('<option value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
                }
            }
        });
    });

    
    $("#nuc").change(function() {

        var nucleos = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();
        console.log(nucleos);
        var clients = $('#clients_select').val();
        var groups = $('#groups_select').val();

        $("#local_execucao").val("");
        $("#local_execucao").empty();
        $("#local_execucao").select2("val", "");

        $("#turm").val("");
        $("#turm").empty();
        $("#turm").select2("val", "");

        $.ajax('<?php echo site_url('admin/get_local_execucao_nucleo_multiple/'); ?>', {
            type: 'POST', // http method
            data: {
                nucleos: nucleos
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);
                //	console.log(data);
                for (var i = 0; i < data.length; i++) {
                    //	console.log(data[i]['local']);
                    $("#local_execucao").append('<option value=' + data[i]['id'] + '>' + data[i]['local'] + '</option>');
                }

                var id_turma = $("#turm").val();
                var prestacao = $("#prest").val();

                $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + nucleos + '&turma=' + id_turma + '&prestacao=' + prestacao);
                $('#link_relatorio_word').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view_word/?projeto='); ?>' + id_projeto + '&nucleo=' + nucleos + '&turma=' + id_turma + '&prestacao=' + prestacao);
                $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + nucleos + '&turma=' + id_turma + '&prestacao=' + prestacao);

                if ((id_turma != null) & (id_projeto != null) & (nucleos != null)) {

                    $('#submit').removeAttr('disabled');
                }
            }
        });
    });



    var class_selection = "";
    jQuery(document).ready(function($) {
        $('#submit').attr('disabled', 'disabled');
    });

    function select_section(class_id) {
        if (class_id !== '') {
            $.ajax({
                url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
                success: function(response) {
                    jQuery('#section_holder').html(response);
                }
            });
        }
    }

    function check_validation() {
        if (class_selection !== '') {
            $('#submit').removeAttr('disabled')
        } else {
            $('#submit').attr('disabled', 'disabled');
        }
    }

    $('#class_selection').change(function() {
        class_selection = $('#class_selection').val();
        check_validation();
    });

    $('#class_selection').change(function() {
        var id_class = $("#class_selection").val();
        select_section(id_class);
    });

    $(document).ready(function() {
        $('#nuc').select2();
        $('#turm').select2();
        $('#local_execucao').select2();
    });
</script>