<link href="https://cdn.datatables.net/responsive/2.1.0/css/responsive.dataTables.min.css" rel="stylesheet" media="screen">
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js
"></script>
<script src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script>

<a href="<?php echo site_url('admin/class_routine_add'); ?>" class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    Adicionar plano de aula
</a>

<div class="form-group" style="padding-bottom: 10px;">

    <div class="col-sm-3">
        <input type="text" id="data" class="form-control datepicker" name="data" data-format="dd-mm-yyyy" value="" placeholder="Selecione uma data inicial" required="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" />
    </div>
    <div class="col-sm-3">
        <input type="text" id="data_fim" class="form-control datepicker" name="data_fim" data-format="dd-mm-yyyy" value="" placeholder="Selecione uma data final" required="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" />
    </div>
</div>
<br><br><br>

<?php
/*$query = $this->db->get_where('section' , array('class_id' => $class_id));
	if($query->num_rows() > 0):
		$sections = $query->result_array();
	foreach($sections as $row):*/
?>
<div class="row">

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Turma - <?php echo $this->db->get_where('class', array('class_id' => $class_id))->row()->name; ?>
                    <?php //echo date('Y-m-d');
                    $data_class_routine = $this->db->get_where('class_routine', array('class_id' => $class_id))->row()->name;
                    $dat = date('Y-m-d');
                    $diasemana = array('DOMINGO', 'SEGUNDA', 'TERÇA', 'QUARTA', 'QUINTA', 'SEXTA', 'SÁBADO');
                    $dia_semana_numero = date('w', strtotime($dat));
                    $dia_correto = $diasemana[$dia_semana_numero];
                    ?>
                </h3>
            </div>
            <div class="panel-body">
                <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_export">
                <thead>
                        <tr>
                            <th>
                                <div>Plano de Aula</div>
                            </th>
                        </tr>
                    </thead>    
                <tbody>

                        <tr class="gradeA">
                            <!-- <td width="100" style="vertical-align: middle; text-align: center;"><span id="dia_semana"><?php echo $dia_correto;
                                                                                                                            //aqui eu posso pegar a data da atividade e verificar qual dia da semana que é e lançar

                                                                                                                            //strtoupper($day);
                                                                                                                            ?></span></td> -->
                            <td>
                                <span id="resultado_consulta">
                                    <?php
                                    $this->db->order_by("time_start", "asc");
                                    //$this->db->where('day' , $day);
                                    $this->db->where('class_id', $class_id);
                                    $this->db->where('data_atividade', date('Y-m-d'));
                                    //$this->db->where('section_id' , $row['section_id']);
                                    $this->db->where('year', $running_year);
                                    $routines   =   $this->db->get('class_routine')->result_array();

                                    if ($routines == null) {
                                        echo '<b>Não existem Planos de Aula lançados para esta data!</b>';
                                    }

                                    foreach ($routines as $row2) :
                                    ?>
                                        <div class="btn-group">
                                            <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                <?php echo $this->crud_model->get_subject_name_by_id($row2['subject_id']); ?>
                                                <?php
                                                if ($row2['time_start_min'] == 0 && $row2['time_end_min'] == 0)
                                                    echo '(' . $row2['time_start'] . '-' . $row2['time_end'] . ')';
                                                if ($row2['time_start_min'] != 0 || $row2['time_end_min'] != 0)
                                                    echo '(' . $row2['time_start'] . ':' . $row2['time_start_min'] . '-' . $row2['time_end'] . ':' . $row2['time_end_min'] . ')';
                                                ?>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_edit_class_routine/' . $row2['class_routine_id']); ?>');">
                                                        <i class="entypo-pencil"></i>
                                                        Editar
                                                    </a>
                                                </li>

                                                <li>
                                                    <a href="#" onclick="confirm_modal('<?php echo site_url('admin/class_routine/delete/' . $row2['class_routine_id']); ?>');">
                                                        <i class="entypo-trash"></i>
                                                        Deletar
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    <?php endforeach; ?>
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <center>
                    <a id='link_imprimir' href="<?php echo site_url('admin/class_routine_print_view/' . $class_id . '/'); ?>" class="btn btn-primary btn-md pull-right" target="_blank">
                        <i class="entypo-print"></i> Imprimir
                    </a>
                </center>
            </div>
        </div>
    </div>
</div>
<?php //endforeach;
?>
<?php //endif;
?>
<style>
    .dataTables_filter {
        display: none;
    }
    .dataTables_length {
        display: none;
    }
    #table_export_wrapper{
        margin-top: 0px !important;
    }
</style>
<script type="text/javascript">
    var date_pri = new Date();

    var dia2 = date_pri.getDate();
    var mes2 = date_pri.getMonth();
    mes2 = mes2 + 1;
    if (mes2 < 10) {
        mes2 = "0" + mes2;
    }
    var anocomp = date_pri.getFullYear();

    // alert(dia2);
    // alert(mes2);
    // alert(anocomp);
    var dt_comp = anocomp + '-' + mes2 + '-' + dia2;

    $('#link_imprimir').attr('href', '<?php echo site_url('admin/class_routine_print_view/' . $class_id . '/?data='); ?>' + dt_comp);

    var data = new Date();

    var dia = data.getDate(); // 1-31
    var mes = data.getMonth(); // 0-11 (zero=janeiro)
    var ano2 = data.getYear(); // 2 dígitos
    var ano4 = data.getFullYear();
    mes = mes + 1;
    if (mes < 10) {
        mes = "0" + mes;
    }
    var datatotal = dia + "-" + mes + "-" + ano4;

    $('#data').val(datatotal);

    $('#data').change(function(event) {
        //chamar função ajax passando o class_id e a data_atividade para atualizar a tabela com os dados referentes do dia requerido.

        var data_atividade_final = $("#data_fim").val();

        if (data_atividade_final == '') {

        } else {
            var data_atividade = $("#data").val();

            if (data_atividade != '') {
                split = data_atividade.split('-');
                novadata = split[1] + "/" + split[0] + "/" + split[2];
                data_input = new Date(novadata);

                split2 = data_atividade.split('-');
                novadata2 = split2[2] + "-" + split2[1] + "-" + split2[0];

                split3 = data_atividade_final.split('-');
                novadata3 = split3[2] + "-" + split3[1] + "-" + split3[0];

                var lk = document.getElementById("link_imprimir").href;

                //alert('valor do link:' + lk);
                //document.getElementById('link_imprimir').setAttribute('href', novadata2);

                $('#link_imprimir').attr('href', '<?php echo site_url('admin/class_routine_print_view/' . $class_id . '/?dataini='); ?>' + novadata2 + '&datafim=' + novadata3);

                var semana = ["DOMINGO", "SEGUNDA", "TERÇA", "QUARTA", "QUINTA", "SEXTA", "SÁBADO"];
                var d = new Date(novadata);

                //document.getElementById("dia_semana").innerHTML = semana[d.getDay()];

                var data_atividade_fim = $("#data_fim").val();
                var class_id = <?php echo $class_id; ?>;

                $.ajax('<?php echo site_url('admin/get_class_routine_data_periodo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        'data_atividade': data_atividade,
                        'class_id': class_id,
                        'data_atividade_fim': data_atividade_fim
                    }, // data to submit
                    success: function(data, status, xhr) {
                        //alert(data);
                        // alert('sucesso');

                        //
                        if ((data == '') | (data == null)) {
                            $('#resultado_consulta').html('');
                            $("#resultado_consulta").append("<b>Não existem Planos de Aula lançados para esta data!</b>");
                           /*  $('#dia_semana').html('');
                            document.getElementById("dia_semana").innerHTML = ''; */
                        } else {
                            $('#resultado_consulta').html('');
                            $("#resultado_consulta").append(data);

                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert("erro");
                    }
                });
            } else {
                $('#resultado_consulta').html('');
                $("#resultado_consulta").append("<b>Não existem Planos de Aula lançados para esta data!</b>");
                /* document.getElementById("dia_semana").innerHTML = 'DIA NÃO ENCONTRADO'; */
            }
        }
    });

    $('#data_fim').change(function() {
        var data_atividade = $("#data").val();

        if (data_atividade == '') {
            alert('Por favor, insira uma data inicial primeiro.');
        } else {
            var data_atividade_fim = $("#data_fim").val();

            split2 = data_atividade.split('-');
            novadata2 = split2[2] + "-" + split2[1] + "-" + split2[0];

            split3 = data_atividade_fim.split('-');
            novadata3 = split3[2] + "-" + split3[1] + "-" + split3[0];

            $('#link_imprimir').attr('href', '<?php echo site_url('admin/class_routine_print_view/' . $class_id . '/?dataini='); ?>' + novadata2 + '&datafim=' + novadata3);

            var class_id = <?php echo $class_id; ?>;

            $.ajax('<?php echo site_url('admin/get_class_routine_data_periodo/'); ?>', {
                type: 'POST', // http method
                data: {
                    'data_atividade': data_atividade,
                    'class_id': class_id,
                    'data_atividade_fim': data_atividade_fim
                }, // data to submit
                success: function(data, status, xhr) {
                    //alert(data);
                    // alert('sucesso');

                    //
                    if ((data == '') | (data == null)) {
                        $('#resultado_consulta').html('');
                        $("#resultado_consulta").append("<b>Não existem Planos de Aula lançados para esta data!</b>");
                       /*  $('#dia_semana').html('');
                        document.getElementById("dia_semana").innerHTML = ''; */
                    } else {
                        $('#resultado_consulta').html('');
                        $("#resultado_consulta").append(data);

                    }
                },
                error: function(jqXhr, textStatus, errorMessage) {
                    alert("erro");
                }
            });
        }
    });
</script> 
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable({
                
    "scrollX": true,
				"oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
    
 }  

		});
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>