<?php 
	$query = $this->db->get('projeto');
	$projetos = $query->result();

		$query2 = $this->db->get('nucleo');
	$nucleos = $query2->result();


		$query3 = $this->db->get('patrocinador');
	$patrocinadores = $query3->result();
	$url = base_url();


?>




<script type="text/javascript" src="<?php echo $url;?>/assets/js/jquery.mask.min.js"></script>

<script type="text/javascript">
	

	 $(document).ready(function () { 
        $(".cpf").mask('000.000.000-00');
        $('.rg').mask('AA.000.000-A');
        $('.tel').mask('(00) 00000-0000');
        $('.cep').mask('00000-000');
        $('.numeracao').mask('00.00');
    });

</script>



<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title">
            		<i class="entypo-plus-circled"></i>
					Relacionar patrocinador com projeto
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(site_url('admin/relacaoprojeto/create/') , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                    
					 

					

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Projeto</label>
                                  <div class="col-sm-5">
									<select name="id_projeto" class="form-control" id="pro"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

										<option selected disabled>Selecione</option>
									<?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>
  									  
									</select>
								</div>

								</div>


						



								<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Patrocinador</label>
                                  <div class="col-sm-5">
									<select name="id_patrocinador[]" class="form-control" id="patro"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" multiple>

										<option selected disabled>Selecione um projeto</option>
									<?php

									// $tama = count($patrocinadores);
									// 		for ($i=0; $i < $tama ; $i++) { 
						
									// 			echo '<option value='.$patrocinadores[$i]->id.'>'.$patrocinadores[$i]->nome.'</option>';

									// 		}



									 ?>
  									  
									</select>
									<div style="margin-top:10px;">Segure a tecla Ctrl e selecione mais de uma ação!</div>
								</div>

								</div>




                    
                    
                    <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Adicionar Relação</button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">




$('#pro').change(function(){

var id_projeto = $('#pro').val();

//alert(id_projeto);



     $.ajax('<?php echo site_url('admin/get_patrocinador_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {
            	//alert(data);
                $("#patro").empty();
                data = $.parseJSON(data);


              

                for (var i = 0; i < data.length; i++) {

                    $("#patro").append('<option value=' + data[i]['id_patrocinador'] + '>' + data[i]['nome_patrocinador'] + '</option>');
                }

            

              
            }
        });




});






</script>


