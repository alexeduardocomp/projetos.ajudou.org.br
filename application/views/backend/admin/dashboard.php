<?php
$account_type =    $this->session->userdata('login_type');
$query = $this->db->order_by("projeto.nome","ASC")->get('projeto');
$projects = $query->result();
$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();
$query3 = $this->db->get('class');
$turmas = $query3->result();
$query4 = $this->db->get('student');
$alunos = $query4->result();
$query5 = $this->db->get('parent');
$responsaveis = $query5->result();
?>

<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>

<style type="text/css">
    .linha01 {
        display: flex;
        align-items: center;
        background: #fff;
        padding: 20px;
        border-radius: 10px;
        transition: 0.5s;
        box-shadow: 0px 5px 10px rgba(0, 0, 0, .1);
    }

    .linha {
        margin-top: 25px;
        background: #fff;
        padding: 20px;
        border-radius: 10px;
        transition: 0.5s;
        box-shadow: 0px 5px 10px rgba(0, 0, 0, .1);
    }

    .linha div {
        padding: 8px 0px;
    }

    .linha:hover {
        transform: translateY(-6px);
        transition: 0.5s;
    }

    .linha01:hover {
        transform: translateY(-6px);
        transition: 0.5s;
    }

    .container {
        width: 100%;
        margin: 0 auto;
        padding: 0;
    }

    .accordion-item {
        background: #fff;
        border-radius: .4rem;
        margin-bottom: 1rem;
        padding: 1rem;
        box-shadow: 0px 5px 10rem rgba(0, 0, 0, .1);
        transition: 0.5s;
    }

    .accordion-link {
        font-size: 1.6rem;
        color: rgba(0, 0, 0, .8);
        text-decoration: none;
        width: 100%;
        display: flex;
        align-items: center;
        justify-content: space-between;
        padding: 1rem;
        background: #fff;
        outline: none;
    }

    .accordion-link i {
        padding: .5rem;
    }

    ion-icon[name='remove-outline'] {
        display: none;
    }

    .answer {
        max-height: 0;
        overflow: hidden;
        position: relative;
        background: #f2f3f8;
        transition: max-height 0.8s;
    }

    .answer::before {
        content: '';
        position: absolute;
        width: .6rem;
        height: 100%;
        background: #e3051b;
        opacity: 0.8;
        top: 50%;
        left: 0;
        transform: translateY(-50%);
    }

    .answer p {
        font-size: 1.1rem;
        padding: 1.2rem 2.5rem;
        margin: 0;
    }

    .accordion-item:hover {
        transform: translateY(-5px);
        transition: 0.5s;
        cursor: pointer;
    }

    .positivo {
        transition: 1s;
    }

    .negativo {
        transition: 1s;
    }

    div#nome_projeto {
        margin-left: 80px;
        font-size: 16px;
        text-transform: uppercase;

    }
</style>

<div class="row">
    <div class="col-md-8">
        <div class="row">
            <!-- CALENDAR-->
            <div class="col-md-12 col-xs-12">
                <!--       <div class="panel panel-primary " data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <i class="fa fa-calendar"></i>
                            Agenda
                        </div>
                    </div>
                    <div class="panel-body" style="padding:0px;">
                        <div class="calendar-env">
                            <div class="calendar-body">
                                <div id="notice_calendar"></div>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div id="dados_projeto">
                    <div class="linha01">
                        <div id="img_projeto"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="80"></div>
                        <div id="nome_projeto"></div>
                    </div>
                    <div class="linha" style="display: none;">
                        <div id="período_projeto"><b>Período: </b></div>
                        <div id="proponente"><b>Proponente: </b></div>
                        <div id="metas_objetivos" style="text-align: justify; padding-right: 20px;"><b>Metas e Objetivos: </b></div>
                    </div>
                </div>

                <div class="container" style="margin-top:25px;">
                    <div class="accordion">
                        <div class="accordion-item" id="patrocinadores_div">
                            <a class="accordion-link">
                                Patrocinadores
                                <ion-icon class="positivo" name="add-outline"></ion-icon>
                                <ion-icon class="negativo" name="remove-outline"></ion-icon>
                            </a>
                            <div class="answer">
                                <div class="col-md-12 col-xs-12" id="texto_patrocinador">

                                </div>
                            </div>
                        </div>

                        <div class="accordion-item" id="nucleos_div">
                            <a class="accordion-link">
                                Núcleos
                                <ion-icon class="positivo" name="add-outline"></ion-icon>
                                <ion-icon class="negativo" name="remove-outline"></ion-icon>

                            </a>
                            <div class="answer" id="texto_nucleo">

                            </div>
                        </div>
                        <div class="accordion-item" id="locais_div">
                            <a class="accordion-link">
                                Locais de Execução
                                <ion-icon class="positivo" name="add-outline"></ion-icon>
                                <ion-icon class="negativo" name="remove-outline"></ion-icon>
                            </a>
                            <div class="answer" id="texto_local">

                            </div>
                        </div>
                        <div class="accordion-item" id="instrutores_div">
                            <a class="accordion-link">
                                Instrutores
                                <ion-icon class="positivo" name="add-outline"></ion-icon>
                                <ion-icon class="negativo" name="remove-outline"></ion-icon>
                            </a>
                            <div class="answer" id="texto_instrutor">

                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="bloco_video">
                    <h3 style="margin: 20px 0px;">
                        <i class="entypo-right-circled"></i>
                        Tutorial de cadastros
                    </h3>
                    <iframe class="tutorial_yt" width="550" src="https://www.youtube.com/embed/rdgYg35BY-8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function() {
            $("#patrocinadores_div2").hide();
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            var url = "<?php echo base_url(); ?>index.php/admin/get_todos";
            $.ajaxSetup({
                async: false
            });
            $.get(url, function(data) {
                //alert(data);
                const json = data;
                const obj = JSON.parse(json);
                var vetor_instrutor = obj.vetor_instrutores;
                // var teste = JSON.stringify(vetor_instrutor);
                // alert(teste);

                for (var i = 0; i < vetor_instrutor.length; i++) {
                    cont = i + 1;
                    $("#texto_instrutor").append('<p><b>' + cont + ' - </b>' + vetor_instrutor[i]['nome_instrutor'] + '</p>');
                }

                var vetor_local = obj.vetor_local;

                for (var i = 0; i < vetor_local.length; i++) {
                    cont2 = i + 1;
                    $("#texto_local").append('<p><b>' + cont2 + ' - </b>' + vetor_local[i]['local'] + '</p>');
                }

                var vetor_nucleo = obj.vetor_nucleos;

                for (var i = 0; i < vetor_nucleo.length; i++) {
                    cont3 = i + 1;
                    $("#texto_nucleo").append('<p><b>' + cont3 + ' - </b>' + vetor_nucleo[i]['nome_nucleo'] + '</p>');
                }

                var vetor_patrocinadores = obj.vetor_patrocinadores;

                for (var i = 0; i < vetor_patrocinadores.length; i++) {
                    cont4 = i + 1;
                    $("#texto_patrocinador").append('<div class="col-md-2 col-xs-2"><img style="margin-top:1rem;" src="<?php echo base_url(); ?>uploads/patrocinador_image/' + vetor_patrocinadores[i]['id'] + '.jpg" width="60" height="60" ><br><b>' + cont4 + ' - </b>' + vetor_patrocinadores[i]['nome'] + ' </div>');
                }

            });
            $.ajaxSetup({
                async: true
            });

            $('.accordion-item').click(function(event) {
                var id_item = this.id;
                //$( "#"+id_item+" .answer" ).css( "border", "13px solid red" );
                var heig = $("#" + id_item + " .answer").css("max-height");
                //alert(heig);
                if (heig == '0px') {
                    $("#" + id_item + " .answer").css("max-height", "200rem");
                    $("#" + id_item + " .positivo").css("display", "none");
                    $("#" + id_item + " .negativo").css("display", "block");
                } else {
                    $("#" + id_item + " .answer").css("max-height", "0");
                    $("#" + id_item + " .positivo").css("display", "block");
                    $("#" + id_item + " .negativo").css("display", "none");
                }

            });

            // vou ter que criar
            $("#project_filter").change(function() {
                var valor = $('#project_filter').val();
                $('#project').val(valor);
                $('#nucleos').empty();
                $('#cidades').empty();
                $('#alunos').empty();
                var project = $('#project').val();

                if (valor == "geral") {
                    $('#img_projeto').html("<img src='<?php echo base_url(); ?>uploads/logos/logo_ajudou.png' width='80'>");
                    $("#nome_projeto").empty();
                    $('.linha').css('display', 'none');

                    $("#texto_instrutor").empty();
                    $("#texto_local").empty();
                    $("#texto_nucleo").empty();
                    $("#texto_patrocinador").empty();

                    $("#projetos_div").show();
                    $("#patrocinadores_div2").hide();
                    $("#faltas_div").show();
                    // window.location.href = window.location.href;
                    $('#projetos').html(<?php $cont = 0;
                                        foreach ($projects as $project) {
                                            $inicio = explode('-', $project->data_inicio);
                                            $ano = explode('-', $running_year);

                                            if ($inicio[0] == $ano[0]) {
                                                $cont++;
                                            }
                                        }
                                        echo $cont; ?>);
                    $('#nucleos').html(<?php $cont3 = 0;
                                        foreach ($projects as $project) {
                                            $inicio = explode('-', $project->data_inicio);
                                            $ano = explode('-', $running_year);
                                            if ($inicio[0] == $ano[0]) {
                                                foreach ($nucleos as $nuc) {
                                                    if ($nuc->id_projeto == $project->id) {
                                                        $cont3++;
                                                    }
                                                }
                                            }
                                        }
                                        echo $cont3; ?>);
                    $('#turmas').html(<?php
                                        $cont2 = 0;
                                        foreach ($projects as $project) {
                                            $inicio = explode('-', $project->data_inicio);
                                            $ano = explode('-', $running_year);

                                            if ($inicio[0] == $ano[0]) {
                                                foreach ($nucleos as $nuc) {
                                                    if ($nuc->id_projeto == $project->id) {
                                                        foreach ($turmas as $tur) {
                                                            if ($tur->id_nucleo == $nuc->id) {
                                                                $cont2++;
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        echo $cont2; ?>);
                    //$('#faltas').html(<?php echo $present_today; ?>);
                    $('#cidades').html(<?php echo $this->db->count_all('cidade'); ?>);
                    $('#alunos').html(<?php $number_of_student_in_current_session = $this->db->get_where('enroll', array('year' => $running_year))->num_rows();
                                        echo $number_of_student_in_current_session; ?>);
                    //$('#responsaveis').html(<?php echo $this->db->count_all('parent') ?>);
                    $('#instrutores').html(<?php echo $this->db->count_all('teacher') ?>);

                    var url = "<?php echo base_url(); ?>index.php/admin/get_todos";
                    $.ajaxSetup({
                        async: false
                    });
                    $.get(url, function(data) {
                        //alert(data);
                        const json = data;
                        const obj = JSON.parse(json);
                        var vetor_instrutor = obj.vetor_instrutores;

                        // var teste = JSON.stringify(vetor_instrutor);
                        // alert(teste);

                        for (var i = 0; i < vetor_instrutor.length; i++) {
                            cont = i + 1;
                            $("#texto_instrutor").append('<p><b>' + cont + ' - </b>' + vetor_instrutor[i]['nome_instrutor'] + '</p>');
                        }

                        var vetor_local = obj.vetor_local;

                        for (var i = 0; i < vetor_local.length; i++) {
                            cont2 = i + 1;
                            $("#texto_local").append('<p><b>' + cont2 + ' - </b>' + vetor_local[i]['local'] + '</p>');
                        }

                        var vetor_nucleo = obj.vetor_nucleos;

                        for (var i = 0; i < vetor_nucleo.length; i++) {
                            cont3 = i + 1;
                            $("#texto_nucleo").append('<p><b>' + cont3 + ' - </b>' + vetor_nucleo[i]['nome_nucleo'] + '</p>');
                        }

                        var vetor_patrocinadores = obj.vetor_patrocinadores;

                        for (var i = 0; i < vetor_patrocinadores.length; i++) {
                            cont4 = i + 1;
                            $("#texto_patrocinador").append('<div class="col-md-2 col-xs-2"><img style="margin-top:1rem;" src="<?php echo base_url(); ?>uploads/patrocinador_image/' + vetor_patrocinadores[i]['id'] + '.jpg" width="60" height="60" ><br><b>' + cont4 + ' - </b>' + vetor_patrocinadores[i]['nome'] + ' </div>');
                        }
                    });
                    $.ajaxSetup({
                        async: true
                    });
                } else {
                    $('.linha').css('display', 'block');

                    //=========================================
                    var id_projeto = valor;

                    $.ajax('<?php echo site_url('admin/get_projeto_dashboard/'); ?>', {
                        type: 'POST', // http method
                        data: {
                            dados: id_projeto
                        }, // data to submit
                        success: function(data2, status, xhr) {
                            $("#nome_projeto").empty();
                            $("#período_projeto").empty();
                            $("#proponente").empty();
                            $("#metas_objetivos").empty();

                            data2 = $.parseJSON(data2);

                            //alert(data2[0]['nome_projeto']);

                            $('#nome_projeto').append(data2[0]['nome_projeto']);
                            $('#período_projeto').append('<b>Período: </b>' + data2[0]['data_inicio'] + ' até ' + data2[0]['data_final']);
                            $('#proponente').append('<b>Proponete: </b>' + data2[0]['proponente']);
                            $('#metas_objetivos').append('<b>Metas e Objetivos: </b>' + data2[0]['metas']);

                            var tipo = data2[0]['tipo_projeto'];
                            if (tipo == "ESTADUAL") {
                                $('#img_projeto').html("<img src='<?php echo base_url(); ?>uploads/logos/estadual.jpg' width='80'>");
                            } else if (tipo == "FEDERAL") {
                                $('#img_projeto').html("<img src='<?php echo base_url(); ?>uploads/logos/federal.jpg' width='80'>");
                            } else {
                                $('#img_projeto').html("");
                            }

                            //       for (var i = 0; i < data2.length; i++) {

                            //    $("#ins").append('<option value='+data2[i]['id_instrutor']+'>'+data2[i]['nome']+'</option>');
                            // }
                        },
                        error: function(jqXhr, textStatus, errorMessage) {
                            alert(errorMessage);
                        }
                    });

                    //=========================================
                    var url = "<?php echo base_url(); ?>index.php/admin/get_users_json?project=" + project + "";
                    $.ajaxSetup({
                        async: false
                    });
                    $.get(url, function(data) {
                        //alert(data);
                        const json = data;
                        const obj = JSON.parse(json);
                        $('#nucleos').html(obj.nucleos);
                        $('#turmas').html(obj.turmas);
                        $('#cidades').html(obj.cidades);
                        $('#alunos').html(obj.alunos);
                        $('#responsaveis').html(obj.responsaveis);
                        $('#instrutores').html(obj.instrutores);
                        $('#patrocinadores').html(obj.patrocinadores);

                        $("#texto_instrutor").empty();
                        $("#texto_local").empty();
                        $("#texto_nucleo").empty();
                        $("#texto_patrocinador").empty();

                        var vetor_instrutor = obj.vetor_instrutores;

                        // var teste = JSON.stringify(vetor_instrutor);
                        // alert(teste);

                        for (var i = 0; i < vetor_instrutor.length; i++) {
                            cont = i + 1;
                            $("#texto_instrutor").append('<p><b>' + cont + ' - </b>' + vetor_instrutor[i]['nome_instrutor'] + '</p>');
                        }

                        var vetor_local = obj.vetor_local;

                        for (var i = 0; i < vetor_local.length; i++) {
                            cont2 = i + 1;
                            $("#texto_local").append('<p><b>' + cont2 + ' - </b>' + vetor_local[i]['local'] + '</p>');
                        }

                        var vetor_nucleo = obj.vetor_nucleos;

                        for (var i = 0; i < vetor_nucleo.length; i++) {
                            cont3 = i + 1;
                            $("#texto_nucleo").append('<p><b>' + cont3 + ' - </b>' + vetor_nucleo[i]['nome_nucleo'] + '</p>');
                        }

                        var vetor_patrocinadores = obj.vetor_patrocinadores;


                        for (var i = 0; i < vetor_patrocinadores.length; i++) {
                            $("#texto_patrocinador").append('<div class="col-md-2 col-xs-2"><img style="margin-top:1rem;" src="<?php echo base_url(); ?>uploads/patrocinador_image/' + vetor_patrocinadores[i]['id'] + '.jpg" width="60" height="60" ><br><b>' + (i + 1) + ' - </b>' + vetor_patrocinadores[i]['nome'] + ' </div>');
                        }

                        if (valor != "geral") {
                            $("#projetos_div").hide();
                            $("#patrocinadores_div2").show();
                            $("#faltas_div").hide();
                        }
                    });
                    $.ajaxSetup({
                        async: true
                    });
                }
            });
        });
        $(document).ready(function() {
            $('#link').on('change', function() {
                var url = $(this).val();
                if (url) {
                    window.open(url, '_blank');
                }
                return false;
            });
        });
    </script>

    <div class="col-md-4">
        <div class="row">
            <div class="col-md-12" style="padding-right: 5px;padding-left: 5px;">
                <div class="form-group">
                    <label for="projects">Filtrar por Projeto</label>
                    <select name="project_filter" id="project_filter" style="margin-bottom: 20px" class="form-control">
                        <option value="geral">Geral</option>
                        <?php
                        foreach ($projects as $project) {
                            $inicio = explode('-', $project->data_inicio);
                            $ano = explode('-', $running_year);

                            if ($inicio[0] == $ano[0]) {
                        ?>
                                <option value="<?php echo $project->id; ?>"><?php echo $project->nome; ?></option>
                        <?php }
                        }
                        ?>
                    </select>
                </div>
                <input name="project" id="project" type="hidden"></input>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 teste" id="projetos_div" style="padding-right: 5px;padding-left: 5px;">
                <div class="tile-stats tile-green">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div id="projetos" class="num" data-start="0" data-end="<?php
                                                                            $cont = 0;
                                                                            foreach ($projects as $project) {
                                                                                $inicio = explode('-', $project->data_inicio);
                                                                                $ano = explode('-', $running_year);

                                                                                if ($inicio[0] == $ano[0]) {
                                                                                    $cont++;
                                                                                }
                                                                            }
                                                                            echo $cont;
                                                                            ?>
                    " data-postfix="" data-duration="800" data-delay="0">0</div>

                    <h3 class="negri">Projetos</h3>
                    <p>Total de projetos </p></br>
                </div>
            </div>

            <div class="col-md-6" id="patrocinadores_div2" style="padding-right: 5px;padding-left: 5px;">
                <div class="tile-stats tile-green">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div id="patrocinadores" class="num" data-start="0" data-end="" data-postfix="" data-duration="800" data-delay="0">0</div>

                    <h3 class="negri">Patrocinadores</h3>
                    <p>Total de patrocinadores</p>
                </div>
            </div>

            <div class="col-md-6" onclick="toggle2()" id="modal-nucleo" style="padding-right: 5px;padding-left: 5px;">
                <div class="tile-stats tile-blue">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div id="nucleos" class="num" data-start="0" data-end="<?php
                                                                            $cont3 = 0;
                                                                            foreach ($projects as $project) {
                                                                                $inicio = explode('-', $project->data_inicio);
                                                                                $ano = explode('-', $running_year);

                                                                                if ($inicio[0] == $ano[0]) {
                                                                                    foreach ($nucleos as $nuc) {
                                                                                        if ($nuc->id_projeto == $project->id) {
                                                                                            $cont3++;
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            echo $cont3;
                                                                            ?>
                    " data-postfix="" data-duration="800" data-delay="0">0</div>
                    <h3 class="negri">Núcleos</h3>
                    <p>Total de núcleos</p>
                    </br>
                </div>
                <!--                          <div id="popup2">
    <h2>Lista de Locais de Execução:</h2>
  
  <?php
    $cont = 1;
    $this->db->order_by('id_nucleo');
    $locais = $this->db->get('local_execucao')->result_array();
    $idnucleo = '';

    foreach ($locais as $row) : ?>
                    <p><?php
                        $idnucleo_inicial = $row['id_nucleo'];
                        if ($idnucleo_inicial != $idnucleo) {
                            $nome_nucleo = $this->db->get_where('nucleo', array('id' => $idnucleo_inicial))->row()->nome_nucleo;
                            echo '<h4 style="margin-top:20px;">' . $nome_nucleo . '</h4>';
                            $idnucleo = $idnucleo_inicial;
                            $cont = 1;
                        }
                        echo $cont . ' - ' . $row['local']; ?>
                                 </p>
                                 <?php
                                    $cont++;
                                endforeach;
                                    ?>
<div style="text-align: center;">
<div id="link" onclick="fechar2()">Fechar</div>   
</div>
</div> -->
            </div>

            <div class="col-md-6" style="padding-right: 5px;padding-left: 5px;">
                <div class="tile-stats tile-red">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div id="turmas" class="num" data-start="0" data-end="<?php
                                                                            $cont2 = 0;
                                                                            foreach ($projects as $project) {
                                                                                $inicio = explode('-', $project->data_inicio);
                                                                                $ano = explode('-', $running_year);

                                                                                if ($inicio[0] == $ano[0]) {
                                                                                    foreach ($nucleos as $nuc) {
                                                                                        if ($nuc->id_projeto == $project->id) {
                                                                                            foreach ($turmas as $tur) {
                                                                                                if ($tur->id_nucleo == $nuc->id) {
                                                                                                    $cont2++;
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            echo $cont2;
                                                                            ?>" data-postfix="" data-duration="800" data-delay="0">0</div>
                    <h3 class="negri">Turmas</h3>
                    <p>Total de turmas</p>
                    </br>
                </div>
            </div>

            <div class="col-md-6" style="padding-right: 5px;padding-left: 5px;">
                <div class="tile-stats tile-aqua">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div id="cidades" class="num" data-start="0" data-end="<?php echo $this->db->count_all('cidade'); ?>" data-postfix="" data-duration="800" data-delay="0">0</div>
                    <h3 class="negri">Cidades</h3>
                    <p>Total de cidades</p>
                    </br>
                </div>
            </div>

            <div class="col-md-6" style="padding-right: 5px;padding-left: 5px;">
                <div class="tile-stats tile-orange">
                    <div class="icon"><i class="fa fa-group"></i></div>
                    <div id="alunos" class="num" data-start="0" data-end="
													<?php
                                                    $number_of_student_in_current_session = $this->db->get_where('enroll', array('year' => $running_year))->num_rows();
                                                    echo $number_of_student_in_current_session;
                                                    //echo $this->db->count_all('student');
                                                    ?>
													" data-postfix="" data-duration="1500" data-delay="0">0</div>
                    <h3 class="negri">Alunos</h3>
                    <p>Total de alunos</p>
                    </br>
                </div>
            </div>



            <div class="col-md-6" onclick="toggle()" id="modal-instrutor" style="padding-right: 5px;padding-left: 5px;">
                <div class="tile-stats tile-brown">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div id="instrutores" class="num" data-start="0" data-end="<?php //echo $this->db->count_all('teacher');
                                                                                $retorno = $this->ajaxload->numero_instrutores();
                                                                                echo $retorno;
                                                                                ?>" data-postfix="" data-duration="800" data-delay="0">0</div>
                    <h3 class="negri">Instrutores</h3>
                    <p>Total de instrutores</p></br>
                </div>

            </div>
        </div>
    </div>
</div>

<style type="text/css">
    .tutorial_yt{
        width: 100%;
        height: 315px;
        border-radius: 5px;
    }
    .bloco_video{
        
    }
    #modal-instrutor {
        /*cursor: pointer;*/
    }

    #modal-nucleo {
        /*cursor: pointer;*/
    }

    #popup h2 {
        font-weight: 600px;
        margin-bottom: 10px;
        color: #333;
    }

    #popup2 h2 {
        font-weight: 600px;
        margin-bottom: 10px;
        color: #333;
    }

    #link {
        position: relative;
        padding: 10px 35px;
        font-size: 14px;
        display: inline-block;
        margin-top: 20px;
        text-decoration: none;
        color: #fff;
        background: #111;
    }

    #popup {
        position: fixed;
        top: 30%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 600px;
        padding: 35px 50px;
        box-shadow: 0 5px 30px rgba(0, 0, 0, .30);
        background: #fff;
        visibility: hidden;
        opacity: 0;
        transition: 0.5s;
    }

    #popup.active {
        visibility: visible !important;
        opacity: 1 !important;
        transition: 0.5s;
        top: 50%;
    }

    #popup2 {
        position: fixed;
        top: 30%;
        left: 50%;
        transform: translate(-50%, -50%);
        width: 600px;
        padding: 35px 50px;
        box-shadow: 0 5px 30px rgba(0, 0, 0, .30);
        background: #fff;
        visibility: hidden;
        opacity: 0;
        transition: 0.5s;
    }

    #popup2.active {
        visibility: visible !important;
        opacity: 1 !important;
        transition: 0.5s;
        top: 50%;
    }
</style>

<script type="text/javascript">
    function fechar() {
        var popup = document.getElementById('popup');
        //popup.style.visibility = 'hidden';
        //popup.style.opacity = '0';
    }
/*
    function toggle() {
        var popup = document.getElementById('popup');
        popup.classList.toggle('active');
        var name = popup.classList.item(0);
        //alert(name);
    }

    function fechar2() {
        var popup = document.getElementById('popup2');
        //popup.style.visibility = 'hidden';
        //popup.style.opacity = '0';
    }

    function toggle2() {
        var popup = document.getElementById('popup2');
        popup.classList.toggle('active');
        var name = popup.classList.item(0);
        //alert(name);
    }
    */
</script>

<style type="text/css">
    .calendar-env .calendar-body .fc-content .fc-view table tbody tr td.fc-day .fc-day-number {
        margin-top: 0px;
        margin-left: 10px;
        text-align: center;
    }

    .fc-day>div {
        display: flex;
        justify-content: center;
        align-items: center;
    }
</style>