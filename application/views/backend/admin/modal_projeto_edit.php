<?php
$query = $this->db->get('patrocinador');
$patrocinadores = $query->result();

$edit_data = $this->db->get_where('projeto', array('id' => $param2))->result_array();
foreach ($edit_data as $row) :
?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						Atualizar projeto
					</div>
				</div>
				<div class="panel-body">
					<?php echo form_open(site_url('admin/projeto/edit/' . $row['id']), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['nome']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Tipo de projeto</label>
						<div class="col-sm-5">
							<select name="tipo_projeto" id="tipo_projeto" class="form-control" value="<?php echo $row['tipo_projeto']; ?>">
								<?php
								if ($row['tipo_projeto'] == "ESTADUAL") {
									echo "<option value='ESTADUAL' selected>ESTADUAL</option>
									  <option value='FEDERAL'>FEDERAL</option>
									  <option value='CONVÊNIO'>CONVÊNIO</option>";
								} elseif ($row['tipo_projeto'] == "FEDERAL") {
									echo "<option value='ESTADUAL' >ESTADUAL</option>
									  <option value='FEDERAL' selected>FEDERAL</option>
									  <option value='CONVÊNIO'>CONVÊNIO</option>";
								} elseif ($row['tipo_projeto'] == "CONVÊNIO") {
									echo "<option value='ESTADUAL' >ESTADUAL</option>
									  <option value='FEDERAL' >FEDERAL</option>
									  <option value='CONVÊNIO' selected>CONVÊNIO</option>";
								}
								?>
							</select>
						</div>

						<div class="col-sm-2">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" id="imagem_logo" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<?php
									if ($row['tipo_projeto'] == "FEDERAL") {
										echo "<img src='" . base_url() . "uploads/logos/federal.jpg'>";
									} elseif ($row['tipo_projeto'] == "ESTADUAL") {
										echo "<img src='" . base_url() . "uploads/logos/estadual.jpg'>";
									}
									?>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="numero" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['numero']; ?>">
						</div>
					</div>


					<!-- <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Patrocinador</label>
						<div class="col-sm-5">
							<select name="id_patrocinador" class="form-control">


								<?php
								$tama = count($patrocinadores);
								for ($i = 0; $i < $tama; $i++) {

									if ($row['patrocinador'] == $patrocinadores[$i]->id) {
										echo '<option selected value=' . $patrocinadores[$i]->id . '>' . $patrocinadores[$i]->nome . '</option>';
									} else {
										echo '<option value=' . $patrocinadores[$i]->id . '>' . $patrocinadores[$i]->nome . '</option>';
									}
								}
								?>

							</select>
						</div>

					</div> -->

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Manifestação esportiva</label>
						<div class="col-sm-5">
							<select name="manifestacao_esportiva" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['manifestacao_esportiva']; ?>">
								<?php
								if ($row['manifestacao_esportiva'] == "EDUCACIONAL") {
									echo "<option value='EDUCACIONAL' selected>EDUCACIONAL</option>
									  <option value='RENDIMENTO'>RENDIMENTO</option>
									  <option value='PARTICIPAÇÃO'>PARTICIPAÇÃO</option>";
								}
								if ($row['manifestacao_esportiva'] == "RENDIMENTO") {
									echo "<option value='RENDIMENTO' selected>RENDIMENTO</option>
									  <option value='EDUCACIONAL'>EDUCACIONAL</option>
									  <option value='PARTICIPAÇÃO'>PARTICIPAÇÃO</option>";
								}
								if ($row['manifestacao_esportiva'] == "PARTICIPAÇÃO") {
									echo "<option value='PARTICIPAÇÃO' selected>PARTICIPAÇÃO</option>
									  <option value='RENDIMENTO'>RENDIMENTO</option>
									  <option value='EDUCACIONAL'>EDUCACIONAL</option>";
								}
								?>
							</select>
						</div>
					</div>
					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Manifestação esportiva</label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="manifestacao_esportiva" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
									
									<?php echo $row['manifestacao_esportiva']; ?>
								</textarea>
							</div>
						</div>
					</div>-->

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Proponente</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="proponente" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['proponente']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Metas</label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="metas" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
									<?php echo $row['metas']; ?>
								</textarea>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Objetivo</label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="objetivo" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">

									<?php echo $row['objetivo']; ?>
								</textarea>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data inicial</label>
						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_inicial" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['data_inicio']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data final</label>
						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_final" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['data_final']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Situação</label>
						<div class="col-sm-5">
							<select name="situacao" class="form-control" value="<?php echo $row['situacao']; ?>">
								<?php
								if ($row['situacao'] == "ATIVO") {
									echo "<option value='ATIVO' selected>ATIVO</option>
									  <option value='INATIVO'>INATIVO</option>";
								} else {
									echo "<option value='ATIVO' >ATIVO</option>
									  <option value='INATIVO' selected>INATIVO</option>";
								}
								?>
							</select>
						</div>
					</div>

					<!--value"<?php echo $row['nome']; ?>"-->

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Atualizar</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>

<script>
	$(document).ready(function() {
		$("#tipo_projeto").change(function() {
			var tipo = $('#tipo_projeto').val();
			if (tipo == "ESTADUAL") {
				$('#imagem_logo').html("<img src='<?php echo base_url(); ?>uploads/logos/estadual.jpg'>");
			} else if (tipo == "FEDERAL") {
				$('#imagem_logo').html("<img src='<?php echo base_url(); ?>uploads/logos/federal.jpg'>");
			} else {
				$('#imagem_logo').html("");
			}
		});
	});
</script>