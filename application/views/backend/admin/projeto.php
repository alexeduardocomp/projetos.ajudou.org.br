<a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_projeto_add/'); ?>');" class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    Adicionar novo projeto

</a>
<br><br>
<table class="table table-bordered" id="projetos">
    <thead>
        <tr>
            <th width="60">
                <div><?php echo get_phrase('projeto_id'); ?></div>
            </th>
            <th>
                <div>Nome</div>
            </th>
            <th>
                <div>Tipo do projeto</div>
            </th>
            <th>
                <div>Número</div>
            </th>
            <th>
                <div>Patrocinador</div>
            </th>
            <!--<th><div>Manifestação esportiva</div></th>
                            <th><div>Proponente</div></th>
                            <th><div>Metas</div></th>
                            <th><div>Objetivo</div></th>-->
            <th>
                <div>Data inicial</div>
            </th>
            <th>
                <div>Data final</div>
            </th>
            <th>
                <div>Situação</div>
            </th>
            <th>
                <div><?php echo get_phrase('options'); ?></div>
            </th>
        </tr>
    </thead>
</table>
 


<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#projetos').DataTable({
            "processing": true,
            "serverSide": true,
            "scrollX": true,
            "ajax": {
                "url": "<?php echo site_url('admin/get_projetos') ?>",
                "dataType": "json",
                "type": "POST",
            },
            
            "columns": [{
                    "data": "id"
                },
                {
                    "data": "nome"
                },
                {
                    "data": "tipo_projeto"
                },
                {
                    "data": "numero"
                },
                {
                    "data": "patrocinador",
                    "orderable": false
                },
                {
                    "data": "data_inicio"
                },
                {
                    "data": "data_final"
                },
                {
                    "data": "situacao"
                },
                {
                    "data": "options",
                    "orderable": false
                },
            ],


            dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
            buttons: [

                {
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'csv',
                    text: 'CSV'
                }
            ],

            "oLanguage": {
                "sProcessing": "Aguarde enquanto os dados são carregados ...",
                "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
                "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                "sInfoFiltered": "",
                "sSearch": "Procurar",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            }
        });
    });

    function projeto_edit_modal(projeto_id) {
        showAjaxModal('<?php echo site_url('modal/popup/modal_projeto_edit/'); ?>' + projeto_id);
    }

    function projeto_delete_confirm(projeto_id) {
        confirm_modal('<?php echo site_url('admin/projeto/delete/'); ?>' + projeto_id);
    }
</script>

<style type="text/css">
    textarea.form-control {
        min-height: 100px !important;
    }
</style>