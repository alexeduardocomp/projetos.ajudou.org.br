<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

 
<a href="<?php echo site_url('admin/video_add'); ?>" class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    Adicionar novo vídeo
</a>
<br><br><br>

<div class="row">
    <div class="col-md-12">

        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#home" data-toggle="tab">
                    <span class="visible-xs"><i class="entypo-users"></i></span>
                    <span class="hidden-xs">Todos os vídeos</span>
                </a>
            </li>
            <?php
            // $query = $this->db->get_where('section' , array('class_id' => $class_id));
            // if ($query->num_rows() > 0):
            //     $sections = $query->result_array();
            //     foreach ($sections as $row):
            ?>
            <!--<li>
                <a href="#<?php echo $row['section_id']; ?>" data-toggle="tab">
                    <span class="visible-xs"><i class="entypo-user"></i></span>
                    <span class="hidden-xs">Seção <?php echo $row['name']; ?> ( <?php echo $row['nick_name']; ?> )</span>
                </a>
            </li>-->
            <?php //endforeach;
            ?>
            <?php //endif;
            ?>
        </ul>

        <div class="tab-content">
            <div class="tab-pane active" id="home">

                <table class="table table-bordered datatable">
                    <thead>
                        <tr>
                            <!--
                            <th width="80">
                                <div>Id</div>
                            </th>
                            -->
                            <th >
                                <div>Título</div>
                            </th>
                            <th class="span3">
                                <div>Link</div>
                            </th>
                            <th>
                                <div>Página</div>
                            </th>
                            <th>
                                <div>Opções</div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $videos   =   $this->db->get_where('video')->result_array();
                        foreach ($videos as $row) : ?>
                            <tr>                                
                                <td>
                                    <?php
                                    echo $row['titulo']
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    echo htmlspecialchars($row['link']);
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    switch ($row['pagina']) {
                                        case 'student_add':
                                            echo "Cadastrar Aluno";
                                            break;
                                        case 'cidade':
                                            echo "Gerenciar Cidade";
                                            break;
                                        case 'escola':
                                            echo "Gerenciar Escolas";
                                            break;
                                        case 'noticeboard':
                                            echo "Gerenciar Eventos";
                                            break;
                                        case 'manage_attendance':
                                            echo "Gerenciar Frequência";
                                            break;
                                        case 'local_execução':
                                            echo "Gerenciar Locais de Execução";
                                            break;
                                        case 'modalidade':
                                            echo "Gerenciar Modalidades";
                                            break;
                                        case 'nucleo':
                                            echo "Gerenciar Núcleo";
                                            break;
                                        case 'patrocinador':
                                            echo "Gerenciar Patrocinadores";
                                            break;
                                        case 'projeto':
                                            echo "Gerenciar Projetos";
                                            break;
                                        case 'class':
                                            echo "Gerenciar Turmas";
                                            break;
                                        case 'admin':
                                            echo "Gerenciar Usuários";
                                            break;
                                    }
                                    ?>
                                </td>

                                
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Ações <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                                            <!-- STUDENT EDITING LINK -->
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_video_edit/' . $row['id_video']) ?>');">
                                                    <i class="entypo-pencil"></i>
                                                    Editar
                                                </a>
                                            </li>
                                            <li class="divider"></li>
                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo site_url('admin/video/delete/' . $row['id_video']); ?>');">
                                                    <i class="entypo-trash"></i>
                                                    Deletar
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            

        </div>


    </div>
</div>


<script type="text/javascript">
    $( "#running_year" ).change(function() {  
        var running_year = $("#running_year").val();
        var id = $("#id_turma").val();
        var conteudo_tabela = document.getElementById('tabela_alunos');
        conteudo_tabela.innerHTML = "";

        $.ajax('<?php echo site_url('admin/get_tabela_alunos_ano/'); ?>', {
            type: 'POST',  // http method
            data: { 
                running_year: running_year,
                id: id,                
            },  // data to submit
            success: function (response) {
                var tabela = response;
                var str = tabela.replace('"', '');
                var string = str.replace(/\\n/g, "")
                                .replace(/\\'/g, "\\'")
                                .replace(/\\"/g, '\\"')
                                .replace(/\\&/g, "\\&")
                                .replace(/\\r/g, "")
                                .replace(/\\/g, "")
                                .replace(/\\t/g, "\\t")
                                .replace(/\\b/g, "\\b")
                                .replace(/\\f/g, "\\f");
                conteudo_tabela.innerHTML = string  ;
            }
        });
    });

    jQuery(document).ready(function($) {
        $('.datatable').DataTable({
            columnDefs: [
            { orderable: false, targets: 3 }
    ],
            "oLanguage": {
                "sProcessing": "Aguarde enquanto os dados são carregados ...",
                "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
                "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                "sInfoFiltered": "",
                "sSearch": "Procurar",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },


            dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
            buttons: [

                
            ],
        });
    });
</script>