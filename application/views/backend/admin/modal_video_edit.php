
<style>
	select[readonly] {
		background: #eee;
		/*Simular campo inativo - Sugestão @GabrielRodrigues*/
		pointer-events: none;
		touch-action: none;
	}
</style>

<?php
$query = $this->db->get('projeto');
$projetos = $query->result();

$query = $this->db->get('nucleo');
$nucleos = $query->result();
?>

<?php
$edit_data		=	$this->db->get_where('video', array(
	'id_video' => $param2
))->result_array();

foreach ($edit_data as $row) :
?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						Editar video
					</div>
				</div>
				<div class="panel-body">

					<?php echo form_open(site_url('admin/video/do_update/' . $row['id_video']), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Título do Vídeo</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="titulo" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['titulo']; ?>" autofocus required>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Para a página</label>
						<div class="col-sm-5">
							<select name="pagina" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" id="problema">
								<option disabled selected>Selecione</option>								
								<option value="student_add" <?php if ($row['pagina'] == "student_add") { echo 'selected';} ?>>Cadastrar Aluno</option>
								<option value="cidade" <?php if ($row['pagina'] == "cidade") { echo 'selected';} ?>>Gerenciar Cidade</option>
								<option value="escola" <?php if ($row['pagina'] == "escola") { echo 'selected';} ?>>Gerenciar Escolas</option>
								<option value="noticeboard" <?php if ($row['pagina'] == "noticeboard") { echo 'selected';} ?>>Gerenciar Eventos</option>
								<option value="manage_attendance <?php if ($row['pagina'] == "manage_attendance") { echo 'selected';} ?>">Gerenciar Frequência</option>
								<option value="local_execução" <?php if ($row['pagina'] == "local_execução") { echo 'selected';} ?>>Gerenciar Locais de Execução </option>
								<option value="modalidade" <?php if ($row['pagina'] == "modalidade") { echo 'selected';} ?>>Gerenciar Modalidades</option>
								<option value="nucleo" <?php if ($row['pagina'] == "nucleo") { echo 'selected';} ?>>Gerenciar Núcleo  </option>
								<option value="patrocinador" <?php if ($row['pagina'] == "patrocinador") { echo 'selected';} ?>>Gerenciar Patrocinadores</option>
								<option value="projeto" <?php if ($row['pagina'] == "projeto") { echo 'selected';} ?>>Gerenciar Projetos </option>
								<option value="class" <?php if ($row['pagina'] == "class") { echo 'selected';} ?>>Gerenciar Turmas</option>
								<option value="admin" <?php if ($row['pagina'] == "admin") { echo 'selected';} ?>>Gerenciar Usuários</option>
							</select>                        
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">
							Link Frame
							<i class="entypo-help-circled tooltip_css"><span><div><img src="<?php echo base_url('uploads/tutorial_video.gif'); ?>" /></div></span></i>
						</label>
						<div class="col-sm-7">
						<textarea style="height: 150px;" class="form-control" rows="5" name="link"  data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" 
							placeholder="<iframe width='560' height='315' src='https://www.youtube.com/embed/123' title='YouTube video player' ></iframe>" > <?php echo htmlspecialchars($row['link']); ?> </textarea>
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info">Atualizar video</button>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>

<?php
endforeach;
?>


<script type="text/javascript">
	$(document).ready(function($){
		$(".cpf").mask('000.000.000-00');
		$('.tel').mask('(00) 00000-0000');
		$('.cep').mask('00000-000');
		$('.numeracao').mask('00.00');
		$('.nt').mask('00');
	});
</script>
<script type="text/javascript">
	

	$("#pro").change(function() {

		var id_projeto = $("#pro").val();

		$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {

				$("#nuc").empty();
				data = $.parseJSON(data);


				for (var i = 0; i < data.length; i++) {

					$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
				}






				var id_nucleo = $("#nuc").val();


				$.ajax('<?php echo site_url('admin/get_escola_nucleo2/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: id_nucleo
					}, // data to submit
					success: function(data2, status, xhr) {

						$("#esc").empty();
						data2 = $.parseJSON(data2);



						if ((data2 == null) | (data2 == '')) {
							$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
						} else {

							for (var i = 0; i < data2.length; i++) {

								$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
							}

						}


					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});



				$.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: id_nucleo
					}, // data to submit
					success: function(data2, status, xhr) {

						$("#turm").empty();
						data2 = $.parseJSON(data2);



						if ((data2 == null) | (data2 == '')) {
							$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
						} else {

							for (var i = 0; i < data2.length; i++) {
								$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');

							}

						}




						var turma = $("#turm").val();

						$.ajax('<?php echo site_url('admin/get_secao_turma2/'); ?>', {
							type: 'POST', // http method
							data: {
								dados: turma
							}, // data to submit
							success: function(data, status, xhr) {



								$("#seca").empty();


								data = $.parseJSON(data);
								if ((data == null) | (data == '')) {
									$("#seca").append('<option value="" selected disabled>Nenhuma sessão encontrada</option>');
								} else {

									for (var i = 0; i < data.length; i++) {

										$("#seca").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_secao'] + '</option>');
									}

								}


							},
							error: function(jqXhr, textStatus, errorMessage) {
								alert("erro");
							}
						});


						var id_turma = $("#turm").val();

						$.ajax({
							url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
							success: function(response) {
								jQuery('#section_selector_holder').html(response);
							}
						});


					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});







			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});


	});





	$("#nuc").change(function() {


		var id_nucleo = $("#nuc").val();


		$.ajax('<?php echo site_url('admin/get_escola_nucleo2/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo
			}, // data to submit
			success: function(data2, status, xhr) {



				$("#esc").empty();
				data2 = $.parseJSON(data2);

				if ((data2 == null) | (data2 == '')) {
					$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
				} else {

					for (var i = 0; i < data2.length; i++) {

						$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
					}
				}

			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});



		$.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo
			}, // data to submit
			success: function(data2, status, xhr) {

				$("#turm").empty();
				data2 = $.parseJSON(data2);



				if ((data2 == null) | (data2 == '')) {
					$("#turm").append('<option value="" selected disabled>Nenhuma cidade encontrada</option>');
				} else {

					for (var i = 0; i < data2.length; i++) {


						$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');

					}

				}


				var turma = $("#turm").val();

				$.ajax('<?php echo site_url('admin/get_secao_turma2/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: turma
					}, // data to submit
					success: function(data, status, xhr) {



						$("#seca").empty();


						data = $.parseJSON(data);

						if ((data == null) | (data == '')) {
							$("#seca").append('<option value="" selected disabled>Nenhuma sessão encontrada</option>');
						} else {


							for (var i = 0; i < data.length; i++) {

								$("#seca").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_secao'] + '</option>');
							}

						}


					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert("erro");
					}
				});


				var id_turma = $("#turm").val();

				$.ajax({
					url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
					success: function(response) {
						jQuery('#section_selector_holder').html(response);
					}
				});


			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});




	});



	// $( "#est" ).change(function() {

	//      var estado = $("#est").val();

	//      $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
	//     type: 'POST',  // http method
	//     data: { dados: estado },  // data to submit
	//     success: function (data, status, xhr) {



	//     		 $("#cid").empty();
	//     	 data = $.parseJSON(data);

	//     	  if((data == null)|(data == '')){
	//     	 	$("#cid").append('<option value="" selected disabled>Nenhuma cidade encontrada</option>');
	//     	 }else{

	//       for (var i = 0; i < data.length; i++) {

	//    $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
	// }

	// }


	//     },
	//     error: function (jqXhr, textStatus, errorMessage) {
	//            alert("erro");
	//     }
	// });

	// });



	$("#turm").change(function() {

		var turma = $("#turm").val();

		$.ajax('<?php echo site_url('admin/get_secao_turma2/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: turma
			}, // data to submit
			success: function(data, status, xhr) {



				$("#seca").empty();


				data = $.parseJSON(data);

				if ((data == null) | (data == '')) {
					$("#seca").append('<option value="" selected disabled>Nenhuma sessão encontrada</option>');
				} else {


					for (var i = 0; i < data.length; i++) {

						$("#seca").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_secao'] + '</option>');
					}

				}


			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});

	});




	$("#problema").change(function() {

		var problema = $("#problema").val();


		if (problema == "SIM") {

			$("#descri").css('display', 'block');
		}

		if (problema == "NÃO") {
			$("#descri").css('display', 'none');
		}

	});



	// $(document).ready(function(){



	//   var estado = $("#est").val();
	//          var cidadeatual = $("#cidadeatual").val();




	//      $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
	//     type: 'POST',  // http method
	//     data: { dados: estado },  // data to submit
	//     success: function (data, status, xhr) {



	//          //$("#cid").empty();
	//        data = $.parseJSON(data);
	//         //alert(data);

	//       for (var i = 0; i < data.length; i++) {

	//         if(data[i]['id'] == cidadeatual){
	// $("#cid").append('<option selected value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
	//         }else{ 

	//    $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
	// }
	// }


	//     },
	//     error: function (jqXhr, textStatus, errorMessage) {
	//            alert("erro");
	//     }
	// });

	// });



	$('#data_nasc').change(function(event) {


		var data = $('#data_nasc').val();

		var data_nova = new Date(data);



		var data_atual = new Date();



		var resultado = data_atual.getFullYear() - data_nova.getFullYear();



		if (resultado <= 2) {
			alert('Data de nascimento inválida!');
			$('#submit').attr('disabled', 'disabled');
		} else {
			$('#submit').removeAttr('disabled');
		}


	});
</script>

<script type="text/javascript">
	jQuery(window).ready(function() {
		var SPMaskBehavior = function(val) {
				return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			},
			spOptions = {
				onKeyPress: function(val, e, field, options) {
					field.mask(SPMaskBehavior.apply({}, arguments), options);
				}
			};
		$('[name="cpf"]').mask('000.000.000-00', {
			reverse: true
		});
		$('[name="cell"]').mask(SPMaskBehavior, spOptions);
		$('[name="cep"]').mask('00000-000');

		$('[name="cep"]').blur(function() {

			//Nova variável "cep" somente com dígitos.
			var cep = $(this).val().replace(/\D/g, '');

			//Verifica se campo cep possui valor informado.
			if (cep != "") {
				//Expressão regular para validar o CEP.
				var validacep = /^[0-9]{8}$/;
				//Valida o formato do CEP.
				if (validacep.test(cep)) {

					//Preenche os campos com "..." enquanto consulta webservice.
					$("#rua").val("Procurando...");
					$("#bairro").val("Procurando...");
					$("#cid").val("Procurando...");
					$("#est").val("Procurando...");
					$("#uf").val("Procurando...");
					$("#estado").val("Procurando...");

					//Consulta o webservice viacep.com.br/
					$.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

						if (!("erro" in dados)) {

							//Atualiza os campos com os valores da consulta.
							$("#rua").val(dados.logradouro);
							$("#bairro").val(dados.bairro);
							$("#cid").val(dados.localidade);
							$("#uf").val(dados.uf);
							$("#est").val(dados.uf);


						} else {
							//CEP pesquisado não foi encontrado.
							limpa_formulario_cep();
							alert("CEP não encontrado.");
						}
					});
				} else {
					//cep é inválido.
					limpa_formulario_cep();
					alert("Formato de CEP inválido.");
				}
			} else {
				//cep sem valor, limpa formulário.
				limpa_formulario_cep();
			}
		});

		function limpa_formulario_cep() {
			// Limpa valores do formulário de cep.
			$("#rua").val("");
			$("#bairro").val("");
			$("#cid").val("");
			$("#est").val("");
			$("#uf").val("");
			$("#cep").val("");
		}
	});
</script>