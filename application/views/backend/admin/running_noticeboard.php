
<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered datatable" id="table_noticeboard">
    <thead>
        <tr>
            <th><div>#</div></th>
            <th><div>Projeto</div></th>
<th><div>Núcleo</div></th> 
<th><div>Título</div></th>
<th><div>Data</div></th>
<!--<th><div><?php echo get_phrase('show_on_website'); ?></div></th>-->
<th><div>Opções</div></th>
</tr>
</thead>
<tbody>
    <?php
    $count = 1;
    $notices = $this->db->get_where('noticeboard', array('status' => 1))->result_array();
    foreach ($notices as $row):
        ?>
        <tr>
            <td><?php echo $count++; ?></td>
            <td><?php



            $projeto = $this->db->get_where('projeto', array('id' => $row['id_projeto']))->row()->nome;


            echo $projeto;



            ?></td>
            <td><?php

            $nucleo = $this->db->get_where('nucleo', array('id' => $row['id_nucleo']))->row()->nome_nucleo;


            echo $nucleo;

             ?></td>
            <td><?php echo $row['notice_title']; ?></td>
            <td><?php echo date('d M,Y', $row['create_timestamp']); ?></td>
            <!--<td align="center">
              <?php if ($row['show_on_website'] == 1) { ?>
                <i class="fa fa-circle" style="color: green"></i>
              <?php } else { ?>
                <i class="fa fa-circle" style="color: red"></i>
              <?php } ?>
            </td>-->
            <td>
                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                        Ações <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                        <!--<li>
                            <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_view_notice/'.$row['notice_id']); ?>');">
                                <i class="entypo-credit-card"></i>
                                <?php echo get_phrase('print/view_notice'); ?>
                            </a>
                        </li>-->
                        <!--<li class="divider"></li>-->
                        <!--<li>
                            <a href="<?php echo site_url('admin/noticeboard/mark_as_archive/'.$row['notice_id']) ?>">
                                <i class="entypo-box"></i>
                                Arquivar
                            </a>
                        </li>-->
                        <!--<li class="divider"></li>-->
                        <!-- EDITING LINK -->
                        <li>
                            <a href="<?php echo site_url('admin/noticeboard_edit/'.$row['notice_id']);?>">
                                <i class="entypo-pencil"></i>
                                Editar
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!-- DELETION LINK -->
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo site_url('admin/noticeboard/delete/'.$row['notice_id']); ?>');">
                                <i class="entypo-trash"></i>
                                Deletar
                            </a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</tbody>
</table>
<style>
    .dataTables_filter {
        display: none;
    }
    .dataTables_length {
        display: none;
    }
    #table_export_wrapper{
        margin-top: 0px !important;
    }
</style>
<script type="text/javascript">
    
    jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#table_noticeboard').DataTable({
            columnDefs: [
            { orderable: false, targets: 5 }
    ],
            "scrollX": true,

             "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 }                              
        });
    });
</script>
