<div class="row">
	<div class="col-md-8">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="entypo-plus-circled"></i> 
					Adicionar Vídeo
				</div>
			</div>
			<div class="panel-body">

				<?php echo form_open(site_url('admin/video/create/'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

				
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Título do Vídeo</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="titulo" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="" autofocus required>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Para a página</label>
					<div class="col-sm-5">
						<select name="pagina" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" id="problema">
							<option disabled selected>Selecione</option>
							<option value="student_add">Cadastrar Aluno</option>
                            <option value="cidade">Gerenciar Cidade</option>
                            <option value="escola">Gerenciar Escolas</option>
                            <option value="noticeboard">Gerenciar Eventos</option>
                            <option value="manage_attendance">Gerenciar Frequência</option>
                            <option value="local_execução">Gerenciar Locais de Execução </option>
                            <option value="modalidade">Gerenciar Modalidades</option>
                            <option value="nucleo">Gerenciar Núcleo  </option>
                            <option value="patrocinador">Gerenciar Patrocinadores</option>
                            <option value="projeto">Gerenciar Projetos </option>
                            <option value="class">Gerenciar Turmas</option>
                            <option value="admin">Gerenciar Usuários</option>
						</select>                        
					</div>
				</div>

                <div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">
                        Link do iFrame
                        <i class="entypo-help-circled tooltip_css"><span><div><img src="<?php echo base_url('uploads/tutorial_video.gif'); ?>" /></div></span></i>
                    </label>
					<div class="col-sm-7">
                    <textarea style="height: 150px;" class="form-control" rows="5" name="link" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" 
                        placeholder="<iframe width='560' height='315' src='https://www.youtube.com/embed/123' title='YouTube video player' ></iframe>"></textarea>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-5">
						<button type="submit" class="btn btn-info" id="submit">Adicionar Vídeo</button>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>
<style media="screen">
    .container {}

    .control-group {
        display: inline-block;
        vertical-align: top;
        background: #fff;
        text-align: left;
        box-shadow: 0 1px 2px rgba(0, 0, 0, 0.1);
        padding: 30px;
        width: 200px;
        height: 210px;
        margin: 10px;
    }

    .control {
        display: block;
        position: relative;
        padding-left: 40px;
        margin-bottom: 15px;
        cursor: pointer;
        font-size: 18px;
    }

    .control input {
        position: absolute;
        z-index: -1;
        opacity: 0;
    }

    .control__indicator {
        position: absolute;
        top: 2px;
        left: -11px;
        height: 20px;
        width: 20px;
        background: #e6e6e6;
    }

    .control--radio .control__indicator {
        border-radius: 50%;
    }

    .control:hover input~.control__indicator,
    .control input:focus~.control__indicator {
        background: #ccc;
    }

    .control input:checked~.control__indicator {
        background: #2aa1c0;
    }

    .control:hover input:not([disabled]):checked~.control__indicator,
    .control input:checked:focus~.control__indicator {
        background: #0e647d;
    }

    .control input:disabled~.control__indicator {
        background: #e6e6e6;
        opacity: 0.6;
        pointer-events: none;
    }

    .control__indicator:after {
        content: '';
        position: absolute;
        display: none;
    }

    .control input:checked~.control__indicator:after {
        display: block;
    }

    .control--checkbox .control__indicator:after {
        left: 8px;
        top: 5px;
        width: 4px;
        height: 9px;
        border: 3px solid #fff;
        border-width: 0 2px 2px 0;
        transform: rotate(45deg);
    }

    .control--checkbox input:disabled~.control__indicator:after {
        border-color: #7b7b7b;
    }
</style>