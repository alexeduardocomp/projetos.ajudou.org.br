<?php
$query = $this->db->order_by('projeto.nome', 'ASC')->get('projeto');
$projetos = $query->result();
$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();
?>
<style type="text/css">
    .page-body .selectboxit-container {
        margin-left: 15px;
    }
</style>
<meta charset="utf-8">
<div class="panel panel-primary">
    <div class="panel-heading">
        Gerar Gráficos
    </div> 
    
    <form id="form_grafico" action="" method="post" target="_blank">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                        <div class="col-sm-12">
                            <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option selected disabled>Selecione</option>
                                <?php
                                $tama = count($projetos);
                                for ($i = 0; $i < $tama; $i++) {
                                    echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                        <div class="col-sm-12">
                            <select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option disabled selected>Selecione um projeto</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div id="section_holder" style="display: none;">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label" style="margin-bottom: 5px;">Sessão</label>
                            <select class="form-control selectboxit" name="section_id">
                            </select>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Data inicial</label>
                        <input type="date" class="form-control" name="data_inicial" id="data_inicial" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Data final</label>
                        <input type="date" class="form-control" name="data_final" id="data_final" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Tipo</label>
                        <div class="col-sm-12">
                            <select name="tipo" class="form-control" id="tipo" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option disabled selected>Selecione um Tipo</option>
                                <option value="todos">Todos</option>
                                <option value="escolas">Escolas</option>
                                <option value="faixa_etaria">Faixa etária</option>
                                <option value="moradores_domicilio">Moradores a Domicílio</option>
                                <option value="problemas_saude">Problemas de Saúde</option>
                                <option value="renda">Renda Familiar</option>
                                <option value="genero">Gênero</option>
                                <option value="responsavel_sustento">Responsável pelo Sustento</option>
                                <option value="situacao">Situação</option>
                                <option value="tipo_escola">Tipo Escola</option>
                                <option value="trabalha_patrocinador">Parente trabalha para Patrocinador</option>
                            </select>
                        </div>
                    </div>
                </div>
                <input name="graficos" id="graficos" type="hidden"></input>
                <input name="total_graficos" id="total_graficos" type="hidden"></input>
                <div class="row">
                    <input type="hidden" name="year" value="<?php echo $running_year; ?>">
                    <div class="col-md-2" style="margin-top: 25px; margin-left: 30px;">
                        <!-- <a id="link_relatorio" target="_blank"><button id="submit"  class="btn btn-info">Gerar Relatório</button></a> -->
                        <button type="button" id="btnsubmit"  class="btn btn-info">Gerar Relatório</button>
                    </div>
                </div>
            </div>
        </div>
    </form>  
    <div class="fundo_modal" style="display: none;">
        <div id="abrirModal" class="carregando">
            <img id="gif" src="<?php echo base_url('assets/carregando.gif') ?>"> 
            <p style="margin-top: 10px; font-weight: 600px">Carregando...</p>
        </div>       
    </div>        
</div>
<div id="chart_div" style="height: 0; overflow: hidden;background-color: white!important;"> 
    <!-- <div id="chartdiv"></div> -->
</div>
<div id="lista_graficos">
    <img id="imagem"> 
</div>

<style>
.chartdiv {
  background-color: white!important;
  width: 1200px;
  height: 1000px;

}

#gif{
    width: 30px;
    height: 30px;
}
.carregando {
  position: fixed;
  text-align: center;
  width: auto;
  background-color: white;
  top: 10%;
  padding: 20px;
  z-index: 9999;
  border-radius: 5px;
}

.fundo_modal{
    position: fixed;
    display: flex;
    align-items: center;
    justify-content: center;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    font-family: Arial, Helvetica, sans-serif;
    background: rgba(0,0,0,0.8);
    z-index: 99999;
    -webkit-transition: opacity 400ms ease-in;
    -moz-transition: opacity 400ms ease-in;
    transition: opacity 400ms ease-in;
    pointer-events: none;
}


</style>

<script type="text/javascript">
    $('#btnsubmit').on('click', function() {
        $('.fundo_modal').css('display', 'flex');
        var tipo = $("#tipo").val();
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();
        var data_inicial = $("#data_inicial").val();
        var data_final = $("#data_final").val();
        $.ajax('<?php echo site_url('admin/get_imagens_grafico/'); ?>', {
            type: 'POST', // http method
            data: {
                tipo: tipo,
                id_nucleo: id_nucleo,
                id_projeto: id_projeto,
                id_turma: id_turma,
                data_inicial: data_inicial,
                data_final: data_final
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);
                console.log(data);
                var array = [];
                var aux;
                if (tipo == "todos") {
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_todos_print_view/'); ?>';

                    for (var i = 0; i < data.escolas.length; i++) {
                        aux = { valor: data.escolas[i]['soma'], texto: data.escolas[i]['nome_escola'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 1, 'escolas');

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.faixa_etaria.length; i++) {
                        aux = { valor: data.faixa_etaria[i]['soma'], texto: data.faixa_etaria[i]['idade']+' anos' };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico2" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 2, 'faixa_etaria');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.genero.length; i++) {
                        aux = { valor: data.genero[i]['soma'], texto: data.genero[i]['sex']};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico3" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 3, 'genero');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.moradores.length; i++) {
                        aux = { valor: data.moradores[i]['soma'], texto: data.moradores[i]['numero_pessoas_domicilio']+' pessoas'};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico4" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 4, 'moradores');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.problemas_saude.length; i++) {
                        aux = { valor: data.problemas_saude[i]['soma'], texto: data.problemas_saude[i]['problema_saude']};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico5" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 5, 'problemas_saude');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.renda.length; i++) {
                        aux = { valor: data.renda[i]['soma'], texto: data.renda[i]['renda_familiar']};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico6" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 6, 'renda');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.responsavel_sustento.length; i++) {
                        aux = { valor: data.responsavel_sustento[i]['soma'], texto: data.responsavel_sustento[i]['responsavel_sustento']};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico7" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 7, 'responsavel_sustento');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.situacao.length; i++) {
                        aux = { valor: data.situacao[i]['soma'], texto: data.situacao[i]['situacao']};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico8" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 8, 'situacao');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.tipo_escola.length; i++) {
                        aux = { valor: data.tipo_escola[i]['soma'], texto: data.tipo_escola[i]['tipo_escola']};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico9" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 9, 'tipo_escola');
                    

                    //gerar novo grafico
                    var array = [];
                    aux = '';
                    for (var i = 0; i < data.trabalha_patrocinador.length; i++) {
                        aux = { valor: data.trabalha_patrocinador[i]['soma'], texto: data.trabalha_patrocinador[i]['parente_trabalha_patrocinadora']};
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico10" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos, 10, 'trabalha_patrocinador');
                    
                    
                    console.log(array);
                     
                }else if(tipo == 'escolas'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('/admin/graficos_escolas_print_view/'); ?>';

                    for (var i = 0; i < data.escolas.length; i++) {
                        aux = { valor: data.escolas[i]['soma'], texto: data.escolas[i]['nome_escola'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'escolas');                    
                }else if(tipo == 'faixa_etaria'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_faixa_etaria_print_view/'); ?>';

                    for (var i = 0; i < data.faixa_etaria.length; i++) {
                        aux = { valor: data.faixa_etaria[i]['soma'], texto: data.faixa_etaria[i]['idade'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'faixa_etaria');                    
                }else if(tipo == 'genero'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_genero_print_view/'); ?>';

                    for (var i = 0; i < data.genero.length; i++) {
                        aux = { valor: data.genero[i]['soma'], texto: data.genero[i]['sex'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'genero');                    
                }else if(tipo == 'moradores_domicilio'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/'); ?>';

                    for (var i = 0; i < data.moradores.length; i++) {
                        aux = { valor: data.moradores[i]['soma'], texto: data.moradores[i]['numero_pessoas_domicilio'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'moradores');                    
                }else if(tipo == 'problemas_saude'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_problemas_saude_print_view/'); ?>';

                    for (var i = 0; i < data.problemas_saude.length; i++) {
                        aux = { valor: data.problemas_saude[i]['soma'], texto: data.problemas_saude[i]['problema_saude'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'problemas_saude');                    
                }else if(tipo == 'renda'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_renda_view/'); ?>';

                    for (var i = 0; i < data.renda.length; i++) {
                        aux = { valor: data.renda[i]['soma'], texto: data.renda[i]['renda_familiar'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'renda');                     
                }else if(tipo == 'responsavel_sustento'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/'); ?>';

                    for (var i = 0; i < data.responsavel_sustento.length; i++) {
                        aux = { valor: data.responsavel_sustento[i]['soma'], texto: data.responsavel_sustento[i]['responsavel_sustento'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'responsavel_sustento');                    
                }else if(tipo == 'situacao'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_situacao_print_view/'); ?>';

                    for (var i = 0; i < data.situacao.length; i++) {
                        aux = { valor: data.situacao[i]['soma'], texto: data.situacao[i]['situacao'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'situacao');                    
                }else if(tipo == 'tipo_escola'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_tipo_escola_print_view/'); ?>';

                    for (var i = 0; i < data.tipo_escola.length; i++) {
                        aux = { valor: data.tipo_escola[i]['soma'], texto: data.tipo_escola[i]['tipo_escola'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'tipo_escola');                    
                }else if(tipo == 'situacao'){
                    document.getElementById('form_grafico').action = '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/'); ?>';

                    for (var i = 0; i < data.trabalha_patrocinador.length; i++) {
                        aux = { valor: data.trabalha_patrocinador[i]['soma'], texto: data.trabalha_patrocinador[i]['parente_trabalha_patrocinadora'] };
                        array.push(aux);
                    }
                    $('#chart_div').append('<div id="grafico1" class="chartdiv"></div>');
                    criarGrafico(array, data.qtd_graficos,1, 'trabalha_patrocinador');                    
                }else{
                    alert('Nenhum tipo encontrado.');
                }
                
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });
        

        
    });

    async function criarGrafico(data, total, id, tipo){
        console.log('chamou a função');
        am4core.ready(function() {
            // Themes begin
            am4core.useTheme(am4themes_dataviz);
            // am4core.useTheme(am4themes_animated);
            // Themes end

            var chart = am4core.create("grafico"+id, am4charts.PieChart3D);
            chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

            chart.data = data;
            
            chart.innerRadius = am4core.percent(40);
            chart.depth = 60;

            chart.responsive.enabled = true;
            chart.responsive.useDefault = false;

            /* 
            chart.legend.maxHeight = 50;
            chart.legend.scrollable = true; */

            var series = chart.series.push(new am4charts.PieSeries3D());
            series.dataFields.value = "valor";
            /* series.dataFields.depthValue = "valor"; */
            series.dataFields.category = "texto";
            series.ticks.template.disabled = true;
            series.alignLabels = false;
            series.labels.template.text = "{value.percent.formatNumber('#.0')}%";
            series.labels.template.radius = am4core.percent(-30);
            series.labels.template.fill = am4core.color("white");
            series.labels.template.fontSize= 20;
            series.labels.template.fontWeight= 600;
            series.labels.template.textAlign= "middle";
            series.labels.template.paddingLeft = 10;
            series.labels.template.paddingRight = 10;

                
            series.slices.template.cornerRadius = 5;
            series.colors.step = 3;

            series.labels.template.adapter
            .add("radius", function(radius, target) {
                if (target.dataItem && (target.dataItem.values.value.percent < 5)) {
                    return 0;
                }
                return radius;
            });

            series.labels.template.adapter
            .add("fill", function(color, target) {
                if (target.dataItem && (target.dataItem.values.value.percent < 5)) {
                    return am4core.color("#000");
                }
                return color;
            });
            chart.legend = new am4charts.Legend();
            chart.legend.itemContainers.template.paddingTop = 0;
            chart.legend.layout = "grid";
            chart.legend.fontSize = 20;
            chart.legend.labels.template.truncate = true;
            converterGrafico(chart, total, tipo);
        });

    }

    async function converterGrafico(chart, total, tipo){
        let img = '';
        chart.exporting.backgroundColor = "#fff";
        chart.exporting.getImage("png").then(function(imgData) {
            if($("#graficos").val() == ''){
                //adicionar imagem ao input #graficos
                var array = {};
                array[tipo] = imgData;
                console.log(array);

                $("#graficos").val(JSON.stringify(array));
                if($("#total_graficos").val() == ''){
                    $("#total_graficos").val('1');
                }
                //total de graficos ja gerados
                /* var int = $("#total_graficos").val();
                var total_graficos = parseInt(int);
                total_graficos = total_graficos + 1;
                console.log(total_graficos);
                $("#total_graficos").val(total_graficos); */
                console.log($("#total_graficos").val());

                if($("#total_graficos").val() == total){
                    console.log('Um gráfico convertido: '+$("#total_graficos").val());
                    document.querySelector('#form_grafico').submit();
                    $('.fundo_modal').css('display', 'none');
                    $("#total_graficos").val('');
                    $("#graficos").val('');
                } 
            }else{
                //adicionar imagem ao input #graficos
                const obj = JSON.parse($("#graficos").val());
                //aux[tipo] = imgData;
                obj[tipo] = imgData;
                $("#graficos").val(JSON.stringify(obj));
                console.log(obj);
                 //total de graficos ja gerados
                 var int = $("#total_graficos").val();
                 var total_graficos = parseInt(int);
                total_graficos = total_graficos + 1;
                    console.log(total_graficos);
                $("#total_graficos").val(total_graficos);
                if($("#total_graficos").val() == total){
                    console.log('Todos os gráficos convertidos: '+obj.length);
                    document.querySelector('#form_grafico').submit();
                    $('.fundo_modal').css('display', 'none');
                    $("#graficos").val('');
                    $("#total_graficos").val('');
                }
            }
        });
    }

    $("#tipo").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_inicial = $("#data_inicial").val();
        var tipo = $("#tipo").val();
        var data_final = $("#data_final").val();


        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if ((tipo != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#btnsubmit').removeAttr('disabled');
        }
    });


    $("#pro").change(function() {

        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                $("#nuc").empty();
                data = $.parseJSON(data);
                $("#nuc").append('<option value="todos">Todos os núcleos</option>');

                for (var i = 0; i < data.length; i++) {
                    $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                }

                var id_nucleo = $("#nuc").val();

                $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: id_nucleo
                    }, // data to submit
                    success: function(data2, status, xhr) {
                        $("#turm").empty();
                        data2 = $.parseJSON(data2);
                        if ((data2 == null) | (data2 == '')) {
                            $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                        } else {
                            for (var i = 0; i < data2.length; i++) {
                                $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                            }
                            var id_turma = $("#turm").val();
                        }

                        var data_inicial = $("#data_inicial").val();

                        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);

                        var id_nucleo = $("#nuc").val();
                        var id_projeto = $("#pro").val();
                        var id_turma = $("#turm").val();

                        var data_final = $("#data_final").val();
                        var tipo = $("#tipo").val();
 
                        if (tipo == "todos") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "renda") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "problemas_saude") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "moradores_domicilio") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "faixa_etaria") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "escolas") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "genero") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "responsavel_sustento") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "situacao") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "tipo_escola") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "trabalha_patrocinador") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }


                        // data = $("#data").val();

                        if ((id_projeto != null) & (id_nucleo != null) & (tipo != null)) {
                            //alert(data);
                            $('#btnsubmit').removeAttr('disabled');
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                    }
                });
            }
        });
    });

    $("#turm").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_inicial = $("#data_inicial").val();

        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_final = $("#data_final").val();
        var tipo = $("#tipo").val();

        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }



        if ((tipo != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#btnsubmit').removeAttr('disabled');
        }
    });

    ////// dsfdas
    $("#nuc").change(function() {

        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();

        //alert(id_nucleo);
        $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_nucleo
            }, // data to submit
            success: function(data2, status, xhr) {
                //alert(data2);
                $("#turm").empty();
                data2 = $.parseJSON(data2);

                if ((data2 == null) | (data2 == '')) {
                    $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                } else {
                    for (var i = 0; i < data2.length; i++) {

                        $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                    }
                }

                var id_turma = $("#turm").val();

                var data_inicial = $("#data_inicial").val();

                //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
                var id_nucleo = $("#nuc").val();
                var id_projeto = $("#pro").val();
                var id_turma = $("#turm").val();

                var data_final = $("#data_final").val();
                var tipo = $("#tipo").val();

                if (tipo == "todos") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "renda") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "problemas_saude") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "moradores_domicilio") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "faixa_etaria") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "escolas") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "genero") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "responsavel_sustento") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "situacao") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "tipo_escola") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "trabalha_patrocinador") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }


                if ((tipo != null) & (id_projeto != null) & (id_nucleo != null)) {

                    $('#btnsubmit').removeAttr('disabled');
                }

                $.ajax({
                    url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
                    success: function(response) {
                        jQuery('#section_selector_holder').html(response);
                    }
                });
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });
    });

    $("#data_inicial").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();
        var data_inicial = $("#data_inicial").val();

        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_final = $("#data_final").val();
        var tipo = $("#tipo").val();

        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }


        if ((tipo != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#btnsubmit').removeAttr('disabled');
        }
    });

    $("#data_final").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();
        var data_inicial = $("#data_inicial").val();

        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_final = $("#data_final").val();
        var tipo = $("#tipo").val();

        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }


        if ((tipo != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#btnsubmit').removeAttr('disabled');
        }
    });

    var class_selection = "";
    jQuery(document).ready(function($) {
        $('#btnsubmit').attr('disabled', 'disabled');
    });

    function select_section(class_id) {
        if (class_id !== '') {
            $.ajax({
                url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
                success: function(response) {
                    jQuery('#section_holder').html(response);
                }
            });
        }
    }

    /* function check_validation() {
        if (class_selection !== '') {
            $('#btnsubmit').removeAttr('disabled')
        } else {
            $('#btnsubmit').attr('disabled', 'disabled');
        }
    } */

    $('#class_selection').change(function() {
        class_selection = $('#class_selection').val();
        check_validation();
    });

    $('#class_selection').change(function() {
        var id_class = $("#class_selection").val();
        select_section(id_class);
    });
</script>