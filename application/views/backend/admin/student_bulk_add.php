<?php 
	$query = $this->db->get('projeto');
	$projetos = $query->result();

		$query2 = $this->db->get('nucleo');
	$nucleos = $query2->result();


		$query3 = $this->db->get('escola');
	$escolas = $query3->result();





?>

<?php echo form_open(site_url('admin/bulk_student_add_using_csv/import') ,
			array('class' => 'form-inline validate',   'enctype' => 'multipart/form-data'));?>

			<style type="text/css">
				
				select{
					min-width: 200px;
				}
			</style>
<div class="row">
	<div class="col-md-8">
		<div class="panel panel-primary " data-collapsed="0">
		    <div class="panel-heading">
		        <div class="panel-title">
		            <i class="fa fa-calendar"></i>
		            Criar vários alunos
		        </div>
		    </div>
		    <div class="panel-body">
		        <div class="row">
				<div class="col-md-4">
						<div class="form_group">
							<label class="control-label" style="margin-bottom: 5px; display: block;">Projeto</label>
								<select name="id_projeto" class="form-control" id="pro"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

										<option selected disabled>Selecione</option>
									<?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>
  									  
									</select>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form_group">
								<label class="control-label" style="margin-bottom: 5px; display: block;">Núcleo</label>
							<select name="id_nucleo" class="form-control" id="nuc"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
											<option disabled selected>Selecione um projeto</option>

  									  
									</select>
						</div>
					</div>

					<div class="col-md-4">
						<div class="form_group">
								<label class="control-label" style="margin-bottom: 5px; display: block;">Turma</label>
							<select name="class_id" id="class_id" class="form-control" required
								  data-validate="required"  data-message-required="<?php echo get_phrase('value_required');?>">
								<option value="" disabled="" selected="">Selecione um núcleo</option>
							
							</select>
						</div>
					</div>


					
			
				</div>

				  <div class="row" style="margin-top:20px; margin-bottom: 20px;">


					<div id="section_holder" class="col-md-4">
						<div class="form_group">
								
							<label class="control-label" style="margin-bottom: 5px; display: block;">Seção</label>
						<select name="section_id" id="section_id" class="form-control">
							<option value="">Selecione uma turma primeiro</option>
						</select>
						</div>
					</div>


					<div class="col-md-4">
						<div class="form_group">
							<label class="control-label" style="margin-bottom: 5px; display: block;">Escola</label>
							<select name="id_escola" class="form-control" id="esc"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

										<option selected disabled>Selecione um núcleo</option>
									
  									  
									</select>
						</div>
					</div>


					<div id="section_holder" class="col-md-4">
						
					</div>
			
				</div>
				<div class="row">
					<div class="col-md-offset-4 col-md-4" style="padding: 15px;">
						<button type="button" class="btn btn-primary" name="generate_csv" id="generate_csv"><?php echo 'Gerar Arquivo CSV '?></button>
					</div>
					<div class="col-md-offset-4 col-md-4" style="padding-bottom:15px;">
					<input type="file" name="userfile" class="form-control file2 inline btn btn-info" data-label="<i class='entypo-tag'></i> Selecione Arquivo CSV"
					                   	data-validate="required" data-message-required="<?php echo get_phrase('required'); ?>"
					               		accept="text/csv, .csv" />
					</div>
					<div class="col-md-offset-4 col-md-4">
						<button type="submit" class="btn btn-success" name="import_csv" id="import_csv"><?php echo "Importar csv" ?></button>
					</div>
				</div>
		    </div>
		</div>
	</div>
	<div class="col-md-4">
		<blockquote class="blockquote-blue">
			<p>
				<strong>
				Instruções para adicionar vários estudantes
				</strong>
			</p>
			<p>
				<ol style="color: #ffffff;font-weight: 200;line-height: 1.5;font-size: 13px;padding-left: 15px;">
					<li style="padding: 5px;"><?php echo get_phrase('at_first_select_the_class_and_section').'.'; ?></li>
					<li style="padding: 5px;"><?php echo get_phrase('after_selecting_class_and_section_click_').'"Gerar arquivo CSV".'; ?></li>
					<li style="padding: 5px;"><?php echo get_phrase('open_the_downloaded_').'"bulk_student.csv". '.get_phrase('enter_student_details_as_written_in_there_and_remember_take_the_parent_ID_from_parent_table').'.';?></li>
					<li style="padding: 5px;"><?php echo get_phrase('save_the_edited_').'"bulk_student.csv".';?></li>
					<li style="padding: 5px;"><?php echo 'Clique em "selecione arquivo CSV" '.get_phrase('and_choose_the_file_you_just_edited').'.';?></li>
					<li style="padding: 5px;"><?php //echo get_phrase('import_that_file.');?>Importe esse arquivo.</li>
					<li style="padding: 5px;"><?php echo ' Clique em "Importar CSV".';?></li>
				</ol>
			</p>
			<p style="font-weight: 500;">
				*** <?php// echo get_phrase('this_system_keeps_track_of_duplication_in_email_ID.').' '.get_phrase('so_please_enter_unique_email_ID_for_every_student').'.'; ?>Este sistema controla a duplicação na identificação de email. Então, insira um ID de email exclusivo para cada aluno.
			</p>
		</blockquote>
	</div>
</div>



<?php echo form_close();?>

		


<a href="" download="bulk_student.csv" style="display: none;" id = "bulk">Download</a>

<script>

</script>
<script type="text/javascript">
var class_selection = '';
jQuery(document).ready(function($) {
$('#submit_button').attr('disabled', 'disabled');

});
	function get_sections(class_id) {
		if (class_id != "") {
			$.ajax({
	            url: '<?php echo site_url('admin/get_sections/');?>' + class_id ,
	            success: function(response)
	            {
	                jQuery('#section_holder').html(response);
	                jQuery('#bulk_add_form').show();
	            }
	        });
		}
	}
	$("#generate_csv").click(function(){
		var class_id 	= $('#class_id').val();
		var section_id 	= $('#section_id').val();
		var id_projeto = $('#pro').val();
		var id_nucleo = $('#nuc').val();
		var id_escola = $('#esc').val();
		var id_projeto = $('#pro').val();

		if(class_id == '' || section_id == '' || id_projeto =='' || id_nucleo == '' || id_escola == '')
			toastr.error("<?php echo 'Certifique-se que a turma e a seção foram selecionadas'; ?>");
		else {
			$.ajax({
			  	url: '<?php echo site_url('admin/generate_bulk_student_csv/');?>' + class_id + '/' + section_id + '/' + id_projeto + '/' + id_nucleo + '/' + id_escola,
			  	success: function(response) {
			    	toastr.success("<?php echo get_phrase('file_generated'); ?>");
						$("#bulk").attr('href', response);
						jQuery('#bulk')[0].click();
			    	//document.location = response;
			  	}
			});
		}
	});
</script>

<script type="text/javascript">
	
$( "#pro" ).change(function() {

  
     var id_projeto = $("#pro").val();

     $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
    type: 'POST',  // http method
    data: { id_projeto: id_projeto },  // data to submit
    success: function (data, status, xhr) {


    		 $("#nuc").empty();
    	 data = $.parseJSON(data);
    	 
    	 

      for (var i = 0; i < data.length; i++) {
  
   $("#nuc").append('<option value='+data[i]['id_nucleo']+'>'+data[i]['nome_nucleo']+'</option>');
}






var id_nucleo = $("#nuc").val();


 $.ajax('<?php echo site_url('admin/get_escola_nucleo2/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#esc").empty();
    	 data2 = $.parseJSON(data2);



    

      for (var i = 0; i < data2.length; i++) {
  
   $("#esc").append('<option value='+data2[i]['id_escola']+'>'+data2[i]['nome_escola']+'</option>');
}
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});

var id_nucleo = $("#nuc").val();

  $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#class_id").empty();
    	 data2 = $.parseJSON(data2);



    

      for (var i = 0; i < data2.length; i++) {
  
   $("#class_id").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
}


var id_turma = $("#class_id").val();



var id_turma = $("#class_id").val();




  $.ajax('<?php echo site_url('admin/get_secao_turma/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_turma },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#section_id").empty();
    	 data2 = $.parseJSON(data2);



    

      for (var i = 0; i < data2.length; i++) {
  
   $("#section_id").append('<option value='+data2[i]['id_secao']+'>'+data2[i]['nome_secao']+'</option>');
}





      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});













      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert("erro");
    }
});
   
});





$( "#nuc" ).change(function() {


var id_nucleo = $("#nuc").val();


 $.ajax('<?php echo site_url('admin/get_escola_nucleo2/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {



    		 $("#esc").empty();
    	 data2 = $.parseJSON(data2);

      for (var i = 0; i < data2.length; i++) {
  
   $("#esc").append('<option value='+data2[i]['id_escola']+'>'+data2[i]['nome_escola']+'</option>');
}
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});


var id_nucleo = $("#nuc").val();

 $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#class_id").empty();
    	 data2 = $.parseJSON(data2);



    

      for (var i = 0; i < data2.length; i++) {
  
   $("#class_id").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
}


var id_turma = $("#class_id").val();


  $.ajax('<?php echo site_url('admin/get_secao_turma/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_turma },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#section_id").empty();
    	 data2 = $.parseJSON(data2);



    

      for (var i = 0; i < data2.length; i++) {
  
   $("#section_id").append('<option value='+data2[i]['id_secao']+'>'+data2[i]['nome_secao']+'</option>');
}





      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});


      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});









});



$( "#class_id" ).change(function() {



	var id_turma = $("#class_id").val();


  $.ajax('<?php echo site_url('admin/get_secao_turma/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_turma },  // data to submit
    success: function (data2, status, xhr) {

    

    		  $("#section_id").empty();
    	 data2 = $.parseJSON(data2);



    

      for (var i = 0; i < data2.length; i++) {
  
   $("#section_id").append('<option value='+data2[i]['id_secao']+'>'+data2[i]['nome_secao']+'</option>');
}





      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});




});

</script>
