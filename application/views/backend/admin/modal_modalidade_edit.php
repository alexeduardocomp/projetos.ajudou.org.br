<?php
$edit_data = $this->db->get_where('modalidade', array('id' => $param2))->result_array();
foreach ($edit_data as $row) :
?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						Atualizar modalidade
					</div>
				</div>
				<div class="panel-body">
					<?php echo form_open(site_url('admin/modalidade/edit/' . $row['id']), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Modalidade</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="modalidade" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['modalidade']; ?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Atualizar</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>