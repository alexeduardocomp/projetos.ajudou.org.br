<?php
$query = $this->db
->order_by('projeto.nome', 'ASC')->get('projeto');
$projetos = $query->result();

$query2 = $this->db
->order_by('teacher.name', 'ASC')->get('teacher');
$instrutores = $query2->result();
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Adicionar plano de aula</h3>
            </div>
            <div class="panel-body">
                <?php echo form_open(site_url('admin/class_routine/create'), array('class' => 'form-horizontal form-groups validate', 'target' => '_top')); ?>





                <div class="form-group">
                    <label class="control-label col-sm-3" style="margin-bottom: 5px;">Projeto</label>
                    <div class="col-sm-5">
                        <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo 'Campo Obrigatório' ?>">

                            <option selected disabled>Selecione um projeto</option>
                            <?php

                            $tama = count($projetos);

                            $ano = explode('-', $running_year);
                            for ($i = 0; $i < $tama; $i++) {
                                $inicio = explode('-', $projetos[$i]->data_inicio);
                                if ($inicio[0] == $ano[0]) {

                                    echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                                }
                            }

                            ?>

                        </select>

                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" style="margin-bottom: 5px;">Núcleo</label>
                    <div class="col-sm-5">
                        <select name="nucleo_id" class="form-control" data-validate="required" id="nucleo_selection" data-message-required="<?php echo 'Campo Obrigatório' ?>">
                            <option selected disabled>Selecione um núcleo</option>

                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <label class="control-label col-sm-3" style="margin-bottom: 5px;">Local Execução</label>
                    <div class="col-sm-5">
                        <select name="local_execucao" class="form-control" data-validate="required" id="local_execucao" data-message-required="<?php echo 'Campo Obrigatório' ?>">
                            <option selected disabled>Selecione um local</option>

                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-5">
                        <select name="class_id[]" class="form-control" id="class_id" data-validate="required" data-message-required="<?php echo 'Campo Obrigatório' ?>" multiple>

                            <option selected disabled>Selecione</option>
                        </select>
                        <div style="margin-top:10px;">Segure a tecla Ctrl e selecione mais de uma Turma!</div>
                    </div>

                </div>
                <div id="section_subject_selection_holder"></div>

                <!-- <div class="form-group">
                        <label class="col-sm-3 control-label">Dia</label>
                        <div class="col-sm-5">
                            <select name="day" class="form-control selectboxit" style="width:100%;">
                                <option value="sunday">Domingo</option>
                                <option value="monday">Segunda-feira</option>
                                <option value="tuesday">Terça-feira</option>
                                <option value="wednesday">Quarta-feira</option>
                                <option value="thursday">Quinta-feira</option>
                                <option value="friday">Sexta-feira</option>
                                <option value="saturday">Sábado</option>
                            </select>
                        </div>
                    </div>-->
                <div class="form-group">
                    <label class="control-label col-sm-3" style="margin-bottom: 5px;">Instrutor</label>
                    <div class="col-sm-5">
                        <select name="id_teacher" class="form-control" id="id_teacher" data-validate="required" data-message-required="<?php echo 'Campo Obrigatório' ?>">

                            <option selected disabled>Selecione um Instrutor</option>
                            <?php

                            $tama = count($instrutores);

                            for ($i = 0; $i < $tama; $i++) {
                                echo '<option value=' . $instrutores[$i]->teacher_id . '>' . $instrutores[$i]->name . '</option>';
                            }

                            ?>

                        </select>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Atividade</label>
                    <div class="col-sm-5">
                        <select name="subject_id[]" class="form-control" id="subject_id" data-validate="required" data-message-required="<?php echo 'Campo Obrigatório' ?>" multiple>

                            <option selected disabled>Selecione</option>

                        </select>
                        <div style="margin-top:10px;">Segure a tecla Ctrl e selecione mais de uma Atividade!</div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Data</label>
                    <div class="col-sm-5">

                        <input type="text" id="data" class="form-control datepicker" name="data" data-format="dd-mm-yyyy" value="" placeholder="Selecione uma data" required="" data-validate="required" data-message-required="<?php echo 'Campo Obrigatório' ?>" />
                    </div>
                </div>

                <!--<div class="form-group">
                        <label class="col-sm-3 control-label">Hora de começo</label>
                        <div class="col-sm-9">
                            <div class="col-md-3">
                                <select name="time_start" id= "starting_hour" class="form-control selectboxit">
                                    <option value="">Hora</option>
                                    <?php for ($i = 0; $i <= 12; $i++) : ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="time_start_min" id= "starting_minute" class="form-control selectboxit">
                                    <option value="">Minutos</option>
                                    <?php for ($i = 0; $i <= 11; $i++) : ?>
                                        <option value="<?php echo $i * 5; ?>"><?php echo $i * 5; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="starting_ampm" class="form-control selectboxit">
                                    <option value="1">am</option>
                                    <option value="2">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora de término</label>
                        <div class="col-sm-9">
                            <div class="col-md-3">
                                <select name="time_end" id= "ending_hour" class="form-control selectboxit">
                                    <option value="">Hora</option>
                                    <?php for ($i = 0; $i <= 12; $i++) : ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="time_end_min" id= "ending_minute" class="form-control selectboxit">
                                    <option value="">Minutos</option>  
                                    <?php for ($i = 0; $i <= 11; $i++) : ?>
                                        <option value="<?php echo $i * 5; ?>"><?php echo $i * 5; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="ending_ampm" class="form-control selectboxit">
                                    <option value="1">am</option>
                                    <option value="2">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>-->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" id="add_class_routine" class="btn btn-info">Adicionar plano de aula</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>


    </div>
</div>


<script type="text/javascript">
    var class_id = '';
    var data = '';
    var subject_id = '';
    var starting_hour = '';
    var starting_minute = '';
    var ending_hour = '';
    var ending_minute = '';
    jQuery(document).ready(function($) {
     //   $('#add_class_routine').attr('disabled', 'disabled')
    });

    function get_class_section_subject(class_id) {
        $.ajax({
            url: '<?php echo site_url('admin/get_class_section_subject/'); ?>' + class_id,
            success: function(response) {
                jQuery('#section_subject_selection_holder').html(response);
            }
        });
    }

    function check_validation() {
        console.log('class_id: ' + class_id + ' starting_hour:' + starting_hour + ' starting_minute: ' + starting_minute + ' ending_hour: ' + ending_hour + ' ending_minute: ' + ending_minute);
        if (class_id != '' && subject_id != '' && data != '') {
          $('#add_class_routine').removeAttr('disabled');
        }
    }
    $('#class_id').change(function() {
        class_id = $('#class_id').val();
        //check_validation();
    });

    $('#data').change(function() {


        var data = $('#data').val();
        //data2 = String(data);
        //var date = new Date(data2);
        //alert(data2);

        split = data.split('-');
        novadata = split[1] + "/" + split[0] + "/" + split[2];
        data_input = new Date(novadata);


        const atual = new Date();
        atual.setHours(0);
        atual.setMinutes(0);
        atual.setMilliseconds(0);
        atual.setSeconds(0);


        var ano = data_input.getFullYear();




    });

    $("#pro").change(function() {
        var id_projeto = $("#pro").val();
        $('#local_execucao').empty();
        $('#class_id').empty();

        $.ajax('<?php echo site_url('admin/get_nucleos_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                //alert(data);

                $('#nucleo_selection').empty();
                data = $.parseJSON(data);

                if (data.length >= 1) {
                    $('#nucleo_selection').append('<option selected disabled>Selecione um núcleo</option>');
                    for (var i = 0; i < data.length; i++) {
                        $('#nucleo_selection').append('<option value=' + data[i]['id'] + '>' + data[i]['nome_nucleo'] + '</option>');

                    }
                    nucleo_selection = 'selecionado';
                    check_validation();
                    var id_nucleo = $("#nucleo_selection").val();
                   // select_section_nucleo(id_nucleo);
                } else {
                    nucleo_selection = '';
                    check_validation();
                    $('#nucleo_selection').append('<option disabled selected>Nenhum núcleo encontrada</option>');
                }




            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });

    });
    $("#nucleo_selection").change(function() {
        var id_nucleo = $("#nucleo_selection").val();

        $.ajax('<?php echo site_url('admin/get_local_execucao_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                id_nucleo: id_nucleo
            }, // data to submit
            success: function(data, status, xhr) {

                //alert(data);

                $('#local_execucao').empty();
                data = $.parseJSON(data);

                if (data.length >= 1) {
                    $('#local_execucao').append('<option selected disabled>Selecione um local</option>');
                    for (var i = 0; i < data.length; i++) {
                        $('#local_execucao').append('<option value=' + data[i]['id'] + '>' + data[i]['local'] + '</option>');

                    }
                    local_execucao = 'selecionado';
                    check_validation();
                    var id_nucleo = $("#local_execucao").val();
                    //select_section_nucleo(id_nucleo);
                } else {
                    local_execucao = '';
                    check_validation();
                    $('#local_execucao').append('<option disabled selected>Nenhum local encontrado</option>');
                }
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });

        /* $.ajax('<?php echo site_url('admin/get_turma_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                dados2: id_nucleo
            }, // data to submit
            success: function(data, status, xhr) {

                //alert(data);

                $('#class_id').empty();
                data = $.parseJSON(data);

                if (data.length >= 1) {
                    for (var i = 0; i < data.length; i++) {

                        if (i == 0) {
                            $('#class_id').append('<option selected value=' + data[i]['id'] + '>' + data[i]['nome'] + '</option>');
                        } else {
                            $('#class_id').append('<option value=' + data[i]['id'] + '>' + data[i]['nome'] + '</option>');
                        }


                    }
                    class_id = 'selecionado';
                    check_validation();
                    var id_class = $("#class_id").val();
                    select_section(id_class);
                } else {
                    class_id = '';
                    check_validation();
                    $('#class_id').append('<option disabled selected>Nenhuma turma encontrada</option>');
                }




            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        }); */

    });

    $("#local_execucao").change(function() {
        var id_nucleo = $("#nucleo_selection").val();
        var id_projeto = $("#pro").val();
        var id_local = $("#local_execucao").val();

        $.ajax('<?php echo site_url('admin/get_turmas_local/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto,
                id_nucleo: id_nucleo,
                id_local: id_local
            }, // data to submit
            success: function(data, status, xhr) {

                //alert(data);

                $('#class_id').empty();
                data = $.parseJSON(data);

                if (data.length >= 1) {
                    for (var i = 0; i < data.length; i++) {

                        if (i == 0) {
                            $('#class_id').append('<option selected value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
                        } else {
                            $('#class_id').append('<option value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
                        }


                    }
                    class_id = 'selecionado';
                    check_validation();
                    var id_class = $("#class_id").val();
                    select_section(id_class);
                } else {
                    class_id = '';
                    check_validation();
                    $('#class_id').append('<option disabled selected>Nenhuma turma encontrada</option>');
                }




            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });
    });

    $("#id_teacher").change(function() {
        var id_teacher = $("#id_teacher").val();

        $.ajax('<?php echo site_url('admin/get_subject_teacher/'); ?>', {
            type: 'POST', // http method
            data: {
                dados4: id_teacher
            }, // data to submit
            success: function(data, status, xhr) {

                //alert(data);

                $('#subject_id').empty();
                data = $.parseJSON(data);

                if (data.length >= 1) {
                    for (var i = 0; i < data.length; i++) {

                        if (i == 0) {
                            $('#subject_id').append('<option selected value=' + data[i]['id'] + '>' + data[i]['nome'] + '</option>');
                        } else {
                            $('#subject_id').append('<option value=' + data[i]['id'] + '>' + data[i]['nome'] + '</option>');
                        }


                    }
                    subject_id = 'selecionado';
                    check_validation();
                    var id_subject = $("#subject_id").val();
                    get_class_section_subject(id_subject);
                } else {
                    subject_id = '';
                    check_validation();
                    $('#subject_id').append('<option disabled selected>Nenhuma atividade encontrada</option>');
                }




            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });

    });
</script>