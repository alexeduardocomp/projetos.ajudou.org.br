<?php
$edit_data		=	$this->db->get_where('teacher' , array('teacher_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
	$links_json = $row['social_links'];
	$links = json_decode($links_json);
?>



<script type="text/javascript">
    

     $(document).ready(function () { 
        $(".cpf").mask('000.000.000-00');
        $('.rg').mask('AA.000.000-A');
        $('.tel').mask('(00) 00000-0000');
        $('.cep').mask('00000-000');
    });

</script>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					Atualizar instrutor
            	</div>
            </div>
			<div class="panel-body">
                    <?php echo form_open(site_url('admin/teacher/do_update/'.$row['teacher_id']) , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top', 'enctype' => 'multipart/form-data'));?>

                                <div class="form-group">
                                <label for="field-1" class="col-sm-3 control-label">Foto</label>

                                <div class="col-sm-5">
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                                            <img src="<?php echo $this->crud_model->get_image_url('teacher' , $row['teacher_id']);?>" alt="...">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                                        <div>
                                            <span class="btn btn-white btn-file">
                                                <span class="fileinput-new">Selecione uma imagem</span>
                                                <span class="fileinput-exists">Mudar</span>
                                                <input type="file" name="userfile" accept="image/*">
                                            </span>
                                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover</a>
                                        </div>
                                    </div>
                                </div>
                            </div>






                            <div class="form-group">
                                <label class="col-sm-3 control-label">Nome</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>" data-validate="required"/>
                                </div>
                            </div>
														<!--<div class="form-group">
															<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('designation');?></label>
															<div class="col-sm-5">
																<input type="text" class="form-control" name="designation"
																	value="<?php echo $row['designation'] == null ? '' : $row['designation'];?>" >
															</div>
														</div>-->
                            <!--<div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="datepicker form-control" name="birthday" value="<?php echo $row['birthday'];?>"/>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Sexo</label>
                                <div class="col-sm-5">
                                    <select name="sex" class="form-control selectboxit">
                                    	<option value="MASCULINO" <?php if($row['sex'] == 'MASCULINO')echo 'selected';?>>MASCULINO</option>
                                    	<option value="FEMININO" <?php if($row['sex'] == 'FEMININO')echo 'selected';?>>FEMININO</option>
                                    </select>
                                </div>
                            </div>
                           <!-- <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('address');?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="address" value="<?php echo $row['address'];?>"/>
                                </div>
                            </div>-->
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Telefone</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control tel" name="phone" value="<?php echo $row['phone'];?>"/>
                                </div>
                            </div>


                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Telefone 2</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control tel" name="telefone_2" value="<?php echo $row['telefone_2'];?>" autofocus>
                        </div>
                    </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label"><?php echo get_phrase('email').'/ Nome de usuário';?></label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="email" value="<?php echo $row['email'];?>" data-validate="required"/>
                                </div>
                            </div>

                             <div class="form-group">
                                <label class="col-sm-3 control-label">Senha</label>
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" name="senha" value="" data-validate="required"/>
                                </div>
                            </div>



                   <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">CPF</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control cpf" name="CPF" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['CPF'];?>" autofocus>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">RG</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control rg" name="RG" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['RG'];?>" autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Cargo</label>
                                  <div class="col-sm-5">
                                    <select name="cargo" class="form-control">
                                        

                                        <?php
                                        if($row['cargo'] == "COORDENADOR"){
                                            echo " <option value='PROFESSOR' >PROFESSOR</option>
                                      <option value='COORDENADOR' selected>COORDENADOR</option>";
                                        }else{
                                              echo " <option value='PROFESSOR' selected>PROFESSOR</option>
                                      <option value='COORDENADOR' >COORDENADOR</option>";
                                        }


                                            ?>
                                     
                                    </select>
                                </div>

                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Estado</label>
                                  <div class="col-sm-5">
                                    <select name="estado" class="form-control"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus id="est">
                                        <option value="<?php echo $row['estado'];?>" selected><?php echo $row['estado'];?></option>
                                      <option value="Acre">Acre</option>
                                      <option value="Alagoas">Alagoas</option>
                                      <option value="Amapá">Amapá</option>
                                      <option value="Amazonas">Amazonas</option>
                                      <option value="Bahia">Bahia</option>
                                      <option value="Ceará">Ceará</option>
                                      <option value="Distrito Federal">Distrito Federal</option>
                                      <option value="Espírito Santo">Espírito Santo</option>
                                      <option value="Goiás">Goiás</option>
                                      <option value="Maranhão">Maranhão</option>
                                      <option value="Mato Grosso">Mato Grosso</option>
                                      <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
                                      <option value="Minas Gerais">Minas Gerais</option>
                                      <option value="Pará ">Pará </option>
                                      <option value="Paraíba">Paraíba</option>
                                      <option value="Paraná">Paraná</option>
                                      <option value="Pernambuco">Pernambuco</option>
                                      <option value="Piauí">Piauí</option>
                                      <option value="Rio de Janeiro">Rio de Janeiro</option>
                                      <option value="Rio Grande do Norte">Rio Grande do Norte</option>
                                      <option value="Rio Grande do Sul">Rio Grande do Sul</option>
                                      <option value="Rondônia">Rondônia</option>
                                      <option value="Roraima">Roraima</option>
                                      <option value="Santa Catarina">Santa Catarina</option>
                                      <option value="São Paulo">São Paulo</option>
                                      <option value="Sergipe">Sergipe</option>
                                      <option value="Tocantins">Tocantins</option>
                                    </select>
                                </div>

                    </div>


                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Cidade</label>
                                  <div class="col-sm-5">
                                    <select name="cidade" class="form-control" id="cid">
                                            <option id="cidadeatual" disabled selected style="display: none;" value="<?php echo $row['cidade'];?>"><?php echo $row['cidade'];?></option>

                                      
                                    </select>
                                </div>

                                </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">CEP</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control cep" name="CEP" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['CEP'];?>" autofocus>
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Rua</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="rua" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['rua'];?>" autofocus>
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Bairro</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="bairro" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['bairro'];?>" autofocus>
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Número</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="numero" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['numero'];?>" autofocus>
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Complemento</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="complemento" value="<?php echo $row['complemento'];?>" autofocus>
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Carga horária</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="carga_horaria" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['carga_horaria'];?>" autofocus>
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Data contratacao</label>
                        
                        <div class="col-sm-5">
                            <input type="date" class="form-control" name="data_contratacao" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus
                                value="<?php echo $row['data_contratacao'];?>">
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Conta bancária</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="conta_bancaria" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['conta_bancaria'];?>" autofocus>
                        </div>
                    </div>

                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Salário bruto</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="salario_bruto" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['salario_bruto'];?>" autofocus>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Salário líquido</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="salario_liquido" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['salario_liquido'];?>" autofocus>
                        </div>
                    </div>

                    <!--<div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Combustível</label>

                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="combustivel" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['combustivel'];?>" autofocus>
                        </div>
                    </div>
                    


                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">Metas qualitativas</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <textarea class="form-control" rows="2" name="metas_qualitativas" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                    <?php echo $row['metas_qualitativas'];?>
                                </textarea>
                            </div>
                        </div>
                    </div>




                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">Metas quantitativas</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <textarea class="form-control" rows="2" name="metas_quantitativas" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                    <?php echo $row['metas_quantitativas'];?>
                                </textarea>
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">Objetivos</label>
                        <div class="col-sm-5">
                            <div class="input-group">
                                <textarea class="form-control" rows="2" name="objetivos" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                    <?php echo $row['objetivos'];?>
                                </textarea>
                            </div>
                        </div>
                    </div> -->


                            <!--<div class="form-group">
                                <label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('about');?></label>
                                <div class="col-sm-5">
                                    <div class="input-group">
                                        <textarea class="form-control" rows="2" name="about"><?php echo $row['about'];?></textarea>
                                    </div>
                                </div>
                            </div>-->

							<!--<div class="form-group">
								<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('social_links');?></label>
								<div class="col-sm-8">
									<div class="input-group">
										<input type="text" class="form-control" name="facebook" placeholder=""
				              value="<?php echo $links[0]->facebook;?>">
										<div class="input-group-addon">
											<a href="#"><i class="entypo-facebook"></i></a>
										</div>
									</div>
									<br>
									<div class="input-group">
										<input type="text" class="form-control" name="twitter" placeholder=""
				              value="<?php echo $links[0]->twitter;?>">
										<div class="input-group-addon">
											<a href="#"><i class="entypo-twitter"></i></a>
										</div>
									</div>
									<br>
									<div class="input-group">
										<input type="text" class="form-control" name="linkedin" placeholder=""
				              value="<?php echo $links[0]->linkedin;?>">
										<div class="input-group-addon">
											<a href="#"><i class="entypo-linkedin"></i></a>
										</div>
									</div>
								</div>
							</div>-->

							<!--<div class="form-group">
								<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('show_on_website');?></label>
								<div class="col-sm-5">
									<select name="show_on_website" class="form-control selectboxit">
                		                  <option value="1" <?php if ($row['show_on_website'] == 1) echo 'selected';?>><?php echo get_phrase('yes');?></option>
                		                  <option value="0" <?php if ($row['show_on_website'] == 0) echo 'selected';?>><?php echo get_phrase('no');?></option>
                		              </select>
								</div>
							</div>-->


                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-5">
                                <button type="submit" class="btn btn-info">Atualizar instrutor</button>
                            </div>
                        </div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>


<script type="text/javascript">


    $( document ).ready(function() {
   

         var estado = $("#est").val();
         var cidadeatual = $("#cidadeatual").val();




     $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
    type: 'POST',  // http method
    data: { dados: estado },  // data to submit
    success: function (data, status, xhr) {



             //$("#cid").empty();
         data = $.parseJSON(data);
          //alert(data);

      for (var i = 0; i < data.length; i++) {

        if(data[i]['nome_cidade'] == cidadeatual){
$("#cid").append('<option selected value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
        }else{ 
  
   $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
}
}
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert("erro");
    }
});









});



$( "#est" ).change(function() {
  
     var estado = $("#est").val();

     $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
    type: 'POST',  // http method
    data: { dados: estado },  // data to submit
    success: function (data, status, xhr) { 



             $("#cid").empty();
         data = $.parseJSON(data);

      for (var i = 0; i < data.length; i++) {
  
   $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
}
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert("erro");
    }
});
   
});

</script>



