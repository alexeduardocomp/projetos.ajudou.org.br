<?php
$query = $this
->db
->order_by('projeto.nome', 'ASC')
->get('projeto');
  $projetos = $query->result();
?>

<div class="panel panel-primary">
	<div class="panel-heading">
		Frequência da
	</div>
	<div class="panel-body">
		<?php echo form_open(site_url('admin/attendance_selector/')); ?>
		<div class="row">
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Projeto</label>
					<select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
						<option selected disabled>Selecione um projeto</option>
						<?php
						$tama = count($projetos);

						$ano = explode('-', $running_year);
						for ($i = 0; $i < $tama; $i++) {
							$inicio = explode('-', $projetos[$i]->data_inicio);
							if ($inicio[0] == $ano[0]) {

								echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
							}
						}

						?>
					</select>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Núcleo</label>
					<select name="nucleo_id" class="form-control" id="nucleo_selection">
						<option selected disabled>Selecione um núcleo</option>
					</select>
				</div>
			</div>

			<div id="section_holder2" style="display: none;">
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label" style="margin-bottom: 5px;">Sessão2</label>
						<select class="form-control selectboxit" name="section_id2">
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Turma</label>
					<select name="class_id" class="form-control" id="class_selection">
						<option selected disabled>Selecione uma turma</option>
					</select>
				</div>
			</div>

			<div id="section_holder" style="display: none;">
				<div class="col-md-3">
					<div class="form-group">
						<label class="control-label" style="margin-bottom: 5px;">Sessão</label>
						<select class="form-control selectboxit" name="section_id">
						</select>
					</div>
				</div>
			</div>

			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Data</label>
					<input type="text" id="data" class="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy" value="Selecione uma data" />
				</div>
			</div>

			<input type="hidden" name="year" value="<?php echo $running_year; ?>">

			<div class="col-md-3" style="margin-top: 20px;">
				<button type="submit" id="submit" class="btn btn-info">Gerenciar frequência</button>
			</div>

		</div>
		<?php echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$('#data').change(function() {

		var data = $('#data').val();
		//data2 = String(data);
		//var date = new Date(data2);
		//alert(data2);

		split = data.split('-');
		novadata = split[1] + "/" + split[0] + "/" + split[2];
		data_input = new Date(novadata);

		const atual = new Date();
		atual.setHours(0);
		atual.setMinutes(0);
		atual.setMilliseconds(0);
		atual.setSeconds(0);

		var ano = data_input.getFullYear();
		//alert(ano);

		if (ano < 1970) {

			alert('Valor de data não é permitido');
			$('#submit').attr('disabled', 'disabled');
		} else {

			if (data_input.getTime() === atual.getTime()) {
				//alert('As datas são iguais');
				$('#submit').removeAttr('disabled');
			} else if (data_input.getTime() > atual.getTime()) {

				// Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
				//alert(data_input.toString() + ' maior que ' + atual.toString());
				$('#submit').attr('disabled', 'disabled');

			} else {
				$('#submit').removeAttr('disabled');
				//alert(data_input.toString() + ' menor que ' + atual.toString());
			}
		}
	});

	var class_selection = "";
	var nucleo_selection = "";

	jQuery(document).ready(function($) {
		$('#submit').attr('disabled', 'disabled');
	});

	function select_section_nucleo(nucleo_id) {
		if (nucleo_id !== '') {
			$.ajax({
				url: '<?php echo site_url('admin/get_section_nucleo/'); ?>' + nucleo_id,
				success: function(response) {

					jQuery('#section_holder2').html(response);
				}
			});
		}
	}

	function select_section(class_id) {
		if (class_id !== '') {
			$.ajax({
				url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
				success: function(response) {

					jQuery('#section_holder').html(response);
				}
			});
		}
	}

	function check_validation() {
		if (class_selection !== '') {
			$('#submit').removeAttr('disabled')
		} else {
			$('#submit').attr('disabled', 'disabled');
		}
	}

	$('#class_selection').change(function() {
		class_selection = $('#class_selection').val();
		check_validation();
	});

	$('#nucleo_selection').change(function() {
		nucleo_selection = $('#nucleo_selection').val();
		check_validation();
	});

	$("#pro").change(function() {
		var id_projeto = $("#pro").val();

		$.ajax('<?php echo site_url('admin/get_nucleos_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {

				//alert(data);

				$('#nucleo_selection').empty();
				data = $.parseJSON(data);

				if (data.length >= 1) {
					$('#nucleo_selection').append('<option selected disabled>Selecione um núcleo</option>');
					for (var i = 0; i < data.length; i++) {
						$('#nucleo_selection').append('<option value=' + data[i]['id'] + '>' + data[i]['nome_nucleo'] + '</option>');

					}
					nucleo_selection = 'selecionado';
					check_validation();
					var id_nucleo = $("#nucleo_selection").val();
					select_section_nucleo(id_nucleo);
				} else {
					nucleo_selection = '';
					check_validation();
					$('#nucleo_selection').append('<option disabled selected>Nenhum núcleo encontrada</option>');
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});

	});
	$("#nucleo_selection").change(function() {
		var id_nucleo = $("#nucleo_selection").val();

		$.ajax('<?php echo site_url('admin/get_turma_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				dados2: id_nucleo
			}, // data to submit
			success: function(data, status, xhr) {

				//alert(data);

				$('#class_selection').empty();
				data = $.parseJSON(data);

				if (data.length >= 1) {
					for (var i = 0; i < data.length; i++) {

						if (i == 0) {
							$('#class_selection').append('<option selected value=' + data[i]['id'] + '>' + data[i]['nome'] + '</option>');
						} else {
							$('#class_selection').append('<option value=' + data[i]['id'] + '>' + data[i]['nome'] + '</option>');
						}


					}
					class_selection = 'selecionado';
					check_validation();
					var id_class = $("#class_selection").val();
					select_section(id_class);
				} else {
					class_selection = '';
					check_validation();
					$('#class_selection').append('<option disabled selected>Nenhuma turma encontrada</option>');
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});

	});
</script>