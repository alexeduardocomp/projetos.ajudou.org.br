

            <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_patrocinador_add/');?>');"
                class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
               Adicionar novo patrocinador

                </a>
                <br><br>
               <table class="table table-bordered" id="patrocinadores">
                    <thead>
                        <tr>
                            <th width="60"><div>Id patrocinador</div></th>
                            <th><div>Foto</div></th>
                            <th><div>Nome</div></th>
                              <th><div>Opções</div></th>
                        </tr>
                    </thead>
                </table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#patrocinadores').DataTable({
            "processing": true,
            "scrollX": true,
            "serverSide": true,
            "ajax":{
                "url": "<?php echo site_url('admin/get_patrocinadores') ?>",
                "dataType": "json",
                "type": "POST",
            },

            
                  dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
  buttons: [

  { extend: 'copy', text: 'Copiar' },
  { extend: 'excel', text: 'Excel' },
  { extend: 'pdf', text: 'PDF' },
  { extend: 'csv', text: 'CSV' }
  ],

            "columns": [
                { "data": "id" },
                { "data": "foto",
                    "orderable": false},
                { "data": "nome" },
                { "data": "options" ,
                    "orderable": false},
            ],

             "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 }                              
        });
    });

    function patrocinador_edit_modal(patrocinador_id) {
        showAjaxModal('<?php echo site_url('modal/popup/modal_patrocinador_edit/');?>' + patrocinador_id);
    }

    function patrocinador_delete_confirm(patrocinador_id) {
        confirm_modal('<?php echo site_url('admin/patrocinador/delete/');?>' + patrocinador_id);
    }

</script>

<style type="text/css">
    
textarea.form-control {
    min-height: 100px !important;
}

</style>

