<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title">
            		<i class="entypo-plus-circled"></i>
					Adicionar escola
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(site_url('admin/escola/create/') , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                    
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus
                            	value="">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Tipo de escola</label>
                                  <div class="col-sm-5">
									<select name="tipo_escola" class="form-control">
  									  <option value="PÚBLICA">PÚBLICA</option>
									  <option value="PARTICULAR">PARTICULAR</option>
									</select>
								</div>

					</div>
               
                    <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Adicionar escola</button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>