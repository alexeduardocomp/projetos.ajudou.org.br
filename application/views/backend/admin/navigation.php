<div class="sidebar-menu">
    <header class="logo-env">
        <!-- logo -->
        <!-- <div class="logo" style="">
            <a href="<?php echo site_url('login'); ?>">
                <img src="<?php echo base_url('uploads/logo.png'); ?>"  style="max-height:30px;"/>
            </a>
        </div> -->

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="margin-top: 0px;">
            <a href="#" class="sidebar-collapse-icon" onclick="hide_brand()">
                <i class="entypo-menu"></i>
            </a>
        </div>
        <script type="text/javascript"> 
            function hide_brand() {
                $('#branding_element').toggle();
                $('#branding_element2').toggle();
            }
        </script>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header> 

    <div style=""></div>
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
        <!-- <li id="search">
			<form class="" action="<?php echo site_url($account_type . '/student_details'); ?>" method="post">
				<input type="text" class="search-input" name="student_identifier" placeholder="<?php echo get_phrase('student_name') . ' / ' . get_phrase('code') . '...'; ?>" value="" required style="font-family: 'Poppins', sans-serif !important; background-color: #2C2E3E !important; color: #868AA8; border-bottom: 1px solid #3F3E5F;">
				<button type="submit">
					<i class="entypo-search"></i>
				</button>
			</form>
	    </li> -->
        <div style="text-align: -webkit-center;" id="branding_element">
            <img src="<?php echo base_url('uploads/logo.png'); ?>" style="max-height:60px;" />
            <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px; font-size:14px; line-height:20px; margin-top:15px;  font-weight: 300;margin-top: 10px;">
                <?php //echo $system_name;
                ?>
                SMA - Sistema de <br />Monitoramento e Avaliação
            </h4>
        </div>

        <!-- Home -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> " style="border-top:1px solid #232540;">
            <a href="<?php echo site_url('admin/dashboard'); ?>">
                <i class="flaticon-home-2"></i>
                <span>Painel Inicial</span>
            </a>
        </li>


        <!-- Gerenciar CADASTROS -->
        <li class="<?php if (
                        $page_name == 'projeto' ||
                        $page_name == 'nucleo' ||
                        $page_name == 'cidade' ||
                        $page_name == 'local_execucao' ||
                        $page_name == 'escola' ||
                        $page_name == 'patrocinador' ||
                        $page_name == 'relacaoprojeto' ||
                        $page_name == 'relacaoescola' ||
                        $page_name == 'class' ||
                        $page_name == 'section' ||
                        $page_name == 'academic_syllabus' ||
                        $page_name == 'study_material'
                    )
                        echo 'opened active has-sub';
                    ?> ">
            <a href="#">
                <i class="fa flaticon-folder-4"></i>
                <span>Cadastros</span>
            </a>

            <!-- PROJETOS -->
            <ul>

                <li class="<?php if ($page_name == 'projeto' ||  $page_name == 'relacaoprojeto') echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-folder"></i>
                        <span>Projetos</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'projeto') echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/projeto/') ?>">
                                <span>Gerenciar Projetos</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'relacaoprojeto') echo 'active'; ?>"><a href="<?php echo site_url('admin/relacaoprojeto'); ?>">Projeto / Patrocinador</a></li>

                    </ul>
                </li>

                <!-- NÚCLEOS -->
                <li class="<?php if ($page_name == 'nucleo') echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-map"></i>
                        <span>Núcleos</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'nucleo') echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/nucleo/') ?>">
                                <span>Gerenciar Núcleos</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- PATROCINADORES -->
                <li class="<?php if ($page_name == 'patrocinador') echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-home"></i>
                        <span>Patrocinadores</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'patrocinador') echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/patrocinador/') ?>">
                                <span>Gerenciar Patrocinadores</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- CIDADES -->
                <li class="<?php if ($page_name == 'cidade') echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-placeholder"></i>
                        <span>Cidades</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'cidade') echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/cidade/') ?>">
                                <span>Gerenciar Cidades</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- LOCAIS DE EXECUÇÃO -->
                <li class="<?php if ($page_name == 'local_execucao') echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-map-location"></i>
                        <span>Local de Execução</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'local_execucao') echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/local_execucao/') ?>">
                                <span>Gerenciar Locais de Execução</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="<?php if ($page_name == 'escola' || $page_name == 'relacaoescola') echo 'opened active'; ?> ">
                    <a href="#">
                        <span><i class="flaticon-buildings"></i> Escolas</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'escola') echo 'active'; ?>"><a href="<?php echo site_url('admin/escola'); ?>">Gerenciar Escolas</a></li>
                        <li class="<?php if ($page_name == 'relacaoescola') echo 'active'; ?>"><a href="<?php echo site_url('admin/relacaoescola'); ?>">Escola / Núcleo</a></li>
                    </ul>


                </li>

                <!-- CLASS -->
                <li class="<?php
                            if (
                                $page_name == 'class' ||
                                $page_name == 'section' ||
                                $page_name == 'academic_syllabus' ||
                                $page_name == 'study_material'
                            )
                                echo 'opened active';
                            ?> ">
                    <a href="#">
                        <i class="flaticon-book"></i>
                        <span><?php //echo get_phrase('academic_&_classes'); 
                                ?>Gerenciar Turmas</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'class') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/classes'); ?>">
                                <span><i class="entypo-dot"></i> Gerenciar Turmas</span>
                            </a>
                        </li>
                        <!-- 
                        <li class="<?php if ($page_name == 'section') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/section'); ?>">
                                <span><i class="entypo-dot"></i> Gerenciar Sessões</span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'academic_syllabus') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/academic_syllabus'); ?>">
                                <span><i class="entypo-dot"></i> Programa Acadêmico</span>
                            </a>
                        </li>
                        <li class="<?php if ($page_name == 'study_material') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/study_material'); ?>">
                                <i class="entypo-dot"></i>
                                <span>Material de Estudo </span>
                            </a>
                        </li>
                        -->
                    </ul>
                </li>

            </ul>
        </li>

        <!-- Gerenciar Operacional -->
        <li class="<?php if (
                        $page_name == 'class_routine_view' ||
                        $page_name == 'manage_attendance' ||
                        $page_name == 'attendance_report' ||
                        $page_name == 'manage_attendance_view' ||
                        $page_name == 'attendance_report' ||
                        $page_name == 'attendance_report_view' ||
                        $page_name == 'manage_attendance_view' ||
                        $page_name == 'acao' ||
                        $page_name == 'subject' ||
                        $page_name == 'class_routine_add' ||
                        $page_name == 'modalidade'
                    )
                        echo 'opened active has-sub';
                    ?> ">
            <a href="#">
                <i class="fa flaticon-layers"></i>
                <span>Operacional</span>
            </a>

            <ul>
                <!-- DAILY ATTENDANCE -->
                <li class="<?php if (
                                $page_name == 'manage_attendance' ||
                                $page_name == 'manage_attendance_view' ||
                                $page_name == 'attendance_report' ||
                                $page_name == 'attendance_report_view' ||
                                $page_name == 'apagar_frequencia'
                            )
                                echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-clipboard"></i>
                        <span>Controle de Frequência</span>
                    </a>
                    <ul>
                        <li class="<?php if (($page_name == 'manage_attendance' || $page_name == 'manage_attendance_view')) echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/manage_attendance'); ?>">
                                <span><i class="entypo-dot"></i>Gerenciar Frequência</span>
                            </a>
                        </li>
                    </ul>

                    <ul>
                        <li class="<?php if (($page_name == 'apagar_frequencia')) echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/apagar_frequencia'); ?>">
                                <span><i class="entypo-dot"></i>Apagar frequência</span>
                            </a>
                        </li>
                    </ul>
                    <!--
                    <ul>
                        <li class="<?php if (($page_name == 'attendance_report' || $page_name == 'attendance_report_view')) echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/attendance_report'); ?>">
                                <span><i class="entypo-dot"></i>Relatório de Frequência</span>
                            </a>
                        </li>
                    </ul>
                    -->
                </li>

                <!-- 
                <li class="<?php if ($page_name == 'acao') echo 'active'; ?> ">
                    <a href="#">
                        <span><i class="flaticon-add"></i> Ações</span>
                    </a>
                    <ul>
                       <li class="<?php if ($page_name == 'acao') echo 'active'; ?>"><a href="<?php echo site_url('admin/acao'); ?>">Gerenciar Ações</a></li>
                       <li class="<?php if ($page_name == 'relacaoacao') echo 'active'; ?>"><a href="<?php echo site_url('admin/relacaoacao'); ?>">Ação / Núcleo</a></li>
                    </ul>
                </li> 
                -->

                <!-- SUBJECT -->
                <li class="<?php if ($page_name == 'subject') echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-comment"></i>
                        <span>Atividades</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'subject') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/subject/'); ?>">
                                <span><i class="entypo-dot"></i>
                                    Gerenciar Atividades</span>
                            </a>
                        </li>
                    </ul>
                </li>

                <!-- CLASS ROUTINE -->
                <li class="<?php if (
                                $page_name == 'class_routine_view' ||
                                $page_name == 'class_routine_add'
                            )
                                echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-calendar-3"></i>
                        <span>Plano de Aula</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'class_routine_add') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/class_routine_add'); ?>">
                                <span><i class="entypo-dot"></i>
                                    Adicionar Plano de Aula</span>
                            </a>
                        </li>


                        <?php
                        $query = $this->db->get('projeto');
                        $projetos = $query->result_array();
                        $nucleos = $this->db->get('nucleo')->result_array();
                        $classes = $this->db->get('class')->result_array();

                        foreach ($projetos as $pro) {
                            echo "<li>  <a href='#'>
                        <span>";
                            echo 'Projeto ';
                            echo $pro['nome'];
                            echo "</span>
                            </a>";

                            echo "<ul>";

                            foreach ($nucleos as $nucleo) {
                                if ($pro['id'] == $nucleo['id_projeto']) {
                                    echo '<li><a href="#">';

                                    echo '<span>';
                                    echo $nucleo['nome_nucleo'];
                                    echo "</span></a>";
                                    echo "<ul>";
                                    foreach ($classes as $clas) {
                                        if ($nucleo['id'] == $clas['id_nucleo']) {
                                            echo '<li><a href="' . site_url("admin/class_routine_view/" . $clas["class_id"]) . '">';

                                            echo '<span>';
                                            echo $clas['name'];
                                            echo "</span></a></li>";
                                        }
                                    }
                                    echo "</ul>";
                                }
                            }
                            echo "</ul>";
                            echo "</li>";
                        }
                        ?>
                    </ul>
                </li>


                <!-- MODALIDADES -->
                <li class="<?php if ($page_name == 'modalidade') echo 'opened active'; ?> ">
                    <a href="#">
                        <i class="flaticon-list-3"></i>
                        <span>Modalidades</span>
                    </a>
                    <ul>
                        <li class="<?php if ($page_name == 'modalidade') echo 'active'; ?>">
                            <a href="<?php echo site_url('admin/modalidade/') ?>">
                                <span>Gerenciar Modalidades</span>
                            </a>
                        </li>

                    </ul>
                </li>
            </ul>
        </li>

        <!-- Gerenciar Operacional -->
        <li class="<?php if (
                        $page_name == 'noticeboard' ||
                        $page_name == 'noticeboard_edit' ||
                        $page_name == 'student_add' ||
                        $page_name == 'student_bulk_add' ||
                        $page_name == 'student_information' ||
                        $page_name == 'student_marksheet' ||
                        $page_name == 'student_promotion' ||
                        $page_name == 'trocar_aluno_turma' ||
                        $page_name == 'student_profile' ||
                        $page_name == 'admin' ||
                        $page_name == 'teacher' ||
                        $page_name == 'parent' ||
                        $page_name == 'relacao'
                    )
                        echo 'opened active has-sub';
                    ?> ">
            <a href="#">
                <i class="fa flaticon-analytics"></i>
                <span>Gerenciar</span>
            </a>


            <ul>
                <!-- Manage Students -->
                <li class="<?php if (
                                $page_name == 'student_add' ||
                                $page_name == 'student_bulk_add' ||
                                $page_name == 'student_information' ||
                                $page_name == 'student_marksheet' ||
                                $page_name == 'student_promotion' ||
                                $page_name == 'trocar_aluno_turma' ||
                                $page_name == 'student_profile'
                            )
                                echo 'opened active has-sub';
                            ?> ">
                    <a href="#">
                        <i class="fa flaticon-avatar"></i>
                        <span>Gerenciar Alunos</span>
                    </a>
                    <ul>
                        <!-- STUDENT ADMISSION -->
                        <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/student_add'); ?>">
                                <span><i class="entypo-dot"></i>Cadastrar Aluno</span>
                            </a>
                        </li>

                        <!-- STUDENT BULK ADMISSION -->
                        <!--<li class="<?php if ($page_name == 'student_bulk_add') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/student_bulk_add'); ?>">
                        <span><i class="entypo-dot"></i> Cadastrar vários alunos</span>
                    </a>
                </li>-->

                        <!-- STUDENT INFORMATION -->
                        <li class="<?php if ($page_name == 'student_information' || $page_name == 'student_marksheet') echo 'opened active'; ?> ">
                            <a href="#">
                                <span><i class="entypo-dot"></i> Informações dos Alunos</span>
                            </a>
                            <ul>

                                <?php

                                $projetos = $this->db->order_by('projeto.nome', 'ASC')->get('projeto')->result_array();
                                $nucleos = $this->db->order_by('nucleo.nome_nucleo', 'ASC')->get('nucleo')->result_array();
                                $classes = $this->db->get('class')->result_array();
                                $ano = explode('-', $running_year);
                                foreach ($projetos as $pro) {
                                    $inicio = explode('-', $pro['data_inicio']);
                                    if ($inicio[0] == $ano[0]) {
                                        echo "<li>  <a href='#'>
                                <span>";
                                        echo 'Projeto ';
                                        echo $pro['nome'];
                                        echo "</span>
                            </a>";

                                        echo "<ul>";

                                        foreach ($nucleos as $nucleo) {
                                            if ($pro['id'] == $nucleo['id_projeto']) {
                                                echo '<li><a href="#">';

                                                echo '<span>';
                                                echo $nucleo['nome_nucleo'];
                                                echo "</span></a>";
                                                echo "<ul>";
                                                foreach ($classes as $clas) {
                                                    if ($nucleo['id'] == $clas['id_nucleo']) {
                                                        echo '<li><a href="' . site_url("admin/student_information/" . $clas["class_id"]) . '">';

                                                        echo '<span>';
                                                        echo $clas['name'];
                                                        echo "</span></a></li>";
                                                    }
                                                }
                                                echo "</ul>";
                                            }
                                        }
                                        echo "</ul>";
                                        echo "</li>";
                                    }
                                }
                                ?>

                                <!--<?php
                                    $classes = $this->db->get('class')->result_array();
                                    foreach ($classes as $row) :
                                    ?>
                            <li class="<?php if ($page_name == 'student_information' && $class_id == $row['class_id'] || $page_name == 'student_marksheet' && $class_id == $row['class_id']) echo 'active'; ?>">
                            -->
                                <!--<li class="<?php if ($page_name == 'student_information' && $page_name == 'student_marksheet' && $class_id == $row['class_id']) echo 'active'; ?>">-->
                                <!--<a href="<?php echo site_url('admin/student_information/' . $row['class_id']); ?>">
                                    <span>Turma <?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?>-->
                            </ul>
                        </li>

                        <!-- STUDENT PROMOTION -->
                        <li class="<?php if ($page_name == 'student_promotion') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/student_promotion'); ?>">
                                <span><i class="entypo-dot"></i>Recadastro de Turma</span>
                            </a>
                        </li>

                        <li class="<?php if ($page_name == 'trocar_aluno_turma') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/trocar_aluno_turma'); ?>">
                                <span><i class="entypo-dot"></i>Trocar Aluno de Turma</span>
                            </a>
                        </li>

                    </ul>
                </li>

                <!-- Manage Users -->
                <li class="<?php if (
                                $page_name == 'admin' ||
                                $page_name == 'teacher' ||
                                $page_name == 'parent' ||
                                $page_name == 'librarian' ||
                                $page_name == 'accountant' ||
                                $page_name == 'teacher' ||
                                $page_name == 'relacao'
                            )
                                echo 'opened active has-sub'; ?>">
                    <a href="#">
                        <i class="fa flaticon-users"></i>
                        <span>Gerenciar Usuários</span>
                    </a>
                    <ul>

                        <!-- manage teachers -->
                        <li class="<?php if (
                                        $page_name == 'teacher' ||
                                        $page_name == 'relacao'
                                    ) echo 'opened active'; ?> ">
                            <a href="#">
                                <span><i class="entypo-dot"></i> Instrutor</span>
                            </a>
                            <ul>
                                <li class="<?php if ($page_name == 'teacher') echo 'active'; ?>"><a href="<?php echo site_url('admin/teacher'); ?>">Gerenciar Instrutores</a></li>
                                <li class="<?php if ($page_name == 'relacao') echo 'active'; ?>"><a href="<?php echo site_url('admin/relacao'); ?>">Instrutor / Núcleo</a></li>
                            </ul>


                        </li>
                        <!-- manage parents -->
                        <li class="<?php if ($page_name == 'parent') echo 'active'; ?> ">
                            <a href="<?php echo site_url('admin/parent'); ?>">
                                <i class="entypo-dot"></i>
                                <span>Responsável</span>
                            </a>
                        </li>

                        <!-- librarian -->
                        <!--
                <li class="<?php if ($page_name == 'librarian') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/librarian'); ?>">
                        <i class="fa entypo-dot"></i>
                        <span><?php echo get_phrase('librarian'); ?></span>
                    </a>
                </li>-->
                        <!-- accountant -->
                        <!--
                <li class="<?php if ($page_name == 'accountant') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/accountant'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('accountant'); ?></span>
                    </a>-->
                </li>
            </ul>
        </li>

        <!-- EXAMS

        <li class="<?php
                    if (
                        $page_name == 'exam' ||
                        $page_name == 'grade' ||
                        $page_name == 'marks_manage' ||
                        $page_name == 'exam_marks_sms' ||
                        $page_name == 'tabulation_sheet' ||
                        $page_name == 'marks_manage_view' || $page_name == 'question_paper'
                    )
                        echo 'opened active';
                    ?> ">
            <a href="#">
                <i class="flaticon-edit"></i>
                <span><?php echo get_phrase('exam'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'exam') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/exam'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_list'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'grade') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/grade'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('exam_grades'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'marks_manage' || $page_name == 'marks_manage_view') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/marks_manage'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_marks'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'exam_marks_sms') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/exam_marks_sms'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('send_marks_by_sms'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'tabulation_sheet') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/tabulation_sheet'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('tabulation_sheet'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'question_paper') echo 'active'; ?>">
                    <a href="<?php echo site_url('admin/question_paper'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('question_paper'); ?></span>
                    </a>
                </li>
            </ul>
        </li>

    -->
        <!-- ONLINE EXAMS

        <li class="<?php if ($page_name == 'manage_online_exam' || $page_name == 'add_online_exam' || $page_name == 'edit_online_exam' || $page_name == 'manage_online_exam_question' || $page_name == 'update_online_exam_question' || $page_name == 'view_online_exam_results') echo 'opened active'; ?> ">
            <a href="#">
                <i class="flaticon-placeholder-2"></i>
                <span><?php echo get_phrase('online_exam'); ?></span>
            </a>
            <ul>
            <li class="<?php if ($page_name == 'add_online_exam') echo 'active'; ?> ">
              <a href="<?php echo site_url($account_type . '/create_online_exam'); ?>">
                  <span><i class="entypo-dot"></i> <?php echo get_phrase('create_online_exam'); ?></span>
              </a>
            </li>

            <li class="<?php if ($page_name == 'manage_online_exam' || $page_name == 'edit_online_exam' || $page_name == 'manage_online_exam_question' || $page_name == 'view_online_exam_results') echo 'active'; ?> ">
                <a href="<?php echo site_url($account_type . '/manage_online_exam'); ?>">
                    <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_online_exam'); ?></span>
                </a>
            </li>
            </ul>
        </li>
        -->

        <!-- PAYMENT -->
        <!-- <li class="<?php //if ($page_name == 'invoice') echo 'active'; 
                        ?> ">
            <a href="<?php //echo base_url(); 
                        ?>index.php?admin/invoice">
                <i class="entypo-credit-card"></i>
                <span><?php //echo get_phrase('payment'); 
                        ?></span>
            </a>
        </li> -->

        <!-- ACCOUNTING
        <li class="<?php
                    if (
                        $page_name == 'income' ||
                        $page_name == 'expense' ||
                        $page_name == 'expense_category' ||
                        $page_name == 'student_payment'
                    )
                        echo 'opened active';
                    ?> ">
            <a href="#">
                <i class="flaticon-statistics"></i>
                <span><?php echo get_phrase('accounting'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'student_payment') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/student_payment'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('create_student_payment'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'income') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/income'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('student_payments'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'expense') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/expense'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('expense'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'expense_category') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/expense_category'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('expense_category'); ?></span>
                    </a>
                </li>
            </ul>
        </li>
        -->

        <!--Manage back office -->
        <li class="<?php if (
                        $page_name == 'book' ||
                        $page_name == 'transport' ||
                        $page_name == 'dormitory' ||
                        $page_name == 'noticeboard' ||
                        $page_name == 'noticeboard_edit' ||
                        $page_name == 'message' ||
                        $page_name == 'group_message'
                    )
                        echo 'opened active has-sub'; ?>">
            <a href="#">
                <i class="flaticon-imac"></i>
                <span>Gerenciar Eventos</span>
            </a>
            <ul>
                <!-- LIBRARY
                <li class="<?php if ($page_name == 'book') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/book'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('library'); ?></span>
                    </a>
                </li>  -->

                <!-- TRANSPORT 
                <li class="<?php if ($page_name == 'transport') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/transport'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('transport'); ?></span>
                    </a>
                </li>
                -->
                <!-- DORMITORY 
                <li class="<?php if ($page_name == 'dormitory') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/dormitory'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('dormitory'); ?></span>
                    </a>
                </li>
                -->

                <li class="<?php if ($page_name == 'noticeboard' || $page_name == 'noticeboard_edit') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/noticeboard'); ?>">
                        <i class="entypo-dot"></i>
                        <span>Quadro de Eventos</span>
                    </a>
                </li>

                <!-- MESSAGE 
                <li class="<?php if ($page_name == 'message' || $page_name == 'group_message') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/message'); ?>">
                        <i class="entypo-dot"></i>
                        <span><?php echo get_phrase('message'); ?></span>
                    </a>
                </li>-->
            </ul>
        </li>

    </ul>
    </li>

    <!-- RELATÓRIOS 
    <li class="<?php if (
                    $page_name == 'manage_relatorio' ||
                    $page_name == 'manage_relatorio_view' ||
                    $page_name == 'manage_relatorio_projetos' ||
                    $page_name == 'manage_relatorio_frequencia' || $page_name == 'relatorio_report' || $page_name == 'relatorio_report_view'
                )
                    echo 'opened active'; ?> ">
        <a href="#">
            <i class="flaticon-graphic-1"></i>
            <span>Relatórios</span>
        </a>
        <ul>

            <li class="<?php if (($page_name == 'manage_relatorio_projetos' || $page_name == 'manage_relatorio_projetos_view')) echo 'active'; ?>">
                <a href="<?php echo site_url('admin/manage_relatorio_projetos'); ?>">
                    <span><i class="entypo-dot"></i>Beneficiados</span>
                </a>
            </li>

            <li class="<?php if (($page_name == 'manage_relatorio_frequencia' || $page_name == 'manage_relatorio_frequencia_view')) echo 'active'; ?>">
                <a href="<?php echo site_url('admin/manage_relatorio_frequencia'); ?>">
                    <span><i class="entypo-dot"></i>Lista de Frequência</span>
                </a>
            </li>

            <li class="<?php if (($page_name == 'manage_relatorio' || $page_name == 'manage_relatorio_view')) echo 'active'; ?>">
                <a href="<?php echo site_url('admin/manage_relatorio'); ?>">
                    <span><i class="entypo-dot"></i>Lista de Presença (Eventos)</span>
                </a>
            </li>

        </ul>
    </li>
    -->

    <!-- Relatórios e Gráficos -->
    <li class="<?php

                if (
                    $page_name == 'manage_relatorio' ||
                    $page_name == 'manage_relatorio_view' ||
                    $page_name == 'manage_relatorio_frequencia' ||
                    $page_name == 'relatorio_report' ||
                    $page_name == 'relatorio_report_view' ||
                    $page_name == 'manage_graficos' ||
                    $page_name == 'manage_relatorio_projetos_federal' ||
                    $page_name == 'manage_relatorio_frequencia_federal' ||
                    $page_name == 'manage_relatorio_plano_aula_federal' ||
                    $page_name == 'manage_relatorio_projetos_estadual' ||
                    $page_name == 'manage_relatorio_frequencia_estadual' ||
                    $page_name == 'manage_relatorio_plano_aula_estadual' ||
                    $page_name == 'manage_relatorio_convenio' ||
                    $page_name == 'manage_relatorio_frequencia_convenio' ||
                    $page_name == 'manage_relatorio_plano_aula_convenio'
                )
                    echo 'opened active has-sub';
                ?> ">
        <a href="#">
            <i class="flaticon-graphic-1"></i>
            <span>Relatórios e Gráficos</span>
        </a>

        <ul>
            <!-- Relatórios Federais -->
            <li class="<?php if (
                            $page_name == 'manage_relatorio_projetos_federal' ||
                            $page_name == 'manage_relatorio_frequencia_federal' ||
                            $page_name == 'manage_relatorio_plano_aula_federal'
                        )
                            echo 'opened active';
                        ?> ">
                <a href="#">
                    <i class="fa flaticon-notes"></i>
                    <span>Federais</span>
                </a>
                <ul>
                    <li class="<?php if (($page_name == 'manage_relatorio_projetos_federal' || $page_name == 'manage_relatorio_projetos_view')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio_projetos_federal'); ?>">
                            <span><i class="entypo-dot"></i>Beneficiados</span>
                        </a>
                    </li>

                    <li class="<?php if (($page_name == 'manage_relatorio_frequencia_federal' || $page_name == 'manage_relatorio_frequencia_view')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio_frequencia_federal'); ?>">
                            <span><i class="entypo-dot"></i>Lista de Frequência</span>
                        </a>
                    </li>

                    <li class="<?php if ($page_name == 'manage_relatorio_plano_aula_federal') echo 'active'; ?> ">
                        <a href="<?php echo site_url('admin/manage_relatorio_plano_aula_federal'); ?>">
                            <i class="entypo-dot"></i>
                            <span>Plano de Aula</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- /. Relatórios Federais -->

            <!-- Relatórios Estaduais -->
            <li class="<?php if (
                            $page_name == 'manage_relatorio_projetos_estadual' ||
                            $page_name == 'manage_relatorio_frequencia_estadual' ||
                            $page_name == 'manage_relatorio_plano_aula_estadual'
                        )
                            echo 'opened active has-sub'; ?>">
                <a href="#">
                    <i class="fa flaticon-notepad"></i>
                    <span>Estaduais</span>
                </a>
                <ul>
                    <li class="<?php if (($page_name == 'manage_relatorio_projetos_estadual' || $page_name == 'manage_relatorio_projetos_view')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio_projetos_estadual'); ?>">
                            <span><i class="entypo-dot"></i>Beneficiados</span>
                        </a>
                    </li>

                    <li class="<?php if (($page_name == 'manage_relatorio_frequencia_estadual' || $page_name == 'manage_relatorio_frequencia_view')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio_frequencia_estadual'); ?>">
                            <span><i class="entypo-dot"></i>Lista de Frequência</span>
                        </a>
                    </li>

                    <li class="<?php if ($page_name == 'manage_relatorio_plano_aula_estadual') echo 'active'; ?> ">
                        <a href="<?php echo site_url('admin/manage_relatorio_plano_aula_estadual'); ?>">
                            <i class="entypo-dot"></i>
                            <span>Plano de Aula</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- /. Relatórios Estaduais -->

            <!-- Relatórios Convênios -->
            <li class="<?php if (
                            $page_name == 'manage_relatorio_convenio' ||
                            $page_name == 'manage_relatorio_frequencia_convenio' ||
                            $page_name == 'manage_relatorio_plano_aula_convenio'
                        )
                            echo 'opened active has-sub'; ?>">
                <a href="#">
                    <i class="fa flaticon-clipboard"></i>
                    <span>Convênios</span>
                </a>
                <ul>
                    <li class="<?php if (($page_name == 'manage_relatorio_convenio' || $page_name == 'manage_relatorio_convenio_view')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio_convenio'); ?>">
                            <span><i class="entypo-dot"></i>Beneficiados</span>
                        </a>
                    </li>

                    <li class="<?php if (($page_name == 'manage_relatorio_frequencia_convenio' || $page_name == 'manage_relatorio_convenio_view')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio_frequencia_convenio'); ?>">
                            <span><i class="entypo-dot"></i>Lista de Frequência</span>
                        </a>
                    </li>

                    <li class="<?php if ($page_name == 'manage_relatorio_plano_aula_convenio') echo 'active'; ?> ">
                        <a href="<?php echo site_url('admin/manage_relatorio_plano_aula_convenio'); ?>">
                            <i class="entypo-dot"></i>
                            <span>Plano de Aula</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- /. Relatórios Convênios -->


            <!-- Gráficos -->
            <li class="<?php if (
                            $page_name == 'manage_graficos'

                        )
                            echo 'opened active has-sub'; ?>">
                <a href="#">
                    <i class="flaticon-graphic"></i>
                    <span>Gráficos</span>
                </a>
                <ul>
                    <li class="<?php if ($page_name == 'manage_graficos') echo 'active'; ?> ">
                        <a href="<?php echo site_url('admin/manage_graficos'); ?>">
                            <i class="entypo-dot"></i>
                            <span>Gráficos</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- /. Gráficos -->

            <!-- Outros Relatórios -->
            <li class="<?php if (
                            $page_name == 'manage_relatorio' || $page_name == 'manage_relatorio_view'
                        )
                            echo 'opened active has-sub'; ?>">
                <a href="#">
                    <i class="flaticon-file-2"></i>
                    <span>Outros</span>
                </a>
                <ul>
                    <li class="<?php if (($page_name == 'manage_relatorio' || $page_name == 'manage_relatorio_view')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio'); ?>">
                            <span><i class="entypo-dot"></i>Lista de Presença (Eventos)</span>
                        </a>
                    </li>
                    <li class="<?php if (($page_name == 'manage_relatorio_assiduidade' || $page_name == 'manage_relatorio_assiduidade')) echo 'active'; ?>">
                        <a href="<?php echo site_url('admin/manage_relatorio_assiduidade'); ?>">
                            <span><i class="entypo-dot"></i>Assiduidade por Projeto</span>
                        </a>
                    </li>
                </ul>
            </li>
            <!-- /. Outros Relatórios -->
        </ul>
    </li>
    <!-- Relatórios e Gráficos -->

    <!-- SETTINGS -->
    <li class="<?php
                if (
                    $page_name == 'system_settings' ||
                    $page_name == 'frontend_pages' ||
                    $page_name == 'manage_language' ||
                    $page_name == 'sms_settings' ||
                    $page_name == 'payment_settings' ||
                    $page_name == 'smtp_settings'
                )
                    echo 'opened active';
                ?> ">
        <a href="#">
            <i class="flaticon-interface-7"></i>
            <span>Configurações</span>
        </a>
        <ul>
            <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                <a href="<?php echo site_url('admin/system_settings'); ?>">
                    <span><i class="entypo-dot"></i>Configurações Gerais</span>
                </a>
            </li>
            <!--<li class="<?php if ($page_name == 'frontend_pages') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/frontend_pages'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('school_website'); ?></span>
                    </a>
                </li>-->
            <!-- <li class="<?php if ($page_name == 'sms_settings') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/sms_settings'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('sms_settings'); ?></span>
                    </a>
                </li>-->
            <!--<li class="<?php if ($page_name == 'manage_language') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/manage_language'); ?>">
                        <span><i class="entypo-dot"></i>Configurações de linguagem</span>
                    </a>
                </li>-->
            <!--<li class="<?php if ($page_name == 'payment_settings') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/payment_settings'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('payment_settings'); ?></span>
                    </a>
                </li>-->
            <!--<li class="<?php if ($page_name == 'smtp_settings') echo 'active'; ?> ">
                    <a href="<?php echo site_url('admin/smtp_settings'); ?>">
                        <span><i class="entypo-dot"></i> Configurações de SMTP</span>
                    </a>
                </li>-->
        </ul>
    </li>

    <!-- VIDEOS -->
    <li class="<?php
                if (
                    $page_name == 'video_add' ||
                    $page_name == 'video_information' ||
                    $page_name == 'video_galery' 
                )
                    echo 'opened active';
                ?> ">
        <a href="#">
            <i class="entypo-video"></i>
            <span>Vídeos</span>
        </a>
        <ul>
            <li class="<?php if (($page_name == 'video_add') || ($page_name == 'video_information')) echo 'active'; ?> ">
                <a href="<?php echo site_url('admin/video_information'); ?>">
                    <span><i class="entypo-help-circled"></i>Gerenciar Tutoriais</span>
                </a>
            </li>
            <!-- <li class="<?php if ($page_name == 'system_settings') echo 'active'; ?> ">
                <a href="<?php echo site_url('admin/video_galery'); ?>">
                    <span><i class="entypo-folder"></i>Gerenciar Gravações</span>
                </a>
            </li> -->
        </ul>
    </li>


    <!-- Session selector -->
    <li class="root-level" style="border-top: 1px solid #262a44; padding: 10px 0px;text-align: -webkit-center;">
        <?php echo form_open(site_url('admin/change_session'), array('id' => 'session_change')); ?>
    <li>

        <div class="form-group" style="margin-top: 15px; margin-bottom: 10px;">
            <select name="running_year" class="form-control" onchange="submit()" style="width: 60%;background-color: #232640;border-color: #242742;color: #a1a2b6;">
                <?php $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description; ?>
                <?php for ($i = 0; $i < 10; $i++) : ?>
                    <option value="<?php echo (2019 + $i); ?>-<?php echo (2019 + $i + 1); ?>" <?php if ($running_year == (2019 + $i) . '-' . (2019 + $i + 1)) echo 'selected'; ?>>
                        Ano : <?php echo (2019 + $i); ?>-<?php echo (2019 + $i + 1); ?>
                    </option>
                <?php endfor; ?>
            </select>
        </div>
    </li>
    <?php echo form_close(); ?>
    </li>

    <div class="img_app" style="text-align:left; margin-left: 20px; margin-top: 20px;" id="branding_element2">
        <a href="http://appmarketing.com.br" target="_blank"><img src="<?php echo base_url('uploads/logo-app.png'); ?>" style="max-height:23px;" /></a>
        <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px;font-weight: 300;margin-top: 10px;">
        </h4>
    </div>

    </ul>
</div>