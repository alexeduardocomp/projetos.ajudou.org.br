<?php
$query = $this->db->get('projeto');
$projetos = $query->result();

$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();

$query3 = $this->db->get('acao');
$acoes = $query3->result();
?>


<script type="text/javascript" src="http://localhost/sistema-ajudou/6.2/Ekattor/assets/js/jquery.mask.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".cpf").mask('000.000.000-00');
		$('.rg').mask('AA.000.000-A');
		$('.tel').mask('(00) 00000-0000');
		$('.cep').mask('00000-000');
		$('.numeracao').mask('00.00');
	});
</script>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="entypo-plus-circled"></i>
					Relacionar ação com núcleo
				</div>
			</div>
			<div class="panel-body">

				<?php echo form_open(site_url('admin/relacaoacao/create/'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Projeto</label>
					<div class="col-sm-5">
						<select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option selected disabled>Selecione</option>
							<?php
							$tama = count($projetos);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
							}
							?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Núcleo</label>
					<div class="col-sm-5">
						<select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option disabled selected>Selecione um projeto</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Acao</label>
					<div class="col-sm-5">
						<select name="id_acao[]" class="form-control" id="esc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" multiple>
							<option selected disabled>Selecione</option>
							<?php
							$tama = count($acoes);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $acoes[$i]->id . '>' . $acoes[$i]->acao . '</option>';
							}
							?>
						</select>
						<div style="margin-top:10px;">Segure a tecla Ctrl e selecione mais de uma ação!</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Quantidade</label>
					<div class="col-sm-5">
						<input type="text" class="form-control quantidade" name="quantidade" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="" maxlength="5" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode))) return true; else return false;">
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Numeração</label>
					<div class="col-sm-5">
						<input type="text" class="form-control numeracao" name="numeracao" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-5">
						<button type="submit" class="btn btn-default">Adicionar Relação</button>
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$("#pro").change(function() {

		var id_projeto = $("#pro").val();

		$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {

				$("#nuc").empty();
				data = $.parseJSON(data);

				for (var i = 0; i < data.length; i++) {
					$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
				}

				var id_nucleo = $("#nuc").val();

				$.ajax('<?php echo site_url('admin/get_acao_nucleo/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: id_nucleo
					}, // data to submit
					success: function(data2, status, xhr) {
						$("#esc").empty();
						data2 = $.parseJSON(data2);

						for (var i = 0; i < data2.length; i++) {
							$("#esc").append('<option value=' + data2[i]['id_acao'] + '>' + data2[i]['nome_acao'] + '</option>');
						}
					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});
	});

	$("#nuc").change(function() {
		var id_nucleo = $("#nuc").val();

		$.ajax('<?php echo site_url('admin/get_acao_nucleo/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo
			}, // data to submit
			success: function(data2, status, xhr) {

				$("#esc").empty();
				data2 = $.parseJSON(data2);

				for (var i = 0; i < data2.length; i++) {
					$("#esc").append('<option value=' + data2[i]['id_acao'] + '>' + data2[i]['nome_acao'] + '</option>');
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});
	});
</script>