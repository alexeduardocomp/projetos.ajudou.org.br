<?php

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(1);
ini_set('display_errors', 1);

// ==========================================================
$id_projeto  = html_escape($this->input->post('id_projeto'));
$nucleos = html_escape($this->input->post('id_nucleo'));
$tipo_escola  = html_escape($this->input->post('tipo_escola'));
$modalidades  = html_escape($this->input->post('modalidade_id'));
$turmas  = html_escape($this->input->post('class_id'));
$sexo  = html_escape($this->input->post('sexo'));
$situacao  = html_escape($this->input->post('situacao'));
$idade_inicial = html_escape($this->input->post('idade_inicial'));
$idade_final  = html_escape($this->input->post('idade_final'));
$tipo  = html_escape($this->input->post('tipo'));
$escolas  = html_escape($this->input->post('escola_id'));
$instrutores  = html_escape($this->input->post('instrutores'));
$local_execucao  = html_escape($this->input->post('local_execucao'));


$data_inicial = $this->input->post('data_inicial');
$data_final = $this->input->post('data_final');

$ano = html_escape($this->input->post('sessional_year'));
$ano1 = $ano + 1;
$ano_utilizado = $ano . '-' . $ano1;

$projeto = $id_projeto;
$ano = $ano;
$ano1 = $ano + 1;


echo "<br>";
echo "id_projeto: " . $id_projeto;
echo "<br>";
echo "nucleos: " . var_dump($nucleos);
echo "<br>";
echo "tipo_escola: " . $tipo_escola;
echo "<br>";
echo "modalidades: " . var_dump($modalidades);
echo "<br>";
echo "turmas: " . var_dump($turmas);
echo "<br>";
echo "sexo: " . $sexo;
echo "<br>";
//echo "situacao: " . $situacao;
//echo "<br>";
echo "idade_inicial: " . $idade_inicial;
echo "<br>";
echo "idade_final: " . $idade_final;
echo "<br>";
echo "escolas: " . var_dump($escolas);
echo "<br>";

echo "instrutores: " . var_dump($instrutores);
echo "<br>";



// ================================= Monta SQL =================================
$where = "";

// WHERE Nucleos
if (!empty($nucleos) || $nucleos != null || $nucleos != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($nucleos as $nuc) {
    if ($contador == 1) {
      $where .= "student.id_nucleo = " . $nuc;
    } else {
      $where .= " OR student.id_nucleo = " . $nuc;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Instrutores
if (!empty($instrutores) || $instrutores != null || $instrutores != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($instrutores as $inst) {
    if ($contador == 1) {
      $where .= "class.teacher_id = " . $inst;
    } else {
      $where .= " OR class.teacher_id = " . $inst;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Locais de execucao
if (!empty($local_execucao) || $local_execucao != null || $local_execucao != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($local_execucao as $local) {
    if ($contador == 1) {
      $where .= "local_execucao.id = " . $local;
    } else {
      $where .= " OR local_execucao.id = " . $local;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Tipo de Escola
if ($tipo_escola != "AMBOS") {
  $where .= " AND (escola.tipo_escola = '" . $tipo_escola . "'";
  $where .= ")";
}

// WHERE Modalidades
if (!empty($modalidades) || $modalidades != null || $modalidades != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($modalidades as $mod) {
    if ($contador == 1) {
      $where .= "modalidade.id = " . $mod;
    } else {
      $where .= " OR modalidade.id = " . $mod;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Turmas
if (!empty($turmas) || $turmas != null || $turmas != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($turmas as $turm) {
    if ($contador == 1) {
      $where .= "class.class_id = " . $turm;
    } else {
      $where .= " OR class.class_id = " . $turm;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Sexo
if ($sexo != "AMBOS") {
  $where .= " AND (student.sex = '" . $sexo . "'";
  $where .= ")";
}

// WHERE Situação
if ($situacao != "AMBOS") {
  $where .= " AND (student.situacao = '" . $situacao . "'";
  $where .= ")";
}

// WHERE Idade
if (!empty($idade_final) || $idade_final != null || $idade_final != "" || !empty($idade_inicial) || $idade_inicial != null || $idade_inicial != "") {
  $where .= " AND (YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(student.data_nascimento))) BETWEEN '" . $idade_inicial . "' AND '" . $idade_final . "')";
}


// WHERE Escolas
if (!empty($escolas) || $escolas != null || $escolas != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($escolas as $esc) {
    if ($contador == 1) {
      $where .= "escola.id = " . $esc;
    } else {
      $where .= " OR escola.id = " . $esc;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Data
if (!empty($data_inicial) || $data_inicial != null || $data_inicial != "" || !empty($data_final) || $data_final != null || $data_final != "") {
  $where .= " AND DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '" . $data_inicial . "' AND '" . $data_final . "'";
}


//FROM student, class, modalidade, enroll 
$sql = " SELECT student.student_id, class.tipo_material as material, student.name as nome, modalidade.modalidade as modalidade,class.name as turma, student.cidade, student.situacao, student.estado, student.rua, student.bairro, student.numero, student.complemento, student.phone as telefone, student.data_nascimento 
FROM student 
INNER JOIN enroll ON student.student_id = enroll.student_id
INNER JOIN class ON enroll.class_id = class.class_id
INNER JOIN modalidade ON class.modalidade = modalidade.id
INNER JOIN local_execucao ON local_execucao.id = class.id_local
INNER JOIN escola ON student.id_escola = escola.id
WHERE class.projeto_id = " . $projeto . ""
  . $where . " 
GROUP BY student.student_id
order by student.name ASC";

echo "" . $sql;
echo "<br>";

$query  =  $this->db->query($sql);

// ================================= Resultado =================================

$resultado =  $query->result();

/*
echo "<pre>";
var_dump($resultado);
echo "</pre>";
*/

$nome_projeto = $this->db->get_where('projeto', array('id' => $projeto))->row()->nome;
$proponente = $this->db->get_where('projeto', array('id' => $projeto))->row()->proponente;
$tipo_projeto = $this->db->get_where('projeto', array('id' => $projeto))->row()->tipo_projeto;
$data_inicio_projeto = $this->db->get_where('projeto', array('id' => $projeto))->row()->data_inicio;
$num_slie = $this->db->get_where('projeto', array('id' => $projeto))->row()->numero;

$data_atual = date('Y/m/d');

$cont4 = 1;
foreach ($resultado as $res) {
  $dataNascimento = $res->data_nascimento;
  $date = new DateTime($dataNascimento);
  $interval = $date->diff(new DateTime(date('Y-m-d')));
  $idade = $data_atual - $res->data_nascimento;

  $sql = "SELECT class.class_id,class.name as nome_turma,modalidade.modalidade FROM `enroll` 
  INNER JOIN class ON enroll.class_id = class.class_id
  INNER JOIN modalidade ON class.modalidade = modalidade.id
  WHERE enroll.student_id=" . $res->student_id . " AND class.projeto_id=".$projeto." ORDER BY date_added DESC LIMIT 1";

  // echo $sql;

  // echo "<br>";
  $query = $this->db->query($sql);
  $ultima_turma_aluno = $query->result();

  //marcar tipo de material  
  if($res->material === 'ambos'){
    $marcarx = '<tr><td style="width: 50px; height: 100%;text-align: center;">X</td><td style="width: 50%; text-align: center;">X</td></tr>';
  }else{
    if($res->material === 'uniforme'){
      $marcarx = '<tr><td style="width: 50px;text-align: center;">X</td><td style="width: 50px;text-align: center;">-</td></tr>';
    }elseif($res->material === 'quimono'){
      $marcarx = '<tr><td style="width: 50px; height: 100%;text-align: center;">-</td><td style="width: 50%; text-align: center;">X</td></tr>';
    }else{
      $marcarx = '<tr><td ><br/><br/><br/></td><td><br/><br/><br/></td></tr>';
    }
  }

  $conteudo = $conteudo . '<tr><td style="text-align:center; font-size:10px;">' . $cont4 . '</td><td style="text-align:left; font-size:10px; padding-left:10px;">' . strtoupper($res->nome) . '</td><td style="text-align:center; font-size:10px; padding:5px;">' . $ultima_turma_aluno[0]->modalidade . '</td><td style="text-align:center; font-size:10px; padding:5px;">' . $ultima_turma_aluno[0]->nome_turma . '</td><td style="text-align:left; font-size:10px; padding:5px;">' . $res->cidade . ' - ' . $res->estado . ', Rua ' . $res->rua . ', ' . $res->bairro . ', ' . $res->numero . ', </td><td style="text-align:center; font-size:10px; padding:5px;">' . $res->telefone . '</td><td style="text-align:center; font-size:10px; padding:5px;">' . $interval->format('%Y anos') . '</td><td style="padding:0; height:100%;"><table border="1" width="100%" style="height:100%;">'.$marcarx.'</table></td><td></td></tr>';
  $cont4++;
}

$corpo = '<table border="1" width="100%" height="100%">';
$cabeca = '<tr><td style="text-align:center;"><b>#</b></td>
                            <td style="text-align:center;"><b>Nome</b></td>
                            <td style="text-align:center; "><b>Modalidade</b></td>
                            <td style="text-align:center; "><b>Turma</b></td>
                            <td style="text-align:center; "><b>Endereço</b></td>
                           
                            <td style="text-align:center;"><b>Telefone</b></td>
                            <td style="text-align:center;"><b>Idade</b></td>
                            <td style="text-align:center;"><table border="1"><tr><td COLSPAN="2"><b>Benefícios</b></td></tr><tr><td width="50%" style="font-size:12px;">Uniforme</td><td width="50%" style="font-size:12px;">Quimono</td></tr></table></td>
                            <td style="text-align:center; width:20%;"><b>OBS</b></td></tr>';



$corpo3 = '</table>';

$data_ass = "<div style='position:absolute; bottom:120px; width:100%; left:70px; text-align:left;'>Local - Data: _____________________________,_______ de _____________________ de ___________</div>";

$assinatura = "<div style='text-align:left; position:absolute; width:100%; bottom:80px; left:70px;'>Assinatura do Profissional(is) responsável(is): ________________________________________________________________________________</div>";
$total = $corpo . $cabeca . $conteudo . $corpo3 . $data_ass . $assinatura;


$parcial_final = "";

if ($tipo == "PARCIAL") {
  $parcial_final = '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Projeto: </b> ' . $nome_projeto . '</p></div></td><td>PARCIAL ( X )</td></tr>
  <tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Proponente: </b> ' . $proponente . '</p></div></td><td>FINAL (   )</td></tr>
  ';
} else {
  $parcial_final = '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Projeto: </b> ' . $nome_projeto . '</p></div></td><td>PARCIAL (   )</td></tr>
  <tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Proponente: </b> ' . $proponente . '</p></div></td><td>FINAL ( X )</td></tr>
  ';
}

$periodo = ""; 

if (!empty($data_inicial) || $data_inicial != null || $data_inicial != "" || !empty($data_final) || $data_final != null || $data_final != "") {
  $periodo = '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Período: </b> ' . date("d/m/Y", strtotime($data_inicial)) . ' - ' .  date("d/m/Y", strtotime($data_final)) . '</p></div></td><td>'.($tipo_projeto === "FEDERAL"?'N° SLIE: '.$num_slie .'':"").'</td></tr>';
} else {
  $periodo =  '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Período: </b> ' . date("d/m/Y", strtotime($data_inicio_projeto)) . ' - ' .  date("d/m/Y") . '</p></div></td><td>'.($tipo_projeto === "FEDERAL"?'N° SLIE: '.$num_slie .'':"").'</td></tr>';
}


if ($tipo_projeto == "CONVÊNIO") {
  $cabecalho = '
<table border="1" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td width="170" style="text-align:center;">
<div class="imagem" ><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
</td>
<td style="text-align:center;">
<div class="titulo" style="text-transform:uppercase;">RELAÇÃO DE DIRETAMENTE BENEFICIADOS</div>
</td>
</tr>
<tr>
<td style="padding:10px 15px;" width="170">
Críterios utilizados:
</td>
<td style="padding:10px 15px;">
<table border="0" cellspacing="0" cellpadding="0" width="100%">';

  $cabecalho .= $parcial_final;
  $cabecalho .= $periodo;


  $cabecalho .=
    '</table>
</td>
</tr>
</table>';

  $mpdf = new \Mpdf\Mpdf([
    'margin_top' => '5',
    'debug' => true,
    'setAutoTopMargin' => 'pad',
    'pagenumPrefix' => 'Página ',
    'pagenumSuffix' => ' - ',
    'nbpgPrefix' => ' de ',
    'nbpgSuffix' => ' páginas / '
  ]);
} else {

  if ($tipo_projeto == "FEDERAL") {
    $logo = base_url() . "uploads/logos/federal.jpg";
  } else {
    $logo = base_url() . "uploads/logos/estadual.jpg";
  }

  $cabecalho = '
<table border="1" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td width="170" style="text-align:center;">
<div class="imagem" ><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
</td>
<td style="text-align:center;">
<div class="titulo" style="text-transform:uppercase;">RELAÇÃO DE DIRETAMENTE BENEFICIADOS</div>
</td>
<td ROWSPAN="2" width="120" style="text-align:center;">
<div><img src="' . $logo . '" width="100" style="padding:5px 0px;"></div>
</td>
</tr>
<tr>
<td style="padding:10px 15px;" width="170">
Críterios utilizados:
</td>
<td style="padding:10px 15px;">
<table border="0" cellspacing="0" cellpadding="0" width="100%">';

  $cabecalho .= $parcial_final;
  $cabecalho .= $periodo;

  $cabecalho .=
    '</table>
</td>
</tr>
</table>';

  $mpdf = new \Mpdf\Mpdf([
    'margin_top' => '0',
    'debug' => true,
    'setAutoTopMargin' => 'pad',
    'pagenumPrefix' => 'Página ',
    'pagenumSuffix' => ' - ',
    'nbpgPrefix' => ' de ',
    'nbpgSuffix' => ' páginas / '
  ]);
}

$mpdf->SetHTMLHeader($cabecalho);
$mpdf->shrink_tables_to_fit = 1.4;
$mpdf->AddPage('L');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');
$mpdf->WriteHTML($total, \Mpdf\HTMLParserMode::HTML_BODY);
ob_clean();
$mpdf->Output();
