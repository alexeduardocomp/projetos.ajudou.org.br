<?php 
	$query = $this
  ->db
  ->order_by('projeto.nome', 'ASC')
  ->get('projeto');
	$projetos = $query->result();




		$query2 = $this->db->get('nucleo');
	$nucleos = $query2->result();



?>

<div class="panel panel-primary">
    <div class="panel-heading">
        Presença da
    </div>
    <div class="panel-body">
    	
		<div class="row">

			<div class="col-md-3">

<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Projeto</label>
                                  <div class="col-sm-12">
									<select name="id_projeto" class="form-control" id="pro"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

										<option selected disabled>Selecione</option>
									<?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>
  									  
									</select>
								</div>

								</div>
							</div>
			


			<div class="col-md-3">


						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                                  <div class="col-sm-12">
									<select name="id_nucleo" class="form-control" id="nuc"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
											<option disabled selected>Selecione um projeto</option>

  									  
									</select>
								</div>

								</div>
			</div>

      <div class="col-md-3">


            <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Evento</label>
                                  <div class="col-sm-12">
                  <select name="id_evento" class="form-control" id="eve"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                      <option disabled selected>Selecione um núcleo</option>

                      
                  </select>
                </div>

                </div>
      </div>

			<div class="col-md-3">
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Turma</label>
                                  <div class="col-sm-12">
									<select name="class_id" class="form-control" id="turm"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" onchange="return get_class_sections(this.value)">

										<option selected disabled>Selecione um núcleo</option>
									
  									  
									</select>
									
								</div>

								</div>
			</div>

			
		    <div id="section_holder" style="display: none;">
			<div class="col-md-3">
				<div class="form-group">
				<label class="control-label" style="margin-bottom: 5px;">Sessão</label>
					<select class="form-control selectboxit" name="section_id">
		                            
						
					</select>
				</div>
			</div>
		    </div>
			
			

		        <div class="col-md-3" style="margin-top: 20px;">
				<div class="form-group">
<label for="field-1" class="col-sm-6 control-label">Data</label>
				<div class="col-sm-12">
					<input type="text" id="data" class="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy"
						value="Selecione uma data"/>
					</div>
				</div>
			</div>

			<div class="row">
			<input type="hidden" name="year" value="<?php echo $running_year;?>">
			<div class="col-md-3" style="margin-top: 40px; margin-left: 30px;">
				<a id="link_relatorio" href="<?php echo site_url('admin/relatorio_print_view/');?>" target="_blank"><button id = "submit" class="btn btn-info">Gerenciar frequência</button></a>
			</div>

		</div>

		</div>
		
    </div>
</div>

		

<script type="text/javascript">





$( "#pro" ).change(function() {
  
     var id_projeto = $("#pro").val();

     $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
    type: 'POST',  // http method
    data: { id_projeto: id_projeto },  // data to submit
    success: function (data, status, xhr) {

    		 $("#nuc").empty();
    	 data = $.parseJSON(data);


      for (var i = 0; i < data.length; i++) {
  
   $("#nuc").append('<option value='+data[i]['id_nucleo']+'>'+data[i]['nome_nucleo']+'</option>');
}






var id_nucleo = $("#nuc").val();






 $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#turm").empty();
    	 data2 = $.parseJSON(data2);



     if((data2 == null)|(data2 == '')){
    	 	$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
    	 }else{

      for (var i = 0; i < data2.length; i++) {
  
   $("#turm").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
}

var id_turma = $("#turm").val();
}


   

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});





 //ajax do evento


var id_nucleo = $("#nuc").val();

 $.ajax('<?php echo site_url('admin/get_evento_nucleo/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

      //alert(data2);

           $("#eve").empty();
       data = $.parseJSON(data2);

        if((data == null)|(data == '')){
        $("#eve").append('<option value="" selected disabled>Nenhum evento encontrado</option>');
       }else{



      for (var i = 0; i < data.length; i++) {
  
   $("#eve").append('<option value='+data[i]['id_evento']+'>'+data[i]['nome_evento']+'</option>');
}

}


 var id_evento = $("#eve").val();
 var id_turma = $("#turm").val();

    $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&evento='+id_evento);

      data = $("#data").val();

      if((data != null) & (data != 'Selecione uma data') & (id_nucleo != null) & (id_turma != null) & (id_evento != null)){
        //alert(data);
           $('#submit').removeAttr('disabled');
      }

      }
    });


 // fecha ajax evento
   
}


});

 });



$("#turm").change(function(){
var id_nucleo = $("#nuc").val();
var id_projeto = $("#pro").val();
 var id_evento = $("#eve").val();
var id_turma = $("#turm").val();
 $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&evento='+id_evento);

});


////// dsfdas


$("#eve").change(function(){

  var id_nucleo = $("#nuc").val();
var id_projeto = $("#pro").val();
 var id_evento = $("#eve").val();
var id_turma = $("#turm").val();
 $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&evento='+id_evento);

});


$( "#nuc" ).change(function() {


var id_nucleo = $("#nuc").val();
var id_projeto = $("#pro").val();



//alert(id_nucleo);
$.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {
//alert(data2);
    		  $("#turm").empty();
    	 data2 = $.parseJSON(data2);



     if((data2 == null)|(data2 == '')){
    	 	$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
    	 }else{

      for (var i = 0; i < data2.length; i++) {
  
   $("#turm").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
}

}



var id_turma = $("#turm").val();


$.ajax({
            url: '<?php echo site_url('admin/get_class_section/');?>' + id_turma ,
            success: function(response)
            {
                jQuery('#section_selector_holder').html(response);
            }
        });
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});




 $.ajax('<?php echo site_url('admin/get_evento_nucleo/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

      //alert(data2);

           $("#eve").empty();
       data = $.parseJSON(data2);

       if((data == null)|(data == '')){
        $("#eve").append('<option value="" selected disabled>Nenhum evento encontrado</option>');
       }else{


      for (var i = 0; i < data.length; i++) {
  
   $("#eve").append('<option value='+data[i]['id_evento']+'>'+data[i]['nome_evento']+'</option>');
}
}

 var id_evento = $("#eve").val();
var id_turma = $("#turm").val();
var id_evento = $("#eve").val();

 $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&evento='+id_evento);

      }
    });




});

////fadsfsdfa

//   $('#data_final').change(function(){

//         var data_final = $('#data_final').val();
//         //data2 = String(data);
//         //var date = new Date(data2);
//         //alert(data2);


//         split = data_final.split('-');
// novadata = split[1] + "/" +split[0]+"/"+split[2];
// data_usar = split[1]+ "-" +split[0]+"-" +split[2];
// data_input = new Date(novadata);
// //alert(data_usar);




//         const atual = new Date();
//         atual.setHours(0);
//         atual.setMinutes(0);
//         atual.setMilliseconds(0);
//         atual.setSeconds(0);
          

//           var ano = data_input.getFullYear();
//           //alert(ano);

//           if(ano < 1970){

//             alert('Valor de data não é permitido');
//             $('#submit').attr('disabled', 'disabled');
//           }else{

//         if (data_input.getTime() === atual.getTime()) {
//     //alert('As datas são iguais');
//     var id_nucleo = $("#nuc").val();
// var id_projeto = $("#pro").val();
// var id_turma = $("#turm").val();
// var data_inicio = $("#data").val();
// if((id_nucleo != null) & (id_projeto != null) & (id_turma != null) & (data_inicio != null) & (data_inicio != 'Selecione uma data')){   


//     $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&data='+data_inicio+'&data_final='+data_usar);
//    $('#submit').removeAttr('disabled');
// }else{
//   alert('Todos os campos são necessários para o relatório.');
// }
// }
// else if (data_input.getTime() > atual.getTime()) {

//     // Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
//     //alert(data_input.toString() + ' maior que ' + atual.toString());
//     $('#submit').attr('disabled', 'disabled');

// }
// else {
//       var id_nucleo = $("#nuc").val();
// var id_projeto = $("#pro").val();
// var id_turma = $("#turm").val();
// var data_inicio = $("#data").val();
// if((id_nucleo != null) & (id_projeto != null) & (id_turma != null) & (data_inicio != null) & (data_inicio != 'Selecione uma data')){
//     $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&data='+data_inicio+'&data_final='+data_usar);

//       $('#submit').removeAttr('disabled');
//     //alert(data_input.toString() + ' menor que ' + atual.toString());
//   }else{
//    alert('Todos os campos são necessários para o relatório.')

//   }
// }

// }





//     });





  $('#data').change(function(){

        var data = $('#data').val();
        //data2 = String(data);
        //var date = new Date(data2);
        //alert(data2);


        split = data.split('-');
novadata = split[1] + "/" +split[0]+"/"+split[2];
data_usar = split[1]+ "-" +split[0]+"-" +split[2];
data_input = new Date(novadata);
//alert(data_usar);




        const atual = new Date();
        atual.setHours(0);
        atual.setMinutes(0);
        atual.setMilliseconds(0);
        atual.setSeconds(0);
          

          var ano = data_input.getFullYear();
          //alert(ano);

          if(ano < 1970){

            alert('Valor de data não é permitido');
            $('#submit').attr('disabled', 'disabled');
          }else{

        if (data_input.getTime() === atual.getTime()) {
    //alert('As datas são iguais');
    var id_nucleo = $("#nuc").val();
var id_projeto = $("#pro").val();
var id_turma = $("#turm").val();
var id_evento = $('#eve').val();   




    $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&evento='+id_evento+'&data='+data_usar);

 if((data != null) & (data != 'Selecione uma data') & (id_nucleo != null) & (id_projeto != null) & (id_turma != null) & (id_evento != null)){
   $('#submit').removeAttr('disabled');
 }else{
  alert('Todos os campos são obrigatórios!');
 }

}
else if (data_input.getTime() > atual.getTime()) {

    // Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
    //alert(data_input.toString() + ' maior que ' + atual.toString());
    $('#submit').attr('disabled', 'disabled');

}
else {
      var id_nucleo = $("#nuc").val();
var id_projeto = $("#pro").val();
var id_turma = $("#turm").val();
var id_evento = $('#eve').val();
    $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_print_view/?projeto='); ?>'+id_projeto+'&nucleo='+id_nucleo+'&turma='+id_turma+'&evento='+id_evento+'&data='+data_usar);

     if((data != null) & (data != 'Selecione uma data') & (id_nucleo != null) & (id_projeto != null) & (id_turma != null) & (id_evento != null)){

      $('#submit').removeAttr('disabled');
    //alert(data_input.toString() + ' menor que ' + atual.toString());

  }else{
    alert('Todos os campos são obrigatórios!');
  }
  
}

}





    });






var class_selection = "";
jQuery(document).ready(function($) {
	$('#submit').attr('disabled', 'disabled');
});

function select_section(class_id) {
	if(class_id !== ''){
		$.ajax({
			url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
			success:function (response)
			{

			jQuery('#section_holder').html(response);
			}
		});
	}
}

function check_validation(){
	if(class_selection !== ''){
		$('#submit').removeAttr('disabled')
	}
	else{
		$('#submit').attr('disabled', 'disabled');
	}
}

$('#class_selection').change(function(){
	class_selection = $('#class_selection').val();
	check_validation();
});



// $( "#pro" ).change(function() {
// var id_projeto = $("#pro").val();

//  $.ajax('<?php echo site_url('admin/get_turma_projeto/'); ?>', {
//     type: 'POST',  // http method
//     data: { dados: id_projeto },  // data to submit
//     success: function (data, status, xhr) {

//     	//alert(data);

//     		  $('#class_selection').empty();
//     	 data = $.parseJSON(data);

//     	 if(data.length > 1){
//     	 	     for (var i = 0; i < data.length; i++) {

//     	 	     	if(i == 0){
//     	 	     		$('#class_selection').append('<option selected value='+data[i]['id']+'>'+data[i]['nome']+'</option>');
//     	 	     	}else{
//     	 	     		$('#class_selection').append('<option value='+data[i]['id']+'>'+data[i]['nome']+'</option>');
//     	 	     	}
  
   
// }
// class_selection = 'selecionado';
// check_validation();
// var id_class = $("#class_selection").val();
// select_section(id_class);
//     	 }else{
//     	 	class_selection = '';
//     	 	check_validation();
//     $('#class_selection').append('<option disabled selected>Nenhuma turma encontrada</option>');
//     	 }   

 
      

//     },
//     error: function (jqXhr, textStatus, errorMessage) {
//            alert(errorMessage);
//     }
// });

// });

$('#class_selection').change(function(){


var id_class = $("#class_selection").val();

select_section(id_class);

});


</script>