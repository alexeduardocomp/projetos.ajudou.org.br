<?php 
	$edit_data = $this->db->get_where('cidade' , array('id' => $param2))->result_array();
	foreach ($edit_data as $row):
?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title">
            		<i class="entypo-plus-circled"></i>
				Atualizar cidade
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(site_url('admin/cidade/edit/'. $row['id']) , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                    
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"
                            	value="<?php echo $row['nome'];?>">
						</div>
					</div>


				
<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Estado</label>
                                  <div class="col-sm-5">
									<select name="estado" class="form-control"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus>
										<option value="<?php echo $row['estado'] ;?>" selected><?php echo $row['estado'] ;?></option>
  									  <option value="Acre">Acre</option>
  									  <option value="Alagoas">Alagoas</option>
  									  <option value="Amapá">Amapá</option>
  									  <option value="Amazonas">Amazonas</option>
  									  <option value="Bahia">Bahia</option>
  									  <option value="Ceará">Ceará</option>
  									  <option value="Distrito Federal">Distrito Federal</option>
  									  <option value="Espírito Santo">Espírito Santo</option>
  									  <option value="Goiás">Goiás</option>
  									  <option value="Maranhão">Maranhão</option>
  									  <option value="Mato Grosso">Mato Grosso</option>
  									  <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
  									  <option value="Minas Gerais">Minas Gerais</option>
  									  <option value="Pará ">Pará </option>
  									  <option value="Paraíba">Paraíba</option>
  									  <option value="Paraná">Paraná</option>
  									  <option value="Pernambuco">Pernambuco</option>
  									  <option value="Piauí">Piauí</option>
  									  <option value="Rio de Janeiro">Rio de Janeiro</option>
  									  <option value="Rio Grande do Norte">Rio Grande do Norte</option>
  									  <option value="Rio Grande do Sul">Rio Grande do Sul</option>
  									  <option value="Rondônia">Rondônia</option>
  									  <option value="Roraima">Roraima</option>
  									  <option value="Santa Catarina">Santa Catarina</option>
  									  <option value="São Paulo">São Paulo</option>
  									  <option value="Sergipe">Sergipe</option>
  									  <option value="Tocantins">Tocantins</option>
									</select>
								</div>

					</div>








                    
                    <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Atualizar</button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>