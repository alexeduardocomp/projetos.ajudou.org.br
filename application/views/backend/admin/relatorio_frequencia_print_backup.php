<?php

require_once __DIR__ . '/vendor/autoload.php';

ini_set('max_execution_time', 0);
/* ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL); */
error_reporting(0);
ini_set('display_errors', 0);

/*$projeto = $_GET['projeto'];
$nucleo = $_GET['nucleo'];
*/

$turmas = $_POST['class_id'];
//$turmas_array = explode(",", $turmas);

$meses = $_POST['mes'];
$meses_array = explode(",", $meses);

$teste = $_POST['class_id'];
$turmas_array = json_decode($_POST['turmas']);



$mpdf = new \Mpdf\Mpdf([
  'setAutoTopMargin' => false,
  'setAutoBottomMargin' => false,
  //'margin_top' => '56',
  'margin-left' => '0',
  'debug' => true,
  'pagenumPrefix' => 'Página ',
  'pagenumSuffix' => ' - ',
  'nbpgPrefix' => ' de ',
  'nbpgSuffix' => ' páginas / '
]);
$cabecalho = "";
$corpo = "";
$cabeca = "";
$conteudo = "";
$corpo3 = "";
$resumo_porcentagem = "";

$media_presenca = 0;
$soma_presenca = 0;
$alunos_turma_ordenado = 0;
$media_ausencia = 0;
$soma_ausencia = 0;
$alunos_turma_ordenado = 0;
$media_justificado = 0;
$soma_justificado = 0;
$alunos_turma_ordenado = 0;

foreach ($turmas_array as $id_turma) {
  
  // =================================== Busca dados da Turma ===================================
  $turma = $this->db->get_where('class', array('class_id' => $id_turma))->row();  
  $projeto = $this->db->get_where('projeto', array('id' => $turma->projeto_id))->row();
  $nucleo = $this->db->get_where('nucleo', array('id' => $turma->id_nucleo))->row();

  $nome_projeto = $projeto->nome;
  $tipo_projeto = $projeto->tipo_projeto;
  $nome_turma = $turma->name;
  $id_nucleo = $turma->id_nucleo;

  // echo "<br>";
  // echo "<h1>Turma: " . $id_turma . "</h1>";
  //  echo "<br>";

  $time_start = $turma->time_start;
  $time_end = $turma->time_end;
  $time_start_min = $turma->time_start_min;
  $time_end_min = $turma->time_end_min;

  if ($time_start_min == "0" || $time_start_min == 0) {
    $time_start_min = "00";
  }

  if ($time_end_min == "0" || $time_end_min == 0) {
    $time_end_min = "00";
  }

  $nome_nucleo = $nucleo->nome_nucleo;
  $id_instrutor = $turma->teacher_id;
  $nome_instrutor = $this->db->get_where('teacher', array('teacher_id' => $id_instrutor))->row()->name;
  $ano = $_POST['ano'];

  // $ano = $ano;

  // $ano = '2019-2020';
  foreach ($meses_array as $mes) {
    //  echo "<br>";
    //   echo "<h1>Mes: " . $mes . "</h1>";
    //  echo "<br>";
   
    // =================================== Atribui a string do Mês ===================================
    if ($mes == 1) {
      $mes_certo = 'Janeiro';
    } else if ($mes == 2) {
      $mes_certo = 'Fevereiro';
    } else if ($mes == 3) {
      $mes_certo = 'Março';
    } else  if ($mes == 4) {
      $mes_certo = 'Abril';
    } else  if ($mes == 5) {
      $mes_certo = 'Maio';
    } else   if ($mes == 6) {
      $mes_certo = 'Junho';
    } else   if ($mes == 7) {
      $mes_certo = 'Julho';
    } else  if ($mes == 8) {
      $mes_certo = 'Agosto';
    } else   if ($mes == 9) {
      $mes_certo = 'Setembro';
    } else   if ($mes == 10) {
      $mes_certo = 'Outubro';
    } else  if ($mes == 11) {
      $mes_certo = 'Novembro';
    } else  if ($mes == 12) {
      $mes_certo = 'Dezembro';
    }

    //   echo "<br>1";

    // =================================== Cabeçalho ===================================
    $horario_turma = " - ";
    if (!empty($time_start)) {
      $horario_turma =  $time_start . ':' . $time_start_min . ' às ' . $time_end . ':' . $time_end_min;
    }

    if ($tipo_projeto == "CONVÊNIO") {

      $cabecalho = '
                    <table border="1" cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                      <td width="170" style="text-align:center;">
                      <div class="imagem" ><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
                      </td>
                      <td style="text-align:center;">
                      <div class="titulo" style="text-transform:uppercase;">Lista de frequência padrão para projetos</div>
                      </td>
                      </tr>
                      <tr>
                      <td style="padding:10px 15px;" width="170">
                      Críterios utilizados:
                      </td>
                      <td style="padding:10px 15px;">
                      <table border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr><td style="padding-bottom:5px;" width="300"><div><p><b>Projeto: </b> ' . $nome_projeto . '</p></div></td> <td style="padding-bottom:5px;"><div><p><b>Instrutor: </b>' . $nome_instrutor . '</p></div></td></tr>
                      <tr><td style="padding-bottom:5px;"><div><p><b>Núcleo: </b> ' . $nome_nucleo . ' </p></div></td><td style="padding-bottom:5px;"><div><p><b>Mês: </b>' . $mes_certo . ' / <b>Ano: </b>' . $ano . '</p></div></td></tr>
                      <tr><td style="padding-bottom:5px;"><div><p><b>Turma: </b>' . $nome_turma . '</p></div></td>
                      <td style="padding-bottom:5px;"><div><p><b>Horário: </b>' . $horario_turma . '</p></div></td>
                      
                      </tr>
                      
                      </table>
                      </td>
                      </tr>
                    </table>
                    <br>';
    } else {
      
      $logo = "";
      if ($tipo_projeto == "FEDERAL") {
        $logo =  '<td ROWSPAN="2" width="120" style="text-align:center;">
          <div><img src="' . base_url() . 'uploads/logos/federal.jpg " width="100" style="padding:5px 0px;"></div>
        </td>';
      } elseif ($tipo_projeto == "ESTADUAL") {
        $logo = '<td ROWSPAN="2" width="120" style="text-align:center;">
          <div><img src="' . base_url() . 'uploads/logos/estadual.jpg " width="100" style="padding:5px 0px;"></div>
        </td>';
      }
      $cabecalho = '
                    <table border="1" cellspacing="0" cellpadding="0" width="100%">
                      <tr>
                      <td width="170" style="text-align:center;">
                      <div class="imagem" ><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
                      </td>
                      <td style="text-align:center;">
                      <div class="titulo" style="text-transform:uppercase;">Lista de frequência padrão para projetos</div>
                      </td>
                      ' . $logo . '
                      </tr>
                      <tr>
                      <td style="padding:10px 15px;" width="170">
                      Críterios utilizados:
                      </td>
                      <td style="padding:10px 15px;">
                      <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr><td style="padding-bottom:5px;" width="300"><div><p><b>Projeto: </b> ' . $nome_projeto . '</p></div></td> <td style="padding-bottom:5px;"><div><p><b>Instrutor: </b>' . $nome_instrutor . '</p></div></td></tr>
                        <tr><td style="padding-bottom:5px;"><div><p><b>Núcleo: </b> ' . $nome_nucleo . ' </p></div></td><td style="padding-bottom:5px;"><div><p><b>Mês: </b>' . $mes_certo . ' / <b>Ano: </b>' . $ano . '</p></div></td></tr>
                        <tr><td style="padding-bottom:5px;"><div><p><b>Turma: </b>' . $nome_turma . '</p></div></td>
                        <td style="padding-bottom:5px;"><div><p><b>Horário: </b>' . $horario_turma . '</p></div></td>
                        </tr>
                      </table>
                      </td>
                      </tr>
                    </table>
                    <br>';
    }
    

    //  echo "<br>2";

    // =================================== Frequências da Turma ===================================

    $sql = "SELECT * FROM `attendance` WHERE year(from_unixtime(timestamp))=" . $ano . " AND month(from_unixtime(timestamp))=" . $mes . " AND class_id=" . $turma->class_id . "";

    // echo $sql;
    // echo "<br>";
    $query = $this->db->query($sql);

    $frequencias = $query->result_array();

    $timestamp_vetor = array();
    $alunos_vetor = array();
    $cont2 = 0;
    foreach ($frequencias as $vet) {
      $timestamp_vetor[$cont2] = $vet['timestamp'];
      $alunos_vetor[$cont2] = $vet['student_id'];
      $cont2++;
      // echo $vet['timestamp'];
      //  echo "<br>";
    }

    $total_de_dias_de_lancamento = array_unique($timestamp_vetor);
    $dias = count($total_de_dias_de_lancamento);
    $alunos = array_unique($alunos_vetor);
    $vetor_resultado_final = array();

    // =================================== Porcentagem de Frequencia de cada aluno ===================================
    $contador = 0;
    $soma_presenca = 0;
    $soma_ausencia = 0;
    $soma_justificado = 0;
    foreach ($alunos as $alu) {

      $cont_presenca = 0;
      $cont_ausencia = 0;
      $cont_justificado = 0;

      foreach ($frequencias as $v) {
        if ($alu == $v['student_id']) {
          if ($v['status'] == 1) {
            //echo 'Presente';
            $cont_presenca++;
          } else if ($v['status'] == 2) {
            //echo 'Ausente';
            $cont_ausencia++;
          } else if (($v['status'] == 0) || ($v['status'] == null)) {
            //echo 'Justificado';
            $cont_justificado++;
          }
        } else {
          //echo 'Não lançado';
        }
      }
      $contador++;

      $soma_dias = $cont_presenca + $cont_ausencia + $cont_justificado;

      //  echo "<br>3";

      if ($soma_dias == $dias) {
        $porcent_presenca = $cont_presenca / $dias;
        $porcent_ausencia = $cont_ausencia / $dias;
        $porcent_justificado = $cont_justificado / $dias;
      } else {
        $porcent_presenca = $cont_presenca / $soma_dias;
        $porcent_ausencia = $cont_ausencia / $soma_dias;
        $porcent_justificado = $cont_justificado / $soma_dias;
      }

      $presenca = number_format($porcent_presenca * 100, 2, ',', '');;
      $ausencia = number_format($porcent_ausencia * 100, 2, ',', '');;
      $justificado = number_format($porcent_justificado * 100, 2, ',', '');

      $situacao_aluno = $this->ajaxload->get_situacao_aluno($alu);
      foreach ($situacao_aluno as $s) {
        $situ = $s->situacao;
      }

      if ($situ == 'ATIVO') {
        $soma_presenca = $soma_presenca + $porcent_presenca;
        $soma_ausencia = $soma_ausencia + $porcent_ausencia;
        $soma_justificado = $soma_justificado + $porcent_justificado;

        $vetor_resultado_final[$contador]['id_aluno'] = $alu;
        $vetor_resultado_final[$contador]['porcentagem_presenca'] = $presenca;
        $vetor_resultado_final[$contador]['porcentagem_ausencia'] = $ausencia;
        $vetor_resultado_final[$contador]['porcentagem_justificado'] = $justificado;
      }
    }

    // =================================== Porcentagem de Frequencia de cada aluno ===================================

    $year = explode('-', $ano);
    $days = cal_days_in_month(CAL_GREGORIAN, $mes, $ano);
    $razao = 1 / $days;

    $sql = "SELECT * FROM `enroll` WHERE class_id=" . $turma->class_id . " AND ((year(from_unixtime(date_added)) <='" . $ano . "' AND month(from_unixtime(date_added)) <='" . $mes . "') OR (year(from_unixtime(date_added)) <'" . $ano . "'))";
    // echo "<br>";
    // echo $sql;
    // echo "<br>";
    $query = $this->db->query($sql);

    $students = $query->result_array();

    // $students = $this->db->get_where('enroll', array('class_id' => $turma->class_id, 'year(from_unixtime(date_added))' => $ano))->result_array();

    // echo "students: " . count($students);
    $id_estudantes = array();
    $cont = 0;
    $cont4 = 1;

    // echo "<br>";

    $ordenado = array();

    //SELECT * FROM `enroll` WHERE class_id=54 AND ((year(from_unixtime(date_added)) <='2020' AND month(from_unixtime(date_added)) <='2') OR (year(from_unixtime(date_added)) <'2020'))

    $sql = "SELECT enroll.* FROM `enroll` INNER JOIN student ON enroll.student_id = student.student_id WHERE class_id=" . $turma->class_id . " AND ((year(from_unixtime(date_added)) <='" . $ano . "' AND month(from_unixtime(date_added)) <='" . $mes . "') OR (year(from_unixtime(date_added)) <'" . $ano . "')) ORDER BY student.name ASC";
    // echo $sql;
    // echo "<br>";

    $query = $this->db->query($sql);

    $alunos_turma_ordenado = $query->result_array();

    // $alunos_turma_ordenado = $this->db->order_by('name', 'ASC')->get_where('student', array('id_turma' => $turma->class_id, 'situacao' => 'ATIVO'))->result_array();
    
    for ($i = 0; $i < count($alunos_turma_ordenado); $i++) {
     
      $sql = "SELECT attendance.class_id FROM `attendance` INNER JOIN student ON attendance.student_id = student.student_id WHERE student.student_id=" . $alunos_turma_ordenado[$i]['student_id'] . " AND year(from_unixtime(timestamp)) ='" . $ano . "' AND month(from_unixtime(timestamp)) ='" . $mes . "'  GROUP BY attendance.`class_id` ORDER BY student.name ASC";
      //  echo $sql;
      //  echo "<br>";
      $query = $this->db->query($sql);

      $turmas_mes_aluno = $query->result_array();
     
      foreach ($turmas_mes_aluno as $t_m) {

        if ($t_m['class_id'] == $turma->class_id) {

          //array ordenado recebe meu $stu; 
          array_push($ordenado, $alunos_turma_ordenado[$i]);
         
        }
      }
    }
    

    // =================================== Pega os dias que foram lançadas frequencia ===================================
    unset($dias_lancado);
    $dias_lancado = array();
   
    // echo "<br>";
    foreach ($ordenado as $stu) {      

      // echo "student_id_: " . $stu['student_id'];
      // echo "<br>";
      for ($i = 1; $i <= $days; $i++) {
        if (isset($dias_lancado[$i])) {
          if ($dias_lancado[$i] == "1") {
            $i++;
          }
        }
        $timestamp = strtotime($i . '-' . $mes . '-' . $ano);

        //  echo "timestamp: " . $timestamp;
        //  echo "<br>";
        // erro
        $attendance = $this->db->get_where('attendance', array('class_id' => $turma->class_id, 'timestamp' => $timestamp, 'student_id' => $stu['student_id']))->result_array();
        
        
        $conta = 1;
        if($attendance){
          foreach ($attendance as $row1) {
            $month_dummy = date('d', $row1['timestamp']);
            if (!empty($month_dummy)) {
              $dias_lancado[$i] = "1";
            }     
          }
        }
        
      }
    }

    // =================================== Monta as linhas com nomes e frequencias de cada aluno ===================================
    $linhas = '';
    $conteudo = '';
    foreach ($ordenado as $stu) {

      //  echo "<br>5";

      $nome_alunos = strtoupper($this->db->get_where('student', array('student_id' => $stu['student_id']))->row()->name);
      $conteudo = $conteudo . '<tr><td style="text-align:center; font-size:8px;">' . $cont4 . '</td><td style="text-align:left; font-size:9px; padding-left:10px;">' . $nome_alunos . '</td>';
      $cont4++;

      
      

      for ($i = 1; $i <= $days; $i++) {
        if (isset($dias_lancado[$i])) {
          if ($dias_lancado[$i] == "1") {
            //      echo "<br>6";
          
            $timestamp = strtotime($i . '-' . $mes . '-' . $ano);
            $dt = $i . '-' . $mes . '-' . $ano;
            $attendance = $this->db->get_where('attendance', array('class_id' => $turma->class_id, 'timestamp' => $timestamp, 'student_id' => $stu['student_id']))->result_array();
            $status = 4;

            foreach ($attendance as $row1) { 
              
              $month_dummy = date('d', $row1['timestamp']);
              if ($i == $month_dummy) {
                $status = $row1['status'];
              }
            }

            $tabela = '<td><table border="1" width="100%"><tr>';

            $linhas = $linhas . '<td colspan="' . count($dias_lancado) . '" style="text-align: center;">';
            if ($status == 1) {
              $linhas = $linhas . "<span style='color:green; font-size:8px;'><b>P</b></span>";
            } else if ($status == 2) {
              $linhas = $linhas . "<span style='color:red; font-size:8px;'><b>A</b></span>";
            } else if ($status == 4) {
              $linhas = $linhas . "<span style='color:black; font-size:8px;'><b>- </b></span>";
            } else  if (($status == null) || ($status == '') || ($status == 0)) {
              $linhas = $linhas . "<span style='color:blue; font-size:8px;'><b>J </b></span>";
            }
            $linhas = $linhas . '</td>';
            $tabela = $tabela . $linhas . '</tr></table></td>';
          }
        }
      }

      $conteudo = $conteudo . $tabela;

      $tabela_porcentagem = '<td style="text-align:center; font-size:9px;">0,00</td><td style="text-align:center; font-size:9px;">0,00</td><td style="text-align:center; font-size:9px;">0,00</td>';

      foreach ($vetor_resultado_final as $vetor) {
        if ($vetor['id_aluno'] == $stu['student_id']) {
          $tabela_porcentagem = '<td style="text-align:center; font-size:9px;">' . $vetor['porcentagem_presenca'] . '</td><td style="text-align:center; font-size:9px;">' . $vetor['porcentagem_ausencia'] . '</td><td style="text-align:center; font-size:9px;">' . $vetor['porcentagem_justificado'] . '</td>';
        }
      }
      //  echo "<br>7";

      $conteudo = $conteudo . $tabela_porcentagem . '</tr>';

      $linhas = '';

      $id_estudantes[$cont] = $stu['student_id'];
      $cont++;
    }

    // =================================== Monta cabeçalho da tabela ===================================
    $tabela_dias = "";
    for ($i = 1; $i <= $days; $i++) {
      if (isset($dias_lancado[$i])) {
        if ($dias_lancado[$i] == "1") {
          if ($i <= 9) {
            $tabela_dias = $tabela_dias . '<td style="text-align: center;font-size:8px;">0' . $i . '</td>';
          } else {
            $tabela_dias = $tabela_dias . '<td style="text-align: center;font-size:8px;">' . $i . '</td>';
          }
        }
      }
    }
    //  echo "<br>8";

    $cont2 = 0;

    foreach ($id_estudantes as $alu) {
      $alunos[$cont2] = $this->db->get_where('student', array('student_id' => $alu))->row()->name;
      $cont2++;
    }

    $corpo = '<table border="1" width="100%">';
    $cabeca = '<tr><td style="text-align:center;" width="2%"><b>#</b></td><td style="text-align:center; width:19%; font-size:10px;"><b>Nome dos participantes</b></td><td style="text-align:center; width:47%;">
                           
                            <table border="1" width="100%">
                            <tr><td colspan="' . count($dias_lancado) . '" style="text-align:center; font-size:10px;"><b>Dia do mês</b></td></tr>
                            <tr>' . $tabela_dias . '</tr>
                            </table>

                            </td>

                            <td style="text-align:center; width:4%; font-size:8px; color:green;"><b>% (P)</b></td>
                            <td style="text-align:center; width:4%; font-size:8px; color:red;"><b>% (A)</b></td>
                            <td style="text-align:center; width:4%; font-size:8px; color:blue;"><b>% (J)</b></td>
                            </tr style="text-align:center; font-size:12px;">';

    $corpo3 = '</table>';

    // =================================== Tabela com Média das porcentagens ===================================
    $media_presenca = $soma_presenca / count($alunos_turma_ordenado);
    $media_ausencia = $soma_ausencia / count($alunos_turma_ordenado);
    $media_justificado = $soma_justificado / count($alunos_turma_ordenado);

    $resumo_porcentagem = '

                            <tr>
                            <td style="font-size:10px; text-align:right; padding:5px;" colspan="3">Média das porcentagens</td>

                            <td style="font-size:10px; text-align:center; padding:5px;">' . number_format($media_presenca * 100, 2, ',', '') . '%' . '</td>
                            <td style="font-size:10px; text-align:center; padding:5px;">' . number_format($media_ausencia * 100, 2, ',', '') . '%' . '</td>
                            <td style="font-size:10px; text-align:center; padding:5px;">' . number_format($media_justificado * 100, 2, ',', '') . '%' . '</td>
                            </tr>
                            </table>
                         ';

    //   echo "<br>9";

    // =================================== Gera o resultado final ===================================
    if (empty($dias_lancado)) {
      $corpo = "<h4 style='color:red;'>Nenhuma frequência lançada nesta turma e/ou mês e/ou ano!</h4>";
      $total = $cabecalho . $corpo;
    } else {
      $total = $cabecalho . $corpo . $cabeca . $conteudo . $resumo_porcentagem . $corpo3;
    }

    $mpdf->AddPage('L');
    $mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');
    $mpdf->WriteHTML($total);

    // echo "<br>10";

    // echo $total;
    $total = "";
    $conteudo = "";

    //   echo "<br>11";
  }
}

// =================================== Gera o PDF ===================================
ob_clean();
$mpdf->Output(); 
