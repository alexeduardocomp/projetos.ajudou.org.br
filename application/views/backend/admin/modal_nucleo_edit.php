<?php
$query = $this->db->get('projeto');
$projetos = $query->result();

$query = $this->db->get('patrocinador');
$patrocinadores = $query->result();

$edit_data = $this->db->get_where('nucleo', array('id' => $param2))->result_array();
foreach ($edit_data as $row) :
?>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						Atualizar núcleo
					</div>
				</div>
				<div class="panel-body">

					<?php echo form_open(site_url('admin/nucleo/edit/' . $row['id']), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Projeto</label>
						<div class="col-sm-5">
							<select name="id_projeto" class="form-control">
								<?php
								$tama = count($projetos);
								for ($i = 0; $i < $tama; $i++) {
									if ($row['id_projeto'] == $projetos[$i]->id) {
										echo '<option selected value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
									} else {
										echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
									}
								}
								?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome_nucleo" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['nome_nucleo']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Meta de alunos</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="meta_alunos" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['meta_alunos']; ?>" maxlength="7" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode))) return true; else return false;">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Patrocinador</label>
						<div class="col-sm-5">
							<select name="id_patrocinador" class="form-control">
								<?php
								$tama = count($patrocinadores);
								for ($i = 0; $i < $tama; $i++) {
									if ($row['patrocinador'] == $patrocinadores[$i]->id) {
										echo '<option selected value=' . $patrocinadores[$i]->id . '>' . $patrocinadores[$i]->nome . '</option>';
									} else {
										echo '<option value=' . $patrocinadores[$i]->id . '>' . $patrocinadores[$i]->nome . '</option>';
									}
								}
								?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">E-mail do coordenador</label>
						<div class="col-sm-5">
							<input type="email" class="form-control" name="email_coordenador" autofocus value="<?php echo $row['email_coordenador']; ?>">
						</div>
					</div>

					<!--value"<?php echo $row['nome']; ?>"-->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Atualizar</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>