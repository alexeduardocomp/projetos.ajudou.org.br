<?php
$query = $this->db->get('patrocinador');
$patrocinadores = $query->result();
?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
			<div class="panel-heading">
				<div class="panel-title">
					<i class="entypo-plus-circled"></i>
					Adicionar projeto
				</div>
			</div>
			<div class="panel-body">

				<?php echo form_open(site_url('admin/projeto/create/'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Nome</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="nome" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Tipo de projeto</label>
					<div class="col-sm-5">
						<select name="tipo_projeto" id="tipo_projeto" class="form-control">
							<option value="ESTADUAL">ESTADUAL</option>
							<option value="FEDERAL">FEDERAL</option>
							<option value="CONVÊNIO">CONVÊNIO</option>
						</select>
					</div>

					<div class="col-sm-2">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" id="imagem_logo" style="width: 100px; height: 100px;" data-trigger="fileinput">
								<img src='<?php echo base_url(); ?>uploads/logos/estadual.jpg'>
							</div>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Número</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="numero" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
					</div>
				</div>

				<!-- <div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Patrocinador</label>
					<div class="col-sm-5">
						<select name="id_patrocinador" class="form-control">
							<?php
							$tama = count($patrocinadores);
							for ($i = 0; $i < $tama; $i++) {

								echo '<option value=' . $patrocinadores[$i]->id . '>' . $patrocinadores[$i]->nome . '</option>';
							}
							?>
						</select>
					</div>

				</div> -->

				<!-- <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Patrocinador</label>
                    <div class="col-sm-5">
                        <select name="id_patrocinador[]" class="form-control" id="id_patrocinador" data-validate="required" data-message-required="<?php echo 'Campo Obrigatório' ?>" multiple>

                            <option selected disabled>Selecione</option>

                            <?php
							$tama = count($patrocinadores);
							for ($i = 0; $i < $tama; $i++) {

								echo '<option value=' . $patrocinadores[$i]->id . '>' . $patrocinadores[$i]->nome . '</option>';
							}
							?>
                        </select>
                        <div style="margin-top:10px;">Segure a tecla Ctrl e selecione mais de uma Turma!</div>
                    </div>

                </div> -->

				<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Manifestação esportiva</label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="manifestacao_esportiva" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>"></textarea>
							</div>
						</div>
					</div>-->

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Manifestação esportiva</label>
					<div class="col-sm-5">
						<select name="manifestacao_esportiva" id="manifestacao_esportiva" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option value="EDUCACIONAL">EDUCACIONAL</option>
							<option value="RENDIMENTO">RENDIMENTO</option>
							<option value="PARTICIPAÇÃO">PARTICIPAÇÃO</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Proponente</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="proponente" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
					</div>
				</div>

				<div class="form-group">
					<label for="field-2" class="col-sm-3 control-label">Metas</label>
					<div class="col-sm-5">
						<div class="input-group">
							<textarea class="form-control" rows="2" name="metas" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>"></textarea>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-2" class="col-sm-3 control-label">Objetivo</label>
					<div class="col-sm-5">
						<div class="input-group">
							<textarea class="form-control" rows="2" name="objetivo" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>"></textarea>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Data inicial</label>
					<div class="col-sm-5">
						<input type="date" class="form-control" name="data_inicial" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Data final</label>
					<div class="col-sm-5">
						<input type="date" class="form-control" name="data_final" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
					</div>
				</div>

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Situação</label>
					<div class="col-sm-5">
						<select name="situacao" class="form-control">
							<option value="ATIVO">ATIVO</option>
							<option value="INATIVO">INATIVO</option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-5">
						<button type="submit" class="btn btn-default">Adicionar projeto</button>
					</div>
				</div>

				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$("#tipo_projeto").change(function() {
			var tipo = $('#tipo_projeto').val();
			if (tipo == "ESTADUAL") {
				$('#imagem_logo').html("<img src='<?php echo base_url(); ?>uploads/logos/estadual.jpg'>");
			} else if (tipo == "FEDERAL") {
				$('#imagem_logo').html("<img src='<?php echo base_url(); ?>uploads/logos/federal.jpg'>");
			} else {
				$('#imagem_logo').html("");
			}
		});
	});
</script>