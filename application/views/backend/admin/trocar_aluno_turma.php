<hr />
<div class="row">
    <div class="col-md-12">
        <blockquote class="blockquote-blue">
            <p>
                <strong>Instruções para a Troca de Turma do Aluno</strong>
            </p>
            <p>
                <!--Promoting student from the present class to the next class will create an enrollment of that student to
                the next session. Make sure to select correct class options from the select menu before promoting.If you don't want
                to promote a student to the next class, please select that option. That will not promote the student to the next class
                but it will create an enrollment to the next session but in the same class.-->

                A troca de turma do aluno da turma atual para a próxima turma criará uma matrícula desse aluno para a outra turma. Certifique-se de selecionar as opções corretas da turma no menu de seleção antes de trocar.

            </p>
        </blockquote>
    </div>
</div>
<?php echo form_open(site_url('admin/trocar_aluno_turma/promote')); ?>
<div class="row">
    <?php
    $running_year_array             = explode("-", $running_year);
    $next_year_first_index          = $running_year_array[1];
    $next_year_second_index         = $running_year_array[1] + 1;
    $next_year                      = $next_year_first_index . "-" . $next_year_second_index;

    $query = $this
    ->db
    ->order_by('projeto.nome', 'ASC')
    ->get('projeto');
    $projetos = $query->result();

    ?>

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informações para troca de Turma</h3>
            </div>
            <div class="panel-body">
                <div class="row">

                    <input type="hidden" name="promotion_year" id="promotion_year" value="<?php echo $running_year ?>">
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Projeto</label>
                            <div class="col-sm-12">

                                <select name="pro" id="pro" class="form-control" required>
                                    <option value="">Selecione</option>
                                    <?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>

                                </select>
                            </div>
                        </div>                          
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Núcleo</label>
                            <div class="col-sm-12">

                                <select name="nucleo" id="nucleo" class="form-control" required>
                                    <option value="">Selecione um projeto</option>

                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">Turma atual</label>
                            <div class="col-sm-12">

                                <select name="turm" id="turm" class="form-control" required>
                                    <option value="">Selecione um núcleo</option>

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Aluno</label>
                            <div class="col-sm-12">
                                <select name="student" id="student" class="form-control" required>
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" >
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="field-1" class="control-label" style="text-align: left;">Data de Matrícula</label>
                                <input type="date" class="form-control" name="data_matricula" id="data_matricula" required>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4" >
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Para o Projeto</label>
                            <div class="col-sm-12">

                                <select name="pro2" id="pro2" class="form-control" required>
                                    <option value="">Selecione</option>
                                    <?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>

                                </select>
                            </div>
                        </div>                          
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Núcleo</label>
                            <div class="col-sm-12">

                                <select name="nucleo2" id="nucleo2" class="form-control" required>
                                    <option value="">Selecione um projeto</option>

                                </select>
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-5 control-label">Para a Turma</label>
                            <div class="col-sm-12">

                                <select name="promotion_to_class_id" id="promotion_to_class_id" class="form-control" required>
                                    <option value="">Selecione um núcleo</option>

                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <center>
                    <button class="btn btn-info" type="submit" style="margin:20px; margin-top:40px;">
                        Trocar de Turma</button>
                </center>

            </div>
        </div>
    </div>

</div>

<?php echo form_close(); ?>
<style>
    .form-control{
        margin-bottom: 10px;
    }
</style>
<script type="text/javascript">
    // Change Projeto
    $("#pro").change(function() {

        $("#nuc").val("");
        $("#nuc").empty();
        $("#turm").val("");
        $("#turm").empty();

        var id_projeto = $("#pro").val(); 

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST',  // http method
            data: { id_projeto: id_projeto },  // data to submit
            success: function (data2, status, xhr) {
                $("#nucleo").empty();
                data2 = $.parseJSON(data2);
                var cont = 0;

                if((data2 == null)|(data2 == '')){
                    $("#nucleo").append('<option value="" selected disabled>Nenhum núcleo encontrado para esse projeto. </option>');
                }else{
                    for (var i = 0; i < data2.length; i++) {  
                        cont = cont + 1                  
                        $("#nucleo").append('<option value='+data2[i]['id_nucleo']+'>'+data2[i]['nome_nucleo']+'</option>');
                    }
                }

                var nucleo_id = $("#nucleo").val();

                // alert(class_id);

                $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: nucleo_id
                    }, // data to submit
                    success: function(data, status, xhr) {
                        data = $.parseJSON(data);
                        // alert(data);

                        for (var i = 0; i < data.length; i++) {
                            $("#turm").append('<option value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
                        }
                    }
                });
                
            }
        });
    });

    // Change Núcleo
    $("#nucleo").change(function() {
        var nucleo_id = $("#nucleo").val();
        $("#turm").val("");
        $("#turm").empty();

        $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: nucleo_id
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);
                // alert(data);

                for (var i = 0; i < data.length; i++) {
                    $("#turm").append('<option value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
                }
            }
        });
    });
    

    // Change Turma
    $("#turm").change(function() {
        // alert(1);
        $("#student").val("");
        $("#student").empty();

        var class_id = $("#turm").val();

        // alert(class_id);

        $.ajax('<?php echo site_url('admin/get_students_turma/'); ?>', {
            type: 'POST', // http method
            data: {
                class_id: class_id
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);
                // alert(data);

                for (var i = 0; i < data.length; i++) {

                    //   alert(data[i]['name']);

                    $("#student").append('<option value=' + data[i]['student_id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    });

    $("#pro2").change(function() {

        $("#nucleo2").val("");
        $("#nucleo2").empty();
        $("#promotion_to_class_id").val("");
        $("#promotion_to_class_id").empty();

        var id_projeto = $("#pro2").val(); 

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST',  // http method
            data: { id_projeto: id_projeto },  // data to submit
            success: function (data2, status, xhr) {
                $("#nucleo2").empty();
                data2 = $.parseJSON(data2);
                var cont = 0;

                if((data2 == null)|(data2 == '')){
                    $("#nucleo2").append('<option value="" selected disabled>Nenhum núcleo encontrado para esse projeto. </option>');
                }else{
                    for (var i = 0; i < data2.length; i++) {  
                        cont = cont + 1                  
                        $("#nucleo2").append('<option value='+data2[i]['id_nucleo']+'>'+data2[i]['nome_nucleo']+'</option>');
                    }
                }

                var nucleo_id = $("#nucleo2").val();

                // alert(class_id);

                $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: nucleo_id
                    }, // data to submit
                    success: function(data, status, xhr) {
                        data = $.parseJSON(data);
                        // alert(data);

                        for (var i = 0; i < data.length; i++) {
                            $("#promotion_to_class_id").append('<option value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
                        }
                    }
                });
                
            }
        });
    });

    // Change Núcleo
    $("#nucleo2").change(function() {
        var nucleo_id = $("#nucleo2").val();
        $("#promotion_to_class_id").val("");
        $("#promotion_to_class_id").empty();

        $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: nucleo_id
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);
                // alert(data);

                for (var i = 0; i < data.length; i++) {
                    $("#promotion_to_class_id").append('<option value=' + data[i]['id_turma'] + '>' + data[i]['nome_turma'] + '</option>');
                }
            }
        });
    });
    
</script>