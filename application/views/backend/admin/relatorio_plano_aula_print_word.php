<?php

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(1);
ini_set('display_errors', 1);

echo "

<html>
<meta charset='utf-8'>";

$prestacao = $_GET['prestacao'];

$turma_id = $_GET['turma'];

$id_modalidade = $this->db->get_where('class', array('class_id' => $turma_id))->row()->modalidade;
$modalidade = $this->db->get_where('modalidade', array('id' => $id_modalidade))->row()->modalidade;

$planos_selecionadas = $this->ajaxload->get_planos($turma_id);
$datas_selecionadas = $this->ajaxload->get_datas($turma_id);

$projeto_id = $_GET['projeto'];
$nome_projeto = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->nome;

$nucleo_id = $_GET['nucleo'];
$nome_nucleo = $this->db->get_where('nucleo', array('id' => $nucleo_id))->row()->nome_nucleo;


$id_instrutor = $this->db->get_where('class', array('class_id' => $turma_id))->row()->teacher_id;
$nome_instrutor = $this->db->get_where('teacher', array('teacher_id' => $id_instrutor))->row()->name;

// Cabeçalho
$cabecalho = "<h3 style='text-align:center;'>RELATÓRIO " . $prestacao . " DOS PLANOS DE AULAS</h3>";
$cabecalho .= "<h3 style='text-align:center;'>PROJETO: " . $nome_projeto . "</h3>";
$cabecalho .= "<h3 style='text-align:center;'>NÚCLEO: " . $nome_nucleo . " - PROFESSOR: " . $nome_instrutor . "</h3>";


$total = $cabecalho;
if (count($datas_selecionadas) > 0) {


  foreach ($datas_selecionadas as $dt) {
    $total .= '<tr>';
    $total .= '<td style="padding-left:10px; width:10%;"><b>';
    $data = date('d/m', strtotime($dt->data_atividade));
    $total .= $data;
    $total .= " - Modalidade: </b>" . $modalidade . "</td><br>";

    $total .= "<b> Método: </b>";

    foreach ($planos_selecionadas as $plan) {
      if ($plan->data_atividade == $dt->data_atividade) {
        $total .= $cont . "- ";
        $total .= $this->crud_model->get_subject_name_by_id($plan->subject_id);

        $descricao_atividade = $this->db->get_where('subject', array('subject_id' => $plan->subject_id))->row()->descricao;
        if ($descricao_atividade) {
          $total .= " - " . $descricao_atividade;
        }
        $total .= "; ";
        $cont = $cont + 1;
      }
    }
    $cont = 1;

    $total .= '<br>';

    $total .= '<br>';
    $total .= '</tr>';
  }
}



echo $total;

echo "
</html>
";

$arquivo = 'relatorio_plano_aula.doc';


// Configurações header para forçar o download
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/x-msexcel");
header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
header("Content-Description: PHP Generated Data");
		// Envia o conteúdo do arquivo
