<?php
$edit_data        =    $this->db->get_where('class', array('class_id' => $param2))->result_array();
foreach ($edit_data as $row) :
?>
    <?php
    $query = $this->db->get('projeto');
    $projetos = $query->result();

    $query2 = $this->db->get('modalidade'); 
    $modalidades = $query2->result();
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary" data-collapsed="0">
                <div class="panel-heading">
                    <div class="panel-title">
                        <i class="entypo-plus-circled"></i>
                        Editar turma
                    </div>
                </div>
                <div class="panel-body">

                    <?php echo form_open(site_url('admin/classes/do_update/' . $row['class_id']), array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                        <div class="col-sm-5">
                            <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['projeto_id']; ?>">
                                <option id="noti" disabled="" selected="">Selecione um projeto</option>
                                <option id="projetoatual" style="display: none;" value="<?php echo $row['projeto_id']; ?>"><?php echo $row['nome_projeto']; ?></option>
                                <?php
                                $tama = count($projetos);
                                for ($i = 0; $i < $tama; $i++) {

                                    if($projetos[$i]->id == $row['projeto_id']){
                                        echo '<option value=' . $projetos[$i]->id . ' selected>' . $projetos[$i]->nome . '</option>';

                                    }else{
                                        echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';

                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                        <div class="col-sm-5">
                            <select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="">
                                <option id="nucleoatual" style="display: none;" value="<?php echo $row['id_nucleo']; ?>"><?php echo $row['nome_nucleo']; ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Local execução</label>
                        <div class="col-sm-5">
                            <select name="id_local" class="form-control" id="loc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option id="localatual" style="display: none;" selected value="<?php echo $row['id_local']; ?>"><?php echo $row['local']; ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Instrutor</label>
                        <div class="col-sm-5">
                            <select name="id_istrutor" class="form-control" id="ins" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option id="instrutoratual" style="display: none;" selected value="<?php echo $row['teacher_id']; ?>"><?php echo $row['nome_instrutor']; ?></option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Modalidade</label>
                        <div class="col-sm-5">
                            <select name="modalidade" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <?php
                                $tama = count($modalidades);
                                for ($i = 0; $i < $tama; $i++) {
                                    if ($row['modalidade'] == $modalidades[$i]->id) {
                                        echo '<option selected value=' . $modalidades[$i]->id . '>' . $modalidades[$i]->modalidade . '</option>';
                                    } else {
                                        echo '<option value=' . $modalidades[$i]->id . '>' . $modalidades[$i]->modalidade . '</option>';
                                    }
                                } 
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Tipo de Material</label>
                        <div class="col-sm-5">
                            <select name="tipo_material" class="form-control" id="tipo_material" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option selected disabled>Selecione</option>
                                <option value="uniforme" <?php if($row['tipo_material'] == 'uniforme'){echo 'selected';} ?>>Uniforme</option>  
                                <option value="quimono" <?php if($row['tipo_material'] == 'quimono'){echo 'selected';} ?>>Quimono</option>                           
                                <option value="ambos" <?php if($row['tipo_material'] == 'ambos'){echo 'selected';} ?>>Ambos</option>                           
                            </select>
                        </div> 
                    </div>

                    <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Dias da Semana</label>
                        <div class="col-sm-5">
                            <select id="dias_semana" class="form-control" name="dias_semana" multiple="multiple" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                <option value="1"> Segunda-feira </option>
                                <option value="2"> Terça-feira </option>
                                <option value="3"> Quarta-feira </option>
                                <option value="4"> Quinta-feira </option>
                                <option value="5"> Sexta-feira </option>
                                <option value="6"> Sábado </option>
                                <option value="7"> Domingo </option>    
                            </select>
                        </div>
				    </div>
                    <input name="dias" id="dias" type="hidden" value="<?php echo str_replace('"', "'", $row['dias_semana']); ?>"></input>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Nome</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="name" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['name']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Categoria</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="categoria" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['categoria']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Intervalo de idades</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="intervalo_idades" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['intervalo_idades']; ?>" />
                        </div>
                    </div>

                    <!--<div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Sexo</label>
                                  <div class="col-sm-5">
                                    <select name="sexo" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                                        <?php

                                        if ($row['sexo'] == "MASCULINO") {
                                            echo "<option selected value='MASCULINO'>MASCULINO</option>
                                         <option value='FEMININO'>FEMININO</option>
                                    ";
                                        } else {

                                            echo "<option  value='MASCULINO'>MASCULINO</option>
                                         <option selected value='FEMININO'>FEMININO</option>
                                    ";
                                        }
                                        ?>

                                    </select>
                                </div>

                                </div>-->

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Número de Atletas</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="numero_atletas" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $row['numero_atletas']; ?>" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora de começo</label>
                        <div class="col-sm-9">
                            <?php
                            if ($row['time_start'] < 13) {
                                $time_start   = $row['time_start'];
                                $time_start_min =   $row['time_start_min'];
                                $starting_ampm  = 1;
                            } else if ($row['time_start'] > 12) {
                                $time_start   = $row['time_start'] - 12;
                                $time_start_min =   $row['time_start_min'];
                                $starting_ampm  = 2;
                            }
                            ?>
                            <div class="col-md-3">
                                <select name="time_start" class="form-control" required>
                                    <option value="">Hora</option>
                                    <?php for ($i = 0; $i <= 12; $i++) : ?>
                                        <option value="<?php echo $i; ?>" <?php if ($i == $time_start) echo 'selected="selected"'; ?>>
                                            <?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="time_start_min" class="form-control" required>
                                    <option value="">Minutos</option>
                                    <?php for ($i = 0; $i <= 11; $i++) : ?>
                                        <option value="<?php echo $i * 5; ?>" <?php if (($i * 5) == $time_start_min) echo 'selected'; ?>><?php echo $i * 5; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="starting_ampm" class="form-control selectboxit">
                                    <option value="1" <?php if ($starting_ampm ==  '1') echo 'selected="selected"'; ?>>am</option>
                                    <option value="2" <?php if ($starting_ampm ==  '2') echo 'selected="selected"'; ?>>pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora do término</label>
                        <div class="col-sm-9">
                            <?php
                            if ($row['time_end'] < 13) {
                                $time_end   = $row['time_end'];
                                $time_end_min   =   $row['time_end_min'];
                                $ending_ampm  = 1;
                            } else if ($row['time_end'] > 12) {
                                $time_end   = $row['time_end'] - 12;
                                $time_end_min   =   $row['time_end_min'];
                                $ending_ampm  = 2;
                            }
                            ?>
                            <div class="col-md-3">
                                <select name="time_end" class="form-control" required>
                                    <option value="">Hora</option>
                                    <?php for ($i = 0; $i <= 12; $i++) : ?>
                                        <option value="<?php echo $i; ?>" <?php if ($i == $time_end) echo 'selected="selected"'; ?>>
                                            <?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="time_end_min" class="form-control" required>
                                    <option value="">Minutos</option>
                                    <?php for ($i = 0; $i <= 11; $i++) : ?>
                                        <option value="<?php echo $i * 5; ?>" <?php if (($i * 5) == $time_end_min) echo 'selected'; ?>><?php echo $i * 5; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="ending_ampm" class="form-control selectboxit">
                                    <option value="1" <?php if ($ending_ampm ==  '1') echo 'selected="selected"'; ?>>am</option>
                                    <option value="2" <?php if ($ending_ampm ==  '2') echo 'selected="selected"'; ?>>pm</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" class="btn btn-info">Atualizar Turma</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
endforeach;
?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dias_semana').select2();
        //adicionar dias da semanas do banco
        var selectedDiasSemana = $('#dias').val().replaceAll("'", '"');
        if(selectedDiasSemana){
            $('#dias_semana').val(JSON.parse((selectedDiasSemana))).trigger('change');
        }
        
    });
    $("#dias_semana").change(function() {        
        $("#dias").val(JSON.stringify($("#dias_semana").val()));
    });

    // PROJETO CHANGE
    $("#pro").change(function() { 
        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                $("#nuc").empty();

                data = $.parseJSON(data);

                if ((data == null) | (data == '')) {
                    $("#nuc").append('<option value="" selected disabled>Nenhum núcleo encontrado</option>');
                } else {
                    for (var i = 0; i < data.length; i++) {
                        $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                    }
                }

                var id_nucleo = $("#nuc").val();

                $.ajax('<?php echo site_url('admin/get_local_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: id_nucleo
                    }, // data to submit
                    success: function(data2, status, xhr) {

                        $("#loc").empty();
                        data2 = $.parseJSON(data2);

                        if ((data2 == null) | (data2 == '')) {
                            $("#loc").append('<option value="" selected disabled>Nenhum local encontrado</option>');
                        } else {
                            for (var i = 0; i < data2.length; i++) {
                                $("#loc").append('<option value=' + data2[i]['id'] + '>' + data2[i]['local'] + '</option>');
                            }
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                    }
                });

                $.ajax('<?php echo site_url('admin/get_instrutor_nucleo2/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: id_nucleo
                    }, // data to submit
                    success: function(data2, status, xhr) {

                        $("#ins").empty();
                        data2 = $.parseJSON(data2);

                        if ((data2 == null) | (data2 == '')) {
                            $("#ins").append('<option value="" selected disabled>Nenhum instrutor encontrado</option>');
                        } else {
                            for (var i = 0; i < data2.length; i++) {
                                $("#ins").append('<option value=' + data2[i]['id_instrutor'] + '>' + data2[i]['nome'] + '</option>');
                            }
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                    }
                });
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert("erro");
            }
        });
    });


    // NUCLEO CHANGE
    $("#nuc").change(function() {

        var id_nucleo = $("#nuc").val();


        $.ajax('<?php echo site_url('admin/get_local_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_nucleo
            }, // data to submit
            success: function(data2, status, xhr) {

                $("#loc").empty();
                data2 = $.parseJSON(data2);

                if ((data2 == null) | (data2 == '')) {
                    $("#loc").append('<option value="" selected disabled>Nenhum local encontrado</option>');
                } else {
                    for (var i = 0; i < data2.length; i++) {
                        $("#loc").append('<option value=' + data2[i]['id'] + '>' + data2[i]['local'] + '</option>');
                    }
                }
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });

        $.ajax('<?php echo site_url('admin/get_instrutor_nucleo2/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_nucleo
            }, // data to submit
            success: function(data2, status, xhr) {

                $("#ins").empty();
                data2 = $.parseJSON(data2);

                if ((data2 == null) | (data2 == '')) {
                    $("#ins").append('<option value="" selected disabled>Nenhum instrutor encontrado</option>');
                } else {
                    for (var i = 0; i < data2.length; i++) {
                        $("#ins").append('<option value=' + data2[i]['id_instrutor'] + '>' + data2[i]['nome'] + '</option>');
                    }
                }
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });

    });

    // DOCUMENTO CARREGOU
</script>