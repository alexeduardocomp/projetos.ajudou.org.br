


            <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_cidade_add/');?>');"
                class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
               Adicionar nova cidade

                </a>
                <br><br>
               <table class="table table-bordered" id="cidades">
                    <thead>
                        <tr>
                            <th width="60"><div>Id cidade</div></th>
                            <th><div>Nome</div></th>
                            <th><div>Estado</div></th>
                              <th><div>Opções</div></th>
                        </tr>
                    </thead>
                </table>



   
    <!--<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>-->




<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#cidades').DataTable({

            "processing": true,
            "scrollX": true,
            "serverSide": true,
            "ajax":{
                "url": "<?php echo site_url('admin/get_cidades') ?>",
                "dataType": "json",
                "type": "POST",
            },
            "columns": [
                { "data": "id" },
                { "data": "nome" },
                { "data": "estado" },
                { "data": "options" ,
                    "orderable": false},
            ],

                  dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
  buttons: [

  { extend: 'copy', text: 'Copiar' },
  { extend: 'excel', text: 'Excel' },
  { extend: 'pdf', text: 'PDF' },
  { extend: 'csv', text: 'CSV' }
  ],

             "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 }                              
        });
    });

    function cidade_edit_modal(cidade_id) {
        showAjaxModal('<?php echo site_url('modal/popup/modal_cidade_edit/');?>' + cidade_id);
    }

    function cidade_delete_confirm(cidade_id) {
        confirm_modal('<?php echo site_url('admin/cidade/delete/');?>' + cidade_id);
    }

</script>


