
<script type="text/javascript" src="http://localhost/sistema-ajudou/6.2/Ekattor/assets/js/jquery.mask.min.js"></script>

<script type="text/javascript">
	

	 $(document).ready(function () { 
        $(".cpf").mask('000.000.000-00');
        $('.rg').mask('AA.000.000-A');
        $('.tel').mask('(00) 00000-0000');
        $('.cep').mask('00000-000');
        $('.numeracao').mask('00.00');
        $('.nt').mask('00');
    });

</script>


<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title">
            		<i class="entypo-plus-circled"></i>
					Adicionar Responsável
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(site_url('admin/parent/create/') , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>
                    
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus
                            	value="">
						</div>
					</div>
                    
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email').'/ Nome de usuário';?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="email" value="" data-validate="required">
						</div>
					</div>
					
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Senha</label>
                        
						<div class="col-sm-5">
							<input type="password" class="form-control" name="password" value="" data-validate="required">
						</div>
					</div>
					
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Telefone</label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="phone" value="">
						</div>
					</div>
					
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Endereço</label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control" name="address" value="">
						</div>
					</div>
					
					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Profissão</label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control" name="profession" value="">
						</div>
					</div>
                    
                    <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Adicionar responsável</button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>