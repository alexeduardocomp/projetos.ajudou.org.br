<?php

$query = $this->db
->order_by('projeto.nome', 'ASC')
->get('projeto');
$projetos = $query->result();


?>


<meta charset="utf-8">
<div class="panel panel-primary">
	<div class="panel-heading">
		Filtros
	</div>
	<div class="panel-body">
 

		<?php //echo form_open(site_url('admin/relatorio_projeto_print_view/'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data', 'id' => 'formulario_beneficiados', 'target' => '_blank')); ?>

		<div class="row">

			<div class="col-md-3">

				<div class="form-group">
					<div class="col-sm-12">
						<label for="field-1" class=" control-label">Projeto</label>

						<select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option selected disabled>Selecione</option>
							<option value="0">Todos</option>
							<?php
							$tama = count($projetos);
							for ($i = 0; $i < $tama; $i++) {
                if(!empty($projetos[$i]->id)){
								  echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                }
              }
							?>
						</select>
					</div>
				</div>
			</div>
			<div class="col-md-3 " style="display: none;" id="div_nuc">
				<div class="form-group">
					<div class="col-sm-12">
						<label for="field-1" class="control-label">Núcleos</label>
						<select name="id_nucleo[]" class="form-control js-example-basic-multiple" multiple="multiple" id="nuc">
							<?php
							$tama = count($nucleos);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $nucleos[$i]->id . '>' . $nucleos[$i]->nome_nucleo . '</option>';
							}
							?>
						</select>
					</div>
				</div>
			</div>
    </div>
			
		<div class="row">

			<div class="col-md-2">
				<button type="submit" class="btn btn-info" id="submit" target="_blank">Pesquisar</button>
			</div>

			<div class="col-md-2">

			</div>
			<input type="hidden" name="year" value="<?php echo $running_year; ?>">

			<?php //echo form_close(); ?>
		</div>

		<br><br>
	</div>
</div>
<div class="panel panel-primary">
	<div class="panel-heading">
		Resultados da Consulta
	</div>
	<div class="panel-body">


		<div id="qtd_result" style="font-weight:bold; font-size:15px;">

		</div>
		<div id="div_projetos_geral">
			<table class="table table-bordered" id="assiduidade_todos_projetos">
				<thead>
					<tr>
						<th>
							<div>Projetos</div>
						</th>
						<th>
							<div>Assiduidade(%)</div>
						</th>
					</tr>
				</thead>
			</table>
		</div>
		<div id="div_projetos">
			<table class="table table-bordered" id="assiduidade_projetos">
				<thead>
					<tr>
						<th>
							<div>Projeto</div>
						</th>
						<th>
							<div>Núcleo</div>
						</th>
						<th>
							<div>Assiduidade(%)</div>
						</th>
					</tr>
				</thead>
			</table>
		</div>

		<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
		<script type="text/javascript">
			$('#submit').on('click', function() {

				var url = "<?php echo site_url('admin/excel_beneficiados'); ?>" + "?id_projeto=" + $("#pro").val() +
					"&id_nucleo=" + $("#nuc").val() +
					"&tipo_escola=" + $("#tipo_escola").val() +
					"&modalidade_id=" + $("#modalidade_id").val() +
					"&class_id=" + $("#turm").val() +
					"&sexo=" + $("#sexo").val() +
					"&situacao=" + $("#situacao").val() +
					"&idade_inicial=" + $("#idade_inicial").val() +
					"&idade_final=" + $("#idade_final").val() +
					"&escolas=" + $("#esc").val() +
					"&instrutores=" + $("#instrutores").val() +
					"&local_execucao=" + $("#local_execucao").val() +
					"&data_inicial=" + $("#data_inicial").val() +
					"&data_final=" + $("#data_final").val();

				$("#link_excel").attr("href", url);

				$.ajax('<?php echo site_url('admin/get_assiduidade'); ?>', {
					type: 'POST', // http method
					data: {
						"id_projeto": $("#pro").val(),
						"nucleos": $("#nuc").val()
					},
					success: function(data2, status, xhr) {
						data = $.parseJSON(data2);
						$("#qtd_result").html("");

						/* Assiduidade do Projeto = Média de Frequência de cada Turma / Número de Turmas daquele Projeto. */

						if($("#pro").val() == 0){
									$("#qtd_result").text("Assiduidade geral: " + data["geral"] +'%');
						}else{
									$("#qtd_result").text("O projeto está com assiduidade dos alunos em: " + data["geral"]+'%');
						}
					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});
				if($('#pro').val() == '0'){
					console.log('entrou no 0');
					$('#div_projetos_geral').css('display', 'block');
					$('#div_projetos').css('display', 'none');

					$('#div_projetos_geral').css('display ', 'block');
					$('#assiduidade_todos_projetos').DataTable().clear();
					$('#assiduidade_todos_projetos').DataTable().destroy();

					$('#assiduidade_todos_projetos').DataTable({
						"processing": true,
						"serverSide": true,
						"scrollX": true,
						"searching": false,
						"paging": false,
						"info": false,
						"ordering": false,
						"ajax": {
							"url": "<?php echo site_url('admin/get_assiduidade') ?>",
							"dataType": "json",
							"type": "POST",
							"data": {
								"id_projeto": $("#pro").val(),
								"nucleos": $("#nuc").val()
							}
						},
						"columns": [{
								"data": "projeto"
							},
							{
								"data": "assiduidade"
							}
						],
						dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-12.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
						buttons: [{
								extend: 'excel',
								text: 'Exportar tabela em Excel'
							},
							{
								extend: 'pdf',
								text: 'Exportar tabela em PDF'
							},
							{
								extend: 'csv',
								text: 'Exportar tabela em CSV'
							}
						],

						"oLanguage": {
							"sProcessing": "Aguarde enquanto os dados são carregados ...",
							"sLengthMenu": "Mostrar _MENU_ registros por pagina",
							"sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
							"sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
							"sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
							"sInfoFiltered": "",
							"sSearch": "Procurar",
							"oPaginate": {
								"sFirst": "Primeiro",
								"sPrevious": "Anterior",
								"sNext": "Próximo",
								"sLast": "Último"
							}
						}
					});
				}else{
					console.log('diferente de 0');

					$('#div_projetos_geral').css('display', 'none');
					$('#div_projetos').css('display', 'block');

					$('#assiduidade_projetos').DataTable().clear();
					$('#assiduidade_projetos').DataTable().destroy();

					$('#assiduidade_projetos').DataTable({
						"processing": true,
						"serverSide": true,
						"scrollX": true,
						"searching": false,
						"paging": false,
						"info": false,
						"ordering": false,
						"ajax": {
							"url": "<?php echo site_url('admin/get_assiduidade') ?>",
							"dataType": "json",
							"type": "POST",
							"data": {
								"id_projeto": $("#pro").val(),
								"nucleos": $("#nuc").val()
							}
						},
						"columns": [{
								"data": "projeto"
							},
							{
								"data": "nucleo"
							},
							{
								"data": "assiduidade"
							}
						],
						dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-12.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
						buttons: [{
								extend: 'excel',
								text: 'Exportar tabela em Excel'
							},
							{
								extend: 'pdf',
								text: 'Exportar tabela em PDF'
							},
							{
								extend: 'csv',
								text: 'Exportar tabela em CSV'
							}
						],

						"oLanguage": {
							"sProcessing": "Aguarde enquanto os dados são carregados ...",
							"sLengthMenu": "Mostrar _MENU_ registros por pagina",
							"sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
							"sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
							"sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
							"sInfoFiltered": "",
							"sSearch": "Procurar",
							"oPaginate": {
								"sFirst": "Primeiro",
								"sPrevious": "Anterior",
								"sNext": "Próximo",
								"sLast": "Último"
							}
						}
					});
				}
			});
		</script>

	</div>

</div>
</div>


<script type="text/javascript">
	/* $(document).ready(function() {
		$('#num_slie').keypress(function (e) {
			var regex = new RegExp("^[a-zA-Z0-9._\b]+$");
			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}

			e.preventDefault();
			return false;
		});
	}); */

	$('#add_filtro').click(function() {
		var action = $('#formulario_beneficiados').attr('action');

		if (action == '<?php echo site_url('admin/relatorio_projeto_beneficiario_original/'); ?>') {
			$('#formulario_beneficiados').attr('action', '<?php echo site_url('admin/relatorio_projeto_print_view/'); ?>');
		} else {
			$('#formulario_beneficiados').attr('action', '<?php echo site_url('admin/relatorio_projeto_beneficiario_original/'); ?>');
		}

		var estilo = $('.aparecer').css('display');

		if (estilo == 'none') {
			$('.aparecer').css('display', 'block');
			$('#add_filtro').html('Remover Filtros');
		} else {
			$('.aparecer').css('display', 'none');
			$('#add_filtro').html('Adicionar Filtros');
		}
	})

	// Change Projeto
	$("#pro").change(function() {

		// Busca núcleos
		$("#nuc").val("");
		$("#nuc").empty();
		$("#nuc").select2("val", "");

		var id_projeto = $("#pro").val();
		if($("#pro").val() == '0'){
			$('#div_nuc').css('display', 'none');
		}else{
			$('#div_nuc').css('display', 'block');
			$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
				type: 'POST', // http method
				data: {
					id_projeto: id_projeto
				}, // data to submit
				success: function(data, status, xhr) {
					data = $.parseJSON(data);

					for (var i = 0; i < data.length; i++) {
						$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
					}
				}
			});
		}

		

		

	});

	
	var class_selection = "";
	jQuery(document).ready(function($) {
		//$('#submit').attr('disabled', 'disabled');
	});

	function select_section(class_id) {
		if (class_id !== '') {
			$.ajax({
				url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
				success: function(response) {
					jQuery('#section_holder').html(response);
				}
			});
		}
	}

	function check_validation() {
		if (class_selection !== '') {
			//$('#submit').removeAttr('disabled')
		} else {
			//$('#submit').attr('disabled', 'disabled');
		}
	}

	$('#class_selection').change(function() {
		class_selection = $('#class_selection').val();
		check_validation();
	});


	$('#class_selection').change(function() {
		var id_class = $("#class_selection").val();
		select_section(id_class);
	});

	// Change Tipo Escola
	$('#tipo_escola').change(function() {

		var id_nucleo = $("#nuc").val();
		var tipo_escola = $("#tipo_escola").val();

		$.ajax('<?php echo site_url('admin/get_escola_nucleo_tipo/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo,
				tipo: tipo_escola
			}, // data to submit
			success: function(data2, status, xhr) {

				$("#esc").val("");
				$("#esc").empty();
				$("#esc").select2("val", "");
				data2 = $.parseJSON(data2);

				if ((data2 == null) | (data2 == '')) {
					$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
				} else {
					for (var i = 0; i < data2.length; i++) {
						$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
					}
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});
	});


	//Select2
	$(document).ready(function() {
		$('#div_projetos').css('display', 'none');
		$('#nuc').select2();
	});
	$(document).ready(function() {
		$('#turm').select2();
	});
	$(document).ready(function() {
		$('#modalidade_id').select2();
	});
	$(document).ready(function() {
		$('#esc').select2();
	});
	$(document).ready(function() {
		$('#instrutores').select2();
	});
	$(document).ready(function() {
		$('#local_execucao').select2();
	});
</script>