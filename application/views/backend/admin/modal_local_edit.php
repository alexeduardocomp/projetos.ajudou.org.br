<?php
$edit_data = $this->db->get_where('local_execucao', array('id' => $param2))->result_array();
foreach ($edit_data as $row) :
?>

	<?php
	$query = $this->db->get('projeto');
	$projetos = $query->result();
	?>

	<script type="text/javascript">
		$(document).ready(function() {
			$(".cpf").mask('000.000.000-00');
			$('.rg').mask('AA.000.000-A');
			$('.tel').mask('(00) 00000-0000');
			$('.cep').mask('00000-000');
		});
	</script>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						Atualizar local de execução
					</div>
				</div>
				<div class="panel-body">

					<?php echo form_open(site_url('admin/local_execucao/edit/' . $row['id']), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Projeto</label>
						<div class="col-sm-5">
							<select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
								<option id="projetoatual" selected style="display: none;" value="<?php echo $row['id_projeto']; ?>"><?php echo $row['nome_projeto']; ?></option>
								<?php
								$tama = count($projetos);
								for ($i = 0; $i < $tama; $i++) {
									echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
								}
								?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Núcleo</label>
						<div class="col-sm-5">
							<select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
								<option disabled style="display: none;" id="nucleoatual" selected value="<?php echo $row['id_nucleo']; ?>"><?php echo $row['nome_nucleo']; ?></option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Local</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="local" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['local']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Telefone</label>
						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="telefone" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['telefone_1']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Telefone 2</label>
						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="telefone2" autofocus value="<?php echo $row['telefone_2']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Estado</label>
						<div class="col-sm-5">
							<select name="estado" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus id="est">
								<option value="<?php echo $row['estado']; ?>" selected><?php echo $row['estado']; ?></option>
								<option value="Acre">Acre</option>
								<option value="Alagoas">Alagoas</option>
								<option value="Amapá">Amapá</option>
								<option value="Amazonas">Amazonas</option>
								<option value="Bahia">Bahia</option>
								<option value="Ceará">Ceará</option>
								<option value="Distrito Federal">Distrito Federal</option>
								<option value="Espírito Santo">Espírito Santo</option>
								<option value="Goiás">Goiás</option>
								<option value="Maranhão">Maranhão</option>
								<option value="Mato Grosso">Mato Grosso</option>
								<option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
								<option value="Minas Gerais">Minas Gerais</option>
								<option value="Pará ">Pará </option>
								<option value="Paraíba">Paraíba</option>
								<option value="Paraná">Paraná</option>
								<option value="Pernambuco">Pernambuco</option>
								<option value="Piauí">Piauí</option>
								<option value="Rio de Janeiro">Rio de Janeiro</option>
								<option value="Rio Grande do Norte">Rio Grande do Norte</option>
								<option value="Rio Grande do Sul">Rio Grande do Sul</option>
								<option value="Rondônia">Rondônia</option>
								<option value="Roraima">Roraima</option>
								<option value="Santa Catarina">Santa Catarina</option>
								<option value="São Paulo">São Paulo</option>
								<option value="Sergipe">Sergipe</option>
								<option value="Tocantins">Tocantins</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Cidade</label>
						<div class="col-sm-5">
							<select name="cidade" class="form-control" id="cid">
								<option id="cidadeatual" disabled selected style="display: none;" value="<?php echo $row['cidade']; ?>"><?php echo $row['cidade']; ?></option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CEP</label>
						<div class="col-sm-5">
							<input type="text" class="form-control cep" name="cep" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['cep']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Bairro</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="bairro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['bairro']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Rua</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="rua" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['rua']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Numero</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="numero" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="<?php echo $row['numero']; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Complemento</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="complemento" autofocus value="<?php echo $row['complemento']; ?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-default">Atualizar</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>

<script type="text/javascript">
	$(document).ready(function() {
		var id_projeto = $("#pro").val();
		var nucleoatual = $("#nucleoatual").val();
		var projetoatual = $("#projetoatual").val();

		$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {

				//$("#nuc").empty();
				data = $.parseJSON(data);

				for (var i = 0; i < data.length; i++) {

					if (data[i]['id_nucleo'] == nucleoatual) {
						$("#nuc").append('<option selected  value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
					} else {
						$("#nuc").append('<option  value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
					}
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});

		var estado = $("#est").val();
		var cidadeatual = $("#cidadeatual").val();

		$.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: estado
			}, // data to submit
			success: function(data, status, xhr) {
				//$("#cid").empty();
				data = $.parseJSON(data);
				//alert(data);

				for (var i = 0; i < data.length; i++) {
					if (data[i]['nome_cidade'] == cidadeatual) {
						$("#cid").append('<option selected value=' + data[i]['id'] + '>' + data[i]['nome_cidade'] + '</option>');
					} else {
						$("#cid").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_cidade'] + '</option>');
					}
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});
	});

	$("#pro").change(function() {
		var id_projeto = $("#pro").val();

		$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {

				$("#nuc").empty();
				data = $.parseJSON(data);

				for (var i = 0; i < data.length; i++) {
					$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});
	});


	$("#est").change(function() {
		var estado = $("#est").val();

		$.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: estado
			}, // data to submit
			success: function(data, status, xhr) {
				$("#cid").empty();
				data = $.parseJSON(data);
				for (var i = 0; i < data.length; i++) {
					$("#cid").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_cidade'] + '</option>');
				}
			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});
	});
</script>