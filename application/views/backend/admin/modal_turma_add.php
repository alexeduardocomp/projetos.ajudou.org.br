<?php
$query = $this
->db
->order_by('projeto.nome', 'ASC')
->get('projeto');
$projetos = $query->result();

$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();

$query3 = $this
->db
->order_by('modalidade.modalidade', 'ASC')
->get('modalidade');

$modalidades = $query3->result();
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title">
                    <i class="entypo-plus-circled"></i>
                    Adicionar turma
                </div>
            </div> 
            <div class="panel-body">

                <?php echo form_open(site_url('admin/classes/create'), array('class' => 'form-horizontal form-groups-bordered validate', 'target' => '_top')); ?>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                    <div class="col-sm-5">
                        <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <?php
                            $tama = count($projetos);
                            for ($i = 0; $i < $tama; $i++) {
                                echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                    <div class="col-sm-5">
                        <select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled selected>Selecione um projeto</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Local execução</label>
                    <div class="col-sm-5">
                        <select name="id_local" class="form-control" id="loc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled selected>Selecione um núcleo</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Instrutor</label>
                    <div class="col-sm-5">
                        <select name="id_istrutor" class="form-control" id="ins" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled selected>Selecione um núcleo</option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Modalidade</label>
                    <div class="col-sm-5">
                        <select name="modalidade" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <?php
                            $tama = count($modalidades);
                            for ($i = 0; $i < $tama; $i++) {
                                echo '<option value=' . $modalidades[$i]->id . '>' . $modalidades[$i]->modalidade . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Tipo de Material</label>
                    <div class="col-sm-5">
                        <select name="tipo_material" class="form-control" id="tipo_material" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <option value="uniforme">Uniforme</option>  
                            <option value="quimono">Quimono</option>                           
                            <option value="ambos">Ambos</option>                           
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="name" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Categoria</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="categoria" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" />
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Intervalo de idades</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="intervalo_idades" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" />
                    </div>
                </div>

                <!-- <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Sexo</label>
                                  <div class="col-sm-5">
                                    <select name="sexo" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">

                                        <option selected value='MASCULINO'>MASCULINO</option>
                                         <option value='FEMININO'>FEMININO</option>
                                    
                                      
                                    </select>
                                </div>

                                </div>-->

                <div class="form-group">
                    <label class="col-sm-3 control-label">Número de Atletas</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" name="numero_atletas" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" />
                    </div>
                </div>
                <div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Dias da Semana</label>
					<div class="col-sm-5">
						<select id="dias_semana" class="form-control" name="dias_semana" multiple="multiple" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option value="1"> Segunda-feira </option>
                            <option value="2"> Terça-feira </option>
                            <option value="3"> Quarta-feira </option>
                            <option value="4"> Quinta-feira </option>
                            <option value="5"> Sexta-feira </option>
                            <option value="6"> Sábado </option>
                            <option value="7"> Domingo </option>    
                        </select>
					</div>
				</div>
                <input name="dias" id="dias" type="hidden"></input>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Hora de começo</label>
                    <div class="col-sm-9">
                        <div class="col-md-4">
                            <select name="time_start" id="starting_hour" class="form-control selectboxit">
                                <option value="">Hora</option>
                                <?php for ($i = 0; $i <= 12; $i++) : ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="time_start_min" id="starting_minute" class="form-control selectboxit">
                                <option value="">Minutos</option>
                                <?php for ($i = 0; $i <= 11; $i++) : ?>
                                    <option value="<?php echo $i * 5; ?>"><?php echo $i * 5; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="starting_ampm" class="form-control selectboxit">
                                <option value="1">am</option>
                                <option value="2">pm</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Hora de término</label>
                    <div class="col-sm-9">
                        <div class="col-md-4">
                            <select name="time_end" id="ending_hour" class="form-control selectboxit">
                                <option value="">Hora</option>
                                <?php for ($i = 0; $i <= 12; $i++) : ?>
                                    <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="time_end_min" id="ending_minute" class="form-control selectboxit">
                                <option value="">Minutos</option>
                                <?php for ($i = 0; $i <= 11; $i++) : ?>
                                    <option value="<?php echo $i * 5; ?>"><?php echo $i * 5; ?></option>
                                <?php endfor; ?>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <select name="ending_ampm" class="form-control selectboxit">
                                <option value="1">am</option>
                                <option value="2">pm</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-default">Adicionar turma</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#dias_semana').select2();
    });
    $("#dias_semana").change(function() {        
        $("#dias").val(JSON.stringify($("#dias_semana").val()));
    });
    $("#pro").change(function() {
        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                id_projeto: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                $("#nuc").empty();
                data = $.parseJSON(data);

                if ((data == null) | (data == '')) {
                    $("#nuc").append('<option value="" selected disabled>Nenhuma núcleo encontrado</option>');
                } else {
                    for (var i = 0; i < data.length; i++) {
                        $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                    }

                }

                var nucleo = $("#nuc").val();

                $.ajax('<?php echo site_url('admin/get_local_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: nucleo
                    }, // data to submit
                    success: function(data, status, xhr) {

                        $("#loc").empty();
                        data = $.parseJSON(data);

                        if ((data == null) | (data == '')) {
                            $("#loc").append('<option value="" selected disabled>Nenhum local encontrado</option>');
                        } else {
                            for (var i = 0; i < data.length; i++) {
                                $("#loc").append('<option value=' + data[i]['id'] + '>' + data[i]['local'] + '</option>');
                            }
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert("erro");
                    }
                });

                $.ajax('<?php echo site_url('admin/get_instrutor_nucleo2/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: nucleo
                    }, // data to submit
                    success: function(data, status, xhr) {

                        $("#ins").empty();
                        data = $.parseJSON(data);

                        if ((data == null) | (data == '')) {
                            $("#ins").append('<option value="" selected disabled>Nenhum instrutor encontrado</option>');
                        } else {
                            for (var i = 0; i < data.length; i++) {
                                $("#ins").append('<option value=' + data[i]['id_instrutor'] + '>' + data[i]['nome'] + '</option>');
                            }
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert("erro");
                    }
                });
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert("erro");
            }
        });
    });


    $("#est").change(function() {
        var estado = $("#est").val();

        $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: estado
            }, // data to submit
            success: function(data, status, xhr) {
                $("#cid").empty();
                data = $.parseJSON(data);

                if ((data == null) | (data == '')) {
                    $("#cid").append('<option value="" selected disabled>Nenhuma cidade encontrada</option>');
                } else {
                    for (var i = 0; i < data.length; i++) {
                        $("#cid").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_cidade'] + '</option>');
                    }
                }
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert("erro");
            }
        });

    });


    $("#nuc").change(function() {
        var nucleo = $("#nuc").val();

        $.ajax('<?php echo site_url('admin/get_local_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: nucleo
            }, // data to submit
            success: function(data, status, xhr) {

                $("#loc").empty();
                data = $.parseJSON(data);

                if ((data == null) | (data == '')) {
                    $("#loc").append('<option value="" selected disabled>Nenhum local encontrado</option>');
                } else {
                    for (var i = 0; i < data.length; i++) {
                        $("#loc").append('<option value=' + data[i]['id'] + '>' + data[i]['local'] + '</option>');
                    }
                }

                $.ajax('<?php echo site_url('admin/get_instrutor_nucleo2/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: nucleo
                    }, // data to submit
                    success: function(data, status, xhr) {

                        $("#ins").empty();
                        data = $.parseJSON(data);

                        if ((data == null) | (data == '')) {
                            $("#ins").append('<option value="" selected disabled>Nenhum instrutor encontrado</option>');
                        } else {
                            for (var i = 0; i < data.length; i++) {
                                $("#ins").append('<option value=' + data[i]['id_instrutor'] + '>' + data[i]['nome'] + '</option>');
                            }
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert("erro");
                    }
                });
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert("erro");
            }
        });
    });
</script>