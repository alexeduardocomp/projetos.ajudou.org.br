<?php

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(1);
ini_set('display_errors', 1);


echo "<meta charset='utf-8'>";

// ==========================================================
$id_projeto  = $this->input->get('id_projeto');
$nucleos = explode(",", $this->input->get('id_nucleo'));
$tipo_escola  = $this->input->get('tipo_escola');
$modalidades  = explode(",", $this->input->get('modalidade_id'));
$turmas  = explode(",", $this->input->get('class_id'));
$sexo  = $this->input->get('sexo');
$situacao  = $this->input->get('situacao');
$idade_inicial = $this->input->get('idade_inicial'); 
$idade_final  = $this->input->get('idade_final');
$tipo  = $this->input->get('tipo');
$escolas  = explode(",", $this->input->get('escolas'));
$instrutores  = explode(",", $this->input->get('instrutores'));
$local_execucao  = explode(",", $this->input->get('local_execucao'));

$data_inicial = $this->input->get('data_inicial');
$data_final = $this->input->get('data_final');

$ano = $this->input->get('sessional_year');
$ano1 = $ano + 1;
$ano_utilizado = $ano . '-' . $ano1;

$projeto = $id_projeto;
$ano = $ano;
$ano1 = $ano + 1;

/*
echo "<br>";
echo "id_projeto: " . $id_projeto;
echo "<br>";
echo "nucleos: " . var_dump($nucleos);
echo "<br>";
echo "tipo_escola: " . $tipo_escola;
echo "<br>";
echo "modalidades: " . var_dump($modalidades);
echo "<br>";
echo "turmas: " . var_dump($turmas);
echo "<br>";
echo "sexo: " . $sexo;
echo "<br>";
echo "situacao: " . $situacao;
echo "<br>";
echo "idade_inicial: " . $idade_inicial;
echo "<br>";
echo "idade_final: " . $idade_final;
echo "<br>";
echo "escolas: " . var_dump($escolas);
echo "<br>";

echo "instrutores: " . var_dump($instrutores);
echo "<br>";

*/

// ================================= Monta SQL =================================
$where = "";

// WHERE Nucleos
if (!empty($nucleos) && $nucleos != null && $nucleos != ""  && !empty($nucleos[0]) && $nucleos[0] != "null" && $nucleos[0] != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($nucleos as $nuc) {
    if ($contador == 1) {
      $where .= "student.id_nucleo = " . $nuc;
    } else {
      $where .= " OR student.id_nucleo = " . $nuc;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Instrutores
if (!empty($instrutores) && $instrutores != null && $instrutores != "" && !empty($instrutores[0]) && $instrutores[0] != "null" && $instrutores[0] != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($instrutores as $inst) {
    if ($contador == 1) {
      $where .= "class.teacher_id = " . $inst;
    } else {
      $where .= " OR class.teacher_id = " . $inst;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Tipo de Escola
if ($tipo_escola != "AMBOS") {
  $where .= " AND (escola.tipo_escola = '" . $tipo_escola . "'";
  $where .= ")";
}

// WHERE Modalidades
if ($modalidades[0] != null && !empty($modalidades) && $modalidades[0] != "" && $modalidades[0] != "null") {
  $where .= " AND (";
  $contador = 1;

  foreach ($modalidades as $mod) {
    if ($contador == 1) {
      $where .= "modalidade.id = " . $mod;
    } else {
      $where .= " OR modalidade.id = " . $mod;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Turmas
if (!empty($turmas) && $turmas != null && $turmas != "" && !empty($turmas[0]) && $turmas[0] != "null" && $turmas[0] != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($turmas as $turm) {
    if ($contador == 1) {
      $where .= "class.class_id = " . $turm;
    } else {
      $where .= " OR class.class_id = " . $turm;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Sexo
if ($sexo != "AMBOS") {
  $where .= " AND (student.sex = '" . $sexo . "'";
  $where .= ")";
}

// WHERE Situação
if ($situacao != "AMBOS") {
  $where .= " AND (student.situacao = '" . $situacao . "'";
  $where .= ")";
}

// WHERE Idade
if (!empty($idade_final) || $idade_final != null || $idade_final != "" || !empty($idade_inicial) || $idade_inicial != null || $idade_inicial != "") {
  $where .= " AND (YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(student.data_nascimento))) BETWEEN '" . $idade_inicial . "' AND '" . $idade_final . "')";
}


// WHERE Escolas
if (!empty($escolas) && $escolas != null && $escolas != "" && !empty($escolas[0]) && $escolas[0] != "null" && $escolas[0] != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($escolas as $esc) {
    if ($contador == 1) {
      $where .= "escola.id = " . $esc;
    } else {
      $where .= " OR escola.id = " . $esc;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Local de Execução
if (!empty($local_execucao) && $local_execucao != null && $local_execucao != "" && !empty($local_execucao[0]) && $local_execucao[0] != "null" && $local_execucao[0] != "") {

  $where .= " AND (";
  $contador = 1;

  foreach ($local_execucao as $local) {
    if ($contador == 1) {
      $where .= "local_execucao.id = " . $local;
    } else {
      $where .= " OR local_execucao.id = " . $local;
    }
    $contador++;
  }

  $where .= ")";
}

// WHERE Data
if (!empty($data_inicial) || $data_inicial != null || $data_inicial != "" || !empty($data_final) || $data_final != null || $data_final != "") {
  $where .= " AND DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '" . $data_inicial . "' AND '" . $data_final . "'";
}

//FROM student, class, modalidade, enroll 
$sql = " SELECT student.student_id,student.name as nome, modalidade.modalidade as modalidade,class.name as turma, student.cidade, student.situacao, student.estado, student.rua, student.bairro, student.numero, student.complemento, student.phone as telefone, student.data_nascimento 
FROM student 
INNER JOIN enroll ON student.student_id = enroll.student_id
INNER JOIN class ON enroll.class_id = class.class_id
INNER JOIN modalidade ON class.modalidade = modalidade.id
INNER JOIN local_execucao ON local_execucao.id = class.id_local
INNER JOIN escola ON student.id_escola = escola.id
WHERE class.projeto_id = " . $projeto . ""
  . $where . " 
GROUP BY student.student_id
order by student.name ASC";

/*
echo "" . $sql;
echo "<br>";
*/

$query  =  $this->db->query($sql);

// ================================= Resultado =================================

$resultado =  $query->result();

/*
echo "<pre>";
var_dump($resultado);
echo "</pre>";
*/

$nome_projeto = $this->db->get_where('projeto', array('id' => $projeto))->row()->nome;
$proponente = $this->db->get_where('projeto', array('id' => $projeto))->row()->proponente;
$tipo_projeto = $this->db->get_where('projeto', array('id' => $projeto))->row()->tipo_projeto;
$data_inicio_projeto = $this->db->get_where('projeto', array('id' => $projeto))->row()->data_inicio;

$data_atual = date('Y/m/d');

$cont4 = 1;
foreach ($resultado as $res) {
  $dataNascimento = $res->data_nascimento;
  $date = new DateTime($dataNascimento);
  $interval = $date->diff(new DateTime(date('Y-m-d')));
  $idade = $data_atual - $res->data_nascimento;

  $sql = "SELECT class.class_id,class.name as nome_turma,modalidade.modalidade FROM `enroll` 
  INNER JOIN class ON enroll.class_id = class.class_id
  INNER JOIN modalidade ON class.modalidade = modalidade.id
  WHERE enroll.student_id=" . $res->student_id . " ORDER BY date_added DESC LIMIT 1";

  // echo $sql;

  // echo "<br>";
  $query = $this->db->query($sql);
  $ultima_turma_aluno = $query->result();


  $conteudo = $conteudo . '<tr><td>' . $cont4 . '</td><td>' . strtoupper($res->nome) . '</td><td>' . $ultima_turma_aluno[0]->modalidade . '</td><td>' . $ultima_turma_aluno[0]->nome_turma . '</td><td style="text-align:left; font-size:10px; padding:5px;">' . $res->cidade . ' - ' . $res->estado . ', Rua ' . $res->rua . ', ' . $res->bairro . ', ' . $res->numero . ', </td><td>' . $res->telefone . '</td><td>' . $interval->format('%Y anos') . '</td><td><table border="1" width="100%" style="height:100%;"><tr><td></td><td></td></tr></table></td><td></td></tr>';
  $cont4++;
}

$corpo = '<table border="1">';
$cabeca = '<tr><td><b>Nº</b></td>
                            <td><b>Nome</b></td>
                            <td><b>Evento/Modalidade</b></td>
                            <td><b>Turma</b></td>
                            <td><b>Endereço</b></td>
                           
                            <td><b>Telefone</b></td>
                            <td><b>Idade</b></td>
                            <td><table border="1"><tr><td COLSPAN="2"><b>Benefícios</b></td></tr><tr><td width="50%" style="font-size:12px;">Camisa</td><td width="50%" style="font-size:12px;">Quimono</td></tr></table></td>
                            <td><b>OBS</b></td></tr>';



$corpo3 = '</table>';

$data_ass = "<div style='position:absolute; bottom:120px; width:100%; left:70px; text-align:left;'>Local - Data: _____________________________,_______ de _____________________ de ___________</div>";

$assinatura = "<div style='text-align:left; position:absolute; width:100%; bottom:80px; left:70px;'>Assinatura do Profissional(is) responsável(is): ________________________________________________________________________________</div>";
$total = $corpo . $cabeca . $conteudo . $corpo3;


$parcial_final = "";

if ($tipo == "PARCIAL") {
  $parcial_final = '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Projeto: </b> ' . $nome_projeto . '</p></div></td><td>PARCIAL ( X )</td></tr>
  <tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Proponente: </b> ' . $proponente . '</p></div></td><td>FINAL (   )</td></tr>
  ';
} else {
  $parcial_final = '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Projeto: </b> ' . $nome_projeto . '</p></div></td><td>PARCIAL (   )</td></tr>
  <tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Proponente: </b> ' . $proponente . '</p></div></td><td>FINAL ( X )</td></tr>
  ';
}

$periodo = "";

if (!empty($data_inicial) || $data_inicial != null || $data_inicial != "" || !empty($data_final) || $data_final != null || $data_final != "") {
  $periodo = '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Período: </b> ' . date("d/m/Y", strtotime($data_inicial)) . ' - ' .  date("d/m/Y", strtotime($data_final)) . '</p></div></td><td>N° SLIE:</td></tr>';
} else {
  $periodo =  '<tr><td style="padding-bottom:5px;" width="70%"><div><p><b>Período: </b> ' . date("d/m/Y", strtotime($data_inicio_projeto)) . ' - ' .  date("d/m/Y") . '</p></div></td><td>N° SLIE:</td></tr>';
} 


if ($tipo_projeto == "CONVÊNIO") {
  $cabecalho = '
<table border="1" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td width="170">
<div class="imagem" ><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
</td>
<td>
<div class="titulo" style="text-transform:uppercase;">RELAÇÃO DE DIRETAMENTE BENEFICIADOS</div>
</td>
</tr>
<tr>
<td style="padding:10px 15px;" width="170">
Críterios utilizados:
</td>
<td style="padding:10px 15px;">
<table border="0" cellspacing="0" cellpadding="0" width="100%">';

  $cabecalho .= $parcial_final;
  $cabecalho .= $periodo;


  $cabecalho .=
    '</table>
</td>
</tr>
</table>';
} else {

  if ($tipo_projeto == "FEDERAL") {
    $logo = base_url() . "uploads/logos/federal.jpg";
  } else {
    $logo = base_url() . "uploads/logos/estadual.jpg";
  }

  $cabecalho = '
<table border="1" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td width="170">
<div class="imagem" ><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
</td>
<td>
<div class="titulo" style="text-transform:uppercase;">RELAÇÃO DE DIRETAMENTE BENEFICIADOS</div>
</td>
<td ROWSPAN="2" width="120">
<div><img src="' . $logo . '" width="100" style="padding:5px 0px;"></div>
</td>
</tr>
<tr>
<td style="padding:10px 15px;" width="170">
Críterios utilizados:
</td>
<td style="padding:10px 15px;">
<table border="0" cellspacing="0" cellpadding="0" width="100%">';

  $cabecalho .= $parcial_final;
  $cabecalho .= $periodo;

  $cabecalho .=
    '</table>
</td>
</tr>
</table>';
}
//echo $cabecalho;
echo $total;

$arquivo = 'relatorio_beneficiados.xls';

// Configurações header para forçar o download
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-type: application/x-msexcel");
header("Content-Disposition: attachment; filename=\"{$arquivo}\"");
header("Content-Description: PHP Generated Data");
// Envia o conteúdo do arquivo
