
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>


            <a href="javascript:;" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_local_add/');?>');"
                class="btn btn-primary pull-right">
                <i class="entypo-plus-circled"></i>
                Adicionar novo local

                </a>
                <br><br>
               <table class="table table-bordered" id="locais">
                    <thead>
                        <tr>
                            <th width="60"><div>Id local</div></th>
                            <th><div>Projeto</div></th>
                            <th><div>Núcleo</div></th>
                            <th><div>Local</div></th>
                            <th><div>Telefone</div></th>
                        
                            <th><div>Cidade</div></th>
        
                            <th><div>Bairro</div></th>
                            <th><div>Rua</div></th>
                            <th><div>Número</div></th>
                            <th><div>Comp</div></th>
                              <th><div>Opções</div></th>
                        </tr>
                    </thead>
                </table>



<!-----  DATA TABLE EXPORT CONFIGURATIONS ---->
<script type="text/javascript">

    jQuery(document).ready(function($) {
        $.fn.dataTable.ext.errMode = 'throw';
        $('#locais').DataTable({
            "processing": true,
            "scrollX": true, 
            "serverSide": true,
            "ajax":{
                "url": "<?php echo site_url('admin/get_locais') ?>",
                "dataType": "json",
                "type": "POST",
            },
            
                  dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
  buttons: [

  { extend: 'copy', text: 'Copiar' },
  { extend: 'excel', text: 'Excel' },
  { extend: 'pdf', text: 'PDF' },
  { extend: 'csv', text: 'CSV' }
  ],
            "columns": [
                { "data": "id" },
                { "data": "nome_projeto" },
                { "data": "nome_nucleo" },
                { "data": "local" },
                { "data": "telefone_1" },
                { "data": "cidade" },
                { "data": "bairro" },
                { "data": "rua" },
                { "data": "numero" },
                { "data": "complemento" },
                { "data": "options",
                    "orderable": false },
            ],

             "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 }                              
        });
    });

    function local_edit_modal(local_id) {
        showAjaxModal('<?php echo site_url('modal/popup/modal_local_edit/');?>' + local_id);
    }

    function local_delete_confirm(local_id) {
        confirm_modal('<?php echo site_url('admin/local_execucao/delete/');?>' + local_id);
    }

</script>

<style type="text/css">
    
textarea.form-control {
    min-height: 100px !important;
}

</style>
