<?php

$query = $this
->db
->order_by('projeto.nome', 'ASC')
->get('projeto');
$projetos = $query->result();

        $query2 = $this->db->get('nucleo');
    $nucleos = $query2->result();

?>

<hr />
<div class="row">
    <div class="col-md-12">

        <!------CONTROL TABS START------>
        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#list" data-toggle="tab"><i class="entypo-menu"></i>
                    Lista de Eventos
                </a></li>
            <li>
                <a href="#add" data-toggle="tab"><i class="entypo-plus-circled"></i>
                    Adicionar Evento
                </a></li>
        </ul>
        <!------CONTROL TABS END------>


        <div class="tab-content">
            <br>
            <!----TABLE LISTING STARTS-->
            <div class="tab-pane box active" id="list">
                <div class="row">

                    <div class="col-md-12">

                        <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
                            <li class="active">
                                <a href="#running" data-toggle="tab">
                                    <span><i class="entypo-home"></i>
                                        Em execução</span>
                                </a>
                            </li>
                            <!--<li class="">
                                <a href="#archived" data-toggle="tab">
                                    <span><i class="entypo-archive"></i>
                                        Arquivados</span>
                                </a>
                            </li>-->
                        </ul>

                        <div class="tab-content"> 
                            <br>
                            <div class="tab-pane active" id="running">

                                <?php include 'running_noticeboard.php'; ?>

                            </div>
                            <div class="tab-pane" id="archived">

                                <?php include 'archived_noticeboard.php'; ?>

                            </div>
                        </div>


                    </div>

                </div>
            </div>
            <!----TABLE LISTING ENDS--->


            <!----CREATION FORM STARTS---->
            <div class="tab-pane box" id="add" style="padding: 5px">
                <div class="box-content">
                    <?php echo form_open(site_url('admin/noticeboard/create') , array(
                      'class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data',
                        'target' => '_top')); ?>


                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                                  <div class="col-sm-5">
                                    <select name="id_projeto" class="form-control" id="pro"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

                                        <option selected disabled>Selecione</option>
                                    <?php

                                    $tama = count($projetos);
                                            for ($i=0; $i < $tama ; $i++) { 
                        
                                                echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

                                            }

                                     ?>
                                      
                                    </select>
                                </div>

                                </div>


                        <div class="form-group">
                        <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                                  <div class="col-sm-5">
                                    <select name="id_nucleo" class="form-control" id="nuc"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
                                            <option disabled selected>Selecione um projeto</option>

                                      
                                    </select>
                                </div>

                                </div>
 
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Título</label>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" name="notice_title" required />
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Descrição</label>
                  		<div class="col-sm-5">
                  		  <textarea class="form-control" rows="5" name="notice"></textarea>
                  		</div>
                  	</div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Data</label>
                        <div class="col-sm-5">
                            <input type="text" class="datepicker form-control" name="create_timestamp"
                              value="<?php echo date('m/d/Y');?>" required />
                        </div>
                    </div>

                    <div class="form-group">
                      <label for="field-1" class="col-sm-3 control-label">Imagem</label>
                      <div class="col-sm-7">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                          <div class="fileinput-new thumbnail" style="width: 300px; height: 150px;" data-trigger="fileinput">
                            <img src="<?php echo base_url(); ?>uploads/placeholder.png" alt="...">
                          </div>
                          <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                          <div>
                            <span class="btn btn-white btn-file">
                              <span class="fileinput-new">Selecione uma imagem</span>
                              <span class="fileinput-exists">Mudar</span>
                              <input type="file" name="image" accept="image/*">
                            </span>
                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover</a>
                          </div>
                        </div>
                      </div>
                    </div>

                    <!--<div class="form-group">
          						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('show_on_website');?></label>
          						<div class="col-sm-4">
          							<select name="show_on_website" class="form-control selectboxit">
                            <option value="1"><?php echo get_phrase('yes');?></option>
                            <option value="0"><?php echo get_phrase('no');?></option>
                        </select>
          						</div>
          					</div>-->

                    <!--<div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo get_phrase('send_sms_to_all'); ?></label>
                        <div class="col-sm-4">
                            <select class="form-control selectboxit" name="check_sms">
                                <option value="1"><?php echo get_phrase('yes'); ?></option>
                                <option value="2"><?php echo get_phrase('no'); ?></option>
                            </select>
                            <br>
                            <span class="badge badge-primary">
                                <?php
                                if ($active_sms_service == 'clickatell')
                                    echo 'Clickatell ' . get_phrase('activated');
                                if ($active_sms_service == 'twilio')
                                    echo 'Twilio ' . get_phrase('activated');
                                if ($active_sms_service == '' || $active_sms_service == 'disabled')
                                    echo get_phrase('sms_service_not_activated');
                                ?>
                            </span>
                        </div>
                    </div>-->

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-5">
                            <button type="submit" id="submit_button" class="btn btn-info">Adicionar evento</button>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
            <!----CREATION FORM ENDS-->

        </div>
    </div>
</div>


<script type="text/javascript">
    


$( "#pro" ).change(function() {
  
     var id_projeto = $("#pro").val();

     $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
    type: 'POST',  // http method
    data: { id_projeto: id_projeto },  // data to submit
    success: function (data, status, xhr) {

             $("#nuc").empty();
         data = $.parseJSON(data);


      for (var i = 0; i < data.length; i++) {
  
   $("#nuc").append('<option value='+data[i]['id_nucleo']+'>'+data[i]['nome_nucleo']+'</option>');
}

    },
    error: function (jqXhr, textStatus, errorMessage) {
        alert("erro");
    }
});
   
});


</script>
<script type="text/javascript">

	jQuery(document).ready(function($)
	{
		

		var datatable = $("#table_export").dataTable({
 
    "scrollX": true,
				"oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
    
 }  

		});
		
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
	});
		
</script>
