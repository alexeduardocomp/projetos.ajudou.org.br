

<script type="text/javascript">
	

	 $(document).ready(function () { 
        $(".cpf").mask('000.000.000-00');
        $('.rg').mask('AA.000.000-A');
        $('.tel').mask('(00) 00000-0000');
        $('.cep').mask('00000-000');
    });

</script>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					Adicionar instrutor
            	</div>
            </div>
			<div class="panel-body">

      <?php echo form_open(site_url('admin/teacher/create/') , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>
<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Telefone</label>
						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="phone" value="" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Telefone 2</label>

						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="telefone_2" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email').'/ Nome de usuário';?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="email" value="" data-validate="required">
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Senha</label>

						<div class="col-sm-5">
							<input type="password" class="form-control" name="password" value="" data-validate="required">
						</div>
					</div>



                   <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CPF</label>

						<div class="col-sm-5">
							<input type="text" class="form-control cpf" name="CPF" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">RG</label>

						<div class="col-sm-5">
							<input type="text" class="form-control rg" name="RG" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Cargo</label>
                                  <div class="col-sm-5">
									<select name="cargo" class="form-control" data-message-required="<?php echo get_phrase('value_required');?>">
  									  <option value="PROFESSOR" selected>PROFESSOR</option>
									  <option value="COORDENADOR">COORDENADOR</option>
									</select>
								</div>

					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Estado</label>
                                  <div class="col-sm-5">
									<select name="estado" class="form-control"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus id="est">
										<option value="" selected disabled>Selecione</option>
  									  <option value="Acre">Acre</option>
  									  <option value="Alagoas">Alagoas</option>
  									  <option value="Amapá">Amapá</option>
  									  <option value="Amazonas">Amazonas</option>
  									  <option value="Bahia">Bahia</option>
  									  <option value="Ceará">Ceará</option>
  									  <option value="Distrito Federal">Distrito Federal</option>
  									  <option value="Espírito Santo">Espírito Santo</option>
  									  <option value="Goiás">Goiás</option>
  									  <option value="Maranhão">Maranhão</option>
  									  <option value="Mato Grosso">Mato Grosso</option>
  									  <option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
  									  <option value="Minas Gerais">Minas Gerais</option>
  									  <option value="Pará ">Pará </option>
  									  <option value="Paraíba">Paraíba</option>
  									  <option value="Paraná">Paraná</option>
  									  <option value="Pernambuco">Pernambuco</option>
  									  <option value="Piauí">Piauí</option>
  									  <option value="Rio de Janeiro">Rio de Janeiro</option>
  									  <option value="Rio Grande do Norte">Rio Grande do Norte</option>
  									  <option value="Rio Grande do Sul">Rio Grande do Sul</option>
  									  <option value="Rondônia">Rondônia</option>
  									  <option value="Roraima">Roraima</option>
  									  <option value="Santa Catarina">Santa Catarina</option>
  									  <option value="São Paulo">São Paulo</option>
  									  <option value="Sergipe">Sergipe</option>
  									  <option value="Tocantins">Tocantins</option>
									</select>
								</div>

					</div>


						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Cidade</label>
                                  <div class="col-sm-5">
									<select name="cidade" class="form-control" id="cid" data-message-required="<?php echo get_phrase('value_required');?>">
											<option disabled selected>Selecione um estado</option>

  									  
									</select>
								</div>

								</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CEP</label>

						<div class="col-sm-5">
							<input type="text" class="form-control cep" name="CEP" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Rua</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="rua" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Bairro</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="bairro" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="numero" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Complemento</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="complemento" value="" autofocus>
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Carga horária</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="carga_horaria" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data contratacao</label>
                        
						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_contratacao" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus
                            	value="">
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Conta bancária</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="conta_bancaria" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Salário bruto</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="salario_bruto" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Salário líquido</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="salario_liquido" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>

					<!--<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Combustível</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="combustivel" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus>
						</div>
					</div>
					


					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Metas qualitativas</label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="metas_qualitativas" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"></textarea>
							</div>
						</div>
					</div>




					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Metas quantitativas</label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="metas_quantitativas" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"></textarea>
							</div>
						</div>
					</div>



					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Objetivos</label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="objetivos" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"></textarea>
							</div>
						</div>
					</div>-->


					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('designation');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="designation" value="" >
						</div>
					</div>-->

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control datepicker" name="birthday" value="" data-start-view="2">
						</div>
					</div>-->

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Sexo</label>

						<div class="col-sm-5">
							<select name="sex" class="form-control selectboxit">
                              <option value="">Selecione</option>
                              <option value="MASCULINO">MASCULINO</option>
                              <option value="FEMININO">FEMININO</option>
                          </select>
						</div>
					</div>

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="address" value="" >
						</div>
					</div>-->

					
					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('about');?></label>
						<div class="col-sm-5">
							<div class="input-group">
								<textarea class="form-control" rows="2" name="about"></textarea>
							</div>
						</div>
					</div>-->
					



					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('social_links');?></label>
						<div class="col-sm-8">
							<div class="input-group">
								<input type="text" class="form-control" name="facebook" placeholder=""
		              value="">
								<div class="input-group-addon">
									<a href="#"><i class="entypo-facebook"></i></a>
								</div>
							</div>
							<br>
							<div class="input-group">
								<input type="text" class="form-control" name="twitter" placeholder=""
		              value="">
								<div class="input-group-addon">
									<a href="#"><i class="entypo-twitter"></i></a>
								</div>
							</div>
							<br>
							<div class="input-group">
								<input type="text" class="form-control" name="linkedin" placeholder=""
		              value="">
								<div class="input-group-addon">
									<a href="#"><i class="entypo-linkedin"></i></a>
								</div>
							</div>
						</div>
					</div>-->

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('show_on_website');?></label>
						<div class="col-sm-5">
							<select name="show_on_website" class="form-control selectboxit">
                  <option value="1"><?php echo get_phrase('yes');?></option>
                  <option value="0"><?php echo get_phrase('no');?></option>
              </select>
						</div>
					</div>-->

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Foto</label>

						<div class="col-sm-5">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="http://placehold.it/200x200" alt="...">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Selecione uma imagem</span>
										<span class="fileinput-exists">Mudar</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover</a>
								</div>
							</div>
						</div>
					</div>

                    <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info">Adicionar instrutor</button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


$( "#est" ).change(function() {
  
     var estado = $("#est").val();

     $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
    type: 'POST',  // http method
    data: { dados: estado },  // data to submit
    success: function (data, status, xhr) {



    		 $("#cid").empty();
    	 data = $.parseJSON(data);

      for (var i = 0; i < data.length; i++) {
  
   $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
}
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert("erro");
    }
});
   
});

</script>


