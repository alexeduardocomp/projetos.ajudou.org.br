<?php
$this->db->where('tipo_projeto', 'ESTADUAL');
$query = $this->db->get('projeto');
$projetos = $query->result();
$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();

$query3 = $this->db->get('modalidade');
$modalidades = $query3->result();
?>
<style type="text/css">
	.page-body .selectboxit-container {
		/*margin-left: 15px;*/
	}
</style>


<meta charset="utf-8">
<div class="panel panel-primary">
	<div class="panel-heading">
		Lista de Beneficiados
	</div>
	<div class="panel-body">


		<?php echo form_open(site_url('admin/relatorio_projeto_beneficiario_original/'), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data', 'id' => 'formulario_beneficiados')); ?>

		<div class="row">

			<div class="col-md-3">

				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Projeto</label>
					<div class="col-sm-12">
						<select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option selected disabled>Selecione</option>
							<?php
							$tama = count($projetos);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
							}
							?>
						</select>
					</div>

				</div>
			</div>


			<div class="col-md-3">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label" style="text-align: left;">Ano</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="sessional_year" id="ano">
							<?php
							$sessional_year_options = explode('-', $running_year); ?>
							<option value="<?php echo $sessional_year_options[0]; ?>">
								<?php echo $sessional_year_options[0]; ?></option>
							<option value="<?php echo $sessional_year_options[1]; ?>">
								<?php echo $sessional_year_options[1]; ?></option>
						</select>
					</div>
				</div>
			</div>

			<!-- <div class="col-md-3">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Núcleo</label>
					<div class="col-sm-12">
						<select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option disabled selected>Selecione um projeto</option>
						</select>
					</div>
				</div>
			</div> -->

			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label">Núcleo</label>
					<div class="col-sm-12">
						<select name="id_nucleo[]" class="form-control js-example-basic-multiple" multiple="multiple" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
							<option disabled>Selecione um projeto</option>
						</select>
					</div>
				</div>
			</div>

			<!-- 
            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-12">
                        <select name="class_id[]" class="form-control" id="turm" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" onchange="return get_class_sections(this.value)">
                            <option selected disabled>Selecione um núcleo</option>





                        </select>
                    </div>
                </div>
            </div> -->

			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Modalidades</label>
					<div class="col-sm-12">
						<select name="modalidade_id[]" class="form-control" id="modalidade_id" multiple>
							<?php
							$tama = count($modalidades);
							for ($i = 0; $i < $tama; $i++) {
								echo '<option value=' . $modalidades[$i]->id . '>' . $modalidades[$i]->modalidade . '</option>';
							}
							?>
						</select>
					</div>

				</div>
			</div>





		</div>

		<div class="row" style="margin-top:0px;" id="linha_02">

			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Turma</label>
					<div class="col-sm-12">
						<select name="class_id[]" class="form-control" id="turm" multiple>

							<option selected disabled>Selecione: núcleo e modalidade</option>

						</select>
					</div>

				</div>
			</div>

			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Tipo Escola</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="tipo_escola" id="tipo_escola">
							<option value="AMBOS" selected>AMBOS</option>
							<option value="PÚBLICA">PÚBLICA
							</option>
							<option value="PARTICULAR">PARTICULAR
							</option>
						</select>
					</div>
				</div>
			</div>


			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label" style="text-align: left;">Escola</label>
					<div class="col-sm-12">
						<select name="escola_id[]" class="form-control" id="esc" onchange="return get_class_sections(this.value)" multiple>
							<option selected disabled>Selecione: núcleo e tipo</option>
						</select>
					</div>
				</div>
			</div>
			<!-- 
            	<div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Local de Execução</label>
                    <div class="col-sm-12">
                        <select name="local" class="form-control" id="local" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" onchange="return get_class_sections(this.value)">
                            <option selected disabled>Selecione um núcleo</option>
                        </select>
                    </div>
                </div>
            </div> -->




			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-8 control-label" style="text-align: left;">Sexo</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="sexo" id="sexo">
							<option value="AMBOS">AMBOS</option>
							<option value="MASCULINO">MASCULINO
							</option>
							<option value="FEMININO">FEMININO
							</option>
						</select>
					</div>
				</div>
			</div>














		</div>

		<div class="row" style="margin-top:0px;" id="linha_03">



			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-3 control-label" style="text-align: left;">Situação</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="situacao" id="situacao">
							<option value="AMBOS" selected>AMBAS</option>
							<option value="ATIVO">ATIVO
							</option>
							<option value="INATIVO">INATIVO
							</option>
						</select>
					</div>
				</div>
			</div>

			<!-- 
			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Idade</label>
					<div class="col-sm-12">
						<select class="form-control selectboxit" name="idade" id="idade">

							<option value="1">4 a 10 anos
							</option>
							<option value="2">10 a 15 anos
							</option>

							<option value="3">15 a 20 anos
							</option>
							<option value="4">20 a 25 anos
							</option>
							<option value="5">25 a 30 anos
							</option>
						</select>
					</div>
				</div>
			</div>
 -->


			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Idade Inicial</label>
					<div class="col-sm-12">
						<input type="number" name="idade_inicial" id="idade_inicial" value="0" class="form-control">
					</div>
				</div>
			</div>



			<div class="col-md-3 aparecer" style="display: none;">
				<div class="form-group">
					<label for="field-1" class="col-sm-6 control-label" style="text-align: left;">Idade Final</label>
					<div class="col-sm-12">
						<input type="number" name="idade_final" id="idade_final" value="100" class="form-control">
					</div>
				</div>
			</div>











			<!-- <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info" id="submit">Gerar Relatório</button>
						</div>
					</div> -->
		</div>

		<div class="row" style="margin-top:20px;">
			<div class="col-md-2">

				<div class="col-md-12">
					<div class="btn btn-info" id="add_filtro">Adicionar Filtros</div>
				</div>
			</div>

			<div class="col-md-2">
				<input type="hidden" name="year" value="<?php echo $running_year; ?>">
				<div class="col-md-12">
					<button type="submit" class="btn btn-info" id="submit" target="_blank">Gerar Relatório</button>
				</div>
			</div>

			<?php echo form_close(); ?>
		</div>
	</div>

</div>
</div>

<script type="text/javascript">
	$('#add_filtro').click(function() {



		var action = $('#formulario_beneficiados').attr('action');
		//alert(action);


		if (action == '<?php echo site_url('admin/relatorio_projeto_beneficiario_original/'); ?>') {
			$('#formulario_beneficiados').attr('action', '<?php echo site_url('admin/relatorio_projeto_print_view/'); ?>');
		} else {
			$('#formulario_beneficiados').attr('action', '<?php echo site_url('admin/relatorio_projeto_beneficiario_original/'); ?>');
		}




		var estilo = $('.aparecer').css('display');

		//alert(estilo);

		if (estilo == 'none') {

			$('.aparecer').css('display', 'block');
			$('#add_filtro').html('Remover Filtros');

		} else {

			$('.aparecer').css('display', 'none');
			$('#add_filtro').html('Adicionar Filtros');

		}

	})

	// Change Projeto
	$("#pro").change(function() {

		$("#nuc").val("");
		$("#nuc").empty();
		$("#nuc").select2("val", "");

		var id_projeto = $("#pro").val();

		$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {
				data = $.parseJSON(data);

				for (var i = 0; i < data.length; i++) {
					$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
				}
			}
		});
	});


	// $("#pro").change(function() {

	// 	var id_projeto = $("#pro").val();

	// 	$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
	// 		type: 'POST', // http method
	// 		data: {
	// 			dados: id_projeto
	// 		}, // data to submit
	// 		success: function(data, status, xhr) {

	// 			$("#nuc").empty();
	// 			data = $.parseJSON(data);


	// 			for (var i = 0; i < data.length; i++) {

	// 				$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
	// 			}






	// 			var id_nucleo = $("#nuc").val();
	// 			var tipo_escola = $("#tipo_escola").val();

	// 			$.ajax('<?php echo site_url('admin/get_escola_nucleo_tipo/'); ?>', {
	// 				type: 'POST', // http method
	// 				data: {
	// 					dados: id_nucleo,
	// 					tipo: tipo_escola
	// 				}, // data to submit
	// 				success: function(data2, status, xhr) {
	// 					//alert(data2);
	// 					$("#esc").empty();
	// 					data2 = $.parseJSON(data2);





	// 					for (var i = 0; i < data2.length; i++) {

	// 						$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
	// 					}


	// 				},
	// 				error: function(jqXhr, textStatus, errorMessage) {
	// 					alert(errorMessage);
	// 				}
	// 			});


	// 			$.ajax('<?php echo site_url('admin/get_local_nucleo/'); ?>', {
	// 				type: 'POST', // http method
	// 				data: {
	// 					dados: id_nucleo
	// 				}, // data to submit
	// 				success: function(data2, status, xhr) {
	// 					//alert(data2);
	// 					$("#local").empty();
	// 					data2 = $.parseJSON(data2);





	// 					for (var i = 0; i < data2.length; i++) {

	// 						$("#local").append('<option value=' + data2[i]['id'] + '>' + data2[i]['local'] + '</option>');
	// 					}


	// 				},
	// 				error: function(jqXhr, textStatus, errorMessage) {
	// 					alert(errorMessage);
	// 				}
	// 			});


	// 			//alert(tipo_escola);


	// 		},
	// 		error: function(jqXhr, textStatus, errorMessage) {
	// 			alert("erro");
	// 		}
	// 	});

	// });


	$('#modalidade_id').change(function() {




		//array de nucleos
		var id_nucleo = $("#nuc").val();

		//alert(id_nucleo);


		$("#turm").val("");
		$("#turm").empty();
		$("#turm").select2("val", "");


		//array de modalidades
		var valor = $('#modalidade_id').val();





		//alert(valor);

		//alert(valor);

		if ((valor == null) | (valor == '')) {
			$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
		}


		for (var j = 0; j < id_nucleo.length; j++) {



			for (var i = 0; i < valor.length; i++) {


				var id_nuc = id_nucleo[j];
				var id_modalidade = valor[i];



				$.ajax('<?php echo site_url('admin/get_turma_nucleo_modalidade/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: id_nuc,
						id_modalidade: id_modalidade
					}, // data to submit
					success: function(data2, status, xhr) {

						//alert(data2);


						data2 = $.parseJSON(data2);

						if ((data2 == null) | (data2 == '')) {
							//$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
						} else {


							for (var i = 0; i < data2.length; i++) {

								$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
							}

						}







						var id_turma = $("#turm").val();

						$.ajax({
							url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
							success: function(response) {
								jQuery('#section_selector_holder').html(response);
							}
						});


					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});





			}

		}




	});





	$("#nuc").change(function() {


		var id_nucleo = $("#nuc").val();
		//alert(id_nucleo);
		var tipo_escola = $("#tipo_escola").val();

		$.ajax('<?php echo site_url('admin/get_escola_nucleo_tipo/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo,
				tipo: tipo_escola
			}, // data to submit
			success: function(data2, status, xhr) {



				$("#esc").val("");
				$("#esc").empty();
				$("#esc").select2("val", "");


				data2 = $.parseJSON(data2);

				if ((data2 == null) | (data2 == '')) {
					$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
				} else {

					for (var i = 0; i < data2.length; i++) {

						$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
					}
				}


			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
				//alert('aconteceu um erro');
			}
		});

		var valor = $('#modalidade_id').val();

		if ((valor != null) | (valor != " ")) {



			$("#turm").val("");
			$("#turm").empty();
			$("#turm").select2("val", "");



			for (var j = 0; j < id_nucleo.length; j++) {



				for (var i = 0; i < valor.length; i++) {


					var id_nuc = id_nucleo[j];
					var id_modalidade = valor[i];

					$.ajax('<?php echo site_url('admin/get_turma_nucleo_modalidade/'); ?>', {
						type: 'POST', // http method
						data: {
							dados: id_nuc,
							id_modalidade: id_modalidade
						}, // data to submit
						success: function(data2, status, xhr) {

							//alert(data2);


							data2 = $.parseJSON(data2);

							if ((data2 == null) | (data2 == '')) {
								//$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
							} else {


								for (var i = 0; i < data2.length; i++) {

									$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
								}

							}







							var id_turma = $("#turm").val();

							$.ajax({
								url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
								success: function(response) {
									jQuery('#section_selector_holder').html(response);
								}
							});


						},
						error: function(jqXhr, textStatus, errorMessage) {
							alert(errorMessage);
						}
					});





				}

			}




		}






		//  $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
		//     type: 'POST',  // http method
		//     data: { dados: id_nucleo },  // data to submit
		//     success: function (data2, status, xhr) {

		//     		  $("#turm").empty();
		//     	 data2 = $.parseJSON(data2);



		//      if((data2 == null)|(data2 == '')){
		//     	 	$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
		//     	 }else{

		//       for (var i = 0; i < data2.length; i++) {

		//    $("#turm").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
		// }

		// }


		// var id_turma = $("#turm").val();

		// $.ajax({
		//             url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma ,
		//             success: function(response)
		//             {
		//                 jQuery('#section_selector_holder').html(response);
		//             }
		//         });


		//     },
		//     error: function (jqXhr, textStatus, errorMessage) {
		//            alert(errorMessage);
		//     }
		// });




	});




	// $("#pro").change(function() {

	// 	var id_projeto = $("#pro").val();
	// 	var ano = $("#ano").val();

	// 	if ((id_projeto != null) & (id_projeto != "")) {
	// 		$('#submit').removeAttr('disabled');
	// 		$('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_projeto_print_view/?projeto='); ?>' + id_projeto + '&ano=' + ano);
	// 	}
	// });


	$("#ano").change(function() {
		var id_projeto = $("#pro").val();
		var ano = $("#ano").val();

		if ((id_projeto != null) & (id_projeto != "")) {
			//$('#submit').removeAttr('disabled');
			$('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_projeto_print_view/?projeto='); ?>' + id_projeto + '&ano=' + ano);
		}
	});

	var class_selection = "";
	jQuery(document).ready(function($) {
		//$('#submit').attr('disabled', 'disabled');
	});

	function select_section(class_id) {
		if (class_id !== '') {
			$.ajax({
				url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
				success: function(response) {

					jQuery('#section_holder').html(response);
				}
			});
		}
	}

	function check_validation() {
		if (class_selection !== '') {
			//$('#submit').removeAttr('disabled')
		} else {
			//$('#submit').attr('disabled', 'disabled');
		}
	}

	$('#class_selection').change(function() {
		class_selection = $('#class_selection').val();
		check_validation();
	});



	// $( "#pro" ).change(function() {
	// var id_projeto = $("#pro").val();

	//  $.ajax('<?php echo site_url('admin/get_turma_projeto/'); ?>', {
	//     type: 'POST',  // http method
	//     data: { dados: id_projeto },  // data to submit
	//     success: function (data, status, xhr) {

	//     	//alert(data);

	//     		  $('#class_selection').empty();
	//     	 data = $.parseJSON(data);

	//     	 if(data.length > 1){
	//     	 	     for (var i = 0; i < data.length; i++) {

	//     	 	     	if(i == 0){
	//     	 	     		$('#class_selection').append('<option selected value='+data[i]['id']+'>'+data[i]['nome']+'</option>');
	//     	 	     	}else{
	//     	 	     		$('#class_selection').append('<option value='+data[i]['id']+'>'+data[i]['nome']+'</option>');
	//     	 	     	}


	// }
	// class_selection = 'selecionado';
	// check_validation();
	// var id_class = $("#class_selection").val();
	// select_section(id_class);
	//     	 }else{
	//     	 	class_selection = '';
	//     	 	check_validation();
	//     $('#class_selection').append('<option disabled selected>Nenhuma turma encontrada</option>');
	//     	 }   




	//     },
	//     error: function (jqXhr, textStatus, errorMessage) {
	//            alert(errorMessage);
	//     }
	// });

	// });

	$('#class_selection').change(function() {


		var id_class = $("#class_selection").val();

		select_section(id_class);

	});


	$('#tipo_escola').change(function() {

		var id_nucleo = $("#nuc").val();

		//alert(id_nucleo);
		//alert(id_nucleo);
		var tipo_escola = $("#tipo_escola").val();

		//alert(tipo_escola);

		$.ajax('<?php echo site_url('admin/get_escola_nucleo_tipo/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo,
				tipo: tipo_escola
			}, // data to submit
			success: function(data2, status, xhr) {
				//alert(data2);





				$("#esc").val("");
				$("#esc").empty();
				$("#esc").select2("val", "");
				data2 = $.parseJSON(data2);

				if ((data2 == null) | (data2 == '')) {
					$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
				} else {

					for (var i = 0; i < data2.length; i++) {

						$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
					}
				}


			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
				//alert('aconteceu um erro');
			}
		});



	});


	//Select2
	$(document).ready(function() {
		$('#nuc').select2();
	});
	$(document).ready(function() {
		$('#turm').select2();
	});
	$(document).ready(function() {
		$('#modalidade_id').select2();
	});

	$(document).ready(function() {
		$('#esc').select2();
	});
</script>