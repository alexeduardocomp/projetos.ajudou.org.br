<?php
$system_name        =	$this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
//$system_title       =	$this->db->get_where('settings' , array('type'=>'system_title'))->row()->description;
$text_align         =	$this->db->get_where('settings', array('type' => 'text_align'))->row()->description;
$account_type       =	$this->session->userdata('login_type');
$account_type_id	=	$this->session->userdata('login_user_id');
$skin_colour        =   $this->db->get_where('settings', array('type' => 'skin_colour'))->row()->description;
$active_sms_service =   $this->db->get_where('settings', array('type' => 'active_sms_service'))->row()->description;
$running_year 		=   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
?>
<!DOCTYPE html>
<html lang="en" dir="<?php if ($text_align == 'right-to-left') echo 'rtl'; ?>">

<head>
	<title><?php echo $page_title; ?> | <?php echo $system_name; ?></title>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Ekattor School Manager Pro - Creativeitem" />
	<meta name="author" content="Creativeitem" />

	<?php include 'includes_top.php'; ?>
</head>

<body class="page-body <?php if ($skin_colour != '') echo 'skin-' . $skin_colour; ?>">
	<div class="page-container <?php if ($text_align == 'right-to-left') echo 'right-sidebar'; ?>
		">
		<?php include $account_type . '/navigation.php'; ?>
		<div class="main-content">
			<?php include 'header.php'; ?>

			<h3 style="margin:20px 0px;">
				<i class="entypo-right-circled"></i>
				<?php echo $page_title; ?>

				<?php 
					//$href = site_url('exibir_tutoriais.php?pagina='.$page_name);
					$href = site_url('admin/videos/tutorial/'.$page_name);
					$query = $this->db->get_where('video', array(
						'pagina' => $page_name
					)); 
					if ($query->num_rows() > 0) {
						echo '<a href="'.$href.'" class="help_tutorial" target="_blank"><i class="entypo-help-circled"></i></a>';
					}				
				?>				
			</h3>
			<!-- Carregar pagina dos videos -->
			<?php 				
				if($page_name == 'exibir_videos'){include $page_name .'.php'; }			
			?>
			

			<?php include $account_type . '/' . $page_name . '.php'; ?>

			<?php include 'footer.php'; ?>
		</div>

	</div>
	<?php include 'modal.php'; ?>
	<?php include 'includes_bottom.php'; ?>
<style>
	.help_tutorial{
		cursor: pointer;
	}
</style>
</body>

</html>