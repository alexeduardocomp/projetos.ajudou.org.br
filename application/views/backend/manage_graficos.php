<?php
$query = $this->db->get('projeto');
$projetos = $query->result();
$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();
?>
<style type="text/css">
    .page-body .selectboxit-container {
        margin-left: 15px;
    }
</style>
<meta charset="utf-8">
<div class="panel panel-primary">
    <div class="panel-heading">
        Gerar Gráficos
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                    <div class="col-sm-12">
                        <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <?php
                            $tama = count($projetos);
                            for ($i = 0; $i < $tama; $i++) {
                                echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                    <div class="col-sm-12">
                        <select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled selected>Selecione um projeto</option>
                        </select>
                    </div>
                </div>
            </div>

            <div id="section_holder" style="display: none;">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label" style="margin-bottom: 5px;">Sessão</label>
                        <select class="form-control selectboxit" name="section_id">
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="control-label">Data inicial</label>
                    <input type="date" class="form-control" name="data_inicial" id="data_inicial" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="control-label">Data final</label>
                    <input type="date" class="form-control" name="data_final" id="data_final" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" autofocus value="">
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Tipo</label>
                    <div class="col-sm-12">
                        <select name="tipo" class="form-control" id="tipo" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled selected>Selecione um Tipo</option>
                            <option value="todos">Todos</option>
                            <option value="escolas">Escolas</option>
                            <option value="faixa_etaria">Faixa etária</option>
                            <option value="moradores_domicilio">Moradores a Domicílio</option>
                            <option value="problemas_saude">Problemas de Saúde</option>
                            <option value="renda">Renda Familiar</option>
                            <option value="genero">Gênero</option>
                            <option value="responsavel_sustento">Responsável pelo Sustento</option>
                            <option value="situacao">Situação</option>
                            <option value="tipo_escola">Tipo Escola</option>
                            <option value="trabalha_patrocinador">Parente trabalha para Patrocinador</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <input type="hidden" name="year" value="<?php echo $running_year; ?>">
                <div class="col-md-2" style="margin-top: 25px; margin-left: 30px;">
                    <a id="link_relatorio" target="_blank"><button id="submit" class="btn btn-info">Gerar Relatório</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#tipo").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_inicial = $("#data_inicial").val();
        var tipo = $("#tipo").val();
        var data_final = $("#data_final").val();


        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }


        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });


    $("#pro").change(function() {

        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                $("#nuc").empty();
                data = $.parseJSON(data);

                for (var i = 0; i < data.length; i++) {

                    $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                }

                var id_nucleo = $("#nuc").val();

                $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: id_nucleo
                    }, // data to submit
                    success: function(data2, status, xhr) {
                        $("#turm").empty();
                        data2 = $.parseJSON(data2);
                        if ((data2 == null) | (data2 == '')) {
                            $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                        } else {
                            for (var i = 0; i < data2.length; i++) {
                                $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                            }
                            var id_turma = $("#turm").val();
                        }

                        var data_inicial = $("#data_inicial").val();

                        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);

                        var id_nucleo = $("#nuc").val();
                        var id_projeto = $("#pro").val();
                        var id_turma = $("#turm").val();

                        var data_final = $("#data_final").val();
                        var tipo = $("#tipo").val();

                        if (tipo == "todos") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "renda") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "problemas_saude") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "moradores_domicilio") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "faixa_etaria") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "escolas") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "genero") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "responsavel_sustento") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "situacao") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "tipo_escola") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }
                        if (tipo == "trabalha_patrocinador") {
                            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                        }


                        // data = $("#data").val();

                        if ((id_projeto != null) & (id_nucleo != null) & (id_turma != null)) {
                            //alert(data);
                            $('#submit').removeAttr('disabled');
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                    }
                });
            }
        });
    });

    $("#turm").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_inicial = $("#data_inicial").val();

        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_final = $("#data_final").val();
        var tipo = $("#tipo").val();

        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }



        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });

    ////// dsfdas
    $("#nuc").change(function() {

        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();

        //alert(id_nucleo);
        $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_nucleo
            }, // data to submit
            success: function(data2, status, xhr) {
                //alert(data2);
                $("#turm").empty();
                data2 = $.parseJSON(data2);

                if ((data2 == null) | (data2 == '')) {
                    $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                } else {
                    for (var i = 0; i < data2.length; i++) {

                        $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                    }
                }

                var id_turma = $("#turm").val();

                var data_inicial = $("#data_inicial").val();

                //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
                var id_nucleo = $("#nuc").val();
                var id_projeto = $("#pro").val();
                var id_turma = $("#turm").val();

                var data_final = $("#data_final").val();
                var tipo = $("#tipo").val();

                if (tipo == "todos") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "renda") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "problemas_saude") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "moradores_domicilio") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "faixa_etaria") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "escolas") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "genero") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "responsavel_sustento") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "situacao") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "tipo_escola") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }
                if (tipo == "trabalha_patrocinador") {
                    $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
                }


                if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {

                    $('#submit').removeAttr('disabled');
                }

                $.ajax({
                    url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
                    success: function(response) {
                        jQuery('#section_selector_holder').html(response);
                    }
                });
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });
    });

    $("#data_inicial").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();
        var data_inicial = $("#data_inicial").val();

        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_final = $("#data_final").val();
        var tipo = $("#tipo").val();

        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }


        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });

    $("#data_final").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();
        var data_inicial = $("#data_inicial").val();

        //$('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial);
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        var data_final = $("#data_final").val();
        var tipo = $("#tipo").val();

        if (tipo == "todos") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_todos_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "renda") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_renda_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "problemas_saude") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_problemas_saude_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "moradores_domicilio") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_moradores_domicilio_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "faixa_etaria") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_faixa_etaria_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "escolas") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_escolas_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "genero") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_genero_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "responsavel_sustento") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_responsavel_sustento_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "situacao") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_situacao_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "tipo_escola") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_tipo_escola_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }
        if (tipo == "trabalha_patrocinador") {
            $('#link_relatorio').attr('href', '<?php echo site_url('admin/graficos_trabalha_patrocinador_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&data_inicial=' + data_inicial + '&data_final=' + data_final);
        }


        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });

    var class_selection = "";
    jQuery(document).ready(function($) {
        $('#submit').attr('disabled', 'disabled');
    });

    function select_section(class_id) {
        if (class_id !== '') {
            $.ajax({
                url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
                success: function(response) {
                    jQuery('#section_holder').html(response);
                }
            });
        }
    }

    function check_validation() {
        if (class_selection !== '') {
            $('#submit').removeAttr('disabled')
        } else {
            $('#submit').attr('disabled', 'disabled');
        }
    }

    $('#class_selection').change(function() {
        class_selection = $('#class_selection').val();
        check_validation();
    });

    $('#class_selection').change(function() {
        var id_class = $("#class_selection").val();
        select_section(id_class);
    });
</script>