<?php
$this->db->where('tipo_projeto', 'CONVÊNIO');
$query = $this->db->get('projeto');
$projetos = $query->result();
$query2 = $this->db->get('nucleo');
$nucleos = $query2->result();
?>
<style type="text/css">
    .page-body .selectboxit-container {
        margin-left: 15px;
    }
</style>
<meta charset="utf-8">
<div class="panel panel-primary">
    <div class="panel-heading">
        Plano de Aula da
    </div>
    <div class="panel-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Projeto</label>
                    <div class="col-sm-12">
                        <select name="id_projeto" class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <?php
                            $tama = count($projetos);
                            for ($i = 0; $i < $tama; $i++) {
                                echo '<option value=' . $projetos[$i]->id . '>' . $projetos[$i]->nome . '</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                    <div class="col-sm-12">
                        <select name="id_nucleo" class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option disabled selected>Selecione um projeto</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-12">
                        <select name="class_id" class="form-control" id="turm" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" onchange="return get_class_sections(this.value)">
                            <option selected disabled>Selecione um núcleo</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Prestação</label>
                    <div class="col-sm-12">
                        <select name="prestacao" class="form-control" id="prest" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
                            <option selected disabled>Selecione</option>
                            <option value='PARCIAL'>PARCIAL</option>
                            <option value='TOTAL'>TOTAL</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <input type="hidden" name="year" value="<?php echo $running_year; ?>">
                <div class="col-md-2" style="margin-top: 25px; margin-left: 30px;">
                    <a id="link_relatorio" target="_blank"><button id="submit" class="btn btn-info">Gerar Relatório</button></a>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#pro").change(function() {

        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                $("#nuc").empty();
                data = $.parseJSON(data);

                for (var i = 0; i < data.length; i++) {

                    $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                }

                var id_nucleo = $("#nuc").val();

                $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: id_nucleo
                    }, // data to submit
                    success: function(data2, status, xhr) {
                        $("#turm").empty();
                        data2 = $.parseJSON(data2);
                        if ((data2 == null) | (data2 == '')) {
                            $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                        } else {
                            for (var i = 0; i < data2.length; i++) {
                                $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                            }
                            var id_turma = $("#turm").val();
                        }

                        var prestacao = $("#prest").val();

                        $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);
                        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);

                        // data = $("#data").val();

                        if ((id_projeto != null) & (id_nucleo != null) & (id_turma != null)) {
                            //alert(data);
                            $('#submit').removeAttr('disabled');
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                    }
                });
            }
        });
    });

    $("#prest").change(function() {

        var id_projeto = $("#pro").val();

        $.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_projeto
            }, // data to submit
            success: function(data, status, xhr) {

                $("#nuc").empty();
                data = $.parseJSON(data);

                for (var i = 0; i < data.length; i++) {

                    $("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
                }

                var id_nucleo = $("#nuc").val();

                $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
                    type: 'POST', // http method
                    data: {
                        dados: id_nucleo
                    }, // data to submit
                    success: function(data2, status, xhr) {
                        $("#turm").empty();
                        data2 = $.parseJSON(data2);
                        if ((data2 == null) | (data2 == '')) {
                            $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                        } else {
                            for (var i = 0; i < data2.length; i++) {
                                $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                            }
                            var id_turma = $("#turm").val();
                        }


                        var prestacao = $("#prest").val();


                        $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);
                        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);

                        // data = $("#data").val();

                        if ((id_projeto != null) & (id_nucleo != null) & (id_turma != null)) {
                            //alert(data);
                            $('#submit').removeAttr('disabled');
                        }
                    },
                    error: function(jqXhr, textStatus, errorMessage) {
                        alert(errorMessage);
                    }
                });
            }
        });
    });

    $("#turm").change(function() {
        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();


        var prestacao = $("#prest").val();


        $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);
        $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);

        if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {
            $('#submit').removeAttr('disabled');
        }
    });

    ////// dsfdas
    $("#nuc").change(function() {

        var id_nucleo = $("#nuc").val();
        var id_projeto = $("#pro").val();

        //alert(id_nucleo);
        $.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
            type: 'POST', // http method
            data: {
                dados: id_nucleo
            }, // data to submit
            success: function(data2, status, xhr) {
                //alert(data2);
                $("#turm").empty();
                data2 = $.parseJSON(data2);

                if ((data2 == null) | (data2 == '')) {
                    $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
                } else {
                    for (var i = 0; i < data2.length; i++) {

                        $("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');
                    }
                }

                var id_turma = $("#turm").val();
                var prestacao = $("#prest").val();

                $('#link_relatorio').attr('href', '<?php echo site_url('admin/relatorio_plano_aula_print_view/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);
                $('#link_excel').attr('href', '<?php echo site_url('admin/excel/?projeto='); ?>' + id_projeto + '&nucleo=' + id_nucleo + '&turma=' + id_turma + '&prestacao=' + prestacao);

                if ((id_turma != null) & (id_projeto != null) & (id_nucleo != null)) {

                    $('#submit').removeAttr('disabled');
                }

                $.ajax({
                    url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
                    success: function(response) {
                        jQuery('#section_selector_holder').html(response);
                    }
                });
            },
            error: function(jqXhr, textStatus, errorMessage) {
                alert(errorMessage);
            }
        });
    });



    var class_selection = "";
    jQuery(document).ready(function($) {
        $('#submit').attr('disabled', 'disabled');
    });

    function select_section(class_id) {
        if (class_id !== '') {
            $.ajax({
                url: '<?php echo site_url('admin/get_section/'); ?>' + class_id,
                success: function(response) {
                    jQuery('#section_holder').html(response);
                }
            });
        }
    }

    function check_validation() {
        if (class_selection !== '') {
            $('#submit').removeAttr('disabled')
        } else {
            $('#submit').attr('disabled', 'disabled');
        }
    }

    $('#class_selection').change(function() {
        class_selection = $('#class_selection').val();
        check_validation();
    });

    $('#class_selection').change(function() {
        var id_class = $("#class_selection").val();
        select_section(id_class);
    });
</script>