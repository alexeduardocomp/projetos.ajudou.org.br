<?php 
$query = $this->db
->order_by('projeto.nome', 'ASC')
->get('projeto');
$projetos = $query->result();

		$query2 = $this->db->get('nucleo');
	$nucleos = $query2->result();


		$query3 = $this->db->get('escola');
	$escolas = $query3->result();





?>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<script type="text/javascript">
	

	 $(document).ready(function () { 
        $(".cpf").mask('000.000.000-00');
        $('.tel').mask('(00) 00000-0000');
        $('.cep').mask('00000-000');
        $('.numeracao').mask('00.00');
        $('.nt').mask('00');
    });
	$('#login_form').submit(function() {

	});

</script>

<div class="row">
	<div class="col-md-8">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					Cadastrar aluno
            	</div>
            </div>
			<div class="panel-body">

                <?php echo form_open(site_url('teacher/student/create/') , array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data'));?>





						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Projeto</label>
                                  <div class="col-sm-5">
									<select name="id_projeto" class="form-control" id="pro"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

										<option selected disabled>Selecione</option>
									<?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>
  									  
									</select>
								</div>

								</div>


						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Núcleo</label>
                                  <div class="col-sm-5">
									<select name="id_nucleo" class="form-control" id="nuc"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
											<option disabled selected>Selecione um projeto</option>

  									  
									</select>
								</div>

								</div>



									<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Escola</label>
                                  <div class="col-sm-5">
									<select name="id_escola" class="form-control" id="esc"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">

										<option selected disabled>Selecione um núcleo</option>
									
  									  
									</select>
									
								</div>

								</div>



									<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Turma</label>
                                  <div class="col-sm-5">
									<select name="class_id" class="form-control" id="turm"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" onchange="return get_class_sections(this.value)">

										<option selected disabled>Selecione um núcleo</option>
									
  									  
									</select>
									
								</div>

								</div>

								<div class="form-group" style="display: none;">
						<label for="field-2" class="col-sm-3 control-label">Seção</label>
		                    <div class="col-sm-5">
		                        <select name="section_id" class="form-control" id="section_selector_holder">
		                            <option value="">Selecione uma turma</option>

			                    </select>
			                </div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('id_no');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="student_code" value="<?php echo substr(md5(uniqid(rand(), true)), 0, 7); ?>" data-validate="required" id="class_id"
								data-message-required="<?php echo get_phrase('value_required');?>">
						</div>
					</div>




					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="name" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>


						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Situação</label>
                                  <div class="col-sm-5">
									<select name="situacao" class="form-control"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>">
											<option disabled selected>Selecione</option>
											<option value="ATIVO">ATIVO</option>
											<option value="INATIVO">INATIVO</option>

  									  
									</select>
								</div>

								</div>

									<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data nascimento</label>

						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_nascimento" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required id="data_nasc">
						</div>
					</div>



					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Responsável</label>

						<div class="col-sm-5">
							<select name="parent_id" class="form-control select2">
                              <option value="">Selecione</option>
                              <?php
								$parents = $this->db->get('parent')->result_array();
								foreach($parents as $row):
									?>
                            		<option value="<?php echo $row['parent_id'];?>">
										<?php echo $row['name'];?>
                                    </option>
                                <?php
								endforeach;
							  ?>
                          </select>
						</div>
					</div>-->

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome do Pai</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome_pai" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome do Mãe</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome_mae" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CPF Aluno</label>

						<div class="col-sm-5">
							<input type="text" class="form-control cpf" id="cpf_aluno" value="" name="cpf_aluno" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CPF Responsável</label>

						<div class="col-sm-5">
							<input type="text" class="form-control cpf" name="cpf_responsavel" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">RG Aluno</label>

						<div class="col-sm-5">
							<input type="text" class="form-control rg" id="rg_aluno" name="rg_aluno" value="" autofocus>					
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">RG Responsável</label>

						<div class="col-sm-5">
							<input type="text" class="form-control rg" name="rg_responsavel" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Telefone</label>

						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="phone" value="" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Telefone 2</label>

						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="telefone_2">
						</div>
					</div>

							<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CEP</label>

						<div class="col-sm-5">
							<input type="text" class="form-control cep" name="cep" placeholder="Busque pelo CEP" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>



            <div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Estado</label>
                                  <div class="col-sm-5">
									
										<input class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus id="est" name="estado">
								</div>

					</div>


						<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Cidade</label>
                                  <div class="col-sm-5">
								
										<input class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus id="cid" name="cidade">		
  									  
									
								</div>

								</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Rua</label>

						<div class="col-sm-5">
							<input class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus id="rua" name="rua">
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Bairro</label>

						<div class="col-sm-5">
							<input class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>"  autofocus id="bairro" name="bairro">
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="numero" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Complemento</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="complemento" value="" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Problemas de Saúde</label>
                                  <div class="col-sm-5">
									<select name="problema_saude" class="form-control"  data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" id="problema">
											<option disabled selected>Selecione</option>
											<option value="SIM">SIM</option>
											<option value="NÃO">NÃO</option>

  									  
									</select>
								</div>

								</div>

								<div class="form-group" id="descri" style="display: none;">
						<label for="field-1" class="col-sm-3 control-label">Descrição do problema</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="descricao_problema" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Uniforme</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="uniforme" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número do tênis</label>

						<div class="col-sm-5">
							<input type="text" class="form-control nt" name="numero_tenis" value="" autofocus>
						</div>
					</div>




					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número de pessoas no domicílio</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="numero_pessoas_domicilio" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required maxlength="3" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode))) return true; else return false;">
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Principal responsável pelo sustento</label>

						<div class="col-sm-5">
							<select name="responsavel_sustento" class="form-control selectboxit">
                              <option value="" disabled="">Selecione</option>
                              <option value="O PRÓPRIO">O PRÓPRIO</option>
                              <option value="PAI">PAI</option>
                              <option value="MÃE">MÃE</option>
                              <option value="CASAL">CASAL</option>
                              <option value="PARENTE">PARENTE</option>
                          </select>
						</div>
					</div>


					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Renda familiar</label>

						<div class="col-sm-5">
							<select name="renda_familiar" class="form-control selectboxit">
                              <option value="" disabled="">Selecione</option>
                              <option value="SEM RENDA">SEM RENDA</option>
                               <option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                              <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>
                          </select>
						</div>
					</div>


					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Tem parente que trabalha na patrocinadora</label>

						<div class="col-sm-5">
							<select name="parente_trabalha_patrocinadora" class="form-control selectboxit">
                              <option value="" disabled="">Selecione</option>
                              <option value="SIM">SIM</option>
                              <option value="NÃO">NÃO</option>           
                          </select>
						</div>
					</div>

							<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data em que o aluno entrou no projeto</label>

						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_aluno_projeto" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" autofocus required>
						</div>
					</div>





					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('class');?></label>

						<div class="col-sm-5">
							<select name="class_id" class="form-control" data-validate="required" id="class_id"
								data-message-required="<?php echo get_phrase('value_required');?>"
									onchange="return get_class_sections(this.value)">
                              <option value=""><?php echo get_phrase('select');?></option>
                              <?php
								$classes = $this->db->get('class')->result_array();
								foreach($classes as $row):
									?>
                            		<option value="<?php echo $row['class_id'];?>">
											<?php echo $row['name'];?>
                                            </option>
                                <?php
								endforeach;
							  ?>
                          </select>
						</div>
					</div>-->

					

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('birthday');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control datepicker" name="birthday" value="" data-start-view="2">
						</div>
					</div>-->

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Sexo</label>

						<div class="col-sm-5">
							<select name="sex" class="form-control selectboxit">
                              <option value="" disabled="">Selecione</option>
                              <option value="MASCULINO">MASCULINO</option>
                              <option value="FEMININO">FEMININO</option>
                          </select>
						</div>
					</div>

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address');?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="address" value="" >
						</div>
					</div>-->

					

					<!--<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email').' / '.get_phrase('username');?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="email" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="">
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Senha</label>

						<div class="col-sm-5">
							<input type="password" class="form-control" name="password" data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="" >
						</div>
					</div>-->

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('dormitory');?></label>

						<div class="col-sm-5">
							<select name="dormitory_id" class="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
	                              <?php
	                              	$dormitories = $this->db->get('dormitory')->result_array();
	                              	foreach($dormitories as $row):
	                              ?>
                              		<option value="<?php echo $row['dormitory_id'];?>"><?php echo $row['name'];?></option>
                          		<?php endforeach;?>
                          </select>
						</div>
					</div>-->

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('transport_route');?></label>

						<div class="col-sm-5">
							<select name="transport_id" class="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select');?></option>
	                              <?php
	                              	$transports = $this->db->get('transport')->result_array();
	                              	foreach($transports as $row):
	                              ?>
                              		<option value="<?php echo $row['transport_id'];?>"><?php echo $row['route_name'];?></option>
                          		<?php endforeach;?>
                          </select>
						</div>
					</div>-->

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Foto</label>

						<div class="col-sm-5">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="http://placehold.it/200x200" alt="...">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Selecione uma imagem</span>
										<span class="fileinput-exists">Mudar</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover</a>
								</div>
							</div>
						</div>
					</div>

                    <div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info" id="submit">Adicionar aluno</button>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <div class="col-md-4">
		<blockquote class="blockquote-blue">
			<p>
				<strong>Instrução de Admissão do Aluno</strong>
			</p>
			<p>
				A admissão de novos alunos criará automaticamente uma inscrição na turma selecionada na sessão em execução.
			</p>
		</blockquote>
	</div>

</div>

<script type="text/javascript">

	function get_class_sections(class_id) {

    	$.ajax({
            url: '<?php echo site_url('teacher/get_class_section/');?>' + class_id ,
            success: function(response)
            {
                jQuery('#section_selector_holder').html(response);
            }
        });

    }

</script>



<script type="text/javascript">
 

$( "#pro" ).change(function() {
  
     var id_projeto = $("#pro").val();

     $.ajax('<?php echo site_url('teacher/get_nucleo_projeto/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_projeto },  // data to submit
    success: function (data, status, xhr) {

    		 $("#nuc").empty();
    	 data = $.parseJSON(data);


      for (var i = 0; i < data.length; i++) {
  
   $("#nuc").append('<option value='+data[i]['id_nucleo']+'>'+data[i]['nome_nucleo']+'</option>');
}






var id_nucleo = $("#nuc").val();


 $.ajax('<?php echo site_url('teacher/get_escola_nucleo2/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#esc").empty();
    	 data2 = $.parseJSON(data2);



    

      for (var i = 0; i < data2.length; i++) {
  
   $("#esc").append('<option value='+data2[i]['id_escola']+'>'+data2[i]['nome_escola']+'</option>');
}
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
}); 


  $.ajax('<?php echo site_url('teacher/get_turma_nucleo_instrutor/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

  

    		  $("#turm").empty();
    	 data2 = $.parseJSON(data2);

    	 if((data2 == null)|(data2 == '')){
    	 	$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada para esse instrutor</option>');
    	 }else{


      for (var i = 0; i < data2.length; i++) {
  
   		$("#turm").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
}

    	 }



    



var id_turma = $("#turm").val();

	$.ajax({
				url: '<?php echo site_url('teacher/get_class_section/');?>' + id_turma ,
				success: function(response)
				{
					jQuery('#section_selector_holder').html(response);
				}
			});
		

		},
		error: function (jqXhr, textStatus, errorMessage) {
			alert(errorMessage);
		}
	});





      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert("erro");
    }
});
   
});





$( "#nuc" ).change(function() {


var id_nucleo = $("#nuc").val();


 $.ajax('<?php echo site_url('teacher/get_escola_nucleo2/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {



    		 $("#esc").empty();
    	 data2 = $.parseJSON(data2);

    	  if((data2 == null)|(data2 == '')){
    	 	$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
    	 }else{

      for (var i = 0; i < data2.length; i++) {
  
   $("#esc").append('<option value='+data2[i]['id_escola']+'>'+data2[i]['nome_escola']+'</option>');
}
}
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});



 $.ajax('<?php echo site_url('teacher/get_turma_nucleo_instrutor/'); ?>', {
    type: 'POST',  // http method
    data: { dados: id_nucleo },  // data to submit
    success: function (data2, status, xhr) {

    		  $("#turm").empty();
    		 
    	 data2 = $.parseJSON(data2); 


     if((data2 == null)|(data2 == '')){
    	 	$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada para esse instrutor</option>');
    	 }else{

      for (var i = 0; i < data2.length; i++) {
  
   $("#turm").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
}

}


var id_turma = $("#turm").val();

$.ajax({
            url: '<?php echo site_url('teacher/get_class_section/');?>' + id_turma ,
            success: function(response)
            {
                jQuery('#section_selector_holder').html(response);
            }
        });
      

    },
    error: function (jqXhr, textStatus, errorMessage) {
           alert(errorMessage);
    }
});




});



// $( "#est" ).change(function() {
  
//      var estado = $("#est").val();

//      $.ajax('<?php echo site_url('teacher/get_cidade_estado/'); ?>', {
//     type: 'POST',  // http method
//     data: { dados: estado },  // data to submit
//     success: function (data, status, xhr) {



//     		 $("#cid").empty();
//     	 data = $.parseJSON(data);

//     	  if((data == null)|(data == '')){
//     	 	$("#cid").append('<option value="" selected disabled>Nenhuma cidade encontrada</option>');
//     	 }else{

//       for (var i = 0; i < data.length; i++) {
  
//    $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
// }
// }
      

//     },
//     error: function (jqXhr, textStatus, errorMessage) {
//            alert("erro");
//     }
// });
   
// });




$( "#problema" ).change(function() {

 var problema = $("#problema").val();


 if(problema == "SIM"){

 	$("#descri").css('display','block');
 }

 if(problema == "NÃO"){
 	$("#descri").css('display','none');
 }

});



$('#data_nasc').change(function(event){


 var data = $('#data_nasc').val();

 var data_nova = new Date(data);



var data_atual = new Date();



var resultado = data_atual.getFullYear() - data_nova.getFullYear();



if(resultado <= 2){
	alert('Data de nascimento inválida!');
	$('#submit').attr('disabled', 'disabled');
}else{
	$('#submit').removeAttr('disabled');
}


});




</script>


<script type="text/javascript">
	

jQuery(window).ready(function(){
			var SPMaskBehavior = function (val) {
			    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			  },
			  spOptions = {
			    onKeyPress: function(val, e, field, options) {
			        field.mask(SPMaskBehavior.apply({}, arguments), options);
			      }
			  };
			$('[name="cpf"]').mask('000.000.000-00', {reverse: true});
			$('[name="cell"]').mask(SPMaskBehavior, spOptions);
			$('[name="cep"]').mask('00000-000');
			

			$('[name="cep"]').blur(function() {

			    //Nova variável "cep" somente com dígitos.
			    var cep = $(this).val().replace(/\D/g, '');

			    //Verifica se campo cep possui valor informado.
			    if (cep != "") {
			        //Expressão regular para validar o CEP.
			        var validacep = /^[0-9]{8}$/;
			        //Valida o formato do CEP.
			        if(validacep.test(cep)) {

			            //Preenche os campos com "..." enquanto consulta webservice.
			            $("#rua").val("Procurando...");
			            $("#bairro").val("Procurando...");
			            $("#cid").val("Procurando...");
			             $("#est").val("Procurando...");
			            $("#uf").val("Procurando...");
			            $("#estado").val("Procurando...");

			            //Consulta o webservice viacep.com.br/
			             $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

			                if (!("erro" in dados)) {

			                    //Atualiza os campos com os valores da consulta.
			                    $("#rua").val(dados.logradouro);
			                    $("#bairro").val(dados.bairro);
			                    $("#cid").val(dados.localidade);
			                    $("#uf").val(dados.uf);
			                    $("#est").val(dados.uf);

			             
			                } else {
			                    //CEP pesquisado não foi encontrado.
			                                limpa_formulario_cep();
			                                alert("CEP não encontrado.");
			                }
			            });
			        } else {
			            //cep é inválido.
			            limpa_formulario_cep();
			            alert("Formato de CEP inválido.");
			        }
			    } else {
			        //cep sem valor, limpa formulário.
			        limpa_formulario_cep();
				}
			});

			function limpa_formulario_cep() {
			    // Limpa valores do formulário de cep.
			    $("#rua").val("");
			    $("#bairro").val("");
			    $("#cid").val("");
			    $("#est").val("");
			    $("#uf").val("");
			    $("#cep").val("");
			}
		});


</script>


