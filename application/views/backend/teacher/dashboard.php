<hr>
<div class="row">
    <div class="col-md-8">
        <div class="row">
            <!-- CALENDAR-->
            <div class="col-md-12 col-xs-12">
                <div class="panel panel-primary " data-collapsed="0">
                    <div class="panel-heading">
                        <div class="panel-title">
                            <i class="fa fa-calendar"></i>
                            Agenda
                        </div>
                    </div>
                    <div class="panel-body" style="padding:0px;">
                        <div class="calendar-env">
                            <div class="calendar-body">
                                <div id="notice_calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="row">


            <div class="col-md-6">

                <div class="tile-stats tile-green">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div class="num" data-start="0" data-end="<?php


                                                                $id_teacher = $this->session->userdata('login_user_id');

                                                                $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

                                                                $this->db->select('instrutor_nucleo.id_projeto');
                                                                $this->db->distinct();
                                                                $this->db->from('instrutor_nucleo');

                                                                $this->db->where('id_instrutor', $id_teacher);

                                                                $query = $this->db->get();

                                                                $tamanho  = count($query->result());

                                                                echo $tamanho;

                                                                //echo $this->db->count_all('projeto');
                                                                ?>" data-postfix="" data-duration="800" data-delay="0">0</div>

                    <h3 class="negri">Projetos</h3>
                    <p>Total de projetos</p>
                </div>

            </div>

            <div class="col-md-6">

                <div class="tile-stats tile-blue">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div class="num" data-start="0" data-end="<?php




                                                                $id_teacher = $this->session->userdata('login_user_id');

                                                                $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

                                                                $this->db->select('instrutor_nucleo.id_instrutor');
                                                                $this->db->from('instrutor_nucleo');
                                                                $this->db->where('id_instrutor', $id_teacher);

                                                                $query = $this->db->get();

                                                                $tamanho  = count($query->result());


                                                                echo $tamanho;



                                                                //echo $this->db->count_all('nucleo');
                                                                ?>" data-postfix="" data-duration="800" data-delay="0">0</div>

                    <h3 class="negri">Núcleos</h3>
                    <p>Total de núcleos</p>
                </div>

            </div>

            <div class="col-md-6">

                <div class="tile-stats tile-red">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div class="num" data-start="0" data-end="<?php


                                                                $id_teacher = $this->session->userdata('login_user_id');

                                                                $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;




                                                                $this->db->select('enroll.class_id');
                                                                $this->db->distinct();
                                                                $this->db->from('enroll');
                                                                $this->db->join('class', 'class.class_id=enroll.class_id', 'inner');
                                                                $this->db->join(
                                                                    'teacher',
                                                                    'class.teacher_id=' . $id_teacher . ' ',
                                                                    'inner'
                                                                );
                                                                $this->db->where('enroll.year', $running_year);

                                                                $query = $this->db->get();

                                                                $tamanho  = count($query->result());


                                                                echo $tamanho;




                                                                // echo $this->db->count_all('class');
                                                                ?>" data-postfix="" data-duration="800" data-delay="0">0</div>

                    <h3 class="negri">Turmas</h3>
                    <p>Total de turmas</p>
                </div>

            </div>





            <div class="col-md-6">

                <div class="tile-stats tile-cyan">
                    <div class="icon"><i class="fa fa-group"></i></div>
                    <div class="num" data-start="0" data-end="<?php


                                                                $id_teacher = $this->session->userdata('login_user_id');

                                                                $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;




                                                                $this->db->select('enroll.student_id');
                                                                $this->db->distinct();
                                                                $this->db->from('enroll');
                                                                $this->db->join('class', 'class.class_id=enroll.class_id', 'inner');
                                                                $this->db->join(
                                                                    'teacher',
                                                                    'class.teacher_id=' . $id_teacher . ' ',
                                                                    'inner'
                                                                );
                                                                $this->db->group_by('enroll.student_id');
                                                                $this->db->where('enroll.year', $running_year);

                                                                $query = $this->db->get();

                                                                $tamanho  = count($query->result());


                                                                echo $tamanho;

                                                                // echo $this->db->count_all('student');
                                                                ?>" data-postfix="" data-duration="1500" data-delay="0">0</div>

                    <h3>Alunos</h3>
                    <p>Total de alunos</p>
                </div>

            </div>
            <!--<div class="col-md-12">

                <div class="tile-stats tile-green">
                    <div class="icon"><i class="entypo-users"></i></div>
                    <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('teacher'); ?>"
                    		data-postfix="" data-duration="800" data-delay="0">0</div>

                    <h3>Instrutores</h3>
                   <p>Total de instrutores</p>
                </div>

            </div>-->
            <!--<div class="col-md-12">

                <div class="tile-stats tile-aqua">
                    <div class="icon"><i class="entypo-user"></i></div>
                    <div class="num" data-start="0" data-end="<?php echo $this->db->count_all('parent'); ?>"
                    		data-postfix="" data-duration="500" data-delay="0">0</div>

                    <h3>Responsáveis</h3>
                   <p>Total de responsáveis</p>
                </div>

            </div>-->
            <!--<div class="col-md-12">

                <div class="tile-stats tile-blue">
                    <div class="icon"><i class="entypo-chart-bar"></i></div>
                    <?php
                    $check   =   array('timestamp' => strtotime(date('Y-m-d')), 'status' => '1');


                    $query = $this->db->get_where('attendance', $check);
                    $present_today      =   $query->num_rows();
                    ?>
                    <div class="num" data-start="0" data-end="<?php echo $present_today; ?>"
                    		data-postfix="" data-duration="500" data-delay="0">0</div>

                    <h3>Presenças</h3>
                   <p>Total de alunos presentes hoje</p>
                </div>

            </div>-->
        </div>
        <div class="bloco_video">
            <h3 style="margin: 20px 0px;">
                <i class="entypo-right-circled"></i>
                Tutorial de cadastros
            </h3>
            <!-- <iframe width="560" height="315" src="https://www.youtube.com/embed/rdgYg35BY-8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> -->
            <iframe class="tutorial_yt" src="https://www.youtube.com/embed/rdgYg35BY-8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>

</div>



<script>
    $(document).ready(function() {

        var calendar = $('#notice_calendar');

        $('#notice_calendar').fullCalendar({
            header: {
                left: 'title',
                right: 'today prev,next'
            },


            monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
            monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
            dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sabado'],
            dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'],
            titleFormat: {
                month: 'MMMM yyyy',
                week: "d[ MMMM][ yyyy]{ - d MMMM yyyy}",
                day: 'dddd, d MMMM yyyy'
            },
            columnFormat: {
                month: 'ddd',
                week: 'ddd d',
                day: ''
            },
            buttonText: {
                prev: "&nbsp;&#9668;&nbsp;",
                next: "&nbsp;&#9658;&nbsp;",
                prevYear: "&nbsp;&lt;&lt;&nbsp;",
                nextYear: "&nbsp;&gt;&gt;&nbsp;",
                today: "Hoje",
                month: "Mês",
                week: "Semana",
                day: "Dia"
            },

            //defaultView: 'basicWeek',

            editable: false,
            firstDay: 1,
            height: 530,
            droppable: false,

            events: [
                <?php
                $notices    =    $this->db->get('noticeboard')->result_array();
                foreach ($notices as $row) :
                ?> {
                        title: "<?php echo $row['notice_title']; ?>",
                        start: new Date(<?php echo date('Y', $row['create_timestamp']); ?>, <?php echo date('m', $row['create_timestamp']) - 1; ?>, <?php echo date('d', $row['create_timestamp']); ?>),
                        end: new Date(<?php echo date('Y', $row['create_timestamp']); ?>, <?php echo date('m', $row['create_timestamp']) - 1; ?>, <?php echo date('d', $row['create_timestamp']); ?>)
                    },
                <?php
                endforeach
                ?>

            ]
        });
    });
</script>


<style type="text/css">
    .calendar-env .calendar-body .fc-content .fc-view table tbody tr td.fc-day .fc-day-number {

        margin-top: 0px;
        margin-left: 10px;
        text-align: center;
    }


    .fc-day>div {
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .tutorial_yt{
        width: 100%;
        height: 315px;
        border-radius: 5px;
    }
    .bloco_video{
        
    }
</style>