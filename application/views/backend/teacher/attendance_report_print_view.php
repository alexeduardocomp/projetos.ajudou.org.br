<?php
$class_name        = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
$section_name          = $this->db->get_where('section', array('section_id' => $section_id))->row()->name;
$system_name        =    $this->db->get_where('settings', array('type' => 'system_name'))->row()->description;
$running_year       =    $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
if ($month == 1) $m = 'Janeiro';
else if ($month == 2) $m = 'Fevereiro';
else if ($month == 3) $m = 'Março';
else if ($month == 4) $m = 'Abril';
else if ($month == 5) $m = 'Maio';
else if ($month == 6) $m = 'Junho';
else if ($month == 7) $m = 'Julho';
else if ($month == 8) $m = 'Agosto';
else if ($month == 9) $m = 'Setembro';
else if ($month == 10) $m = 'Outubro';
else if ($month == 11) $m = 'Novembro';
else if ($month == 12) $m = 'Dezembro';
?>
<div id="print">
    <script src="<?php echo base_url('assets/js/jquery-1.11.0.min.js'); ?>"></script>
    <style type="text/css">
        td {
            padding: 5px;
        }
    </style>

    <center>
        <img src="<?php echo base_url('uploads/logo.png'); ?>" style="max-height : 60px;"><br>
        <h3 style="font-weight: 100;"><?php echo $system_name; ?></h3>
        <?php echo 'Folha de frequência'; ?><br>
        <?php echo  'Turma ' . $class_name; ?><br>
        <?php echo 'Sessão ' . $section_name; ?><br>
        <?php echo $m . ', ' . $sessional_year; ?>

    </center>

    <table border="1" style="width:100%; border-collapse:collapse;border: 1px solid #ccc; margin-top: 10px;">
        <thead>
            <tr>
                <td style="text-align: center;">
                    Alunos <i class="entypo-down-thin"></i> | Data <i class="entypo-right-thin"></i>
                </td>
                <?php
                $year = explode('-', $running_year);
                $days = cal_days_in_month(CAL_GREGORIAN, $month, $sessional_year);
                for ($i = 1; $i <= $days; $i++) {
                ?>
                    <td style="text-align: center;"><?php echo $i; ?></td>
                <?php } ?>

            </tr>
        </thead>

        <tbody>
            <?php
            $data = array();
            $ordenado = array();


            $id_estudantes = array();
            $cont = 0;
            $cont4 = 1;

            $ordenado = array();

            $sql = "SELECT enroll.* FROM `enroll` INNER JOIN student ON enroll.student_id = student.student_id WHERE class_id=" . $class_id . " AND ((year(from_unixtime(date_added)) <='" . $sessional_year . "' AND month(from_unixtime(date_added)) <='" . $month . "') OR (year(from_unixtime(date_added)) <'" . $sessional_year . "'))";
            //  echo $sql;
            //  echo "<br>";
            $query = $this->db->query($sql);

            $alunos_turma_ordenado = $query->result_array();
            for ($i = 0; $i < count($alunos_turma_ordenado); $i++) {
                $sql = "SELECT attendance.class_id FROM `attendance` INNER JOIN student ON attendance.student_id = student.student_id WHERE student.student_id=" . $alunos_turma_ordenado[$i]['student_id'] . " AND year(from_unixtime(timestamp)) ='" . $sessional_year . "' AND month(from_unixtime(timestamp)) ='" . $month . "'  GROUP BY attendance.`class_id`";
                //  echo $sql;
                //  echo "<br>";
                $query = $this->db->query($sql);

                $turmas_mes_aluno = $query->result_array();
                foreach ($turmas_mes_aluno as $t_m) {

                    if ($t_m['class_id'] == $class_id) {

                        //array ordenado recebe meu $stu;
                        array_push($ordenado, $alunos_turma_ordenado[$i]);
                    }
                }
            }

            foreach ($ordenado as $row) :
            ?>
                <tr>
                    <td style="text-align: center;">
                        <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name; ?>
                    </td>
                    <?php
                    $status = 4;
                    for ($i = 1; $i <= $days; $i++) {
                        $timestamp = strtotime($i . '-' . $month . '-' . $sessional_year);
                        //$this->db->group_by('timestamp');
                        $attendance = $this->db->get_where('attendance', array('section_id' => $section_id, 'class_id' => $class_id, 'year' => $running_year, 'timestamp' => $timestamp, 'student_id' => $row['student_id']))->result_array();


                        foreach ($attendance as $row1) :
                            $month_dummy = date('m', $row1['timestamp']);
                            if ($i == $month_dummy);
                            $status = $row1['status'];
                        endforeach;
                    ?>
                        <td style="text-align: center;" data-class="">
                            <?php if ($status == 1) { ?>
                                <div style="color: #00a651">P</div>
                            <?php } else if ($status == 2) { ?>
                                <div style="color: #ff3030">A</div>
                            <?php }
                            if ($status == 0) { ?>
                                <div style="color: #D9D919;">J</div>
                            <?php }
                            $status = 4; ?>
                        </td>

                    <?php } ?>
                <?php endforeach; ?>

                </tr>

                <?php ?>

        </tbody>
    </table>
</div>



<script type="text/javascript">
    jQuery(document).ready(function($) {
        var elem = $('#print');
        PrintElem(elem);
        Popup(data);

    });

    function PrintElem(elem) {
        Popup($(elem).html());
    }

    function Popup(data) {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>