<button onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_study_material_add');?>');" 
    class="btn btn-primary pull-right">
       Adicionar material
</button>
<div style="clear:both;"></div>
<br>
<table class="table table-bordered" id="table_export">
    <thead>
        <tr>
            <th>#</th>
            <th>Data</th>
            <th>Título</th>
            <th>Descrição</th>
            <th>Turma</th>
            <th>Assunto</th>
            <th>Download</th>
            <th>Opções</th>
        </tr>
    </thead>

    <tbody>
        <?php
        $count = 1;
        foreach ($study_material_info as $row) { ?>   
            <tr>
                <td><?php echo $count++; ?></td>
                <td><?php echo date("d M, Y", $row['timestamp']); ?></td>
                <td><?php echo $row['title']?></td>
                <td><?php echo $row['description']?></td>
                <td>
                    <?php $name = $this->db->get_where('class' , array('class_id' => $row['class_id'] ))->row()->name;
                        echo $name;?>
                </td>
                <td>
                    <?php $name = $this->db->get_where('subject' , array('subject_id' => $row['subject_id'] ))->row()->name;
                        echo $name;?>
                </td>
                <td>
                    <a href="<?php echo 'http://localhost/sistema-ajudou/6.2/Ekattor/uploads/document/'.$row['file_name']; ?>" class="btn btn-blue btn-icon icon-left">
                        <i class="entypo-download"></i>
                        <?php echo get_phrase('download');?>
                    </a>
                </td>
                <td>
                    <a  onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_study_material_edit/'.$row['document_id']);?>');" 
                        class="btn btn-default btn-sm btn-icon icon-left">
                            <i class="entypo-pencil"></i>
                            Editar
                    </a>
                    <a href="<?php echo site_url('teacher/study_material/delete/'.$row['document_id']);?>" 
                        class="btn btn-danger btn-sm btn-icon icon-left" onclick="return confirm('Tem certeza de que quer deletar?');">
                            <i class="entypo-cancel"></i>
                            Deletar
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>

<script type="text/javascript">
    
 jQuery(document).ready(function($) {
       
        $('#table_export').DataTable({

            "oLanguage": {
    "sProcessing": "Aguarde enquanto os dados são carregados ...",
    "sLengthMenu": "Mostrar _MENU_ registros por pagina",
    "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
    "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
    "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
    "sInfoFiltered": "",
    "sSearch": "Procurar",
    "oPaginate": {
       "sFirst":    "Primeiro",
       "sPrevious": "Anterior",
       "sNext":     "Próximo",
       "sLast":     "Último"
    }
 }  

        });

    });

</script>
 