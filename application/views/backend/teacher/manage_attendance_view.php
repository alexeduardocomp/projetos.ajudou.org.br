<?php
$query = $this->db->get('projeto');
$projetos = $query->result();

/* $verifica = $this->db->get_where('class_routine', array(
	'class_id' => $class_id
))->result_array();


if (($verifica == null) || ($verifica == '')) {
	echo "<script>alert('Nenhum Plano de Aula cadastrado para essa turma. Por favor cadastre um plano antes de seguir.');</script>";

	echo "<script>window.location.href = 'http://projetos.ajudou.org.br/index.php/teacher/manage_attendance';</script>";
} else {
} */
?>

<?php echo form_open(site_url('teacher/attendance_selector/')); ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Frequência da</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3 col-md-offset-2">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Data</label>
					<input type="text" id="data" class="form-control datepicker" name="timestamp" data-format="dd-mm-yyyy" value="Selecione uma data" />
				</div>
			</div>

			<?php
			/*$query = $this->db->get_where('section' , array('class_id' => $class_id,'teacher_id'=>$this->session->userdata('teacher_id')));
				if($query->num_rows() > 0):
					$sections = $query->result_array();*/

			$query = $this->db->get_where('section', array(
				'class_id' => $class_id
			));

			if ($query->num_rows() > 0) :
				$sections = $query->result_array();
			?>
				<div class="col-md-3" style="display: none;">
					<div class="form-group">
						<label class="control-label" style="margin-bottom: 5px;">Sessão</label>
						<select class="form-control selectboxit" name="section_id">
							<?php foreach ($sections as $row) : ?>
								<option value="<?php echo $row['section_id']; ?>" <?php if ($section_id == $row['section_id']) echo 'selected'; ?>><?php echo $row['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			<?php endif; ?>
			<input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
			<input type="hidden" name="year" value="<?php echo $running_year; ?>">

			<div class="col-md-3" style="margin-top: 20px;">
				<button type="submit" class="btn btn-info" id="submit_sy">Gerenciar frequência</button>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>
<hr />
<div class="row" style="text-align: center;">
	<div class="col-sm-4"></div>
	<div class="col-sm-4">
		<div class="tile-stats tile-white">
			<div class="icon"><i class="entypo-chart-area"></i></div>

			<h3 style="color: #696969;"><?php echo 'Frequência da turma'; ?> <?php echo $this->db->get_where('class', array('class_id' => $class_id))->row()->name; ?></h3>
			<!--<h4 style="color: #696969;">
				<?php echo 'Sessão'; ?> <?php echo $this->db->get_where('section', array('section_id' => $section_id))->row()->name; ?>
			</h4>-->
			<h4 style="color: #696969;">
				<?php echo date("d M Y", $timestamp); ?>
			</h4>
		</div>
	</div>
	<div class="col-sm-4"></div>
</div>

<center>
	<a class="btn btn-default" onclick="mark_all_justify()">
		<i class="entypo-check"></i> Marcar todos como justificados
	</a>
	<a class="btn btn-default" onclick="mark_all_present()">
		<i class="entypo-check"></i> Marcar todos como presentes
	</a>
	<a class="btn btn-default" onclick="mark_all_absent()">
		<i class="entypo-cancel"></i> Marcar todos como ausentes
	</a>
</center>
<br>

<div class="row">

	<div class="col-md-2"></div>

	<div class="col-md-8">

		<?php echo form_open(site_url('teacher/attendance_update/' . $class_id . '/' . $section_id . '/' . $timestamp)); ?>
		<div id="attendance_update">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Folha de frequência</h3>
				</div>
				<div class="panel-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th>#</th>
								<!--<th>Id</th>-->
								<th>Nome</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$data2  = date("d/m/Y", $timestamp);
							$data  = date("Y-m-d", $timestamp);

							//	echo $data;
								echo $timestamp.'<br>';
								echo $class_id.'<br>';
							$count = 1;
							$select_id = 0;

							$sql = "SELECT attendance.* FROM attendance INNER JOIN student ON student.student_id = attendance.student_id WHERE class_id=" . $class_id . " AND `timestamp` = '" . $timestamp . "' ORDER BY student.name";

					//		echo $sql;

							$query = $this->db->query($sql);

							$attendance_of_students = $query->result_array();

							/*$attendance_of_students = $this->db->get_where('attendance', array(
								'class_id' => $class_id, 'timestamp' => $timestamp
							))->result_array();*/



							$ordenado = array();
							//$alunos_turma_ordenado = $this->db->order_by('name', 'ASC')->get_where('student', array('id_turma' => $class_id))->result_array();

							$this->db->order_by('name', 'ASC')->get_where('student', array('id_turma' => $class_id))->result_array();

							$sql = "SELECT student.*,enroll.class_id,enroll.date_added FROM `enroll` INNER JOIN student ON enroll.student_id = student.student_id WHERE enroll.class_id=" . $class_id . " AND DATE_FORMAT(FROM_UNIXTIME(`date_added`), '%Y-%m-%d') <= '" . $data . "' order by student.name";
						//	echo "<br>";
							//echo $sql;

							$query = $this->db->query($sql);

							$alunos_turma_ordenado = $query->result_array();
							//	echo "<br>";

							for ($i = 0; $i < count($alunos_turma_ordenado); $i++) {
								foreach ($attendance_of_students as $stu) {

									//	echo "<br>" . $alunos_turma_ordenado[$i]['name']."<br>";
									$sql = "SELECT enroll.* FROM `enroll` WHERE enroll.class_id<>" . $class_id . " AND DATE_FORMAT(from_unixtime(`date_added`), '%Y-%m-%d') >= DATE_FORMAT(from_unixtime('" . $alunos_turma_ordenado[$i]['date_added'] . "'), '%Y-%m-%d') AND enroll.student_id=" . $alunos_turma_ordenado[$i]['student_id'] . " AND  `date_added` <= '" . $timestamp . "'";

									//	echo $sql;
									///	echo "<br>";

									//	echo $query->num_rows();
									//	echo "<br>";
									$query = $this->db->query($sql);

									if ($alunos_turma_ordenado[$i]['student_id'] == $stu['student_id'] && $query->num_rows() < 1) {
										//array ordenado recebe meu $stu;
										//	echo "entrou: " . $alunos_turma_ordenado[$i]['name'];
										//	echo "<br>";
										array_push($ordenado, $stu);
									}
								}
							}

							//	var_dump($ordenado);

							foreach ($ordenado as $row) :
								$situacao = $this->db->get_where('student', array('student_id' => $row['student_id'], 'situacao' => 'ATIVO'))->row()->student_code;
								if ($situacao != null) {
									$data_entrou = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->data_aluno_projeto;
									$data_atual = date('Y-m-d');
									$data_entrou_modificada = strtotime($data_entrou);
									$data_atual_modificada = strtotime($data_atual);

									if ($data_atual_modificada >= $data_entrou_modificada) {

							?>
										<tr>
											<td><?php echo $count++; ?></td>
											<!--<td>
		                            <?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->student_code; ?>
								</td>-->
											<td>
												<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name; ?>
											</td>
											<td>
												<input type="radio" name="status_<?php echo $row['attendance_id']; ?>" value="0" <?php if ($row['status'] == 0) echo 'checked'; ?>>&nbsp;<?php echo 'Justificado'; ?> &nbsp;
												<input type="radio" name="status_<?php echo $row['attendance_id']; ?>" value="1" <?php if ($row['status'] == 1) echo 'checked'; ?>>&nbsp;<?php echo 'Presente'; ?> &nbsp;
												<input type="radio" name="status_<?php echo $row['attendance_id']; ?>" value="2" <?php if ($row['status'] == 2) echo 'checked'; ?>>&nbsp;<?php echo 'Ausente'; ?> &nbsp;
											</td>
										</tr>
							<?php
										$select_id++;
									}
								} else {
								}
							endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<center>
			<button type="submit" class="btn btn-success" id="submit_button">
				<i class="entypo-thumbs-up"></i> <?php echo 'Salvar mudanças'; ?>
			</button>
		</center>
		<?php echo form_close(); ?>
	</div>
</div>

<script type="text/javascript">
	$('#data').change(function() {

		var data = $('#data').val();
		//data2 = String(data);
		//var date = new Date(data2);
		//alert(data2);

		split = data.split('-');
		novadata = split[1] + "/" + split[0] + "/" + split[2];
		data_input = new Date(novadata);

		const atual = new Date();
		atual.setHours(0);
		atual.setMinutes(0);
		atual.setMilliseconds(0);
		atual.setSeconds(0);


		var ano = data_input.getFullYear();
		//alert(ano);

		if (ano < 1970) {

			alert('Valor de data não é permitido');
			$('#submit_sy').attr('disabled', 'disabled');
		} else {

			if (data_input.getTime() === atual.getTime()) {
				// alert('As datas são iguais');
				$('#submit_sy').removeAttr('disabled');
			} else if (data_input.getTime() > atual.getTime()) {

				// Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
				//alert(data_input.toString() + ' maior que ' + atual.toString());
				$('#submit_sy').attr('disabled', 'disabled');

			} else {

				$('#submit_sy').removeAttr('disabled');
				//alert(data_input.toString() + ' menor que ' + atual.toString());
			}
		}
	});


	function mark_all_present() {
		var count = <?php echo count($attendance_of_students); ?>;
		for (var i = 0; i < count; i++) {
			$(":radio[value=1]").prop('checked', true);
		}
	}

	function mark_all_justify() {
		var count = <?php echo count($attendance_of_students); ?>;
		for (var i = 0; i < count; i++) {
			$(":radio[value=0]").prop('checked', true);
		}
	}

	function mark_all_absent() {
		var count = <?php echo count($attendance_of_students); ?>;
		for (var i = 0; i < count; i++)
			$(":radio[value=2]").prop('checked', true);
	}
</script>