<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
 *  @author     : Creativeitem
 *  date        : 14 september, 2017
 *  Ekattor School Management System Pro
 *  http://codecanyon.net/user/Creativeitem
 *  http://support.creativeitem.com
 */
class Admin extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
        $this->load->model('Barcode_model');
        $this->load->model(array('Ajaxdataload_model' => 'ajaxload'));

        /*cache control*/
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
    }

    /***default functin, redirects to login page if no admin logged in yet***/
    public function index()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($this->session->userdata('admin_login') == 1)
            redirect(site_url('admin/dashboard'), 'refresh');
    }

    /***ADMIN DASHBOARD***/
    function dashboard()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  = 'dashboard';
        $page_data['page_title'] = 'Painel de Controle';
        $this->load->view('backend/index', $page_data);
    }

    /***MANAGE ADMIN***/
    function admin($param1 = "", $param2 = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {

            $data['name']       = html_escape($this->input->post('name'));
            $data['email']      = html_escape($this->input->post('email'));
            $data['password']   = sha1($this->input->post('password'));
            $data['phone']      = html_escape($this->input->post('phone'));
            $data['address']    = html_escape($this->input->post('address'));

            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('admin', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                $this->email_model->account_opening_email('admin', $data['email'], $this->input->post('password')); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/admin'), 'refresh');
        }

        if ($param1 == 'edit') {
            $data['name']   = html_escape($this->input->post('name'));
            $data['email']  = html_escape($this->input->post('email'));
            $data['phone']  = html_escape($this->input->post('phone'));
            $data['address']  = html_escape($this->input->post('address'));

            $validation = email_validation_for_edit($data['email'], $param2, 'admin');
            if ($validation == 1) {
                $this->db->where('admin_id', $param2);
                $this->db->update('admin', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/admin'), 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('admin_id', $param2);
            $this->db->delete('admin');

            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/admin'), 'refresh');
        }

        $page_data['page_name']  = 'admin';
        $page_data['page_title'] = 'Gerenciar Administradores';
        $this->load->view('backend/index', $page_data);
    }

    /****MANAGE STUDENTS CLASSWISE*****/
    function student_add()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  = 'student_add';
        $page_data['page_title'] = 'Cadastrar Alunos';
        $this->load->view('backend/index', $page_data);
    }

    function student_bulk_add()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  = 'student_bulk_add';
        $page_data['page_title'] = "Adicionar vários alunos";
        $this->load->view('backend/index', $page_data);
    }

    function student_profile($student_id)
    {
        if ($this->session->userdata('admin_login') != 1) {
            redirect(site_url('login'), 'refresh');
        }
        $page_data['page_name']  = 'student_profile';
        $page_data['page_title'] = "Perfil do Aluno";
        $page_data['student_id']  = $student_id;
        $this->load->view('backend/index', $page_data);
    }

    function get_evento_nucleo()
    {
        $id = $this->input->post("dados");
        $retorno = $this->ajaxload->get_eventos_nucleo($id);
        $cont = 0;
        $eventos = array();

        foreach ($retorno as $ret) {
            $eventos[$cont]["id_evento"] = $ret->notice_id;
            $eventos[$cont]["nome_evento"] = $ret->notice_title;
            $cont++;
        }

        echo json_encode($eventos);
    }

    function get_sections($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/admin/student_bulk_add_sections', $page_data);
    }

    function student_information($class_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']      = 'student_information';
        $page_data['page_title']     = "Informações dos Alunos - Turma : " .
            $this->crud_model->get_class_name($class_id);
        $page_data['class_id']     = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function get_students($class_id, $running_year)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'photo',
            2 => 'name',
            3 => 'phone',
            4 => 'email',
            5 => 'options',
            6 => 'nome_projeto',
            7 => 'nome_nucleo',
            8 => 'nome_turma',
            9 => 'nome_escola',
            10 => 'situacao',
            11 => 'id'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_students_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $students = $this->ajaxload->all_students($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $students =  $this->ajaxload->student_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->student_search_count($search);
        }

        $data = array();
        if (!empty($students)) {
            foreach ($students as $row) {
                $nestedData['id'] = $row->enroll_code;
                $nestedData['photo'] = '1';
                $nestedData['name'] = '2';
                $nestedData['address'] = '3';
                $nestedData['email'] = '4';
                $nestedData['nome_projeto'] = $row->nome_projeto;
                $nestedData['nome_nucleo'] = $row->nome_nucleo;
                $nestedData['nome_turma'] = $row->nome_turma;
                $nestedData['nome_escola'] = $row->nome_escola;
                $nestedData['situacao'] = $row->situacao;
                $nestedData['options'] = '5';

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    function student_marksheet($student_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $class_id     = $this->db->get_where('enroll', array(
            'student_id' => $student_id, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->row()->class_id;
        $student_name = $this->db->get_where('student', array('student_id' => $student_id))->row()->name;
        $class_name   = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
        $page_data['page_name']  =   'student_marksheet';
        $page_data['page_title'] =   get_phrase('marksheet_for') . ' ' . $student_name . ' (' . get_phrase('class') . ' ' . $class_name . ')';
        $page_data['student_id'] =   $student_id;
        $page_data['class_id']   =   $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function student_marksheet_print_view($student_id, $exam_id)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $class_id     = $this->db->get_where('enroll', array(
            'student_id' => $student_id, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->row()->class_id;
        $class_name   = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;

        $page_data['student_id'] =   $student_id;
        $page_data['class_id']   =   $class_id;
        $page_data['exam_id']    =   $exam_id;
        $this->load->view('backend/admin/student_marksheet_print_view', $page_data);
    }

    function student($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $running_year = $this->db->get_where('settings', array(
            'type' => 'running_year'
        ))->row()->description;

        if ($param1 == 'create') {
            $data['name']         = strtoupper(html_escape($this->input->post('name')));
            if (html_escape($this->input->post('birthday')) != null) {
                $data['birthday']     = html_escape($this->input->post('birthday'));
            }
            if ($this->input->post('sex') != null) {
                $data['sex']          = $this->input->post('sex');
            }
            if (html_escape($this->input->post('address')) != null) {
                $data['address']      = html_escape($this->input->post('address'));
            }
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone']        = html_escape($this->input->post('phone'));
            }
            if (html_escape($this->input->post('student_code')) != null) {
                $data['student_code'] = html_escape($this->input->post('student_code'));
                $code_validation = code_validation_insert(html_escape($data['student_code']));
                if (!$code_validation) {
                    $this->session->set_flashdata('error_message', get_phrase('this_id_no_is_not_available'));
                    redirect(site_url('admin/student_add'), 'refresh');
                }
            }

            $data['email']  =  $data['student_code'] . '@ajudou.com';

            $data['password'] = "123456";

            $data['id_projeto']  = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']  = html_escape($this->input->post('id_nucleo'));
            $data['id_turma']  = html_escape($this->input->post('class_id'));
            $data['id_escola']  = html_escape($this->input->post('id_escola'));
            $data['situacao']  = html_escape($this->input->post('situacao'));
            $data['data_nascimento']  = html_escape($this->input->post('data_nascimento'));
            $data['nome_pai']  = html_escape($this->input->post('nome_pai'));
            $data['nome_mae']  = html_escape($this->input->post('nome_mae'));
            $data['cpf_aluno']  = html_escape($this->input->post('cpf_aluno'));
            $data['cpf_responsavel']  = html_escape($this->input->post('cpf_responsavel'));
            $data['rg_aluno']  = html_escape($this->input->post('rg_aluno'));
            $data['rg_responsavel']  = html_escape($this->input->post('rg_responsavel'));
            $data['telefone_2']  = html_escape($this->input->post('telefone_2'));
            $data['cep']  = html_escape($this->input->post('cep'));
            $data['cidade']  = html_escape($this->input->post('cidade'));
            $data['estado']  = html_escape($this->input->post('estado'));
            $data['bairro']  = html_escape($this->input->post('bairro'));
            $data['rua']  = html_escape($this->input->post('rua'));
            $data['numero']  = html_escape($this->input->post('numero'));
            $data['complemento']  = html_escape($this->input->post('complemento'));
            $data['problema_saude']  = html_escape($this->input->post('problema_saude'));
            $data['descricao_problema']  = html_escape($this->input->post('descricao_problema'));
            $data['uniforme']  = html_escape($this->input->post('uniforme'));
            $data['numero_tenis']  = html_escape($this->input->post('numero_tenis'));
            $data['numero_pessoas_domicilio']  = html_escape($this->input->post('numero_pessoas_domicilio'));
            $data['responsavel_sustento']  = html_escape($this->input->post('responsavel_sustento'));
            $data['renda_familiar']  = html_escape($this->input->post('renda_familiar'));
            $data['parente_trabalha_patrocinadora']  = html_escape($this->input->post('parente_trabalha_patrocinadora'));
            $data['data_aluno_projeto']  = html_escape($this->input->post('data_aluno_projeto'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;

            $turma_selecionada = $this->ajaxload->get_turma_by_id($data['id_turma']);

            foreach ($turma_selecionada as $turmax) {
                $nome_turma = $turmax->name;
            }

            $data['nome_turma'] = $nome_turma;

            $escola_selecionada = $this->ajaxload->get_escola_by_id($data['id_escola']);

            foreach ($escola_selecionada as $escolax) {
                $nome_escola = $escolax->nome;
            }

            $data['nome_escola'] = $nome_escola;

            if ($this->input->post('parent_id') != null) {
                $data['parent_id']    = $this->input->post('parent_id');
            }
            if ($this->input->post('dormitory_id') != null) {
                $data['dormitory_id'] = $this->input->post('dormitory_id');
            }
            if ($this->input->post('transport_id') != null) {
                $data['transport_id'] = $this->input->post('transport_id');
            }
            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('student', $data);
                $student_id = $this->db->insert_id();

                $data2['student_id']     = $student_id;
                $data2['enroll_code']    = substr(md5(rand(0, 1000000)), 0, 7);

                if ($this->input->post('class_id') != null) {
                    $data2['class_id']       = $this->input->post('class_id');
                }
                if ($this->input->post('section_id') != '') {
                    $data2['section_id'] = $this->input->post('section_id');
                }
                if (html_escape($this->input->post('roll')) != '') {
                    $data2['roll']           = html_escape($this->input->post('roll'));
                }
                $data2['date_added']     = strtotime(date("Y-m-d H:i:s"));
                $data2['year']           = $running_year;
                $this->db->insert('enroll', $data2);
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $student_id . '.jpg');

                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso');
                $this->email_model->account_opening_email('student', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', 'Este email não é válido');
            }
            redirect(site_url('admin/student_add'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']           = strtoupper(html_escape($this->input->post('name')));
            $data['email']          = html_escape($this->input->post('email'));
            $data['parent_id']      = $this->input->post('parent_id');

            // $data['id_projeto']  = html_escape($this->input->post('id_projeto'));
            //  $data['id_nucleo']  = html_escape($this->input->post('id_nucleo'));
            //  $data['id_turma']  = html_escape($this->input->post('class_id'));
            //  $data2['class_id']  = html_escape($this->input->post('class_id'));
            $data['id_escola']  = html_escape($this->input->post('id_escola'));
            $data['situacao']  = html_escape($this->input->post('situacao'));
            $data['data_nascimento']  = html_escape($this->input->post('data_nascimento'));
            $data['nome_pai']  = html_escape($this->input->post('nome_pai'));
            $data['nome_mae']  = html_escape($this->input->post('nome_mae'));
            $data['cpf_aluno']  = html_escape($this->input->post('cpf_aluno'));
            $data['cpf_responsavel']  = html_escape($this->input->post('cpf_responsavel'));
            $data['rg_aluno']  = html_escape($this->input->post('rg_aluno'));
            $data['rg_responsavel']  = html_escape($this->input->post('rg_responsavel'));
            $data['telefone_2']  = html_escape($this->input->post('telefone_2'));
            $data['cep']  = html_escape($this->input->post('cep'));
            $data['cidade']  = html_escape($this->input->post('cidade'));
            $data['estado']  = html_escape($this->input->post('estado'));
            $data['bairro']  = html_escape($this->input->post('bairro'));
            $data['rua']  = html_escape($this->input->post('rua'));
            $data['numero']  = html_escape($this->input->post('numero'));
            $data['complemento']  = html_escape($this->input->post('complemento'));
            $data['problema_saude']  = html_escape($this->input->post('problema_saude'));
            $data['descricao_problema']  = html_escape($this->input->post('descricao_problema'));
            $data['uniforme']  = html_escape($this->input->post('uniforme'));
            $data['numero_tenis']  = html_escape($this->input->post('numero_tenis'));
            $data['numero_pessoas_domicilio']  = html_escape($this->input->post('numero_pessoas_domicilio'));
            $data['responsavel_sustento']  = html_escape($this->input->post('responsavel_sustento'));
            $data['renda_familiar']  = html_escape($this->input->post('renda_familiar'));
            $data['parente_trabalha_patrocinadora']  = html_escape($this->input->post('parente_trabalha_patrocinadora'));
            $data['data_aluno_projeto']  = html_escape($this->input->post('data_aluno_projeto'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;

            $turma_selecionada = $this->ajaxload->get_turma_by_id($data['id_turma']);

            foreach ($turma_selecionada as $turmax) {
                $nome_turma = $turmax->name;
            }

            $data['nome_turma'] = $nome_turma;

            $escola_selecionada = $this->ajaxload->get_escola_by_id($data['id_escola']);

            foreach ($escola_selecionada as $escolax) {
                $nome_escola = $escolax->nome;
            }

            $data['nome_escola'] = $nome_escola;

            if (html_escape($this->input->post('birthday')) != null) {
                $data['birthday']   = html_escape($this->input->post('birthday'));
            }
            if ($this->input->post('sex') != null) {
                $data['sex']            = $this->input->post('sex');
            }
            if (html_escape($this->input->post('address')) != null) {
                $data['address']        = html_escape($this->input->post('address'));
            }
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone']          = html_escape($this->input->post('phone'));
            }
            if ($this->input->post('dormitory_id') != null) {
                $data['dormitory_id']   = $this->input->post('dormitory_id');
            }
            if ($this->input->post('transport_id') != null) {
                $data['transport_id']   = $this->input->post('transport_id');
            }

            //student id
            if (html_escape($this->input->post('student_code')) != null) {
                $data['student_code'] = html_escape($this->input->post('student_code'));
                $code_validation = code_validation_update($data['student_code'], $param2);
                if (!$code_validation) {
                    $this->session->set_flashdata('error_message', get_phrase('this_id_no_is_not_available'));
                    redirect(site_url('admin/student_information/' . $param3), 'refresh');
                }
            }

            $validation = email_validation_for_edit($data['email'], $param2, 'student');
            if ($validation == 1) {
                $this->db->where('student_id', $param2);
                $this->db->update('student', $data);

                $data2['section_id'] = $this->input->post('section_id');

                if (html_escape($this->input->post('roll')) != null) {
                    $data2['roll'] = html_escape($this->input->post('roll'));
                } else {
                    $data2['roll'] = null;
                }
                $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
                $this->db->where('student_id', $param2);
                $this->db->where('year', $running_year);
                $this->db->update('enroll', array(
                    'section_id' => $data2['section_id'], 'roll' => $data2['roll'], 'class_id' => $data2['class_id']
                ));

                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/student_image/' . $param2 . '.jpg');
                $this->crud_model->clear_cache();
                $this->session->set_flashdata('flash_message', 'Dado atualizado');
            } else {
                $this->session->set_flashdata('error_message', 'Este email não é válido');
            }
            redirect(site_url('admin/student_information/' . $param3), 'refresh');
        }
    }

    function delete_student($student_id = '', $class_id = '')
    {
        $this->crud_model->delete_student($student_id);
        $this->db->where('student_id', $student_id);
        $this->db->delete('enroll');
        $this->session->set_flashdata('flash_message', 'Aluno deletado');
        redirect(site_url('admin/student_information/' . $class_id), 'refresh');
    }

    // STUDENT PROMOTION
    function student_promotion($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'promote') {
            $running_year  =   $this->input->post('running_year');
            $from_class_id =   $this->input->post('promotion_from_class_id');
            $students_of_promotion_class =   $this->db->get_where('enroll', array(
                'class_id' => $from_class_id, 'year' => $running_year
            ))->result_array();
            foreach ($students_of_promotion_class as $row) {
                $sections = $this->db->get_where('section', array('class_id' => $this->input->post('promotion_status_' . $row['student_id'])))->row_array();
                $enroll_data['enroll_code']     =   substr(md5(rand(0, 1000000)), 0, 7);
                $enroll_data['student_id']      =   $row['student_id'];
                $enroll_data['class_id']        =   $this->input->post('promotion_status_' . $row['student_id']);
                $enroll_data['section_id']      =   $sections['section_id'];
                $enroll_data['year']            =   $this->input->post('promotion_year');
                $enroll_data['date_added']      =   strtotime(date("Y-m-d H:i:s"));
                $this->db->insert('enroll', $enroll_data);
            }
            $this->session->set_flashdata('flash_message', "Recadastro de Turmas realizada com sucesso!");
            redirect(site_url('admin/student_promotion'), 'refresh');
        }

        $page_data['page_title']    = 'Recadastro de Turma';
        $page_data['page_name']  = 'student_promotion';
        $this->load->view('backend/index', $page_data);
    }


    // STUDENT PROMOTION
    function trocar_aluno_turma($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'promote') {
            $student_id = $this->input->post('student');
            $sections = $this->db->get_where('section', array('class_id' => $this->input->post('promotion_to_class_id')))->row_array();
            $enroll_data['enroll_code']     =   substr(md5(rand(0, 1000000)), 0, 7);
            $enroll_data['student_id']      =   $student_id;
            $enroll_data['class_id']        =   $this->input->post('promotion_to_class_id');
            $enroll_data['section_id']      =   $sections['section_id'];
            $enroll_data['year']            =   $this->input->post('promotion_year');
            $enroll_data['date_added']      =   strtotime(date("Y-m-d", strtotime($this->input->post('data_matricula'))));
            $this->db->insert('enroll', $enroll_data);
            // var_dump($enroll_data);

            $this->session->set_flashdata('flash_message', "Troca de turma realizada com sucesso!");
            redirect(site_url('admin/trocar_aluno_turma'), 'refresh');
        }

        $page_data['page_title']    = 'Trocar Aluno de Turma';
        $page_data['page_name']  = 'trocar_aluno_turma';
        $this->load->view('backend/index', $page_data);
    }

    function get_students_to_promote($class_id_from, $class_id_to, $running_year, $promotion_year)
    {
        $page_data['class_id_from']     =   $class_id_from;
        $page_data['class_id_to']       =   $class_id_to;
        $page_data['running_year']      =   $running_year;
        $page_data['promotion_year']    =   $promotion_year;
        $this->load->view('backend/admin/student_promotion_selector', $page_data);
    }

    /****MANAGE PARENTS CLASSWISE*****/
    function parent($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']                    = html_escape($this->input->post('name'));
            $data['email']                   = html_escape($this->input->post('email'));
            $data['password']                = sha1($this->input->post('password'));
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone'] = html_escape($this->input->post('phone'));
            }
            if (html_escape($this->input->post('address')) != null) {
                $data['address'] = html_escape($this->input->post('address'));
            }
            if (html_escape($this->input->post('profession')) != null) {
                $data['profession'] = html_escape($this->input->post('profession'));
            }
            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('parent', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                $this->email_model->account_opening_email('parent', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/parent'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['name']                   = html_escape($this->input->post('name'));
            $data['email']                  = html_escape($this->input->post('email'));
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone'] = html_escape($this->input->post('phone'));
            } else {
                $data['phone'] = null;
            }
            if (html_escape($this->input->post('address')) != null) {
                $data['address'] = html_escape($this->input->post('address'));
            } else {
                $data['address'] = null;
            }
            if (html_escape($this->input->post('profession')) != null) {
                $data['profession'] = html_escape($this->input->post('profession'));
            } else {
                $data['profession'] = null;
            }
            $validation = email_validation_for_edit($data['email'], $param2, 'parent');
            if ($validation == 1) {
                $this->db->where('parent_id', $param2);
                $this->db->update('parent', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/parent'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('parent_id', $param2);
            $this->db->delete('parent');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/parent'), 'refresh');
        }
        $page_data['page_title']     = "Todos os Responsáveis";
        $page_data['page_name']  = 'parent';
        $this->load->view('backend/index', $page_data);
    }

    function get_parents()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'parent_id',
            1 => 'name',
            2 => 'email',
            3 => 'phone',
            4 => 'profession',
            5 => 'options',
            6 => 'parent_id'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_parents_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $parents = $this->ajaxload->all_parents($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $parents =  $this->ajaxload->parent_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->parent_search_count($search);
        }

        $data = array();
        if (!empty($parents)) {
            foreach ($parents as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="parent_edit_modal(' . $row->parent_id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="parent_delete_confirm(' . $row->parent_id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';
                $nestedData['parent_id'] = $row->parent_id;
                $nestedData['name'] = $row->name;
                $nestedData['email'] = $row->email;
                $nestedData['phone'] = $row->phone;
                $nestedData['profession'] = $row->profession;
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR CIDADES ******/
    function cidade($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['nome']                   = strtoupper(html_escape($this->input->post('nome')));
            $data['estado']                 = html_escape($this->input->post('estado'));

            if (($data['nome'] != null) && ($data['estado'] != null)) {
                $this->db->insert('cidade', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('Todos os campos devem ser preenchidos'));
            }

            redirect(site_url('admin/cidade'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['nome']                   = strtoupper(html_escape($this->input->post('nome')));
            $data['estado']                 = html_escape($this->input->post('estado'));

            if (($data['nome'] != null) && ($data['estado'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('cidade', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_update_successfully'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('Todos os campos devem ser preenchidos'));
            }

            redirect(site_url('admin/cidade'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('cidade');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/cidade'), 'refresh');
        }
        $page_data['page_title']    = "Todas as Cidades";
        $page_data['page_name']  = 'cidade';
        $this->load->view('backend/index', $page_data);
    }

    function get_cidades()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'nome',
            2 => 'estado',
            3 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_cidades_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $cidades = $this->ajaxload->all_cidades($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $cidades =  $this->ajaxload->cidades_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->cidade_search_count($search);
        }

        $data = array();
        if (!empty($cidades)) {
            foreach ($cidades as $row) {
                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="cidade_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="cidade_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';
                $nestedData['id'] = $row->id;
                $nestedData['nome'] = $row->nome;
                $nestedData['estado'] = $row->estado;
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR ESCOLAS ******/
    function escola($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['nome']                   = html_escape($this->input->post('nome'));
            $data['tipo_escola']                 = html_escape($this->input->post('tipo_escola'));

            if (($data['nome'] != null) && ($data['tipo_escola'] != null)) {

                $this->db->insert('escola', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/escola'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['nome']                   = html_escape($this->input->post('nome'));
            $data['tipo_escola']                 = html_escape($this->input->post('tipo_escola'));


            if (($data['nome'] != null) && ($data['tipo_escola'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('escola', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/escola'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('escola');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/escola'), 'refresh');
        }

        $page_data['page_title']    = "Todas as Escolas";
        $page_data['page_name']  = 'escola';
        $this->load->view('backend/index', $page_data);
    }

    function get_escolas()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'nome',
            2 => 'tipo_escola',
            3 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_escolas_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $escolas = $this->ajaxload->all_escolas($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $escolas =  $this->ajaxload->escolas_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->escola_search_count($search);
        }

        $data = array();
        if (!empty($escolas)) {
            foreach ($escolas as $row) {
                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="escola_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbsp;' . get_phrase('edit') . '</a></li><li class="divider"></li><li><a href="#" onclick="escola_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp;' . get_phrase('delete') . '</a></li></ul></div>';

                $nestedData['id'] = $row->id;
                $nestedData['nome'] = $row->nome;
                $nestedData['tipo_escola'] = $row->tipo_escola;
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR AÇÕES ******/
    function acao($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {

            $data['acao'] = html_escape($this->input->post('acao'));

            if (($data['acao'] != null)) {
                $this->db->insert('acao', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/acao'), 'refresh');
        }

        if ($param1 == 'edit') {
            $data['acao'] = html_escape($this->input->post('acao'));

            if (($data['acao'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('acao', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/acao'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('acao');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/acao'), 'refresh');
        }

        $page_data['page_title']    = "Todas as Ações";
        $page_data['page_name']  = 'acao';
        $this->load->view('backend/index', $page_data);
    }

    function get_acoes()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'acao',
            2 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_acoes_count();

        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $acoes = $this->ajaxload->all_acoes($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $acoes =  $this->ajaxload->acoes_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->acao_search_count($search);
        }

        $data = array();
        if (!empty($acoes)) {
            foreach ($acoes as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="acao_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="acao_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp; Deletar</a></li></ul></div>';

                $nestedData['id'] = $row->id;
                $nestedData['acao'] = $row->acao;
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }


    function get_projeto_dashboard()
    {
        $id = $this->input->post("dados");
        $projeto_selecionado = $this->ajaxload->get_projeto_by_id($id);

        $cont = 0;
        $pro = array();

        foreach ($projeto_selecionado as $ret) {
            $pro[$cont]["id_projeto"] = $ret->id;
            $pro[$cont]["nome_projeto"] = $ret->nome;
            $pro[$cont]["tipo_projeto"] = $ret->tipo_projeto;
            $pro[$cont]["proponente"] = $ret->proponente;
            $pro[$cont]["metas"] = $ret->metas;
            $data_in = date('d/m/Y', strtotime($ret->data_inicio));
            $data_fim = date('d/m/Y', strtotime($ret->data_final));
            $pro[$cont]["data_inicio"] = $data_in;
            $pro[$cont]["data_final"] = $data_fim;
            $cont++;
        }

        echo json_encode($pro);
    }

    /****** GERENCIAR NÚCLEOS ******/
    function nucleo($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['nome_nucleo'] = strtoupper(html_escape($this->input->post('nome_nucleo')));
            $data['id_projeto'] = html_escape($this->input->post('id_projeto'));
            $data['meta_alunos'] = html_escape($this->input->post('meta_alunos'));
            $data['patrocinador'] = html_escape($this->input->post('id_patrocinador'));
            $data['email_coordenador'] = html_escape($this->input->post('email_coordenador'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;


            if (($data['nome_nucleo'] != null) && ($data['id_projeto'] != null) && ($data['meta_alunos'] != null) && ($data['patrocinador'] != null)) {
                $this->db->insert('nucleo', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/nucleo'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['nome_nucleo'] = strtoupper(html_escape($this->input->post('nome_nucleo')));
            $data['id_projeto'] = html_escape($this->input->post('id_projeto'));
            $data['meta_alunos'] = html_escape($this->input->post('meta_alunos'));
            $data['patrocinador'] = html_escape($this->input->post('id_patrocinador'));
            $data['email_coordenador'] = html_escape($this->input->post('email_coordenador'));


            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            if (($data['nome_nucleo'] != null) && ($data['id_projeto'] != null) && ($data['meta_alunos'] != null) && ($data['patrocinador'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('nucleo', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/nucleo'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('nucleo');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/nucleo'), 'refresh');
        }

        $page_data['page_title']    = "Todos os Núcleos";
        $page_data['page_name']  = 'nucleo';
        $this->load->view('backend/index', $page_data);
    }

    function get_nucleos()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'nome_nucleo',
            2 => 'id_projeto',
            3 => 'nome_projeto',
            4 => 'meta_alunos',
            5 => 'patrocinador',
            6 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_nucleos_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $nucleos = $this->ajaxload->all_nucleos($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $nucleos =  $this->ajaxload->nucleos_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->nucleo_search_count($search);
        }

        $data = array();
        if (!empty($nucleos)) {
            foreach ($nucleos as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="nucleo_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="nucleo_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $patrocinador_selecionado = $this->ajaxload->get_patrocinador_by_id($row->patrocinador);

                foreach ($patrocinador_selecionado as $patrox) {
                    $nome_patro = $patrox->nome;
                }

                $nestedData['id'] = $row->id;
                $nestedData['nome_nucleo'] = $row->nome_nucleo;
                $nestedData['id_projeto'] = $row->id_projeto;
                $nestedData['nome_projeto'] = $row->nome_projeto;
                $nestedData['meta_alunos'] = $row->meta_alunos;
                $nestedData['patrocinador'] = $nome_patro;

                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR MODALIDADES ******/
    function modalidade($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {

            $data['modalidade'] = html_escape($this->input->post('modalidade'));

            if (($data['modalidade'] != null)) {

                $this->db->insert('modalidade', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/modalidade'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['modalidade']                   = html_escape($this->input->post('modalidade'));

            if (($data['modalidade'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('modalidade', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/modalidade'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('modalidade');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/modalidade'), 'refresh');
        }

        $page_data['page_title']    = "Todas as Modalidades";
        $page_data['page_name']  = 'modalidade';
        $this->load->view('backend/index', $page_data);
    }

    function get_modalidades()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'modalidade',
            2 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_modalidades_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $modalidades = $this->ajaxload->all_modalidades($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $modalidades =  $this->ajaxload->modalidades_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->modalidade_search_count($search);
        }

        $data = array();
        if (!empty($modalidades)) {
            foreach ($modalidades as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="modalidade_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="modalidade_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $nestedData['id'] = $row->id;
                $nestedData['modalidade'] = $row->modalidade;

                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR RELACAO ******/
    function relacao($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {

            $data['id_projeto'] = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo'] = html_escape($this->input->post('id_nucleo'));

            $array_instrutores = html_escape($this->input->post('id_instrutor'));
            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;
            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;

            $conta = 0;
            foreach ($array_instrutores as $int) {
                $instrutor_selecionado = $this->ajaxload->get_instrutor_by_id($int);

                foreach ($instrutor_selecionado as $instx) {
                    $nome_instrutor = $instx->name;
                    $id_instrutor = $instx->teacher_id;
                }

                $instrutores[$conta]['nome_instrutor'] = $nome_instrutor;
                $instrutores[$conta]['id_instrutor'] = $id_instrutor;
                $conta++;
            }

            if (($data['id_projeto'] != null) && ($data['id_nucleo'] != null) && (!empty($instrutores))) {
                foreach ($instrutores as $instru) {
                    $data['id_instrutor'] = $instru['id_instrutor'];
                    $data['nome_instrutor'] = $instru['nome_instrutor'];
                    $this->db->insert('instrutor_nucleo', $data);
                    $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }
        }
        if ($param1 == 'edit') {
            $data['id_projeto'] = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo'] = html_escape($this->input->post('id_nucleo'));
            $data['id_instrutor'] = html_escape($this->input->post('id_instrutor'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;
            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;
            $instrutor_selecionado = $this->ajaxload->get_instrutor_by_id($data['id_instrutor']);

            foreach ($instrutor_selecionado as $instx) {
                $nome_instrutor = $instx->name;
            }

            $data['nome_instrutor'] = $nome_instrutor;

            if (($data['id_projeto'] != null) && ($data['id_nucleo'] != null) && ($data['id_instrutor'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('instrutor_nucleo', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/relacao'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('instrutor_nucleo');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/relacao'), 'refresh');
        }

        $page_data['page_title']    = "Todas as Relações";
        $page_data['page_name']  = 'relacao';
        $this->load->view('backend/index', $page_data);
    }

    function get_relacao()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'id_nucleo',
            2 => 'id_instrutor',
            3 => 'id_projeto',
            4 => 'nome_projeto',
            5 => 'nome_nucleo',
            6 => 'nome_instrutor',
            7 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_relacoes_count();

        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $relacoes = $this->ajaxload->all_relacoes($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $relacoes =  $this->ajaxload->relacoes_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->relacao_search_count($search);
        }

        $data = array();
        if (!empty($relacoes)) {
            foreach ($relacoes as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li class="divider"></li><li><a href="#" onclick="relacao_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $nestedData['id'] = $row->id;
                $nestedData['projeto'] = $row->nome_projeto;
                $nestedData['nucleo'] = $row->nome_nucleo;
                $nestedData['instrutor'] = $row->nome_instrutor;


                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR RELACAO ESCOLA ******/
    function relacaoescola($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {

            $data['id_projeto']                   = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']                   = html_escape($this->input->post('id_nucleo'));
            $array_escolas                 = html_escape($this->input->post('id_escola'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;
            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;

            $conta = 0;
            foreach ($array_escolas as $esc) {
                $escola_selecionado = $this->ajaxload->get_escola_by_id($esc);

                foreach ($escola_selecionado as $escx) {
                    $nome_escola = $escx->nome;
                    $id_escola = $escx->id;
                }

                $escolas[$conta]['nome_escola'] = $nome_escola;
                $escolas[$conta]['id_escola'] = $id_escola;
                $conta++;
            }

            if (($data['id_projeto'] != null) && ($data['id_nucleo'] != null) && (!empty($escolas))) {
                foreach ($escolas as $esco) {
                    $data['id_escola'] = $esco['id_escola'];
                    $data['nome_escola'] = $esco['nome_escola'];
                    $this->db->insert('escola_nucleo', $data);
                    $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }
        }
        if ($param1 == 'edit') {
            $data['id_projeto']                   = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']                   = html_escape($this->input->post('id_nucleo'));

            $array_escolas                 = html_escape($this->input->post('id_escola'));

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;
            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;
            $instrutor_selecionado = $this->ajaxload->get_instrutor_by_id($data['id_instrutor']);

            foreach ($instrutor_selecionado as $instx) {
                $nome_instrutor = $instx->name;
            }

            $data['nome_instrutor'] = $nome_instrutor;

            if (($data['id_projeto'] != null) && ($data['id_nucleo'] != null) && ($data['id_instrutor'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('instrutor_nucleo', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/relacao'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id_relacao', $param2);
            $this->db->delete('escola_nucleo');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/relacaoescola'), 'refresh');
        }
        $page_data['page_title']    = "Todas as Relações";
        $page_data['page_name']  = 'relacaoescola';
        $this->load->view('backend/index', $page_data);
    }

    function get_relacao_escola()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');


        $columns = array(
            0 => 'id_relacao',
            1 => 'id_nucleo',
            2 => 'id_escola',
            3 => 'id_projeto',
            4 => 'nome_projeto',
            5 => 'nome_nucleo',
            6 => 'nome_escola',
            7 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_relacoes_escola_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $relacoes2 = $this->ajaxload->all_relacoes_escola($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $relacoes2 =  $this->ajaxload->relacoes_escola_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->relacao_escola_search_count($search);
        }

        $data = array();
        if (!empty($relacoes2)) {
            foreach ($relacoes2 as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li class="divider"></li><li><a href="#" onclick="relacaoescola_delete_confirm(' . $row->id_relacao . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $nestedData['id'] = $row->id_relacao;
                $nestedData['projeto'] = $row->nome_projeto;
                $nestedData['nucleo'] = $row->nome_nucleo;
                $nestedData['escola'] = $row->nome_escola;


                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR RELACAO ACAO ******/
    function relacaoacao($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {

            $data['id_projeto']                   = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']                   = html_escape($this->input->post('id_nucleo'));
            $data['quantidade']                   = html_escape($this->input->post('quantidade'));
            $data['numeracao']                   = html_escape($this->input->post('numeracao'));

            $array_acoes                 = html_escape($this->input->post('id_acao'));
            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;
            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;
            $conta = 0;
            foreach ($array_acoes as $ac) {
                $acao_selecionado = $this->ajaxload->get_acao_by_id($ac);
                foreach ($acao_selecionado as $acx) {
                    $nome_acao = $acx->acao;
                    $id_acao = $acx->id;
                }

                $acoes[$conta]['nome_acao'] = $nome_acao;
                $acoes[$conta]['id_acao'] = $id_acao;
                $conta++;
            }

            if (($data['id_projeto'] != null) && ($data['id_nucleo'] != null) && ($data['quantidade'] != null) && ($data['numeracao'] != null) && (!empty($acoes))) {
                foreach ($acoes as $aco) {

                    $data['id_acao'] = $aco['id_acao'];
                    $data['nome_acao'] = $aco['nome_acao'];
                    $this->db->insert('acao_nucleo', $data);
                    $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }
        }
        if ($param1 == 'edit') {
        }
        if ($param1 == 'delete') {
            $this->db->where('id_relacao', $param2);
            $this->db->delete('acao_nucleo');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/relacaoacao'), 'refresh');
        }

        $page_data['page_title']    = "Todas as Relações";
        $page_data['page_name']  = 'relacaoacao';
        $this->load->view('backend/index', $page_data);
    }

    function get_relacao_acao()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');


        $columns = array(
            0 => 'id_relacao',
            1 => 'id_nucleo',
            2 => 'id_acao',
            3 => 'id_projeto',
            4 => 'nome_projeto',
            5 => 'nome_acao',
            6 => 'nome_nucleo',
            7 => 'numeracao',
            8 => 'quantidade',
            9 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_relacoes_acao_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $relacoes3 = $this->ajaxload->all_relacoes_acao($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $relacoes3 =  $this->ajaxload->relacoes_acao_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->relacao_acao_search_count($search);
        }

        $data = array();
        if (!empty($relacoes3)) {
            foreach ($relacoes3 as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li class="divider"></li><li><a href="#" onclick="relacaoacao_delete_confirm(' . $row->id_relacao . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $nestedData['id'] = $row->id_relacao;
                $nestedData['projeto'] = $row->nome_projeto;
                $nestedData['nucleo'] = $row->nome_nucleo;
                $nestedData['acao'] = $row->nome_acao;
                $nestedData['quantidade'] = $row->quantidade;
                $nestedData['numeracao'] = $row->numeracao;
                $nestedData['options'] = $options;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****** GERENCIAR RELACAO PROJETO ******/
    function relacaoprojeto($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['id_projeto']                   = html_escape($this->input->post('id_projeto'));
            $patrocinadores                  = html_escape($this->input->post('id_patrocinador'));


            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            $conta = 0;
            foreach ($patrocinadores as $patro) {


                $patrocinador_selecionado = $this->ajaxload->get_patrocinador_by_id($patro);


                foreach ($patrocinador_selecionado as $pa) {
                    $nome_patrocinador = $pa->nome;
                    $id_patrocinador = $pa->id;
                }

                $acoes[$conta]['nome_patrocinador'] = $nome_patrocinador;
                $acoes[$conta]['id_patrocinador'] = $id_patrocinador;
                $conta++;
            }

            if (($data['id_projeto'] != null) && (!empty($acoes))) {


                foreach ($acoes as $aco) {

                    $data['id_patrocinador'] = $aco['id_patrocinador'];
                    $data['nome_patrocinador'] = $aco['nome_patrocinador'];
                    $this->db->insert('patrocinador_projeto', $data);
                    $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }
        }
        if ($param1 == 'edit') {
        }
        if ($param1 == 'delete') {
            $this->db->where('id_relacao', $param2);
            $this->db->delete('patrocinador_projeto');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/relacaoprojeto'), 'refresh');
        }

        $page_data['page_title']    = "Todas as Relações";
        $page_data['page_name']  = 'relacaoprojeto';
        $this->load->view('backend/index', $page_data);
    }

    function get_relacao_projeto()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');


        $columns = array(
            0 => 'id_relacao',
            1 => 'id_projeto',
            2 => 'nome_projeto',
            3 => 'id_patrocinador',
            4 => 'nome_patrocinador',
            5 => 'options'
        );





        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];




        $totalData = $this->ajaxload->all_relacoes_patrocinador_count();


        $totalFiltered = $totalData;




        if (empty($this->input->post('search')['value'])) {
            $relacoes3 = $this->ajaxload->all_relacoes_patrocinador($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $relacoes3 =  $this->ajaxload->relacoes_patrocinador_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->relacao_patrocinador_search_count($search);
        }



        $data = array();
        if (!empty($relacoes3)) {
            foreach ($relacoes3 as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li class="divider"></li><li><a href="#" onclick="relacaoprojeto_delete_confirm(' . $row->id_relacao . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $nestedData['id'] = $row->id_relacao;
                $nestedData['projeto'] = $row->nome_projeto;
                $nestedData['patrocinador'] = $row->nome_patrocinador;

                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );




        echo json_encode($json_data);
    }







    function get_instrutor_nucleo2()
    {

        $id = $this->input->post("dados");

        $retorno = $this->ajaxload->get_instrutores_nucleo2($id);


        $cont = 0;
        $instrutores = array();

        foreach ($retorno as $ret) {
            $instrutores[$cont]["id_instrutor"] = $ret->id_instrutor;
            $instrutores[$cont]["nome"] = $ret->nome_instrutor;
            $cont++;
        }

        echo json_encode($instrutores);
    }



    function get_secao_turma2()
    {

        $id = $this->input->post("dados");

        $retorno = $this->ajaxload->get_secoes_turma2($id);


        $cont = 0;
        $secoes = array();

        foreach ($retorno as $ret) {
            $secoes[$cont]["id"] = $ret->section_id;
            $secoes[$cont]["nome_secao"] = $ret->name;
            $cont++;
        }

        echo json_encode($secoes);
    }






    function get_instrutor_nucleo()
    {

        $id = $this->input->post("dados");


        $retorno = $this->ajaxload->get_instrutores_nucleo($id);








        $cont = 0;
        $instrutores = array();

        foreach ($retorno as $ret) {
            $instrutores[$cont]["id_instrutor"] = $ret->teacher_id;
            $instrutores[$cont]["nome"] = $ret->name;
            $cont++;
        }

        echo json_encode($instrutores);
    }



    function get_escola_nucleo()
    {

        $id = $this->input->post("dados");


        $retorno = $this->ajaxload->get_escolas_nucleo($id);









        $cont = 0;
        $escolas = array();

        foreach ($retorno as $ret) {
            $escolas[$cont]["id_escola"] = $ret->id;
            $escolas[$cont]["nome_escola"] = $ret->nome;
            $cont++;
        }

        echo json_encode($escolas);
    }



    function get_escola_nucleo2()
    {

        $id = $this->input->post("dados");


        $retorno = $this->ajaxload->get_escolas_nucleo2($id);









        $cont = 0;
        $escolas = array();

        foreach ($retorno as $ret) {
            $escolas[$cont]["id_escola"] = $ret->id;
            $escolas[$cont]["nome_escola"] = $ret->nome;
            $cont++;
        }

        echo json_encode($escolas);
    }

    function get_turma_nucleo_tipo()
    {
        $id = $this->input->post("dados");
        $tipo = $this->input->post("tipo");

        $retorno = $this->ajaxload->get_turmas_nucleo($id);
        $cont = 0;
        $turmas = array();

        foreach ($retorno as $ret) {
            $turmas[$cont]["id_turma"] = $ret->class_id;
            $turmas[$cont]["nome_turma"] = $ret->name;
            $cont++;
        }

        echo json_encode($turmas);
    }


    function get_escola_nucleo_tipo()
    {
        $id = $this->input->post("dados");
        $tipo = $this->input->post("tipo");

        $retorno = array();
        $contador = 0;

        if ($tipo == "AMBOS") {
            for ($i = 0; $i < count($id); $i++) {

                $sql1 = "SELECT * FROM escola
                            INNER JOIN escola_nucleo ON escola.id = escola_nucleo.id_escola AND escola_nucleo.id_nucleo = " . $id[$i] . "";

                $query1 =  $this->db->query($sql1);

                $retorno[$contador] = $query1->result();

                $contador++;
            }
        } else {


            for ($i = 0; $i < count($id); $i++) {

                $sql1 = "SELECT * FROM escola
           INNER JOIN escola_nucleo ON escola.id = escola_nucleo.id_escola AND escola.tipo_escola = '" . $tipo . "' AND escola_nucleo.id_nucleo = " . $id[$i] . "";


                $query1 =  $this->db->query($sql1);


                $retorno[$contador] = $query1->result();

                $contador++;
            }
        }


        $cont = 0;
        $escolas = array();


        for ($i = 0; $i < count($retorno); $i++) {

            foreach ($retorno[$i] as $ret) {
                $escolas[$cont]["id_escola"] = $ret->id;
                $escolas[$cont]["nome_escola"] = $ret->nome;
                $cont++;
            }
        }

        echo json_encode($escolas);
    }


    function get_turma_nucleo()
    {
        $id = $this->input->post("dados");

        $retorno = $this->ajaxload->get_turmas_nucleo($id);

        $cont = 0;
        $turmas = array();

        foreach ($retorno as $ret) {
            $turmas[$cont]["id_turma"] = $ret->class_id;
            $turmas[$cont]["nome_turma"] = $ret->name;
            $cont++;
        }

        echo json_encode($turmas);
    }


    function get_turma_nucleo_modalidade()
    {
        $id = $this->input->post("dados");
        $id_modalidade = $this->input->post("id_modalidade");

        $retorno = $this->ajaxload->get_turmas_nucleo_modalidade($id, $id_modalidade);

        $cont = 0;
        $turmas = array();

        foreach ($retorno as $ret) {
            $turmas[$cont]["id_turma"] = $ret->class_id;
            $turmas[$cont]["nome_turma"] = $ret->name;
            $cont++;
        }

        echo json_encode($turmas);
    }








    function get_patrocinador_projeto()
    {



        $id = $this->input->post("dados");

        $nucleos_selecionados = $this->ajaxload->get_p_p($id);


        $cont = 0;
        $nucleos = array();

        foreach ($nucleos_selecionados as $nuc) {
            $nucleo[$cont]["nome_patrocinador"] = $nuc->nome;
            $nucleo[$cont]["id_patrocinador"] = $nuc->id;
            $cont++;
        }

        echo json_encode($nucleo);
    }








    function get_secao_turma()
    {

        $id = $this->input->post("dados");


        $retorno = $this->ajaxload->get_secoes_turma($id);









        $cont = 0;
        $secoes = array();

        foreach ($retorno as $ret) {
            $secoes[$cont]["id_secao"] = $ret->section_id;
            $secoes[$cont]["nome_secao"] = $ret->name;
            $cont++;
        }

        echo json_encode($secoes);
    }






    function get_acao_nucleo()
    {

        $id = $this->input->post("dados");


        $retorno = $this->ajaxload->get_acoes_nucleo($id);









        $cont = 0;
        $acoes = array();

        foreach ($retorno as $ret) {
            $acoes[$cont]["id_acao"] = $ret->id;
            $acoes[$cont]["nome_acao"] = $ret->acao;
            $cont++;
        }

        echo json_encode($acoes);
    }








    /****** GERENCIAR LOCAIS ******/

    function local_execucao($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');


        // vou exibir os projetos mas no value eu passo o id, ai faço um get_projeto_by_id




        if ($param1 == 'create') {

            $data['id_projeto']                   = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']                   = html_escape($this->input->post('id_nucleo'));
            $data['local']                   = html_escape($this->input->post('local'));
            $data['telefone_1']                   = html_escape($this->input->post('telefone'));
            $data['telefone_2']                   = html_escape($this->input->post('telefone2'));
            $data['estado']                   = html_escape($this->input->post('estado'));
            $id_cidade                  = html_escape($this->input->post('cidade'));
            $data['cep']                   = html_escape($this->input->post('cep'));
            $data['bairro']                   = html_escape($this->input->post('bairro'));
            $data['rua']                   = html_escape($this->input->post('rua'));
            $data['numero']                   = html_escape($this->input->post('numero'));
            $data['complemento']                   = html_escape($this->input->post('complemento'));



            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;


            $cidade_selecionado = $this->ajaxload->get_cidade_by_id($id_cidade);

            foreach ($cidade_selecionado as $cidadex) {
                $nome_cidade = $cidadex->nome;
            }

            $data['cidade'] = $nome_cidade;



            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;




            if (($data['id_projeto'] != null) && ($data['id_nucleo'] != null) && ($data['local'] != null) && ($data['telefone_1'] != null) && ($data['estado'] != null) && ($data['cidade'] != null) && ($data['cep'] != null) && ($data['bairro'] != null) && ($data['rua'] != null) && ($data['numero'] != null)) {

                $this->db->insert('local_execucao', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos obrigatórios devem ser preenchidos');
            }

            redirect(site_url('admin/local_execucao'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['id_projeto']                   = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']                   = html_escape($this->input->post('id_nucleo'));
            $data['local']                   = html_escape($this->input->post('local'));
            $data['telefone_1']                   = html_escape($this->input->post('telefone'));
            $data['telefone_2']                   = html_escape($this->input->post('telefone2'));
            $data['estado']                   = html_escape($this->input->post('estado'));
            $id_cidade                  = html_escape($this->input->post('cidade'));
            $data['cep']                   = html_escape($this->input->post('cep'));
            $data['bairro']                   = html_escape($this->input->post('bairro'));
            $data['rua']                   = html_escape($this->input->post('rua'));
            $data['numero']                   = html_escape($this->input->post('numero'));
            $data['complemento']                   = html_escape($this->input->post('complemento'));



            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['id_projeto']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            $cidade_selecionado = $this->ajaxload->get_cidade_by_id($id_cidade);

            foreach ($cidade_selecionado as $cidadex) {
                $nome_cidade = $cidadex->nome;
            }

            $data['cidade'] = $nome_cidade;



            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;

            if (($data['id_projeto'] != null) && ($data['id_nucleo'] != null) && ($data['local'] != null) && ($data['telefone_1'] != null) && ($data['estado'] != null) && ($data['cidade'] != null) && ($data['cep'] != null) && ($data['bairro'] != null) && ($data['rua'] != null) && ($data['numero'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('local_execucao', $data);
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos obrigatórios devem ser preenchidos');
            }

            redirect(site_url('admin/local_execucao'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('local_execucao');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/local_execucao'), 'refresh');
        }



        $page_data['page_title']    = "Todos os Locais de Execução";
        $page_data['page_name']  = 'local_execucao';
        $this->load->view('backend/index', $page_data);
    }




    function get_locais()
    {



        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');


        $columns = array(
            0 => 'id',
            1 => 'id_projeto',
            2 => 'id_nucleo',
            3 => 'local',
            4 => 'telefone',
            5 => 'telefone2',
            6 => 'estado',
            7 => 'cidade',
            8 => 'cep',
            9 => 'bairro',
            10 => 'rua',
            11 => 'numero',
            12 => 'complemento',
            13 => 'nome_projeto',
            14 => 'nome_nucleo',

            15 => 'options'
        );





        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];




        $totalData = $this->ajaxload->all_locais_count();


        $totalFiltered = $totalData;




        if (empty($this->input->post('search')['value'])) {
            $locais = $this->ajaxload->all_locais($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $locais =  $this->ajaxload->locais_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->local_search_count($search);
        }



        $data = array();
        if (!empty($locais)) {
            foreach ($locais as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="local_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="local_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';




                $nestedData['id'] = $row->id;
                $nestedData['nome_projeto'] = $row->nome_projeto;
                $nestedData['nome_nucleo'] = $row->nome_nucleo;
                $nestedData['local'] = $row->local;
                $nestedData['telefone1'] = $row->telefone_1;
                $nestedData['estado'] = $row->estado;
                $nestedData['cidade'] = $row->cidade;
                $nestedData['cep'] = $row->cep;
                $nestedData['bairro'] = $row->bairro;
                $nestedData['rua'] = $row->rua;
                $nestedData['numero'] = $row->numero;
                $nestedData['complemento'] = $row->complemento;


                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );




        echo json_encode($json_data);
    }



    /****** GET NUCLEO PROJETO ********/


    function get_nucleo_projeto()
    {

        $id_projeto = $this->input->post("id_projeto");

        $nucleos_selecionados = $this->ajaxload->get_n_p($id_projeto);


        $cont = 0;
        $nucleos = array();

        foreach ($nucleos_selecionados as $nuc) {
            $nucleo[$cont]["nome_nucleo"] = $nuc->nome_nucleo;
            $nucleo[$cont]["id_nucleo"] = $nuc->id;
            $cont++;
        }

        echo json_encode($nucleo);
    }



    function get_students_turma()
    {

        $class_id = $this->input->post("class_id");
        // $class_id = $this->input->get("class_id");


        $students_selecionados = $this->ajaxload->get_students_turma($class_id);


        $cont = 0;
        $alunos = array();

        foreach ($students_selecionados as $student) {
            $alunos[$cont]["name"] = $student->name;
            $alunos[$cont]["student_id"] = $student->student_id;
            $cont++;
        }

        echo json_encode($students_selecionados);
    }

    function get_teacher_nucleo()
    {

        $id = $this->input->post("dados");
        $instrutores_selecionados = $this->ajaxload->get_teacher_nucleo($id);
        $cont = 0;
        $instrutores = array();

        foreach ($instrutores_selecionados as $instrutor) {
            $instrutores[$cont]["nome_instrutor"] = $instrutor->name;
            $instrutores[$cont]["id_instrutor"] = $instrutor->id;
            $cont++;
        }

        echo json_encode($instrutores);
    }

    function get_teacher_modalidade()
    {

        $id = $this->input->post("dados");
        $instrutores_selecionados = $this->ajaxload->get_teacher_modalidade($id);
        $cont = 0;
        $instrutores = array();

        foreach ($instrutores_selecionados as $instrutor) {
            $instrutores[$cont]["nome_instrutor"] = $instrutor->name;
            $instrutores[$cont]["id_instrutor"] = $instrutor->id;
            $cont++;
        }

        echo json_encode($instrutores);
    }


    function controle_beneficiario()
    {


        $data['id_projeto']  = html_escape($this->input->post('id_projeto')); //aluno / ja faz (OK)
        $data['id_nucleo']  = html_escape($this->input->post('id_nucleo')); //aluno (OK)
        $turmas  = html_escape($this->input->post('class_id')); //aluno (OK)
        $data['id_local']  = html_escape($this->input->post('local')); //turma 
        $data['sexo']  = html_escape($this->input->post('sexo')); //aluno (OK)
        $data['situacao']  = html_escape($this->input->post('situacao')); //aluno (OK)
        $data['idade']  = html_escape($this->input->post('idade')); //aluno
        $data['tipo_escola']  = html_escape($this->input->post('tipo_escola')); //escola (OK)
        $data['id_escola']  = html_escape($this->input->post('escola_id')); //escola_nucleo (ok)
        $modalidades  = html_escape($this->input->post('modalidade_id[]')); //turma (OK)
        $data['ano'] = html_escape($this->input->post('sessional_year')); //ja tem (OK)
        $ano1 = $data['ano'] + 1;
        $ano_utilizado = $data['ano'] . '-' . $ano1;

        echo $ano_utilizado;

        $data['ano'] = '2019-2020';

        $array_tur = array();
        $cont = 0;

        foreach ($turmas as $tur) {
            echo $tur;

            $array_tur[$cont] = $tur;


            $cont++;
        }

        $data['turma'] = $array_tur;

        $tamanho = count($array_tur);

        $vetor_total = array();


        for ($i = 0; $i < $tamanho; $i++) {



            $sql = 'SELECT student.name as nome, modalidade.modalidade as modalidade,nome_turma as turma, cidade, situacao, estado, rua, bairro, numero, complemento, phone as telefone, data_nascimento, student.id_projeto, student.id_nucleo, student.id_turma, student.id_escola FROM student, class, modalidade, enroll WHERE student.id_projeto = ' . $data['id_projeto'] . '  and student.id_escola = ' . $data['id_escola'] . ' and student.situacao = "' . $data['situacao'] . '" and student.sex="' . $data['sexo'] . '" and student.id_nucleo = ' . $data['id_nucleo'] . ' and student.id_turma = ' . $array_tur[$i] . ' and class.class_id = student.id_turma and class.class_id = enroll.class_id and enroll.year = "' . $ano_utilizado . '" and class.modalidade = modalidade.id group by student.student_id order by student.name ASC';


            $query =  $this->db->query($sql);


            $vetor_total[$i] = $query->result();
        }


















        echo "<pre>";
        var_dump($data);

        echo "</pre>";


        echo "<pre>";
        var_dump($vetor_total);

        echo "</pre>";

        //$query  =  $this->db->query('SELECT student.name as nome, modalidade.modalidade as modalidade,nome_turma as turma, cidade, situacao, estado, rua, bairro, numero, complemento, phone as telefone, data_nascimento FROM student, class, modalidade, enroll WHERE student.id_projeto = ' . $projeto . ' and class.class_id = student.id_turma and class.class_id = enroll.class_id and enroll.year = ' . $ano . ' and class.modalidade = modalidade.id group by student.student_id order by student.name ASC');











    }









    /****** GET CIDADE ESTADO ********/


    function get_cidade_estado()
    {

        $estado = $this->input->post("dados");

        $cidades_estado = $this->ajaxload->get_c_e($estado);


        $cont = 0;
        $cidades = array();

        foreach ($cidades_estado as $cid) {
            $cidades[$cont]["nome_cidade"] = $cid->nome;
            $cidades[$cont]["id"] = $cid->id;
            $cont++;
        }

        echo json_encode($cidades);
    }

    /****** GET LOCAL NUCLEO ********/
    function get_local_nucleo()
    {
        $nucleo = $this->input->post("dados");
        $locais_nucleo = $this->ajaxload->get_l_n($nucleo);

        $cont = 0;
        $locais = array();

        foreach ($locais_nucleo as $loc) {
            $locais[$cont]["local"] = $loc->local;
            $locais[$cont]["id"] = $loc->id;
            $cont++;
        }

        echo json_encode($locais);
    }

    function projeto($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['nome']                   = strtoupper(html_escape($this->input->post('nome')));
            $data['tipo_projeto']                 = html_escape($this->input->post('tipo_projeto'));
            $data['numero']                   = html_escape($this->input->post('numero'));
            //$patrocinador                 = html_escape($this->input->post('id_patrocinador'));
            $data['manifestacao_esportiva']                   = html_escape($this->input->post('manifestacao_esportiva'));
            $data['proponente']                   = html_escape($this->input->post('proponente'));
            $data['metas']                   = html_escape($this->input->post('metas'));
            $data['objetivo']                   = html_escape($this->input->post('objetivo'));
            $data['data_inicio']                   = date('Y/m/d', strtotime(html_escape($this->input->post('data_inicial'))));
            $data['data_final']                   = date('Y/m/d', strtotime(html_escape($this->input->post('data_final'))));
            $data['situacao']                   = html_escape($this->input->post('situacao'));

            if (($data['nome'] != null) && ($data['tipo_projeto'] != null) && ($data['numero'] != null) && ($data['proponente'] != null) && ($data['metas'] != null) && ($data['objetivo'] != null) && ($data['data_inicio'] != null) && ($data['data_final'] != null) && ($data['situacao'] != null)) {
                $this->db->insert('projeto', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('Todos os campos obrigatórios devem ser preenchidos'));
            }

            redirect(site_url('admin/projeto'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['nome']                   = strtoupper(html_escape($this->input->post('nome')));
            $data['tipo_projeto']                 = html_escape($this->input->post('tipo_projeto'));
            $data['numero']                   = html_escape($this->input->post('numero'));
            $data['patrocinador']                   = html_escape($this->input->post('id_patrocinador'));
            $data['manifestacao_esportiva']                   = html_escape($this->input->post('manifestacao_esportiva'));
            $data['proponente']                   = html_escape($this->input->post('proponente'));
            $data['metas']                   = html_escape($this->input->post('metas'));
            $data['objetivo']                   = html_escape($this->input->post('objetivo'));
            $data['data_inicio']                   = html_escape($this->input->post('data_inicial'));
            $data['data_final']                   = html_escape($this->input->post('data_final'));
            $data['situacao']                   = html_escape($this->input->post('situacao'));

            if (($data['nome'] != null) && ($data['tipo_projeto'] != null) && ($data['numero'] != null) && ($data['proponente'] != null) && ($data['metas'] != null) && ($data['objetivo'] != null) && ($data['data_inicio'] != null) && ($data['data_final'] != null) && ($data['situacao'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('projeto', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_update_successfully'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('Todos os campos obrigatórios devem ser preenchidos'));
            }

            redirect(site_url('admin/projeto'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('projeto');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/projeto'), 'refresh');
        }
        $page_data['page_title']    = "Todos os Projetos";
        $page_data['page_name']  = 'projeto';
        $this->load->view('backend/index', $page_data);
    }

    function get_projetos()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'id',
            1 => 'nome',
            2 => 'tipo_projeto',
            3 => 'numero',
            4 => 'patrocinador',
            5 => 'manifestacao_esportiva',
            6 => 'proponente',
            7 => 'metas',
            8 => 'objetivo',
            9 => 'data_inicial',
            10 => 'data_final',
            11 => 'situacao',
            12 => 'options'
        );

        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_projetos_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $projetos = $this->ajaxload->all_projetos($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $projetos =  $this->ajaxload->projetos_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->projeto_search_count($search);
        }



        $data = array();
        if (!empty($projetos)) {
            foreach ($projetos as $row) {

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="projeto_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbspEditar</a></li><li class="divider"></li><li><a href="#" onclick="projeto_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp Deletar</a></li></ul></div>';

                $patrocinador_selecionado = $this->ajaxload->get_patrocinador_by_id($row->patrocinador);

                foreach ($patrocinador_selecionado as $patrox) {
                    $nome_patro = $patrox->nome;
                }

                $nestedData['id'] = $row->id;
                $nestedData['nome'] = $row->nome;
                $nestedData['tipo_projeto'] = $row->tipo_projeto;
                $nestedData['numero'] = $row->numero;
                $nestedData['patrocinador'] = $nome_patro;
                //$nestedData['manifestacao_esportiva'] = $row->manifestacao_esportiva;
                //$nestedData['proponente'] = $row->proponente;
                //$nestedData['metas'] = $row->metas;
                //$nestedData['objetivo'] = $row->objetivo;
                $nestedData['data_de_inicio'] = date('d/m/Y', strtotime($row->data_inicio));
                $nestedData['data_final'] = date('d/m/Y', strtotime($row->data_final));
                $nestedData['situacao'] = $row->situacao;
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }






    /****MANAGE TEACHERS*****/
    function teacher($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']     = strtoupper(html_escape($this->input->post('name')));
            $data['email']    = html_escape($this->input->post('email'));
            $data['CPF']    = html_escape($this->input->post('CPF'));
            $data['RG']    = html_escape($this->input->post('RG'));
            $data['cargo']    = html_escape($this->input->post('cargo'));
            $data['telefone_2']    = html_escape($this->input->post('telefone_2'));
            $data['estado']    = html_escape($this->input->post('estado'));
            $data['id_cidade']    = html_escape($this->input->post('cidade'));
            $data['CEP']    = html_escape($this->input->post('CEP'));
            $data['rua']    = html_escape($this->input->post('rua'));
            $data['bairro']    = html_escape($this->input->post('bairro'));
            $data['numero']    = html_escape($this->input->post('numero'));
            $data['complemento']    = html_escape($this->input->post('complemento'));
            $data['carga_horaria']    = html_escape($this->input->post('carga_horaria'));
            $data['data_contratacao']    = html_escape($this->input->post('data_contratacao'));
            $data['conta_bancaria']    = html_escape($this->input->post('conta_bancaria'));
            $data['salario_bruto']    = html_escape($this->input->post('salario_bruto'));
            $data['salario_liquido']    = html_escape($this->input->post('salario_liquido'));
            //$data['combustivel']    = html_escape($this->input->post('combustivel'));
            //$data['metas_quantitativas']    = html_escape($this->input->post('metas_quantitativas'));
            //$data['metas_qualitativas']    = html_escape($this->input->post('metas_qualitativas'));
            //$data['objetivos']    = html_escape($this->input->post('objetivos'));




            if (($data['id_cidade'] == '') || ($data['id_cidade'] == null)) {
                $this->session->set_flashdata('error_message', get_phrase('Todos os campos obrigatórios devem ser preenchidos'));
                redirect(site_url('admin/teacher'), 'refresh');
            }







            $cidade_selecionado = $this->ajaxload->get_cidade_by_id($data['id_cidade']);

            foreach ($cidade_selecionado as $cidadex) {
                $nome_cidade = $cidadex->nome;
            }

            $data['cidade'] = $nome_cidade;


            $data['password'] = sha1($this->input->post('password'));
            if (html_escape($this->input->post('birthday')) != null) {
                $data['birthday'] = html_escape($this->input->post('birthday'));
            }
            if ($this->input->post('sex') != null) {
                $data['sex'] = $this->input->post('sex');
            }
            if (html_escape($this->input->post('address')) != null) {
                // $data['address'] = html_escape($this->input->post('address'));
            }
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone'] = html_escape($this->input->post('phone'));
            }
            if (html_escape($this->input->post('designation')) != null) {
                // $data['designation'] = html_escape($this->input->post('designation'));
            }
            if ($this->input->post('show_on_website') != null) {
                $data['show_on_website'] = "1";
            }
            if ($this->input->post('about') != null) {
                $data['about'] = $this->input->post('about');
            }
            $links = array();
            $social['facebook'] = html_escape($this->input->post('facebook'));
            $social['twitter'] = html_escape($this->input->post('twitter'));
            $social['linkedin'] = html_escape($this->input->post('linkedin'));
            array_push($links, $social);
            $data['social_links'] = json_encode($links);

            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('teacher', $data);
                $teacher_id = $this->db->insert_id();
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $teacher_id . '.jpg');
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                $this->email_model->account_opening_email('teacher', $data['email']); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/teacher'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']        = strtoupper(html_escape($this->input->post('name')));
            $data['email']       = html_escape($this->input->post('email'));
            $data['password'] = sha1($this->input->post('senha'));
            $data['CPF']    = html_escape($this->input->post('CPF'));
            $data['RG']    = html_escape($this->input->post('RG'));
            $data['cargo']    = html_escape($this->input->post('cargo'));
            $data['telefone_2']    = html_escape($this->input->post('telefone_2'));
            $data['estado']    = html_escape($this->input->post('estado'));
            $data['id_cidade']    = html_escape($this->input->post('cidade'));
            $data['CEP']    = html_escape($this->input->post('CEP'));
            $data['rua']    = html_escape($this->input->post('rua'));
            $data['bairro']    = html_escape($this->input->post('bairro'));
            $data['numero']    = html_escape($this->input->post('numero'));
            $data['complemento']    = html_escape($this->input->post('complemento'));
            $data['carga_horaria']    = html_escape($this->input->post('carga_horaria'));
            $data['data_contratacao']    = html_escape($this->input->post('data_contratacao'));
            $data['conta_bancaria']    = html_escape($this->input->post('conta_bancaria'));
            $data['salario_bruto']    = html_escape($this->input->post('salario_bruto'));
            $data['salario_liquido']    = html_escape($this->input->post('salario_liquido'));
            //$data['combustivel']    = html_escape($this->input->post('combustivel'));
            //$data['metas_quantitativas']    = html_escape($this->input->post('metas_quantitativas'));
            //$data['metas_qualitativas']    = html_escape($this->input->post('metas_qualitativas'));
            //$data['objetivos']    = html_escape($this->input->post('objetivos'));


            $cidade_selecionado = $this->ajaxload->get_cidade_by_id($data['id_cidade']);

            foreach ($cidade_selecionado as $cidadex) {
                $nome_cidade = $cidadex->nome;
            }

            $data['cidade'] = $nome_cidade;

            if (html_escape($this->input->post('birthday')) != null) {
                $data['birthday'] = html_escape($this->input->post('birthday'));
            } else {
                $data['birthday'] = null;
            }
            if ($this->input->post('sex') != null) {
                $data['sex']         = $this->input->post('sex');
            }
            if (html_escape($this->input->post('address')) != null) {
                //$data['address']     = html_escape($this->input->post('address'));
            } else {
                $data['address'] = null;
            }
            if (html_escape($this->input->post('phone')) != null) {
                $data['phone']       = html_escape($this->input->post('phone'));
            } else {
                $data['phone'] = null;
            }
            if (html_escape($this->input->post('designation')) != null) {
                //$data['designation']       = html_escape($this->input->post('designation'));
            } else {
                $data['designation'] = null;
            }
            if ($this->input->post('show_on_website') != null) {
                $data['show_on_website']       = "1";
            } else {
                $data['show_on_website'] = null;
            }

            if ($this->input->post('about') != null) {
                $data['about'] = $this->input->post('about');
            } else {
                $data['about'] = null;
            }
            $links = array();
            $social['facebook'] = html_escape($this->input->post('facebook'));
            $social['twitter'] = html_escape($this->input->post('twitter'));
            $social['linkedin'] = html_escape($this->input->post('linkedin'));
            array_push($links, $social);
            $data['social_links'] = json_encode($links);

            $validation = email_validation_for_edit($data['email'], $param2, 'teacher');
            if ($validation == 1) {
                $this->db->where('teacher_id', $param2);
                $this->db->update('teacher', $data);
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/teacher_image/' . $param2 . '.jpg');
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/teacher'), 'refresh');
        } else if ($param1 == 'personal_profile') {
            $page_data['personal_profile']   = true;
            $page_data['current_teacher_id'] = $param2;
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('teacher', array(
                'teacher_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('teacher_id', $param2);
            $this->db->delete('teacher');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/teacher'), 'refresh');
        }
        $page_data['teachers']   = $this->db->get('teacher')->result_array();
        $page_data['page_name']  = 'teacher';
        $page_data['page_title'] = 'Gerenciar Instrutores';
        $this->load->view('backend/index', $page_data);
    }

    function get_teachers()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'teacher_id',
            1 => 'photo',
            2 => 'name',
            3 => 'email',
            4 => 'phone',
            5 => 'CPF',
            6 => 'RG',
            7 => 'cargo',
            8 => 'telefone_2',
            9 => 'estado',
            10 => 'cidade',
            11 => 'CEP',
            12 => 'rua',
            13 => 'bairro',
            14 => 'numero',
            15 => 'complemento',
            16 => 'carga_horaria',
            17 => 'data_contratacao',
            18 => 'conta_bancaria',
            19 => 'salario_bruto',
            20 => 'salario_liquido',
            21 => 'teacher_id'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_teachers_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $teachers = $this->ajaxload->all_teachers($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $teachers =  $this->ajaxload->teacher_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->teacher_search_count($search);
        }

        $data = array();
        if (!empty($teachers)) {
            foreach ($teachers as $row) {

                $photo = '<img src="' . $this->crud_model->get_image_url('teacher', $row->teacher_id) . '" class="img-circle" width="30" />';

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="teacher_edit_modal(' . $row->teacher_id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="teacher_delete_confirm(' . $row->teacher_id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $nestedData['teacher_id'] = $row->teacher_id;
                $nestedData['photo'] = $photo;
                $nestedData['name'] = $row->name;
                $nestedData['email'] = $row->email;
                $nestedData['phone'] = $row->phone;
                $nestedData['CPF'] = $row->CPF;
                $nestedData['RG'] = $row->RG;
                $nestedData['cargo'] = $row->cargo;
                $nestedData['telefone_2'] = $row->telefone_2;
                $nestedData['estado'] = $row->estado;
                $nestedData['cidade'] = $row->cidade;
                $nestedData['CEP'] = $row->CEP;
                $nestedData['rua'] = $row->rua;
                $nestedData['bairro'] = $row->bairro;
                $nestedData['numero'] = $row->numero;
                $nestedData['complemento'] = $row->complemento;
                $nestedData['carga_horaria'] = $row->carga_horaria;
                $nestedData['data_contratacao'] = date('d/m/Y', strtotime($row->data_contratacao));
                $nestedData['conta_bancaria'] = $row->conta_bancaria;
                $nestedData['salario_liquido'] = $row->salario_liquido;
                $nestedData['salario_bruto'] = $row->salario_bruto;
                //$nestedData['combustivel'] = $row->combustivel;
                //$nestedData['metas_qualitativas'] = $row->metas_qualitativas;
                //$nestedData['metas_quantitativas'] = $row->metas_quantitativas;
                //$nestedData['objetivos'] = $row->objetivos;
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****MANAGE SUBJECTS*****/
    function subject($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']       = html_escape($this->input->post('name'));
            //$data['class_id']   = $this->input->post('class_id');
            $data['descricao'] = $this->input->post('descricao');
            $data['year']       = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($this->input->post('teacher_id') != null) {
                $data['teacher_id'] = $this->input->post('teacher_id');
            }

            $this->db->insert('subject', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/subject/' . $data['class_id']), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']       = html_escape($this->input->post('name'));
            //$data['class_id']   = $this->input->post('class_id');
            $data['teacher_id'] = $this->input->post('teacher_id');
            $data['descricao'] = $this->input->post('descricao');
            $data['year']       = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

            $this->db->where('subject_id', $param2);
            $this->db->update('subject', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/subject/' . $data['class_id']), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('subject', array(
                'subject_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('subject_id', $param2);
            $this->db->delete('subject');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/subject/' . $param3), 'refresh');
        }
        $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        //$page_data['class_id']   = $param1;
        $page_data['subjects']   = $this->db->get_where('subject', array('year' => $running_year))->result_array();
        $page_data['page_name']  = 'subject';
        $page_data['page_title'] = 'Gerenciar Atividades';
        $this->load->view('backend/index', $page_data);
    }



    /****** GERENCIAR PATROCINADORES ******/

    function patrocinador($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');





        if ($param1 == 'create') {


            $data['nome']                   = html_escape($this->input->post('nome'));


            if (($data['nome'] != null)) {

                $this->db->insert('patrocinador', $data);
                $patrocinador_id = $this->db->insert_id();
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/patrocinador_image/' . $patrocinador_id . '.jpg');
                $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/patrocinador'), 'refresh');
        }


        if ($param1 == 'edit') {
            $data['nome']                   = html_escape($this->input->post('nome'));



            if (($data['nome'] != null)) {
                $this->db->where('id', $param2);
                $this->db->update('patrocinador', $data);

                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/patrocinador_image/' . $param2 . '.jpg');
                $this->session->set_flashdata('flash_message', 'Dado alterado com sucesso!');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/patrocinador'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('id', $param2);
            $this->db->delete('patrocinador');
            $this->session->set_flashdata('flash_message', 'Dado deletado');
            redirect(site_url('admin/patrocinador'), 'refresh');
        }



        $page_data['page_title']    = "Todas os Patrocinadores";
        $page_data['page_name']  = 'patrocinador';
        $this->load->view('backend/index', $page_data);
    }




    function get_patrocinadores()
    {


        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');


        $columns = array(
            0 => 'id',
            1 => 'nome',
            2 => 'options',
            3 => 'foto'
        );





        $limit = html_escape($this->input->post('length'));
        $start = html_escape($this->input->post('start'));
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];




        $totalData = $this->ajaxload->all_patrocinadores_count();


        $totalFiltered = $totalData;




        if (empty($this->input->post('search')['value'])) {
            $patrocinadores = $this->ajaxload->all_patrocinadores($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $patrocinadores =  $this->ajaxload->patrocinadores_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->patrocinador_search_count($search);
        }



        $data = array();
        if (!empty($patrocinadores)) {
            foreach ($patrocinadores as $row) {

                $img = $this->crud_model->get_image_url('patrocinador', $row->id);

                $foto = '<img src="' . $img . '" class="" width="30" />';

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="patrocinador_edit_modal(' . $row->id . ')"><i class="entypo-pencil"></i>&nbsp;Editar</a></li><li class="divider"></li><li><a href="#" onclick="patrocinador_delete_confirm(' . $row->id . ')"><i class="entypo-trash"></i>&nbsp;Deletar</a></li></ul></div>';

                $nestedData['id'] = $row->id;
                $nestedData['nome'] = $row->nome;
                $nestedData['options'] = $options;
                $nestedData['foto'] = $foto;
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /****MANAGE CLASSES*****/
    function classes($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {

            $data['name']         = strtoupper(html_escape($this->input->post('name')));
            $data['teacher_id']   = $this->input->post('id_istrutor');
            $data['projeto_id']   = $this->input->post('id_projeto');
            $data['id_nucleo']   = $this->input->post('id_nucleo');
            $data['id_local']   = $this->input->post('id_local');
            $data['modalidade']   = $this->input->post('modalidade');
            $data['categoria']   = $this->input->post('categoria');
            $data['intervalo_idades']   = $this->input->post('intervalo_idades');
            //$data['sexo']   = $this->input->post('sexo');
            $data['numero_atletas']   = $this->input->post('numero_atletas');

            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['projeto_id']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            // 12 AM for starting time
            if ($this->input->post('time_start') == 12 && $this->input->post('starting_ampm') == 1) {
                $data['time_start'] = 24;
            }
            // 12 PM for starting time
            else if ($this->input->post('time_start') == 12 && $this->input->post('starting_ampm') == 2) {
                $data['time_start'] = 12;
            }
            // otherwise for starting time
            else {
                $data['time_start']     = $this->input->post('time_start') + (12 * ($this->input->post('starting_ampm') - 1));
            }
            // 12 AM for ending time
            if ($this->input->post('time_end') == 12 && $this->input->post('ending_ampm') == 1) {
                $data['time_end'] = 24;
            }
            // 12 PM for ending time
            else if ($this->input->post('time_end') == 12 && $this->input->post('ending_ampm') == 2) {
                $data['time_end'] = 12;
            }
            // otherwise for ending time
            else {
                $data['time_end']       = $this->input->post('time_end') + (12 * ($this->input->post('ending_ampm') - 1));
            }

            $data['time_start_min'] = $this->input->post('time_start_min');
            $data['time_end_min']   = $this->input->post('time_end_min');

            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;


            $local_selecionado = $this->ajaxload->get_local_by_id($data['id_local']);

            foreach ($local_selecionado as $localx) {
                $nome_local = $localx->local;
            }

            $data['local'] = $nome_local;

            $instrutor_selecionado = $this->ajaxload->get_instrutor_by_id($data['teacher_id']);

            foreach ($instrutor_selecionado as $instx) {
                $nome_instrutor = $instx->name;
            }

            $data['nome_instrutor'] = $nome_instrutor;

            if (($data['name'] != null) && ($data['teacher_id'] != null) && ($data['projeto_id'] != null) && ($data['id_nucleo'] != null) && ($data['id_local'] != null) && ($data['modalidade'] != null) && ($data['categoria'] != null) && ($data['intervalo_idades'] != null) && ($data['numero_atletas'] != null) && ($data['time_start'] != null) && ($data['time_end'] != null) && ($data['time_start_min'] != null) && ($data['time_end_min'] != null)) {

                $this->db->insert('class', $data);
                $class_id = $this->db->insert_id();
                //create a section by default
                $data2['class_id']  =   $class_id;
                $data2['name']      =   'A';
                $data2['teacher_id'] = $data['teacher_id'];
                $this->db->insert('section', $data2);

                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            } else {

                $this->session->set_flashdata('error_message', 'Todos os campos obrigatórios devem ser preenchidos');
            }

            redirect(site_url('admin/classes'), 'refresh');
        }


        if ($param1 == 'do_update') {
            $data['name']         = html_escape($this->input->post('name'));
            $data['teacher_id']   = $this->input->post('id_istrutor');
            $data['projeto_id']   = $this->input->post('id_projeto');
            $data['id_nucleo']   = $this->input->post('id_nucleo');
            $data['id_local']   = $this->input->post('id_local');
            $data['modalidade']   = $this->input->post('modalidade');
            $data['categoria']   = $this->input->post('categoria');
            $data['intervalo_idades']   = $this->input->post('intervalo_idades');
            //$data['sexo']   = $this->input->post('sexo');
            $data['numero_atletas']   = $this->input->post('numero_atletas');

            // 12 AM for starting time
            if ($this->input->post('time_start') == 12 && $this->input->post('starting_ampm') == 1) {
                $data['time_start'] = 24;
                $dataroutine['time_start'] = 24;
            }
            // 12 PM for starting time
            else if ($this->input->post('time_start') == 12 && $this->input->post('starting_ampm') == 2) {
                $data['time_start'] = 12;
                $dataroutine['time_start'] = 12;
            }
            // otherwise for starting time
            else {
                $data['time_start']     = $this->input->post('time_start') + (12 * ($this->input->post('starting_ampm') - 1));
                $dataroutine['time_start']     = $this->input->post('time_start') + (12 * ($this->input->post('starting_ampm') - 1));
            }
            // 12 AM for ending time
            if ($this->input->post('time_end') == 12 && $this->input->post('ending_ampm') == 1) {
                $data['time_end'] = 24;
                $dataroutine['time_end'] = 24;
            }
            // 12 PM for ending time
            else if ($this->input->post('time_end') == 12 && $this->input->post('ending_ampm') == 2) {
                $data['time_end'] = 12;
                $dataroutine['time_end'] = 12;
            }
            // otherwise for ending time
            else {
                $data['time_end']       = $this->input->post('time_end') + (12 * ($this->input->post('ending_ampm') - 1));
                $dataroutine['time_end']       = $this->input->post('time_end') + (12 * ($this->input->post('ending_ampm') - 1));
            }

            $data['time_start_min'] = $this->input->post('time_start_min');
            $data['time_end_min']   = $this->input->post('time_end_min');

            $dataroutine['time_start_min'] = $this->input->post('time_start_min');
            $dataroutine['time_end_min']   = $this->input->post('time_end_min');


            $projeto_selecionado = $this->ajaxload->get_projeto_by_id($data['projeto_id']);

            foreach ($projeto_selecionado as $projetox) {
                $nome_projeto = $projetox->nome;
            }

            $data['nome_projeto'] = $nome_projeto;

            $nucleo_selecionado = $this->ajaxload->get_nucleo_by_id($data['id_nucleo']);

            foreach ($nucleo_selecionado as $nucleox) {
                $nome_nucleo = $nucleox->nome_nucleo;
            }

            $data['nome_nucleo'] = $nome_nucleo;


            $local_selecionado = $this->ajaxload->get_local_by_id($data['id_local']);

            foreach ($local_selecionado as $localx) {
                $nome_local = $localx->local;
            }

            $data['local'] = $nome_local;

            $instrutor_selecionado = $this->ajaxload->get_instrutor_by_id($data['teacher_id']);

            foreach ($instrutor_selecionado as $instx) {
                $nome_instrutor = $instx->name;
            }

            $data['nome_instrutor'] = $nome_instrutor;

            if ($this->input->post('name_numeric') != null) {
                $data['name_numeric'] = html_escape($this->input->post('name_numeric'));
            } else {
                $data['name_numeric'] = null;
            }

            if (($data['name'] != null) && ($data['teacher_id'] != null) && ($data['projeto_id'] != null) && ($data['id_nucleo'] != null) && ($data['id_local'] != null) && ($data['modalidade'] != null) && ($data['categoria'] != null) && ($data['intervalo_idades'] != null) && ($data['numero_atletas'] != null) && ($data['time_start'] != null) && ($data['time_end'] != null) && ($data['time_start_min'] != null) && ($data['time_end_min'] != null)) {
                $this->db->where('class_id', $param2);
                $this->db->update('class', $data);

                $this->db->where('class_id', $param2);
                $this->db->update('class_routine', $dataroutine);

                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
                redirect(site_url('admin/classes'), 'refresh');
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos obrigatórios devem ser preenchidos');
            }
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('class', array(
                'class_id' => $param2
            ))->result_array();
        }

        if ($param1 == 'delete') {
            $this->db->where('class_id', $param2);
            $this->db->delete('class');

            $this->db->where('class_id', $param2);
            $this->db->delete('enroll');

            $this->db->where('class_id', $param2);
            $this->db->delete('class_routine');


            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/classes'), 'refresh');
        }
        $page_data['classes']    = $this->db->get('class')->result_array();
        $page_data['page_name']  = 'class';
        $page_data['page_title'] = "Gerenciar Turmas";
        $this->load->view('backend/index', $page_data);
    }

    function get_subject($class_id)
    {
        $subject = $this->db->get_where('subject', array(
            'class_id' => $class_id
        ))->result_array();
        foreach ($subject as $row) {
            echo '<option value="' . $row['subject_id'] . '">' . $row['name'] . '</option>';
        }
    }

    // ACADEMIC SYLLABUS
    function academic_syllabus($class_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        // detect the first class
        if ($class_id == '')
            $class_id           =   $this->db->get('class')->first_row()->class_id;

        $page_data['page_name']  = 'academic_syllabus';
        $page_data['page_title'] = "Programa Acadêmico";
        $page_data['class_id']   = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function upload_academic_syllabus()
    {
        $data['academic_syllabus_code'] =   substr(md5(rand(0, 1000000)), 0, 7);
        if ($this->input->post('description') != null) {
            $data['description'] = html_escape($this->input->post('description'));
        }
        $data['title']                  =   html_escape($this->input->post('title'));
        $data['class_id']               =   $this->input->post('class_id');
        $data['subject_id']             =   $this->input->post('subject_id');
        $data['uploader_type']          =   $this->session->userdata('login_type');
        $data['uploader_id']            =   $this->session->userdata('login_user_id');
        $data['year']                   =   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        $data['timestamp']              =   strtotime(date("Y-m-d H:i:s"));
        //uploading file using codeigniter upload library
        $files = $_FILES['file_name'];
        $this->load->library('upload');
        $config['upload_path']   =  'uploads/syllabus/';
        $config['allowed_types'] =  '*';
        $_FILES['file_name']['name']     = $files['name'];
        $_FILES['file_name']['type']     = $files['type'];
        $_FILES['file_name']['tmp_name'] = $files['tmp_name'];
        $_FILES['file_name']['size']     = $files['size'];
        $this->upload->initialize($config);
        $this->upload->do_upload('file_name');

        $data['file_name'] = $_FILES['file_name']['name'];

        $this->db->insert('academic_syllabus', $data);
        $this->session->set_flashdata('flash_message', get_phrase('syllabus_uploaded'));
        redirect(site_url('admin/academic_syllabus/' . $data['class_id']), 'refresh');
    }

    function download_academic_syllabus($academic_syllabus_code)
    {
        $file_name = $this->db->get_where('academic_syllabus', array(
            'academic_syllabus_code' => $academic_syllabus_code
        ))->row()->file_name;
        $this->load->helper('download');
        $data = file_get_contents("uploads/syllabus/" . $file_name);
        $name = $file_name;

        force_download($name, $data);
    }

    function delete_academic_syllabus($academic_syllabus_code)
    {
        $file_name = $this->db->get_where('academic_syllabus', array(
            'academic_syllabus_code' => $academic_syllabus_code
        ))->row()->file_name;
        if (file_exists('uploads/syllabus/' . $file_name)) {
            // unlink('uploads/syllabus/'.$file_name);
        }
        $this->db->where('academic_syllabus_code', $academic_syllabus_code);
        $this->db->delete('academic_syllabus');

        $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
        redirect(site_url('admin/academic_syllabus'), 'refresh');
    }

    /****MANAGE SECTIONS*****/
    function section($class_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        // detect the first class
        if ($class_id == '')
            $class_id           =   $this->db->get('class')->first_row()->class_id;

        $page_data['page_name']  = 'section';
        $page_data['page_title'] = 'Gerenciar Sessões';
        $page_data['class_id']   = $class_id;
        $this->load->view('backend/index', $page_data);
    }

    function sections($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']       =   html_escape($this->input->post('name'));
            $data['class_id']   =   $this->input->post('class_id');
            $data['teacher_id'] =   $this->input->post('teacher_id');
            if ($this->input->post('nick_name') != null) {
                $data['nick_name'] = html_escape($this->input->post('nick_name'));
            }
            $validation = duplication_of_section_on_create($data['class_id'], $data['name']);
            if ($validation == 1) {
                $this->db->insert('section', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('duplicate_name_of_section_is_not_allowed'));
            }

            redirect(site_url('admin/section/' . $data['class_id']), 'refresh');
        }

        if ($param1 == 'edit') {
            $data['name']       =   html_escape($this->input->post('name'));
            $data['class_id']   =   $this->input->post('class_id');
            $data['teacher_id'] =   $this->input->post('teacher_id');
            if ($this->input->post('nick_name') != null) {
                $data['nick_name'] = html_escape($this->input->post('nick_name'));
            } else {
                $data['nick_name'] = null;
            }
            $validation = duplication_of_section_on_edit($param2, $data['class_id'], $data['name']);
            if ($validation == 1) {
                $this->db->where('section_id', $param2);
                $this->db->update('section', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('duplicate_name_of_section_is_not_allowed'));
            }

            redirect(site_url('admin/section/' . $data['class_id']), 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('section_id', $param2);
            $this->db->delete('section');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/section'), 'refresh');
        }
    }

    function get_class_section($class_id)
    {
        $sections = $this->db->get_where('section', array(
            'class_id' => $class_id
        ))->result_array();
        foreach ($sections as $row) {
            echo '<option value="' . $row['section_id'] . '">' . $row['name'] . '</option>';
        }
    }

    function get_class_section_selector($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/admin/get_class_section_selector', $page_data);
    }

    function get_class_subject_selector($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/admin/get_class_subject_selector', $page_data);
    }

    function get_class_subject($class_id)
    {
        $subjects = $this->db->get_where('subject', array(
            'class_id' => $class_id
        ))->result_array();
        foreach ($subjects as $row) {
            echo '<option value="' . $row['subject_id'] . '">' . $row['name'] . '</option>';
        }
    }

    function get_class_students($class_id)
    {
        $students = $this->db->get_where('enroll', array(
            'class_id' => $class_id, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->result_array();
        foreach ($students as $row) {
            $name = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;
            echo '<option value="' . $row['student_id'] . '">' . $name . '</option>';
        }
    }

    function get_class_students_mass($class_id)
    {
        $students = $this->db->get_where('enroll', array(
            'class_id' => $class_id, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
        ))->result_array();
        echo '<div class="form-group">
                <label class="col-sm-3 control-label">' . get_phrase('students') . '</label>
                <div class="col-sm-9">';
        foreach ($students as $row) {
            $name = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;
            echo '<div class="checkbox">
                    <label><input type="checkbox" class="check" name="student_id[]" value="' . $row['student_id'] . '">' . $name . '</label>
                </div>';
        }
        echo '<br><button type="button" class="btn btn-default" onClick="select()">' . get_phrase('select_all') . '</button>';
        echo '<button style="margin-left: 5px;" type="button" class="btn btn-default" onClick="unselect()"> ' . get_phrase('select_none') . ' </button>';
        echo '</div></div>';
    }

    /****MANAGE EXAMS*****/
    function exam($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']    = html_escape($this->input->post('name'));
            $data['date']    = html_escape($this->input->post('date'));
            $data['year']    = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($this->input->post('comment') != null) {
                $data['comment'] = html_escape($this->input->post('comment'));
            }
            $this->db->insert('exam', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/exam'), 'refresh');
        }
        if ($param1 == 'edit' && $param2 == 'do_update') {
            $data['name']    = html_escape($this->input->post('name'));
            $data['date']    = html_escape($this->input->post('date'));
            if ($this->input->post('comment') != null) {
                $data['comment'] = html_escape($this->input->post('comment'));
            } else {
                $data['comment'] = null;
            }
            $data['year']    = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

            $this->db->where('exam_id', $param3);
            $this->db->update('exam', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/exam'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('exam', array(
                'exam_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('exam_id', $param2);
            $this->db->delete('exam');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/exam'), 'refresh');
        }
        $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        $page_data['exams']      = $this->db->get_where('exam', array('year' => $running_year))->result_array();
        $page_data['page_name']  = 'exam';
        $page_data['page_title'] = get_phrase('manage_exam');
        $this->load->view('backend/index', $page_data);
    }

    /****** SEND EXAM MARKS VIA SMS ********/
    function exam_marks_sms($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'send_sms') {

            $exam_id    =   $this->input->post('exam_id');
            $class_id   =   $this->input->post('class_id');
            $receiver   =   $this->input->post('receiver');
            if ($exam_id != '' && $class_id != '' && $receiver != '') {
                // get all the students of the selected class
                $students = $this->db->get_where('enroll', array(
                    'class_id' => $class_id,
                    'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
                ))->result_array();
                // get the marks of the student for selected exam
                foreach ($students as $row) {
                    if ($receiver == 'student')
                        $receiver_phone = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->phone;
                    if ($receiver == 'parent') {
                        $parent_id =  $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->parent_id;
                        if ($parent_id != '' || $parent_id != null) {
                            $receiver_phone = $this->db->get_where('parent', array('parent_id' => $row['parent_id']))->row()->phone;
                            if ($receiver_phone == null) {
                                $this->session->set_flashdata('error_message', get_phrase('parent_phone_number_is_not_found'));
                            }
                        }
                    }
                    $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
                    $this->db->where('exam_id', $exam_id);
                    $this->db->where('student_id', $row['student_id']);
                    $this->db->where('year', $running_year);
                    $marks = $this->db->get('mark')->result_array();

                    $message = '';
                    foreach ($marks as $row2) {
                        $subject       = $this->db->get_where('subject', array('subject_id' => $row2['subject_id']))->row()->name;
                        $mark_obtained = $row2['mark_obtained'];
                        $message      .= $row2['student_id'] . $subject . ' : ' . $mark_obtained . ' , ';
                    }
                    // send sms
                    $this->sms_model->send_sms($message, $receiver_phone);
                }
                $this->session->set_flashdata('flash_message', get_phrase('message_sent'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('select_all_the_fields'));
            }
            redirect(site_url('admin/exam_marks_sms'), 'refresh');
        }

        $page_data['page_name']  = 'exam_marks_sms';
        $page_data['page_title'] = get_phrase('send_marks_by_sms');
        $this->load->view('backend/index', $page_data);
    }

    function marks_manage()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  =   'marks_manage';
        $page_data['page_title'] = get_phrase('manage_exam_marks');
        $this->load->view('backend/index', $page_data);
    }

    function marks_manage_view($exam_id = '', $class_id = '', $section_id = '', $subject_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['exam_id']    =   $exam_id;
        $page_data['class_id']   =   $class_id;
        $page_data['subject_id'] =   $subject_id;
        $page_data['section_id'] =   $section_id;
        $page_data['page_name']  =   'marks_manage_view';
        $page_data['page_title'] = get_phrase('manage_exam_marks');
        $this->load->view('backend/index', $page_data);
    }

    function marks_selector()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $data['exam_id']    = $this->input->post('exam_id');
        $data['class_id']   = $this->input->post('class_id');
        $data['section_id'] = $this->input->post('section_id');
        $data['subject_id'] = $this->input->post('subject_id');
        $data['year']       = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        if ($data['class_id'] != '' && $data['exam_id'] != '') {
            $query = $this->db->get_where('mark', array(
                'exam_id' => $data['exam_id'],
                'class_id' => $data['class_id'],
                'section_id' => $data['section_id'],
                'subject_id' => $data['subject_id'],
                'year' => $data['year']
            ));
            if ($query->num_rows() < 1) {
                $students = $this->db->get_where('enroll', array(
                    'class_id' => $data['class_id'], 'section_id' => $data['section_id'], 'year' => $data['year']
                ))->result_array();
                foreach ($students as $row) {
                    $data['student_id'] = $row['student_id'];
                    $this->db->insert('mark', $data);
                }
            }
            redirect(site_url('admin/marks_manage_view/' . $data['exam_id'] . '/' . $data['class_id'] . '/' . $data['section_id'] . '/' . $data['subject_id']), 'refresh');
        } else {
            $this->session->set_flashdata('error_message', get_phrase('select_all_the_fields'));
            $page_data['page_name']  =   'marks_manage';
            $page_data['page_title'] = get_phrase('manage_exam_marks');
            $this->load->view('backend/index', $page_data);
        }
    }

    function marks_update($exam_id = '', $class_id = '', $section_id = '', $subject_id = '')
    {
        if ($class_id != '' && $exam_id != '') {
            $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            $marks_of_students = $this->db->get_where('mark', array(
                'exam_id' => $exam_id,
                'class_id' => $class_id,
                'section_id' => $section_id,
                'year' => $running_year,
                'subject_id' => $subject_id
            ))->result_array();
            foreach ($marks_of_students as $row) {
                $obtained_marks = html_escape($this->input->post('marks_obtained_' . $row['mark_id']));
                $comment = html_escape($this->input->post('comment_' . $row['mark_id']));
                $this->db->where('mark_id', $row['mark_id']);
                $this->db->update('mark', array('mark_obtained' => $obtained_marks, 'comment' => $comment));
            }
            $this->session->set_flashdata('flash_message', get_phrase('marks_updated'));
            redirect(site_url('admin/marks_manage_view/' . $exam_id . '/' . $class_id . '/' . $section_id . '/' . $subject_id), 'refresh');
        } else {
            $this->session->set_flashdata('error_message', get_phrase('select_all_the_fields'));
            $page_data['page_name']  =   'marks_manage';
            $page_data['page_title'] = get_phrase('manage_exam_marks');
            $this->load->view('backend/index', $page_data);
        }
    }
    function marks_get_subject($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/admin/marks_get_subject', $page_data);
    }

    // TABULATION SHEET
    function tabulation_sheet($class_id = '', $exam_id = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($this->input->post('operation') == 'selection') {
            $page_data['exam_id']    = html_escape($this->input->post('exam_id'));
            $page_data['class_id']   = html_escape($this->input->post('class_id'));

            if ($page_data['exam_id'] > 0 && $page_data['class_id'] > 0) {
                redirect(site_url('admin/tabulation_sheet/' . $page_data['class_id'] . '/' . $page_data['exam_id']), 'refresh');
            } else {
                $this->session->set_flashdata('mark_message', 'Choose class and exam');
                redirect(site_url('admin/tabulation_sheet'), 'refresh');
            }
        }
        $page_data['exam_id']    = $exam_id;
        $page_data['class_id']   = $class_id;

        $page_data['page_info'] = 'Exam marks';

        $page_data['page_name']  = 'tabulation_sheet';
        $page_data['page_title'] = get_phrase('tabulation_sheet');
        $this->load->view('backend/index', $page_data);
    }

    function tabulation_sheet_print_view($class_id, $exam_id)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['class_id'] = $class_id;
        $page_data['exam_id']  = $exam_id;
        $this->load->view('backend/admin/tabulation_sheet_print_view', $page_data);
    }

    /****MANAGE GRADES*****/
    function grade($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']        = html_escape($this->input->post('name'));
            $data['grade_point'] = html_escape($this->input->post('grade_point'));
            $data['mark_from']   = html_escape($this->input->post('mark_from'));
            $data['mark_upto']   = html_escape($this->input->post('mark_upto'));
            if ($this->input->post('comment') != null) {
                $data['comment'] = html_escape($this->input->post('comment'));
            }

            $this->db->insert('grade', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/grade'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']        = html_escape($this->input->post('name'));
            $data['grade_point'] = html_escape($this->input->post('grade_point'));
            $data['mark_from']   = html_escape($this->input->post('mark_from'));
            $data['mark_upto']   = html_escape($this->input->post('mark_upto'));
            if ($this->input->post('comment') != null) {
                $data['comment'] = html_escape($this->input->post('comment'));
            } else {
                $data['comment'] = null;
            }

            $this->db->where('grade_id', $param2);
            $this->db->update('grade', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/grade'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('grade', array(
                'grade_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('grade_id', $param2);
            $this->db->delete('grade');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/grade'), 'refresh');
        }
        $page_data['grades']     = $this->db->get('grade')->result_array();
        $page_data['page_name']  = 'grade';
        $page_data['page_title'] = get_phrase('manage_grade');
        $this->load->view('backend/index', $page_data);
    }

    /**********MANAGING CLASS ROUTINE******************/
    function class_routine($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {


            // $data['subject_id']     = $this->input->post('subject_id');

            $data1 = $this->input->post('data');

            $data_at = str_replace("/", "-",  $data1);
            $data['data_atividade'] = date('Y-m-d', strtotime($data_at));


            //$data['day']            = $this->input->post('day');
            $data['year']           = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

            $array = array(
                'class_id'      => $data['class_id'],
                'year'          => $data['year']
            );

            $turmas = html_escape($this->input->post('class_id'));
            $atividades = html_escape($this->input->post('subject_id'));

            if ((!empty($atividades)) && (!empty($turmas))) {

                foreach ($turmas as $tur) {
                    $data['class_id'] = $tur;
                    $data['time_start'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start;
                    $data['time_end'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end;
                    $data['time_end_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end_min;
                    $data['time_start_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start_min;

                    foreach ($atividades as $ati) {
                        $id_ativ = $ati;
                        $data['subject_id'] = $id_ativ;
                        $this->db->insert('class_routine', $data);
                    }

                    $this->session->set_flashdata('flash_message', 'Dado adicionado com sucesso!');
                }
            } else {
                $this->session->set_flashdata('error_message', 'Todos os campos devem ser preenchidos');
            }

            redirect(site_url('admin/class_routine_add'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['class_id']       = $this->input->post('class_id');

            $data1 = $this->input->post('data');

            $data_at = str_replace("/", "-",  $data1);
            $data['data_atividade'] = date('Y-m-d', strtotime($data_at));

            $data['time_start'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start;
            $data['time_end'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end;
            $data['time_end_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_end_min;
            $data['time_start_min'] = $this->db->get_where('class', array('class_id' => $data['class_id']))->row()->time_start_min;

            /*if($this->input->post('section_id') != '') {
                $data['section_id'] = $this->input->post('section_id');
            }*/

            $data['subject_id']     = $this->input->post('subject_id');

            // $data['day']            = $this->input->post('day');
            $data['year']           = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($data['subject_id'] != '') {
                // checking duplication
                $array = array(
                    'class_id'      => $data['class_id'],
                    'year'          => $data['year']
                );
                //$validation = duplication_of_class_routine_on_edit($array, $param2);

                $validation = 1;

                if ($validation == 1) {
                    $this->db->where('class_routine_id', $param2);
                    $this->db->update('class_routine', $data);
                    $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
                } else {
                    $this->session->set_flashdata('error_message', get_phrase('time_conflicts'));
                }
            } else {
                $this->session->set_flashdata('error_message', get_phrase('subject_is_not_found'));
            }

            redirect(site_url('admin/class_routine_view/' . $data['class_id']), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('class_routine', array(
                'class_routine_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $class_id = $this->db->get_where('class_routine', array('class_routine_id' => $param2))->row()->class_id;
            $this->db->where('class_routine_id', $param2);
            $this->db->delete('class_routine');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/class_routine_view/' . $class_id), 'refresh');
        }
    }

    function class_routine_add()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  = 'class_routine_add';
        $page_data['page_title'] = 'Adicionar Plano de Aula';
        $this->load->view('backend/index', $page_data);
    }

    function class_routine_view($class_id)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  = 'class_routine_view';
        $page_data['class_id']  =   $class_id;
        $page_data['page_title'] = 'Plano de Aula';
        $this->load->view('backend/index', $page_data);
    }

    function relatorio_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/relatorio_frequencia');
    }

    function relatorio_frequencia_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/relatorio_frequencia_print');
    }

    function relatorio_plano_aula_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/relatorio_plano_aula_print');
    }

    function relatorio_plano_aula_print_view_word()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/relatorio_plano_aula_print_word');
    }

    function graficos_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_renda_print');
    }

    function graficos_renda_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_renda_print');
    }

    function graficos_problemas_saude_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_problemas_saude_print');
    }

    function graficos_moradores_domicilio_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_moradores_domicilio_print');
    }

    function graficos_faixa_etaria_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_faixa_etaria_print');
    }

    function graficos_escolas_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_escolas_print');
    }

    function graficos_genero_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_genero_print');
    }

    function graficos_responsavel_sustento_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_responsavel_sustento_print');
    }

    function graficos_situacao_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_situacao_print');
    }

    function graficos_tipo_escola_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_tipo_escola_print');
    }

    function graficos_trabalha_patrocinador_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_trabalha_patrocinador_print');
    }

    function graficos_todos_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/graficos_todos_print');
    }

    function excel()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/excel');
    }

    function excel_beneficiados()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $this->load->view('backend/admin/excel_beneficiados');
    }

    function testefrequencia()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/testefrequencia');
    }

    function relatorio_projeto_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/relatorio_projeto_print');
    }


    function relatorio_projeto_beneficiario_original()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/relatorio_projeto_beneficiario_original');
    }

    function relatorio_convenio_print_view()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        //$page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/relatorio_convenio_print');
    }

    function class_routine_print_view($class_id)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['class_id']   =   $class_id;
        //$page_data['section_id'] =   $section_id;
        $this->load->view('backend/admin/class_routine_print_view', $page_data);
    }

    function get_class_section_subject($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/admin/class_routine_section_subject_selector', $page_data);
    }

    function section_subject_edit($class_id, $class_routine_id)
    {
        $page_data['class_id']          =   $class_id;
        $page_data['class_routine_id']  =   $class_routine_id;
        $this->load->view('backend/admin/class_routine_section_subject_edit', $page_data);
    }

    function manage_attendance()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_attendance';
        $page_data['page_title'] =  'Gerenciar Frequência da Turma';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio';
        $page_data['page_title'] =  'Relatório de Presença';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_frequencia_federal()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_frequencia_federal';
        $page_data['page_title'] =  'Relatório de Frequência Federal';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_frequencia_estadual()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_frequencia_estadual';
        $page_data['page_title'] =  'Relatório de Frequência Estadual';
        $this->load->view('backend/index', $page_data);
    }


    function manage_relatorio_frequencia_convenio()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_frequencia_convenio';
        $page_data['page_title'] =  'Relatório de Frequência Convênio';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_frequencia_2()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_frequencia_2';
        $page_data['page_title'] =  'Relatório de Frequência';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_projetos_federal()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_projetos_federal';
        $page_data['page_title'] =  'Relatório de Projetos Federais';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_projetos_estadual()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_projetos_estadual';
        $page_data['page_title'] =  'Relatório de Projetos Estaduais';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_convenio()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_convenio';
        $page_data['page_title'] =  'Relatório de Convênios';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_plano_aula_federal()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_plano_aula_federal';
        $page_data['page_title'] =  'Relatório de Projetos Federais';
        $this->load->view('backend/index', $page_data);
    }

    function manage_relatorio_plano_aula_estadual()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_plano_aula_estadual';
        $page_data['page_title'] =  'Relatório de Projetos Estaduais';
        $this->load->view('backend/index', $page_data);
    }


    function manage_relatorio_plano_aula_convenio()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_relatorio_plano_aula_convenio';
        $page_data['page_title'] =  'Relatório de Convênios';
        $this->load->view('backend/index', $page_data);
    }

    function manage_graficos()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  =  'manage_graficos';
        $page_data['page_title'] =  'Gráficos';
        $this->load->view('backend/index', $page_data);
    }

    function manage_attendance_view($class_id = '', $section_id = '', $timestamp = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $class_name = $this->db->get_where('class', array(
            'class_id' => $class_id
        ))->row()->name;
        $page_data['class_id'] = $class_id;
        $page_data['timestamp'] = $timestamp;
        $page_data['page_name'] = 'manage_attendance_view';
        $section_name = $this->db->get_where('section', array(
            'section_id' => $section_id
        ))->row()->name;
        $page_data['section_id'] = $section_id;
        $page_data['page_title'] = 'Gerenciar Frequência da Turma ' . $class_name;
        $this->load->view('backend/index', $page_data);
    }

    function get_section($class_id)
    {
        $page_data['class_id'] = $class_id;
        $this->load->view('backend/admin/manage_attendance_section_holder', $page_data);
    }

    function get_section_nucleo($nucleo_id)
    {
        $page_data['nucleo_id'] = $nucleo_id;
        $this->load->view('backend/admin/manage_attendance_section_holder_nucleos', $page_data);
    }

    function attendance_selector()
    {
        $data['class_id']   = $this->input->post('class_id');
        $data['year']       = $this->input->post('year');
        $data['timestamp']  = strtotime($this->input->post('timestamp'));

        $data['data']  = date("d/m/Y", strtotime($this->input->post('timestamp')));
        $data['section_id'] = $this->input->post('section_id');
        $query = $this->db->get_where('attendance', array(
            'class_id' => $data['class_id'],
            //'section_id' => $data['section_id'],
            //'year' => $data['year'],
            'timestamp' => $data['timestamp']
        ));

        //   echo $this->input->post('timestamp');
        //  echo "<br>";
        //  echo $data['data'];


        if ($query->num_rows() < 1) {
            $sql = "SELECT student.*,enroll.class_id,enroll.date_added FROM `enroll` INNER JOIN student ON enroll.student_id = student.student_id WHERE enroll.class_id=" . $data['class_id'] . " AND from_unixtime(`date_added`, '%d/%m/%Y') <= '" . $data['data'] . "' GROUP BY student.student_id order by date_added DESC,student.name";

            //    echo $sql;
            //   echo "<br>";
            $query = $this->db->query($sql);

            $students = $query->result_array();

            foreach ($students as $row) {
              //  echo "<br>";
                $sql = "SELECT enroll.* FROM `enroll` WHERE enroll.class_id<>" . $data['class_id'] . " AND from_unixtime(`date_added`, '%d/%m/%Y') > from_unixtime('" . $row['date_added'] . "', '%d/%m/%Y') AND enroll.student_id=" . $row['student_id'] . " AND  from_unixtime(`date_added`, '%d/%m/%Y') <= '" . $data['data'] . "'";

                //   echo $sql;
                //     echo "<br>";
                $query = $this->db->query($sql);
                if ($query->num_rows() < 1) {
                    $attn_data['class_id']   = $data['class_id'];
                    $attn_data['year']       = $data['year'];
                    $attn_data['timestamp']  = $data['timestamp'];
                    //$attn_data['section_id'] = $data['section_id'];
                    $attn_data['student_id'] = $row['student_id'];
                    $this->db->insert('attendance', $attn_data);
                }
            }
        }
        redirect(site_url('admin/manage_attendance_view/' . $data['class_id'] . '/' . $data['section_id'] . '/' . $data['timestamp']), 'refresh');
    }

    function attendance_update($class_id = '', $section_id = '', $timestamp = '')
    {
        $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
        $active_sms_service = $this->db->get_where('settings', array('type' => 'active_sms_service'))->row()->description;
        $attendance_of_students = $this->db->get_where('attendance', array(
            'class_id' => $class_id, 'timestamp' => $timestamp
        ))->result_array();
        foreach ($attendance_of_students as $row) {
            $attendance_status = $this->input->post('status_' . $row['attendance_id']);
            $this->db->where('attendance_id', $row['attendance_id']);
            $this->db->update('attendance', array('status' => $attendance_status));

            if ($attendance_status == 2) {

                if ($active_sms_service != '' || $active_sms_service != 'disabled') {
                    $student_name   = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;
                    $parent_id      = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->parent_id;
                    $message        = 'Your child' . ' ' . $student_name . 'is absent today.';
                    if ($parent_id != null && $parent_id != 0) {
                        $receiver_phone = $this->db->get_where('parent', array('parent_id' => $parent_id))->row()->phone;
                        if ($receiver_phone != '' || $receiver_phone != null) {
                            //  $this->sms_model->send_sms($message, $receiver_phone);
                        } else {
                            //  $this->session->set_flashdata('error_message', get_phrase('parent_phone_number_is_not_found'));
                        }
                    } else {
                        //  $this->session->set_flashdata('error_message', get_phrase('parent_phone_number_is_not_found'));
                    }
                }
            }
        }
        $this->session->set_flashdata('flash_message', "Frequência atualizada.");
        redirect(site_url('admin/manage_attendance_view/' . $class_id . '/' . $section_id . '/' . $timestamp), 'refresh');
    }

    /****** DAILY ATTENDANCE *****************/
    function manage_attendance2($date = '', $month = '', $year = '', $class_id = '', $section_id = '', $session = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $active_sms_service = $this->db->get_where('settings', array('type' => 'active_sms_service'))->row()->description;
        $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;


        if ($_POST) {
            // Loop all the students of $class_id
            $this->db->where('class_id', $class_id);
            if ($section_id != '') {
                $this->db->where('section_id', $section_id);
            }
            //$session = base64_decode( urldecode( $session ) );
            $this->db->where('year', $session);
            $students = $this->db->get('enroll')->result_array();
            foreach ($students as $row) {
                $attendance_status  =   $this->input->post('status_' . $row['student_id']);

                $this->db->where('student_id', $row['student_id']);
                $this->db->where('date', $date);
                $this->db->where('year', $year);
                $this->db->where('class_id', $row['class_id']);
                if ($row['section_id'] != '' && $row['section_id'] != 0) {
                    $this->db->where('section_id', $row['section_id']);
                }
                $this->db->where('session', $session);

                $this->db->update('attendance', array('status' => $attendance_status));

                if ($attendance_status == 2) {

                    if ($active_sms_service != '' || $active_sms_service != 'disabled') {
                        $student_name   = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;
                        $parent_id      = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->parent_id;
                        $receiver_phone = $this->db->get_where('parent', array('parent_id' => $parent_id))->row()->phone;
                        $message        = 'Your child' . ' ' . $student_name . 'is absent today.';
                        $this->sms_model->send_sms($message, $receiver_phone);
                    }
                }
            }

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/manage_attendance/' . $date . '/' . $month . '/' . $year . '/' . $class_id . '/' . $section_id . '/' . $session), 'refresh');
        }
        $page_data['date']       =    $date;
        $page_data['month']      =    $month;
        $page_data['year']       =    $year;
        $page_data['class_id']   =  $class_id;
        $page_data['section_id'] =  $section_id;
        $page_data['session']    =  $session;

        $page_data['page_name']  =    'manage_attendance';
        $page_data['page_title'] =    get_phrase('manage_daily_attendance');
        $this->load->view('backend/index', $page_data);
    }
    function attendance_selector2()
    {
        //$session = $this->input->post('session');
        //$encoded_session = urlencode( base64_encode( $session ) );
        redirect(site_url('admin/manage_attendance/' . $this->input->post('date') . '/' .
            $this->input->post('month') . '/' .
            $this->input->post('year') . '/' .
            $this->input->post('class_id') . '/' .
            $this->input->post('section_id') . '/' .
            $this->input->post('session')), 'refresh');
    }
    ///////ATTENDANCE REPORT /////
    function attendance_report()
    {
        $page_data['month']        = date('m');
        $page_data['page_name']    = 'attendance_report';
        $page_data['page_title']   = 'Relatório de Frequência';
        $this->load->view('backend/index', $page_data);
    }
    function attendance_report_view($class_id = '', $section_id = '', $month = '', $sessional_year = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $class_name                     = $this->db->get_where('class', array('class_id' => $class_id))->row()->name;
        $section_name                   = $this->db->get_where('section', array('section_id' => $section_id))->row()->name;
        $page_data['class_id']          = $class_id;
        $page_data['section_id']        = $section_id;
        $page_data['month']             = $month;
        $page_data['sessional_year']    = $sessional_year;
        $page_data['page_name']         = 'attendance_report_view';
        $page_data['page_title']        =   'Relatório de Frequência da Turma ' . $class_name;
        $this->load->view('backend/index', $page_data);
    }
    function attendance_report_print_view($class_id = '', $section_id = '', $month = '', $sessional_year = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(base_url(), 'refresh');

        $page_data['class_id']          = $class_id;
        $page_data['section_id']        = $section_id;
        $page_data['month']             = $month;
        $page_data['sessional_year']    = $sessional_year;
        $this->load->view('backend/admin/attendance_report_print_view', $page_data);
    }

    function attendance_report_selector()
    {
        if ($this->input->post('class_id') == '' || $this->input->post('sessional_year') == '') {
            $this->session->set_flashdata('error_message', get_phrase('please_make_sure_class_and_sessional_year_are_selected'));
            redirect(site_url('admin/attendance_report'), 'refresh');
        }
        $data['class_id']       = $this->input->post('class_id');
        $data['section_id']     = $this->input->post('section_id');
        $data['month']          = $this->input->post('month');
        $data['sessional_year'] = $this->input->post('sessional_year');
        redirect(site_url('admin/attendance_report_view/' . $data['class_id'] . '/' . $data['section_id'] . '/' . $data['month'] . '/' . $data['sessional_year']), 'refresh');
    }

    /******MANAGE BILLING / INVOICES WITH STATUS*****/
    function invoice($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['student_id']         = $this->input->post('student_id');
            $data['title']              = html_escape($this->input->post('title'));
            $data['amount']             = html_escape($this->input->post('amount'));
            $data['amount_paid']        = html_escape($this->input->post('amount_paid'));
            $data['due']                = $data['amount'] - $data['amount_paid'];
            $data['status']             = html_escape($this->input->post('status'));
            $data['creation_timestamp'] = strtotime($this->input->post('date'));
            $data['year']               = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($this->input->post('description') != null) {
                $data['description']    = $this->input->post('description');
            }

            $this->db->insert('invoice', $data);
            $invoice_id = $this->db->insert_id();

            $data2['invoice_id']        =   $invoice_id;
            $data2['student_id']        =   $this->input->post('student_id');
            $data2['title']             =   html_escape($this->input->post('title'));
            $data2['payment_type']      =  'income';
            $data2['method']            =   $this->input->post('method');
            $data2['amount']            =   html_escape($this->input->post('amount_paid'));
            $data2['timestamp']         =   strtotime($this->input->post('date'));
            $data2['year']              =  $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($this->input->post('description') != null) {
                $data2['description']    = html_escape($this->input->post('description'));
            }
            $this->db->insert('payment', $data2);

            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/student_payment'), 'refresh');
        }

        if ($param1 == 'create_mass_invoice') {
            foreach ($this->input->post('student_id') as $id) {

                $data['student_id']         = $id;
                $data['title']              = html_escape($this->input->post('title'));
                $data['description']        = html_escape($this->input->post('description'));
                $data['amount']             = html_escape($this->input->post('amount'));
                $data['amount_paid']        = html_escape($this->input->post('amount_paid'));
                $data['due']                = $data['amount'] - $data['amount_paid'];
                $data['status']             = $this->input->post('status');
                $data['creation_timestamp'] = strtotime($this->input->post('date'));
                $data['year']               = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

                $this->db->insert('invoice', $data);
                $invoice_id = $this->db->insert_id();

                $data2['invoice_id']        =   $invoice_id;
                $data2['student_id']        =   $id;
                $data2['title']             =   html_escape($this->input->post('title'));
                $data2['description']       =   html_escape($this->input->post('description'));
                $data2['payment_type']      =  'income';
                $data2['method']            =   $this->input->post('method');
                $data2['amount']            =   html_escape($this->input->post('amount_paid'));
                $data2['timestamp']         =   strtotime($this->input->post('date'));
                $data2['year']               =   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

                $this->db->insert('payment', $data2);
            }

            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/student_payment'), 'refresh');
        }

        if ($param1 == 'do_update') {
            $data['student_id']         = $this->input->post('student_id');
            $data['title']              = html_escape($this->input->post('title'));
            $data['description']        = html_escape($this->input->post('description'));
            $data['amount']             = html_escape($this->input->post('amount'));
            $data['status']             = $this->input->post('status');
            $data['creation_timestamp'] = strtotime($this->input->post('date'));

            $this->db->where('invoice_id', $param2);
            $this->db->update('invoice', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/income'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('invoice', array(
                'invoice_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'take_payment') {
            $data['invoice_id']   =   $this->input->post('invoice_id');
            $data['student_id']   =   $this->input->post('student_id');
            $data['title']        =   html_escape($this->input->post('title'));
            $data['description']  =   html_escape($this->input->post('description'));
            $data['payment_type'] =   'income';
            $data['method']       =   $this->input->post('method');
            $data['amount']       =   html_escape($this->input->post('amount'));
            $data['timestamp']    =   strtotime($this->input->post('timestamp'));
            $data['year']         =   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            $this->db->insert('payment', $data);

            $status['status']   =   $this->input->post('status');
            $this->db->where('invoice_id', $param2);
            $this->db->update('invoice', array('status' => $status['status']));

            $data2['amount_paid']   =   html_escape($this->input->post('amount'));
            $data2['status']        =   $this->input->post('status');
            $this->db->where('invoice_id', $param2);
            $this->db->set('amount_paid', 'amount_paid + ' . $data2['amount_paid'], FALSE);
            $this->db->set('due', 'due - ' . $data2['amount_paid'], FALSE);
            $this->db->update('invoice');

            $this->session->set_flashdata('flash_message', get_phrase('payment_successfull'));
            redirect(site_url('admin/income'), 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('invoice_id', $param2);
            $this->db->delete('invoice');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/income'), 'refresh');
        }
        $page_data['page_name']  = 'invoice';
        $page_data['page_title'] = get_phrase('manage_invoice/payment');
        $this->db->order_by('creation_timestamp', 'desc');
        $page_data['invoices'] = $this->db->get('invoice')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function income($param1 = 'invoices', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name'] = 'income';
        $page_data['inner'] = $param1;
        $page_data['page_title'] = get_phrase('student_payments');
        $this->load->view('backend/index', $page_data);
    }

    function get_invoices()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'invoice_id',
            1 => 'student',
            2 => 'title',
            3 => 'total',
            4 => 'paid',
            5 => 'status',
            6 => 'date',
            7 => 'options',
            8 => 'invoice_id'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_invoices_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $invoices = $this->ajaxload->all_invoices($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $invoices =  $this->ajaxload->invoice_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->invoice_search_count($search);
        }

        $data = array();
        if (!empty($invoices)) {
            foreach ($invoices as $row) {

                if ($row->due == 0) {
                    $status = '<button class="btn btn-success btn-xs">' . get_phrase('paid') . '</button>';
                    $payment_option = '';
                } else {
                    $status = '<button class="btn btn-danger btn-xs">' . get_phrase('unpaid') . '</button>';
                    $payment_option = '<li><a href="#" onclick="invoice_pay_modal(' . $row->invoice_id . ')"><i class="entypo-bookmarks"></i>&nbsp;' . get_phrase('take_payment') . '</a></li><li class="divider"></li>';
                }


                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu">' . $payment_option . '<li><a href="#" onclick="invoice_view_modal(' . $row->invoice_id . ')"><i class="entypo-credit-card"></i>&nbsp;' . get_phrase('view_invoice') . '</a></li><li class="divider"></li><li><a href="#" onclick="invoice_edit_modal(' . $row->invoice_id . ')"><i class="entypo-pencil"></i>&nbsp;' . get_phrase('edit') . '</a></li><li class="divider"></li><li><a href="#" onclick="invoice_delete_confirm(' . $row->invoice_id . ')"><i class="entypo-trash"></i>&nbsp;' . get_phrase('delete') . '</a></li></ul></div>';

                $nestedData['invoice_id'] = $row->invoice_id;
                $nestedData['student'] = $this->crud_model->get_type_name_by_id('student', $row->student_id);
                $nestedData['title'] = $row->title;
                $nestedData['total'] = $row->amount;
                $nestedData['paid'] = $row->amount_paid;
                $nestedData['status'] = $status;
                $nestedData['date'] = date('d M,Y', $row->creation_timestamp);
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    function get_payments()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'payment_id',
            1 => 'title',
            2 => 'description',
            3 => 'method',
            4 => 'amount',
            5 => 'date',
            6 => 'options',
            7 => 'payment_id'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_payments_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $payments = $this->ajaxload->all_payments($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $payments =  $this->ajaxload->payment_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->payment_search_count($search);
        }

        $data = array();
        if (!empty($payments)) {
            foreach ($payments as $row) {

                if ($row->method == 1)
                    $method = get_phrase('cash');
                else if ($row->method == 2)
                    $method = get_phrase('cheque');
                else if ($row->method == 3)
                    $method = get_phrase('card');
                else if ($row->method == 'Paypal')
                    $method = 'Paypal';
                else
                    $method = 'Stripe';


                $options = '<a href="#" onclick="invoice_view_modal(' . $row->invoice_id . ')"><i class="entypo-credit-card"></i>&nbsp;' . get_phrase('view_invoice') . '</a>';

                $nestedData['payment_id'] = $row->payment_id;
                $nestedData['title'] = $row->title;
                $nestedData['description'] = $row->description;
                $nestedData['method'] = $method;
                $nestedData['amount'] = $row->amount;
                $nestedData['date'] = date('d M,Y', $row->timestamp);
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    function student_payment($param1 = '', $param2 = '', $param3 = '')
    {

        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $page_data['page_name']  = 'student_payment';
        $page_data['page_title'] = get_phrase('create_student_payment');
        $this->load->view('backend/index', $page_data);
    }

    function expense($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['title']               =   html_escape($this->input->post('title'));
            $data['expense_category_id'] =   $this->input->post('expense_category_id');
            $data['payment_type']        =   'expense';
            $data['method']              =   $this->input->post('method');
            $data['amount']              =   html_escape($this->input->post('amount'));
            $data['timestamp']           =   strtotime($this->input->post('timestamp'));
            $data['year']                =   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($this->input->post('description') != null) {
                $data['description']     =   html_escape($this->input->post('description'));
            }
            $this->db->insert('payment', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/expense'), 'refresh');
        }

        if ($param1 == 'edit') {
            $data['title']               =   html_escape($this->input->post('title'));
            $data['expense_category_id'] =   $this->input->post('expense_category_id');
            $data['payment_type']        =   'expense';
            $data['method']              =   $this->input->post('method');
            $data['amount']              =   html_escape($this->input->post('amount'));
            $data['timestamp']           =   strtotime($this->input->post('timestamp'));
            $data['year']                =   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
            if ($this->input->post('description') != null) {
                $data['description']     =   html_escape($this->input->post('description'));
            } else {
                $data['description']     =   null;
            }
            $this->db->where('payment_id', $param2);
            $this->db->update('payment', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/expense'), 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('payment_id', $param2);
            $this->db->delete('payment');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/expense'), 'refresh');
        }

        $page_data['page_name']  = 'expense';
        $page_data['page_title'] = get_phrase('expenses');
        $this->load->view('backend/index', $page_data);
    }

    function get_expenses()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'payment_id',
            1 => 'title',
            2 => 'category',
            3 => 'method',
            4 => 'amount',
            5 => 'date',
            6 => 'options',
            7 => 'payment_id'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_expenses_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $expenses = $this->ajaxload->all_expenses($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $expenses =  $this->ajaxload->expense_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->expense_search_count($search);
        }

        $data = array();
        if (!empty($expenses)) {
            foreach ($expenses as $row) {
                $category = $this->db->get_where('expense_category', array('expense_category_id' => $row->expense_category_id))->row()->name;
                if ($row->method == 1)
                    $method = get_phrase('cash');
                else if ($row->method == 2)
                    $method = get_phrase('cheque');
                else
                    $method = get_phrase('card');
                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="expense_edit_modal(' . $row->payment_id . ')"><i class="entypo-pencil"></i>&nbsp;' . get_phrase('edit') . '</a></li><li class="divider"></li><li><a href="#" onclick="expense_delete_confirm(' . $row->payment_id . ')"><i class="entypo-trash"></i>&nbsp;' . get_phrase('delete') . '</a></li></ul></div>';

                $nestedData['payment_id'] = $row->payment_id;
                $nestedData['title'] = $row->title;
                $nestedData['category'] = $category;
                $nestedData['method'] = $method;
                $nestedData['amount'] = $row->amount;
                $nestedData['date'] = date('d M,Y', $row->timestamp);
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    function expense_category($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']   =   html_escape($this->input->post('name'));
            $this->db->insert('expense_category', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/expense_category'), 'refresh');
        }
        if ($param1 == 'edit') {
            $data['name']   =   html_escape($this->input->post('name'));
            $this->db->where('expense_category_id', $param2);
            $this->db->update('expense_category', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/expense_category'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->db->where('expense_category_id', $param2);
            $this->db->delete('expense_category');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/expense_category'), 'refresh');
        }

        $page_data['page_name']  = 'expense_category';
        $page_data['page_title'] = get_phrase('expense_category');
        $this->load->view('backend/index', $page_data);
    }

    /**********MANAGE LIBRARY / BOOKS********************/
    function book($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']        = html_escape($this->input->post('name'));
            $data['class_id']    = $this->input->post('class_id');
            if ($this->input->post('description') != null) {
                $data['description'] = html_escape($this->input->post('description'));
            }
            if ($this->input->post('price') != null) {
                $data['price'] = html_escape($this->input->post('price'));
            }
            if ($this->input->post('author') != null) {
                $data['author'] = html_escape($this->input->post('author'));
            }
            if (!empty($_FILES["file_name"]["name"])) {
                $data['file_name'] = $_FILES["file_name"]["name"];
            }

            $this->db->insert('book', $data);

            if (!empty($_FILES["file_name"]["name"])) {
                move_uploaded_file($_FILES["file_name"]["tmp_name"], "uploads/document/" . $_FILES["file_name"]["name"]);
            }

            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/book'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']        = html_escape($this->input->post('name'));
            $data['class_id']    = $this->input->post('class_id');
            if ($this->input->post('description') != null) {
                $data['description'] = html_escape($this->input->post('description'));
            } else {
                $data['description'] = null;
            }
            if ($this->input->post('price') != null) {
                $data['price'] = html_escape($this->input->post('price'));
            } else {
                $data['price'] = null;
            }
            if ($this->input->post('author') != null) {
                $data['author'] = html_escape($this->input->post('author'));
            } else {
                $data['author'] = null;
            }

            if (!empty($_FILES["file_name"]["name"])) {
                $data['file_name'] = $_FILES["file_name"]["name"];
            }

            $this->db->where('book_id', $param2);
            $this->db->update('book', $data);

            if (!empty($_FILES["file_name"]["name"])) {
                move_uploaded_file($_FILES["file_name"]["tmp_name"], "uploads/document/" . $_FILES["file_name"]["name"]);
            }

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/book'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('book', array(
                'book_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('book_id', $param2);
            $this->db->delete('book');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/book'), 'refresh');
        }
        $page_data['page_name']  = 'book';
        $page_data['page_title'] = get_phrase('manage_library_books');
        $this->load->view('backend/index', $page_data);
    }

    function get_books()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $columns = array(
            0 => 'book_id',
            1 => 'name',
            2 => 'author',
            3 => 'description',
            4 => 'price',
            5 => 'class',
            6 => 'download',
            7 => 'options',
            8 => 'book_id'
        );

        $limit = $this->input->post('length');
        $start = $this->input->post('start');
        $order = $columns[$this->input->post('order')[0]['column']];
        $dir   = $this->input->post('order')[0]['dir'];

        $totalData = $this->ajaxload->all_books_count();
        $totalFiltered = $totalData;

        if (empty($this->input->post('search')['value'])) {
            $books = $this->ajaxload->all_books($limit, $start, $order, $dir);
        } else {
            $search = $this->input->post('search')['value'];
            $books =  $this->ajaxload->book_search($limit, $start, $search, $order, $dir);
            $totalFiltered = $this->ajaxload->book_search_count($search);
        }

        $data = array();
        if (!empty($books)) {
            foreach ($books as $row) {
                if ($row->file_name == null)
                    $download = '';
                else
                    $download = '<a href="' . site_url("uploads/document/$row->file_name") . '" class="btn btn-blue btn-icon icon-left"><i class="entypo-download"></i>' . get_phrase('download') . '</a>';

                $options = '<div class="btn-group"><button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                    Ações <span class="caret"></span></button><ul class="dropdown-menu dropdown-default pull-right" role="menu"><li><a href="#" onclick="book_edit_modal(' . $row->book_id . ')"><i class="entypo-pencil"></i>&nbsp;' . get_phrase('edit') . '</a></li><li class="divider"></li><li><a href="#" onclick="book_delete_confirm(' . $row->book_id . ')"><i class="entypo-trash"></i>&nbsp;' . get_phrase('delete') . '</a></li></ul></div>';

                $nestedData['book_id'] = $row->book_id;
                $nestedData['name'] = $row->name;
                $nestedData['author'] = $row->author;
                $nestedData['description'] = $row->description;
                $nestedData['price'] = $row->price;
                $nestedData['class'] = $this->db->get_where('class', array('class_id' => $row->class_id))->row()->name;
                $nestedData['download'] = $download;
                $nestedData['options'] = $options;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);
    }

    /**********MANAGE TRANSPORT / VEHICLES / ROUTES********************/
    function transport($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect('login', 'refresh');
        if ($param1 == 'create') {
            $data['route_name']        = html_escape($this->input->post('route_name'));
            $data['number_of_vehicle'] = html_escape($this->input->post('number_of_vehicle'));
            if ($this->input->post('description') != null) {
                $data['description']    = html_escape($this->input->post('description'));
            }
            if ($this->input->post('route_fare') != null) {
                $data['route_fare']     = html_escape($this->input->post('route_fare'));
            }

            $this->db->insert('transport', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/transport'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['route_name']        = html_escape($this->input->post('route_name'));
            $data['number_of_vehicle'] = html_escape($this->input->post('number_of_vehicle'));
            if ($this->input->post('description') != null) {
                $data['description']    = html_escape($this->input->post('description'));
            } else {
                $data['description'] = null;
            }
            if ($this->input->post('route_fare') != null) {
                $data['route_fare']   = html_escape($this->input->post('route_fare'));
            } else {
                $data['route_fare']  = null;
            }

            $this->db->where('transport_id', $param2);
            $this->db->update('transport', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/transport'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('transport', array(
                'transport_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('transport_id', $param2);
            $this->db->delete('transport');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/transport'), 'refresh');
        }
        $page_data['transports'] = $this->db->get('transport')->result_array();
        $page_data['page_name']  = 'transport';
        $page_data['page_title'] = get_phrase('manage_transport');
        $this->load->view('backend/index', $page_data);
    }

    /**********MANAGE DORMITORY / HOSTELS / ROOMS ********************/
    function dormitory($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'create') {
            $data['name']           = html_escape($this->input->post('name'));
            $data['number_of_room'] = html_escape($this->input->post('number_of_room'));
            if ($this->input->post('description') != null) {
                $data['description']    = html_escape($this->input->post('description'));
            }

            $this->db->insert('dormitory', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/dormitory'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $data['name']           = html_escape($this->input->post('name'));
            $data['number_of_room'] = html_escape($this->input->post('number_of_room'));
            if ($this->input->post('description') != null) {
                $data['description']    = html_escape($this->input->post('description'));
            } else {
                $data['description'] = null;
            }
            $this->db->where('dormitory_id', $param2);
            $this->db->update('dormitory', $data);
            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/dormitory'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('dormitory', array(
                'dormitory_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('dormitory_id', $param2);
            $this->db->delete('dormitory');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/dormitory'), 'refresh');
        }
        $page_data['dormitories'] = $this->db->get('dormitory')->result_array();
        $page_data['page_name']   = 'dormitory';
        $page_data['page_title']  = get_phrase('manage_dormitory');
        $this->load->view('backend/index', $page_data);
    }

    /***MANAGE EVENT / NOTICEBOARD, WILL BE SEEN BY ALL ACCOUNTS DASHBOARD**/
    function noticeboard($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['notice_title']     = html_escape($this->input->post('notice_title'));
            $data['notice']           = html_escape($this->input->post('notice'));
            $data['id_projeto']     = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']     = html_escape($this->input->post('id_nucleo'));
            $data['show_on_website']  = $this->input->post('show_on_website');
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            if ($_FILES['image']['name'] != '') {
                $data['image']  = $_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/frontend/noticeboard/' . $_FILES['image']['name']);
            }
            $this->db->insert('noticeboard', $data);

            $check_sms_send = $this->input->post('check_sms');

            if ($check_sms_send == 1) {
                // sms sending configurations

                $parents  = $this->db->get('parent')->result_array();
                $students = $this->db->get('student')->result_array();
                $teachers = $this->db->get('teacher')->result_array();
                $date     = $this->input->post('create_timestamp');
                $message  = $data['notice_title'] . ' ';
                $message .= get_phrase('on') . ' ' . $date;
                foreach ($parents as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message, $reciever_phone);
                }
                foreach ($students as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message, $reciever_phone);
                }
                foreach ($teachers as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message, $reciever_phone);
                }
            }

            $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
            redirect(site_url('admin/noticeboard'), 'refresh');
        }
        if ($param1 == 'do_update') {
            $image = $this->db->get_where('noticeboard', array('notice_id' => $param2))->row()->image;
            $data['notice_title']     = html_escape($this->input->post('notice_title'));
            $data['notice']           = html_escape($this->input->post('notice'));
            $data['id_projeto']     = html_escape($this->input->post('id_projeto'));
            $data['id_nucleo']     = html_escape($this->input->post('id_nucleo'));
            $data['show_on_website']  = html_escape($this->input->post('show_on_website'));
            $data['create_timestamp'] = strtotime($this->input->post('create_timestamp'));
            if ($_FILES['image']['name'] != '') {
                $data['image']  = $_FILES['image']['name'];
                move_uploaded_file($_FILES['image']['tmp_name'], 'uploads/frontend/noticeboard/' . $_FILES['image']['name']);
            } else {
                $data['image']  = $image;
            }

            $this->db->where('notice_id', $param2);
            $this->db->update('noticeboard', $data);

            $check_sms_send = $this->input->post('check_sms');

            if ($check_sms_send == 1) {
                // sms sending configurations

                $parents  = $this->db->get('parent')->result_array();
                $students = $this->db->get('student')->result_array();
                $teachers = $this->db->get('teacher')->result_array();
                $date     = $this->input->post('create_timestamp');
                $message  = $data['notice_title'] . ' ';
                $message .= get_phrase('on') . ' ' . $date;
                foreach ($parents as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message, $reciever_phone);
                }
                foreach ($students as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message, $reciever_phone);
                }
                foreach ($teachers as $row) {
                    $reciever_phone = $row['phone'];
                    $this->sms_model->send_sms($message, $reciever_phone);
                }
            }

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/noticeboard'), 'refresh');
        } else if ($param1 == 'edit') {
            $page_data['edit_data'] = $this->db->get_where('noticeboard', array(
                'notice_id' => $param2
            ))->result_array();
        }
        if ($param1 == 'delete') {
            $this->db->where('notice_id', $param2);
            $this->db->delete('noticeboard');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/noticeboard'), 'refresh');
        }
        if ($param1 == 'mark_as_archive') {
            $this->db->where('notice_id', $param2);
            $this->db->update('noticeboard', array('status' => 0));
            redirect(site_url('admin/noticeboard'), 'refresh');
        }

        if ($param1 == 'remove_from_archived') {
            $this->db->where('notice_id', $param2);
            $this->db->update('noticeboard', array('status' => 1));
            redirect(site_url('admin/noticeboard'), 'refresh');
        }
        $page_data['page_name']  = 'noticeboard';
        $page_data['page_title'] = 'Gerenciar Quadro de Eventos';
        $this->load->view('backend/index', $page_data);
    }

    function noticeboard_edit($notice_id)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['page_name']  = 'noticeboard_edit';
        $page_data['notice_id'] = $notice_id;
        $page_data['page_title'] = "Editar Evento";
        $this->load->view('backend/index', $page_data);
    }

    function reload_noticeboard()
    {
        $this->load->view('backend/admin/noticeboard');
    }

    /* private messaging */
    function message($param1 = 'message_home', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $max_size = 2097152;
        if ($param1 == 'send_new') {
            if (!file_exists('uploads/private_messaging_attached_file/')) {
                $oldmask = umask(0);  // helpful when used in linux server
                mkdir('uploads/private_messaging_attached_file/', 0777);
            }
            if ($_FILES['attached_file_on_messaging']['name'] != "") {
                if ($_FILES['attached_file_on_messaging']['size'] > $max_size) {
                    $this->session->set_flashdata('error_message', get_phrase('file_size_can_not_be_larger_that_2_Megabyte'));
                    redirect(site_url('admin/message/message_new'), 'refresh');
                } else {
                    $file_path = 'uploads/private_messaging_attached_file/' . $_FILES['attached_file_on_messaging']['name'];
                    move_uploaded_file($_FILES['attached_file_on_messaging']['tmp_name'], $file_path);
                }
            }

            $message_thread_code = $this->crud_model->send_new_private_message();
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(site_url('admin/message/message_read/' . $message_thread_code), 'refresh');
        }

        if ($param1 == 'send_reply') {

            if (!file_exists('uploads/private_messaging_attached_file/')) {
                $oldmask = umask(0);  // helpful when used in linux server
                mkdir('uploads/private_messaging_attached_file/', 0777);
            }
            if ($_FILES['attached_file_on_messaging']['name'] != "") {
                if ($_FILES['attached_file_on_messaging']['size'] > $max_size) {
                    $this->session->set_flashdata('error_message', get_phrase('file_size_can_not_be_larger_that_2_Megabyte'));
                    redirect(site_url('admin/message/message_read/' . $param2), 'refresh');
                } else {
                    $file_path = 'uploads/private_messaging_attached_file/' . $_FILES['attached_file_on_messaging']['name'];
                    move_uploaded_file($_FILES['attached_file_on_messaging']['tmp_name'], $file_path);
                }
            }

            $this->crud_model->send_reply_message($param2);  //$param2 = message_thread_code
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(site_url('admin/message/message_read/' . $param2), 'refresh');
        }

        if ($param1 == 'message_read') {
            $page_data['current_message_thread_code'] = $param2;  // $param2 = message_thread_code
            $this->crud_model->mark_thread_messages_read($param2);
        }

        $page_data['message_inner_page_name']   = $param1;
        $page_data['page_name']                 = 'message';
        $page_data['page_title']                = get_phrase('private_messaging');
        $this->load->view('backend/index', $page_data);
    }

    function group_message($param1 = "group_message_home", $param2 = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $max_size = 2097152;
        if ($param1 == "create_group") {
            $this->crud_model->create_group();
        } elseif ($param1 == "edit_group") {
            $this->crud_model->update_group($param2);
        } elseif ($param1 == 'group_message_read') {
            $page_data['current_message_thread_code'] = $param2;
        } else if ($param1 == 'send_reply') {
            if (!file_exists('uploads/group_messaging_attached_file/')) {
                $oldmask = umask(0);  // helpful when used in linux server
                mkdir('uploads/group_messaging_attached_file/', 0777);
            }
            if ($_FILES['attached_file_on_messaging']['name'] != "") {
                if ($_FILES['attached_file_on_messaging']['size'] > $max_size) {
                    $this->session->set_flashdata('error_message', get_phrase('file_size_can_not_be_larger_that_2_Megabyte'));
                    redirect(site_url('admin/group_message/group_message_read/' . $param2), 'refresh');
                } else {
                    $file_path = 'uploads/group_messaging_attached_file/' . $_FILES['attached_file_on_messaging']['name'];
                    move_uploaded_file($_FILES['attached_file_on_messaging']['tmp_name'], $file_path);
                }
            }

            $this->crud_model->send_reply_group_message($param2);  //$param2 = message_thread_code
            $this->session->set_flashdata('flash_message', get_phrase('message_sent!'));
            redirect(site_url('admin/group_message/group_message_read/' . $param2), 'refresh');
        }
        $page_data['message_inner_page_name']   = $param1;
        $page_data['page_name']                 = 'group_message';
        $page_data['page_title']                = get_phrase('group_messaging');
        $this->load->view('backend/index', $page_data);
    }
    /*****SITE/SYSTEM SETTINGS*********/
    function system_settings($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'do_update') {

            if (isset($_POST['disable_frontend'])) {
                $data['description'] = 1;
                $this->db->where('type', 'disable_frontend');
                $this->db->update('settings', $data);
            } else {
                $data['description'] = 0;
                $this->db->where('type', 'disable_frontend');
                $this->db->update('settings', $data);
            }

            $this->update_default_controller();

            $data['description'] = html_escape($this->input->post('system_name'));
            $this->db->where('type', 'system_name');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('system_title'));
            $this->db->where('type', 'system_title');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('address'));
            $this->db->where('type', 'address');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('phone'));
            $this->db->where('type', 'phone');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('paypal_email'));
            $this->db->where('type', 'paypal_email');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('currency'));
            $this->db->where('type', 'currency');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('system_email'));
            $this->db->where('type', 'system_email');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('system_name'));
            $this->db->where('type', 'system_name');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('language'));
            $this->db->where('type', 'language');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('text_align'));
            $this->db->where('type', 'text_align');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('running_year'));
            $this->db->where('type', 'running_year');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('purchase_code'));
            $this->db->where('type', 'purchase_code');
            $this->db->update('settings', $data);

            move_uploaded_file($_FILES['logo_federal']['tmp_name'], 'uploads/logos/federal.jpg');
            move_uploaded_file($_FILES['logo_estadual']['tmp_name'], 'uploads/logos/estadual.jpg');

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/system_settings'), 'refresh');
        }
        if ($param1 == 'upload_logo') {
            move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/logo.png');
            $this->session->set_flashdata('flash_message', get_phrase('settings_updated'));
            redirect(site_url('admin/system_settings'), 'refresh');
        }
        if ($param1 == 'change_skin') {
            $data['description'] = $param2;
            $this->db->where('type', 'skin_colour');
            $this->db->update('settings', $data);
            $this->session->set_flashdata('flash_message', get_phrase('theme_selected'));
            redirect(site_url('admin/system_settings'), 'refresh');
        }
        $page_data['languages']  = $this->get_all_languages();
        $page_data['page_name']  = 'system_settings';
        $page_data['page_title'] = 'Configurações do Sistema';
        $page_data['settings']   = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    

    // update default controller
    function update_default_controller()
    {
        $status = $this->db->get_where('settings', array('type' => 'disable_frontend'))->row()->description;
        if ($status == 1) {
            $default_controller          = 'login';
            $previous_default_controller = 'home';
        } else {
            $default_controller          = 'home';
            $previous_default_controller = 'login';
        }
        // write routes.php
        $data = file_get_contents('./application/config/routes.php');
        $data = str_replace($previous_default_controller,    $default_controller,    $data);
        file_put_contents('./application/config/routes.php', $data);
    }

    //Payment settings
    function payment_settings($param1 = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'update_stripe_keys') {
            $this->crud_model->update_stripe_keys();
            $this->session->set_flashdata('flash_message', get_phrase('payment_settings_updated'));
            redirect(site_url('admin/payment_settings'), 'refresh');
        }

        if ($param1 == 'update_paypal_keys') {
            $this->crud_model->update_paypal_keys();
            $this->session->set_flashdata('flash_message', get_phrase('payment_settings_updated'));
            redirect(site_url('admin/payment_settings'), 'refresh');
        }
        if ($param1 == 'update_payumoney_keys') {
            $this->crud_model->update_payumoney_keys();
            $this->session->set_flashdata('flash_message', get_phrase('payment_settings_updated'));
            redirect(site_url('admin/payment_settings'), 'refresh');
        }
        $page_data['page_name']  = 'payment_settings';
        $page_data['page_title'] = get_phrase('payment_settings');
        $page_data['settings']   = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function smtp_settings($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1) {
            redirect(site_url('login'), 'refresh');
        }

        if ($param1 == 'update') {
            $this->crud_model->update_smtp_settings();
            $this->session->set_flashdata('flash_message', 'SMTP' . ' ' . get_phrase('settings_updated'));
            redirect(site_url('admin/smtp_settings'), 'refresh');
        }

        $page_data['page_name']  = 'smtp_settings';
        $page_data['page_title'] = 'SMTP' . ' ' . get_phrase('settings');
        $page_data['settings']   = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    // FRONTEND
    function frontend_pages($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1) {
            redirect(site_url('login'), 'refresh');
        }
        if ($param1 == 'events') {
            $page_data['page_content']  = 'frontend_events';
        }
        if ($param1 == 'gallery') {
            $page_data['page_content']  = 'frontend_gallery';
        }
        if ($param1 == 'privacy_policy') {
            $page_data['page_content']  = 'frontend_privacy_policy';
        }
        if ($param1 == 'about_us') {
            $page_data['page_content']  = 'frontend_about_us';
        }
        if ($param1 == 'terms_conditions') {
            $page_data['page_content']  = 'frontend_terms_conditions';
        }
        if ($param1 == 'homepage_slider') {
            $page_data['page_content']  = 'frontend_slider';
        }
        if ($param1 == '' || $param1 == 'general') {
            $page_data['page_content']  = 'frontend_general_settings';
        }
        if ($param1 == 'gallery_image') {
            $page_data['page_content']  = 'frontend_gallery_image';
            $page_data['gallery_id']  = $param2;
        }
        $page_data['page_name'] = 'frontend_pages';
        $page_data['page_title']  = get_phrase('pages');
        $this->load->view('backend/index', $page_data);
    }

    function frontend_events($param1 = '', $param2 = '')
    {
        if ($param1 == 'add_event') {
            $this->frontend_model->add_event();
            $this->session->set_flashdata('flash_message', get_phrase('event_added_successfully'));
            redirect(site_url('admin/frontend_pages/events'), 'refresh');
        }
        if ($param1 == 'edit_event') {
            $this->frontend_model->edit_event($param2);
            $this->session->set_flashdata('flash_message', get_phrase('event_updated_successfully'));
            redirect(site_url('admin/frontend_pages/events'), 'refresh');
        }
        if ($param1 == 'delete') {
            $this->frontend_model->delete_event($param2);
            $this->session->set_flashdata('flash_message', get_phrase('event_deleted'));
            redirect(site_url('admin/frontend_pages/events'), 'refresh');
        }
    }

    function frontend_gallery($param1 = '', $param2 = '', $param3 = '')
    {
        if ($param1 == 'add_gallery') {
            $this->frontend_model->add_gallery();
            $this->session->set_flashdata('flash_message', get_phrase('gallery_added_successfully'));
            redirect(site_url('admin/frontend_pages/gallery'), 'refresh');
        }
        if ($param1 == 'edit_gallery') {
            $this->frontend_model->edit_gallery($param2);
            $this->session->set_flashdata('flash_message', get_phrase('gallery_updated_successfully'));
            redirect(site_url('admin/frontend_pages/gallery'), 'refresh');
        }
        if ($param1 == 'upload_images') {
            $this->frontend_model->add_gallery_images($param2);
            $this->session->set_flashdata('flash_message', get_phrase('images_uploaded'));
            redirect(site_url('admin/frontend_pages/gallery_image/' . $param2), 'refresh');
        }
        if ($param1 == 'delete_image') {
            $this->frontend_model->delete_gallery_image($param2);
            $this->session->set_flashdata('flash_message', get_phrase('images_deleted'));
            redirect(site_url('admin/frontend_pages/gallery_image/' . $param3), 'refresh');
        }
    }

    function frontend_news($param1 = '', $param2 = '')
    {
        if ($param1 == 'add_news') {
            $this->frontend_model->add_news();
            $this->session->set_flashdata('flash_message', get_phrase('news_added_successfully'));
            redirect(site_url('admin/frontend_pages/news'), 'refresh');
        }
        if ($param1 == 'edit_news') {
        }
        if ($param1 == 'delete') {
            $this->frontend_model->delete_news($param2);
            $this->session->set_flashdata('flash_message', get_phrase('news_was_deleted'));
            redirect(site_url('admin/frontend_pages/news'), 'refresh');
        }
    }

    function frontend_settings($task)
    {
        if ($task == 'update_terms_conditions') {
            $this->frontend_model->update_terms_conditions();
            $this->session->set_flashdata('flash_message', get_phrase('terms_updated'));
            redirect(site_url('admin/frontend_pages/terms_conditions'), 'refresh');
        }
        if ($task == 'update_about_us') {
            $this->frontend_model->update_about_us();
            $this->session->set_flashdata('flash_message', get_phrase('about_us_updated'));
            redirect(site_url('admin/frontend_pages/about_us'), 'refresh');
        }
        if ($task == 'update_privacy_policy') {
            $this->frontend_model->update_privacy_policy();
            $this->session->set_flashdata('flash_message', get_phrase('privacy_policy_updated'));
            redirect(site_url('admin/frontend_pages/privacy_policy'), 'refresh');
        }
        if ($task == 'update_general_settings') {
            $this->frontend_model->update_frontend_general_settings();
            $this->session->set_flashdata('flash_message', get_phrase('general_settings_updated'));
            redirect(site_url('admin/frontend_pages/general'), 'refresh');
        }
        if ($task == 'update_slider_images') {
            $this->frontend_model->update_slider_images();
            $this->session->set_flashdata('flash_message', get_phrase('slider_images_updated'));
            redirect(site_url('admin/frontend_pages/homepage_slider'), 'refresh');
        }
    }

    function frontend_themes()
    {
        if ($this->session->userdata('admin_login') != 1) {
            redirect(site_url('login'), 'refresh');
        }
        $page_data['page_name'] = 'frontend_themes';
        $page_data['page_title']  = get_phrase('themes');
        $this->load->view('backend/index', $page_data);
    }

    // FRONTEND

    function get_session_changer()
    {
        $this->load->view('backend/admin/change_session');
    }

    function change_session()
    {
        $data['description'] = $this->input->post('running_year');
        $this->db->where('type', 'running_year');
        $this->db->update('settings', $data);
        $this->session->set_flashdata('flash_message', get_phrase('session_changed'));
        redirect(site_url('admin/dashboard'), 'refresh');
    }

    /***** UPDATE PRODUCT *****/
    function update($task = '', $purchase_code = '')
    {

        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        // Create update directory.
        $dir    = 'update';
        if (!is_dir($dir))
            mkdir($dir, 0777, true);

        $zipped_file_name   = $_FILES["file_name"]["name"];
        $path               = 'update/' . $zipped_file_name;

        move_uploaded_file($_FILES["file_name"]["tmp_name"], $path);

        // Unzip uploaded update file and remove zip file.
        $zip = new ZipArchive;
        $res = $zip->open($path);
        if ($res === TRUE) {
            $zip->extractTo('update');
            $zip->close();
            unlink($path);
        }

        $unzipped_file_name = substr($zipped_file_name, 0, -4);
        $str                = file_get_contents('./update/' . $unzipped_file_name . '/update_config.json');
        $json               = json_decode($str, true);

        // Run php modifications
        require './update/' . $unzipped_file_name . '/update_script.php';

        // Create new directories.
        if (!empty($json['directory'])) {
            foreach ($json['directory'] as $directory) {
                if (!is_dir($directory['name']))
                    mkdir($directory['name'], 0777, true);
            }
        }

        // Create/Replace new files.
        if (!empty($json['files'])) {
            foreach ($json['files'] as $file)
                copy($file['root_directory'], $file['update_directory']);
        }

        $this->session->set_flashdata('flash_message', get_phrase('product_updated_successfully'));
        redirect(site_url('admin/system_settings'), 'refresh');
    }

    /*****SMS SETTINGS*********/
    function sms_settings($param1 = '', $param2 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'clickatell') {

            $data['description'] = html_escape($this->input->post('clickatell_user'));
            $this->db->where('type', 'clickatell_user');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('clickatell_password'));
            $this->db->where('type', 'clickatell_password');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('clickatell_api_id'));
            $this->db->where('type', 'clickatell_api_id');
            $this->db->update('settings', $data);

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/sms_settings'), 'refresh');
        }

        if ($param1 == 'twilio') {

            $data['description'] = html_escape($this->input->post('twilio_account_sid'));
            $this->db->where('type', 'twilio_account_sid');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('twilio_auth_token'));
            $this->db->where('type', 'twilio_auth_token');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('twilio_sender_phone_number'));
            $this->db->where('type', 'twilio_sender_phone_number');
            $this->db->update('settings', $data);

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/sms_settings'), 'refresh');
        }
        if ($param1 == 'msg91') {

            $data['description'] = html_escape($this->input->post('authentication_key'));
            $this->db->where('type', 'msg91_authentication_key');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('sender_ID'));
            $this->db->where('type', 'msg91_sender_ID');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('msg91_route'));
            $this->db->where('type', 'msg91_route');
            $this->db->update('settings', $data);

            $data['description'] = html_escape($this->input->post('msg91_country_code'));
            $this->db->where('type', 'msg91_country_code');
            $this->db->update('settings', $data);

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/sms_settings'), 'refresh');
        }

        if ($param1 == 'active_service') {

            $data['description'] = html_escape($this->input->post('active_sms_service'));
            $this->db->where('type', 'active_sms_service');
            $this->db->update('settings', $data);

            $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            redirect(site_url('admin/sms_settings'), 'refresh');
        }

        $page_data['page_name']  = 'sms_settings';
        $page_data['page_title'] = get_phrase('sms_settings');
        $page_data['settings']   = $this->db->get('settings')->result_array();
        $this->load->view('backend/index', $page_data);
    }

    function get_class_routine_data()
    {
        $class_id = $this->input->post("class_id");
        $data1 = $this->input->post('data_atividade');
        $data_at = str_replace("/", "-",  $data1);
        $data = date('Y-m-d', strtotime($data_at));
        $planos_selecionadas = $this->ajaxload->get_planos_data($class_id, $data);

        if (count($planos_selecionadas) > 0) {

            foreach ($planos_selecionadas as $row2) {

                echo '<div class="btn-group" style="margin-right:3px;">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
                echo $this->crud_model->get_subject_name_by_id($row2->subject_id);

                if ($row2->time_start_min == 0 && $row2->time_end_min == 0)
                    echo ' (' . $row2->time_start . '-' . $row2->time_end . ') ';
                if ($row2->time_start_min != 0 || $row2->time_end_min != 0)
                    echo ' (' . $row2->time_start . ':' . $row2->time_start_min . '-' . $row2->time_end . ':' . $row2->time_end_min . ') ';

                echo '<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                        <a href="#" onclick="showAjaxModal(';
                echo "'";
                echo site_url('modal/popup/modal_edit_class_routine/' . $row2->class_routine_id);
                echo "'";
                echo ');">
                                            <i class="entypo-pencil"></i>
                                               Editar
                                                        </a>
                                 </li>';

                echo '<li>
                                    <a href="#" onclick="confirm_modal(';
                echo "'";
                echo site_url('admin/class_routine/delete/' . $row2->class_routine_id);
                echo "'";
                echo ');">
                                        <i class="entypo-trash"></i>
                                            Deletar
                                        </a>
                                    </li>
                                    </ul>
                                </div>';
            }
        } else {
        }
    }


    function get_class_routine_data_periodo()
    {
        $class_id = $this->input->post("class_id");
        $data1 = $this->input->post('data_atividade');
        $data2 = $this->input->post('data_atividade_fim');

        $data_at = str_replace("/", "-",  $data1);
        $data_at2 = str_replace("/", "-",  $data2);
        $data = date('Y-m-d', strtotime($data_at));
        $data22 = date('Y-m-d', strtotime($data_at2));

        $planos_selecionadas = $this->ajaxload->get_planos_data_periodo($class_id, $data, $data22);
        $datas_selecionadas = $this->ajaxload->get_datas_periodo($class_id, $data, $data22);

        if (count($datas_selecionadas) > 0) {

            foreach ($datas_selecionadas as $dt) {
                echo '<tr>';
                echo '<td style="padding-left:10px; width:10%;"><b>';
                $data = date('d-m-Y', strtotime($dt->data_atividade));
                echo $data;
                echo " : </b></td>";
                echo '<td style="padding:20px; width:100%;">';
                foreach ($planos_selecionadas as $plan) {
                    if ($plan->data_atividade == $dt->data_atividade) {
                        echo '<div class="btn-group" style="margin-right:3px;">
                                    <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">';
                        echo $this->crud_model->get_subject_name_by_id($plan->subject_id);

                        if ($plan->time_start_min == 0 && $plan->time_end_min == 0)
                            echo ' (' . $plan->time_start . '-' . $plan->time_end . ') ';
                        if ($plan->time_start_min != 0 || $plan->time_end_min != 0)
                            echo ' (' . $plan->time_start . ':' . $plan->time_start_min . '-' . $plan->time_end . ':' . $plan->time_end_min . ') ';

                        echo '<span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                        <a href="#" onclick="showAjaxModal(';
                        echo "'";
                        echo site_url('modal/popup/modal_edit_class_routine/' . $plan->class_routine_id);
                        echo "'";
                        echo ');">
                                            <i class="entypo-pencil"></i>
                                               Editar
                                                        </a>
                                 </li>';

                        echo '<li>
                                    <a href="#" onclick="confirm_modal(';
                        echo "'";
                        echo site_url('admin/class_routine/delete/' . $plan->class_routine_id);
                        echo "'";
                        echo ');">
                                        <i class="entypo-trash"></i>
                                            Deletar
                                        </a>
                                    </li>
                                    </ul>
                                </div>';
                    }
                }
                echo "</td>";
                echo '</tr>';
            }
        }
    }

    function get_turma_projeto()
    {
        $id = $this->input->post("dados2");
        $turmas_selecionadas = $this->ajaxload->get_turmas_projeto($id);

        $cont = 0;
        $turmas = array();

        foreach ($turmas_selecionadas as $tur) {
            $turmas[$cont]["nome"] = $tur->name;
            $turmas[$cont]["id"] = $tur->class_id;
            $cont++;
        }

        echo json_encode($turmas);
    }

    function get_subject_teacher()
    {
        $id = $this->input->post("dados4");
        $atividades_selecionadas = $this->ajaxload->get_subject_teacher($id);

        $cont = 0;
        $atividades = array();

        foreach ($atividades_selecionadas as $atr) {
            $atividades[$cont]["nome"] = $atr->name;
            $atividades[$cont]["id"] = $atr->subject_id;
            $cont++;
        }

        echo json_encode($atividades);
    }

    function get_nucleos_projeto()
    {
        $id = $this->input->post("dados");
        $nucleos_selecionados = $this->ajaxload->get_nucleos_projeto($id);

        $cont = 0;
        $nucleos = array();

        foreach ($nucleos_selecionados as $nuc) {
            $nucleos[$cont]["nome_nucleo"] = $nuc->nome_nucleo;
            $nucleos[$cont]["id"] = $nuc->id;
            $cont++;
        }

        echo json_encode($nucleos);
    }

    function get_list_of_directories_and_files($dir = APPPATH, &$results = array())
    {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->get_list_of_directories_and_files($path, $results);
                $results[] = $path;
            }
        }
        return $results;
    }

    function get_all_php_files()
    {
        $all_files = $this->get_list_of_directories_and_files();
        foreach ($all_files as $file) {
            $info = pathinfo($file);
            if (isset($info['extension']) && strtolower($info['extension']) == 'php') {
                // echo $file.' <br/> ';
                if ($fh = fopen($file, 'r')) {
                    while (!feof($fh)) {
                        $line = fgets($fh);
                        preg_match_all('/get_phrase\(\'(.*?)\'\)\;/s', $line, $matches);
                        foreach ($matches[1] as $matche) {
                            get_phrase($matche);
                        }
                    }
                    fclose($fh);
                }
            }
        }

        echo 'I Am So Lit';
    }

    function get_list_of_language_files($dir = APPPATH_LANGUAGE, &$results = array())
    {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                $results[] = $path;
            } else if ($value != "." && $value != "..") {
                $this->get_list_of_directories_and_files($path, $results);
                $results[] = $path;
            }
        }
        return $results;
    }

    function get_all_languages()
    {
        $language_files = array();
        $all_files = $this->get_list_of_language_files();
        foreach ($all_files as $file) {
            $info = pathinfo($file);
            if (isset($info['extension']) && strtolower($info['extension']) == 'json') {
                $file_name = explode('.json', $info['basename']);
                array_push($language_files, $file_name[0]);
            }
        }

        return $language_files;
    }

    // Language Functions
    public function manage_language($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'add_language') {
            saveDefaultJSONFile($this->input->post('language'));
            $this->session->set_flashdata('flash_message', get_phrase('language_added_successfully'));
            redirect(site_url('admin/manage_language'), 'refresh');
        }

        if ($param1 == 'delete_language') {
            if (file_exists('application/language/' . $param2 . '.json')) {
                unlink('application/language/' . $param2 . '.json');
                $this->session->set_flashdata('flash_message', get_phrase('language_deleted_successfully'));
                redirect(site_url('admin/manage_language'), 'refresh');
            }
        }

        if ($param1 == 'add_phrase') {
            $new_phrase = get_phrase($this->input->post('phrase'));
            $this->session->set_flashdata('flash_message', $new_phrase . ' ' . get_phrase('has_been_added_successfully'));
            redirect(site_url('admin/manage_language'), 'refresh');
        }

        if ($param1 == 'edit_phrase') {
            $page_data['edit_profile'] = $param2;
        }

        $page_data['languages']             = $this->get_all_languages();
        $page_data['page_name']             =   'manage_language';
        $page_data['page_title']            =   'Configurações dos idiomas';
        $this->load->view('backend/index', $page_data);
    }

    public function update_phrase_with_ajax()
    {
        $current_editing_language = $this->input->post('currentEditingLanguage');
        $updatedValue = $this->input->post('updatedValue');
        $key = $this->input->post('key');
        saveJSONFile($current_editing_language, $key, $updatedValue);
        echo $current_editing_language . ' ' . $key . ' ' . $updatedValue;
    }

    /******MANAGE OWN PROFILE AND CHANGE PASSWORD***/
    function manage_profile($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        if ($param1 == 'update_profile_info') {
            $data['name']  = html_escape($this->input->post('name'));
            $data['email'] = html_escape($this->input->post('email'));

            $admin_id = $param2;

            $validation = email_validation_for_edit($data['email'], $admin_id, 'admin');
            if ($validation == 1) {
                $this->db->where('admin_id', $this->session->userdata('admin_id'));
                $this->db->update('admin', $data);
                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/admin_image/' . $this->session->userdata('admin_id') . '.jpg');
                $this->session->set_flashdata('flash_message', get_phrase('account_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }
            redirect(site_url('admin/manage_profile'), 'refresh');
        }
        if ($param1 == 'change_password') {
            $data['password']             = sha1($this->input->post('password'));
            $data['new_password']         = sha1($this->input->post('new_password'));
            $data['confirm_new_password'] = sha1($this->input->post('confirm_new_password'));

            $current_password = $this->db->get_where('admin', array(
                'admin_id' => $this->session->userdata('admin_id')
            ))->row()->password;
            if ($current_password == $data['password'] && $data['new_password'] == $data['confirm_new_password']) {
                $this->db->where('admin_id', $this->session->userdata('admin_id'));
                $this->db->update('admin', array(
                    'password' => $data['new_password']
                ));
                $this->session->set_flashdata('flash_message', get_phrase('password_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('password_mismatch'));
            }
            redirect(site_url('admin/manage_profile'), 'refresh');
        }
        $page_data['page_name']  = 'manage_profile';
        $page_data['page_title'] = 'Gerenciar Perfil';
        $page_data['edit_data']  = $this->db->get_where('admin', array(
            'admin_id' => $this->session->userdata('admin_id')
        ))->result_array();
        $this->load->view('backend/index', $page_data);
    }

    // VIEW QUESTION PAPERS
    function question_paper($param1 = "", $param2 = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $data['page_name']  = 'question_paper';
        $data['page_title'] = get_phrase('question_paper');
        $this->load->view('backend/index', $data);
    }

    // MANAGE LIBRARIANS
    function librarian($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['name']       = html_escape($this->input->post('name'));
            $data['email']      = html_escape($this->input->post('email'));
            $data['password']   = sha1($this->input->post('password'));
            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('librarian', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                $this->email_model->account_opening_email('librarian', $data['email'], $this->input->post('password')); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }
            redirect(site_url('admin/librarian'), 'refresh');
        }

        if ($param1 == 'edit') {
            $data['name']   = html_escape($this->input->post('name'));
            $data['email']  = html_escape($this->input->post('email'));
            $validation = email_validation_for_edit($data['email'], $param2, 'librarian');
            if ($validation == 1) {
                $this->db->where('librarian_id', $param2);
                $this->db->update('librarian', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/librarian'), 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('librarian_id', $param2);
            $this->db->delete('librarian');

            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/librarian'), 'refresh');
        }

        $page_data['page_title']    = get_phrase('all_librarians');
        $page_data['page_name']     = 'librarian';
        $this->load->view('backend/index', $page_data);
    }

    // MANAGE ACCOUNTANTS
    function accountant($param1 = '', $param2 = '', $param3 = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'create') {
            $data['name']       = html_escape($this->input->post('name'));
            $data['email']      = html_escape($this->input->post('email'));
            $data['password']   = sha1($this->input->post('password'));

            $validation = email_validation($data['email']);
            if ($validation == 1) {
                $this->db->insert('accountant', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                $this->email_model->account_opening_email('accountant', $data['email'], html_escape($this->input->post('password'))); //SEND EMAIL ACCOUNT OPENING EMAIL
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/accountant'), 'refresh');
        }

        if ($param1 == 'edit') {
            $data['name']   = html_escape($this->input->post('name'));
            $data['email']  = html_escape($this->input->post('email'));

            $validation = email_validation_for_edit($data['email'], $param2, 'accountant');
            if ($validation == 1) {
                $this->db->where('accountant_id', $param2);
                $this->db->update('accountant', $data);
                $this->session->set_flashdata('flash_message', get_phrase('data_updated'));
            } else {
                $this->session->set_flashdata('error_message', get_phrase('this_email_id_is_not_available'));
            }

            redirect(site_url('admin/accountant'), 'refresh');
        }

        if ($param1 == 'delete') {
            $this->db->where('accountant_id', $param2);
            $this->db->delete('accountant');

            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/accountant'), 'refresh');
        }

        $page_data['page_title']    = get_phrase('all_accountants');
        $page_data['page_name']     = 'accountant';
        $this->load->view('backend/index', $page_data);
    }

    // bulk student_add using CSV
    function generate_bulk_student_csv($class_id = '', $section_id = '', $id_projeto = '', $id_nucleo = '', $id_escola = '')
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $data['class_id']   = $class_id;
        $data['section_id'] = $section_id;
        $data['id_projeto'] = $id_projeto;
        $data['id_nucleo'] = $id_nucleo;
        $data['id_escola'] = $id_escola;
        $data['year']       = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;

        $file   = fopen("uploads/bulk_student.csv", "w");
        $line   = array('StudentName', 'Id', 'Email', 'Password', 'Phone', 'Address', 'ParentID', 'Gender');
        fputcsv($file, $line, ',');
        echo $file_path = base_url() . 'uploads/bulk_student.csv';
    }


    // CSV IMPORT
    function bulk_student_add_using_csv($param1 = '')
    {

        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($param1 == 'import') {
            if ($this->input->post('class_id') != '' && $this->input->post('section_id') != '') {

                move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/bulk_student.csv');
                $csv = array_map('str_getcsv', file('uploads/bulk_student.csv'));
                $count = 1;
                $array_size = sizeof($csv);

                foreach ($csv as $row) {
                    if ($count == 1) {
                        $count++;
                        continue;
                    }
                    $password = $row[3];

                    $data['name']      = $row[0];
                    $data['student_code']  = $row[1];
                    $data['email']     = $row[2];
                    $data['password']  = sha1($row[3]);
                    $data['phone']     = $row[4];
                    $data['address']   = $row[5];
                    $data['parent_id'] = $row[6];
                    $data['sex']       = strtolower($row[7]);
                    //student id (code) validation
                    $code_validation = code_validation_insert($data['student_code']);
                    if (!$code_validation) {
                        $this->session->set_flashdata('error_message', get_phrase('this_id_no_is_not_available'));
                        redirect(site_url('admin/student_add'), 'refresh');
                    }
                    //student id validation ends

                    $validation = email_validation($data['email']);
                    if ($validation == 1) {
                        $this->db->insert('student', $data);
                        $student_id = $this->db->insert_id();

                        $data2['student_id']  = $student_id;
                        $data2['class_id']    = $this->input->post('class_id');
                        $data2['section_id']  = $this->input->post('section_id');
                        //                    $data2['roll']        = $row[1];
                        $data2['enroll_code'] =   substr(md5(rand(0, 1000000)), 0, 7);
                        $data2['date_added']  =   strtotime(date("Y-m-d H:i:s"));
                        $data2['year']        =   $this->db->get_where('settings', array('type' => 'running_year'))->row()->description;
                        $this->db->insert('enroll', $data2);
                    } else {
                        if ($array_size == 2) {
                            $this->session->set_flashdata('error_message', get_phrase('this_email_id_"') . $data['email'] . get_phrase('"_is_not_available'));
                            redirect(site_url('admin/student_bulk_add'), 'refresh');
                        } elseif ($array_size > 2) {
                            $this->session->set_flashdata('error_message', get_phrase('some_email_IDs_are_not_available'));
                        }
                    }
                }


                $this->session->set_flashdata('flash_message', get_phrase('student_imported'));
                redirect(site_url('admin/student_bulk_add'), 'refresh');
            } else {
                $this->session->set_flashdata('error_message', get_phrase('please_make_sure_class_and_section_is_selected'));
                redirect(site_url('admin/student_bulk_add'), 'refresh');
            }
        }
        $page_data['page_name']  = 'student_bulk_add';
        $page_data['page_title'] = get_phrase('add_bulk_student');
        $this->load->view('backend/index', $page_data);
    }

    function study_material($task = "", $document_id = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($task == "create") {
            $this->crud_model->save_study_material_info();
            $this->session->set_flashdata('flash_message', get_phrase('study_material_info_saved_successfuly'));
            redirect(site_url('admin/study_material'), 'refresh');
        }

        if ($task == "update") {
            $this->crud_model->update_study_material_info($document_id);
            $this->session->set_flashdata('flash_message', get_phrase('study_material_info_updated_successfuly'));
            redirect(site_url('admin/study_material'), 'refresh');
        }

        if ($task == "delete") {
            $this->crud_model->delete_study_material_info($document_id);
            redirect(site_url('admin/study_material'), 'refresh');
        }

        $data['study_material_info']    = $this->crud_model->select_study_material_info();
        $data['page_name']              = 'study_material';
        $data['page_title']             = 'Material de Estudo';
        $this->load->view('backend/index', $data);
    }

    //new code
    function print_id($id)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $data['id'] = $id;
        $this->load->view('backend/admin/print_id', $data);
    }

    function create_barcode($student_id)
    {

        return $this->Barcode_model->create_barcode($student_id);
    }

    // Details of searched student
    function student_details($param1 = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $student_identifier = html_escape($this->input->post('student_identifier'));
        $query_by_code = $this->db->get_where('student', array('student_code' => $student_identifier));

        if ($query_by_code->num_rows() == 0) {
            $this->db->like('name', $student_identifier);
            $query_by_name = $this->db->get('student');
            if ($query_by_name->num_rows() == 0) {
                $this->session->set_flashdata('error_message', get_phrase('no_student_found'));
                redirect(site_url('admin/dashboard'), 'refresh');
            } else {
                $page_data['student_information'] = $query_by_name->result_array();
            }
        } else {
            $page_data['student_information'] = $query_by_code->result_array();
        }
        $page_data['page_name']      = 'search_result';
        $page_data['page_title']     = get_phrase('search_result');
        $this->load->view('backend/index', $page_data);
    }


    // online exam
    function manage_online_exam($param1 = "", $param2 = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $running_year = get_settings('running_year');

        if ($param1 == '') {
            $match = array('status !=' => 'expired', 'running_year' => $running_year);
            $page_data['status'] = 'active';
            $this->db->order_by("exam_date", "dsc");
            $page_data['online_exams'] = $this->db->where($match)->get('online_exam')->result_array();
        }

        if ($param1 == 'expired') {
            $match = array('status' => 'expired', 'running_year' => $running_year);
            $page_data['status'] = 'expired';
            $this->db->order_by("exam_date", "dsc");
            $page_data['online_exams'] = $this->db->where($match)->get('online_exam')->result_array();
        }

        if ($param1 == 'create') {
            if ($this->input->post('class_id') > 0 && $this->input->post('section_id') > 0 && $this->input->post('subject_id') > 0) {
                $this->crud_model->create_online_exam();
                $this->session->set_flashdata('flash_message', get_phrase('data_added_successfully'));
                redirect(site_url('admin/manage_online_exam'), 'refresh');
            } else {
                $this->session->set_flashdata('error_message', get_phrase('make_sure_to_select_valid_class_') . ',' . get_phrase('_section_and_subject'));
                redirect(site_url('admin/manage_online_exam'), 'refresh');
            }
        }
        if ($param1 == 'edit') {
            if ($this->input->post('class_id') > 0 && $this->input->post('section_id') > 0 && $this->input->post('subject_id') > 0) {
                $this->crud_model->update_online_exam();
                $this->session->set_flashdata('flash_message', get_phrase('data_updated_successfully'));
                redirect(site_url('admin/manage_online_exam'), 'refresh');
            } else {
                $this->session->set_flashdata('error_message', get_phrase('make_sure_to_select_valid_class_') . ',' . get_phrase('_section_and_subject'));
                redirect(site_url('admin/manage_online_exam'), 'refresh');
            }
        }
        if ($param1 == 'delete') {
            $this->db->where('online_exam_id', $param2);
            $this->db->delete('online_exam');
            $this->session->set_flashdata('flash_message', get_phrase('data_deleted'));
            redirect(site_url('admin/manage_online_exam'), 'refresh');
        }
        $page_data['page_name'] = 'manage_online_exam';
        $page_data['page_title'] = get_phrase('manage_online_exam');
        $this->load->view('backend/index', $page_data);
    }

    function online_exam_questions_print_view($online_exam_id, $answers)
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $page_data['online_exam_id'] = $online_exam_id;
        $page_data['answers'] = $answers;
        $page_data['page_title'] = get_phrase('questions_print');
        $this->load->view('backend/admin/online_exam_questions_print_view', $page_data);
    }

    function create_online_exam()
    {
        $page_data['page_name'] = 'add_online_exam';
        $page_data['page_title'] = get_phrase('add_an_online_exam');
        $this->load->view('backend/index', $page_data);
    }

    function update_online_exam($param1 = "")
    {
        $page_data['online_exam_id'] = $param1;
        $page_data['page_name'] = 'edit_online_exam';
        $page_data['page_title'] = get_phrase('update_online_exam');
        $this->load->view('backend/index', $page_data);
    }

    function manage_online_exam_status($online_exam_id = "", $status = "")
    {
        $this->crud_model->manage_online_exam_status($online_exam_id, $status);
        redirect(site_url('admin/manage_online_exam'), 'refresh');
    }

    function load_question_type($type, $online_exam_id)
    {
        $page_data['question_type'] = $type;
        $page_data['online_exam_id'] = $online_exam_id;
        $this->load->view('backend/admin/online_exam_add_' . $type, $page_data);
    }

    function manage_online_exam_question($online_exam_id = "", $task = "", $type = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        if ($task == 'add') {
            if ($type == 'multiple_choice') {
                $this->crud_model->add_multiple_choice_question_to_online_exam($online_exam_id);
            } elseif ($type == 'true_false') {
                $this->crud_model->add_true_false_question_to_online_exam($online_exam_id);
            } elseif ($type == 'fill_in_the_blanks') {
                $this->crud_model->add_fill_in_the_blanks_question_to_online_exam($online_exam_id);
            }
            redirect(site_url('admin/manage_online_exam_question/' . $online_exam_id), 'refresh');
        }

        $page_data['online_exam_id'] = $online_exam_id;
        $page_data['page_name'] = 'manage_online_exam_question';
        $page_data['page_title'] = $this->db->get_where('online_exam', array('online_exam_id' => $online_exam_id))->row()->title;
        $this->load->view('backend/index', $page_data);
    }

    function update_online_exam_question($question_id = "", $task = "", $online_exam_id = "")
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');
        $online_exam_id = $this->db->get_where('question_bank', array('question_bank_id' => $question_id))->row()->online_exam_id;
        $type = $this->db->get_where('question_bank', array('question_bank_id' => $question_id))->row()->type;
        if ($task == "update") {
            if ($type == 'multiple_choice') {
                $this->crud_model->update_multiple_choice_question($question_id);
            } elseif ($type == 'true_false') {
                $this->crud_model->update_true_false_question($question_id);
            } elseif ($type == 'fill_in_the_blanks') {
                $this->crud_model->update_fill_in_the_blanks_question($question_id);
            }
            redirect(site_url('admin/manage_online_exam_question/' . $online_exam_id), 'refresh');
        }
        $page_data['question_id'] = $question_id;
        $page_data['page_name'] = 'update_online_exam_question';
        $page_data['page_title'] = get_phrase('update_question');
        $this->load->view('backend/index', $page_data);
    }

    function delete_question_from_online_exam($question_id)
    {
        $online_exam_id = $this->db->get_where('question_bank', array('question_bank_id' => $question_id))->row()->online_exam_id;
        $this->crud_model->delete_question_from_online_exam($question_id);
        $this->session->set_flashdata('flash_message', get_phrase('question_deleted'));
        redirect(site_url('admin/manage_online_exam_question/' . $online_exam_id), 'refresh');
    }

    function manage_multiple_choices_options()
    {
        $page_data['number_of_options'] = $this->input->post('number_of_options');
        $this->load->view('backend/admin/manage_multiple_choices_options', $page_data);
    }

    function get_sections_for_ssph($class_id)
    {
        $sections = $this->db->get_where('section', array('class_id' => $class_id))->result_array();
        $options = '';
        foreach ($sections as $row) {
            $options .= '<option value="' . $row['section_id'] . '">' . $row['name'] . '</option>';
        }
        echo '<select class="" name="section_id" id="section_id">' . $options . '</select>';
    }

    function get_students_for_ssph($class_id, $section_id)
    {
        $enrolls = $this->db->get_where('enroll', array('class_id' => $class_id, 'section_id' => $section_id))->result_array();
        $options = '';
        foreach ($enrolls as $row) {
            $name = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name;
            $options .= '<option value="' . $row['student_id'] . '">' . $name . '</option>';
        }
        echo '<select class="" name="student_id" id="student_id">' . $options . '</select>';
    }

    function get_payment_history_for_ssph($student_id)
    {
        $page_data['student_id'] = $student_id;
        $this->load->view('backend/admin/student_specific_payment_history_table', $page_data);
    }

    function view_online_exam_result($online_exam_id)
    {
        $page_data['page_name'] = 'view_online_exam_results';
        $page_data['page_title'] = get_phrase('result');
        $page_data['online_exam_id'] = $online_exam_id;
        $this->load->view('backend/index', $page_data);
    }


    public function get_todos()
    {
        $sql1 = "SELECT * FROM nucleo
           ";

        $sql2 = "SELECT * FROM local_execucao
           ";

        $sql3 = "SELECT * FROM patrocinador
           ";

        $sql4 = "SELECT * FROM teacher
         INNER JOIN class ON class.teacher_id = teacher.teacher_id GROUP BY teacher.teacher_id
         
        ";

        $query1 =  $this->db->query($sql1);
        $query2 =  $this->db->query($sql2);
        $query3 =  $this->db->query($sql3);
        $query4 =  $this->db->query($sql4);

        $vetor_instrutores = $query4->result();
        $vetor_nucleos = $query1->result();
        $vetor_local = $query2->result();
        $vetor_patrocinadores = $query3->result();

        $data = array(
            'vetor_instrutores' => $vetor_instrutores,
            'vetor_nucleos' => $vetor_nucleos,
            'vetor_local' => $vetor_local,
            'vetor_patrocinadores' => $vetor_patrocinadores
        );

        $json =  json_encode($data);
        echo $json;
    }


    public function get_users_json() 
    {
        $project = $this->input->get('project');

        $sql1 = "SELECT * FROM nucleo
            WHERE id_projeto = " . $project . "
           ";

        $sql7 = "SELECT * FROM local_execucao
            WHERE id_projeto = " . $project . "
           ";

        $sql8 = "SELECT patrocinador.id as id, patrocinador.nome as nome FROM patrocinador_projeto
                INNER JOIN patrocinador ON patrocinador.id = patrocinador_projeto.id_patrocinador
                WHERE patrocinador_projeto.id_projeto = " . $project . "
           ";

        $sql2 = "SELECT * FROM cidade
           INNER JOIN nucleo ON nucleo.nome_nucleo = cidade.nome AND nucleo.id_projeto = " . $project . "
          ";
        $sql3 = "SELECT * FROM student
          WHERE id_projeto = " . $project . "
         ";
        $sql4 = "SELECT * FROM parent
           INNER JOIN student ON student.parent_id = parent.parent_id AND student.id_projeto = " . $project . "
          ";
        $sql5 = "SELECT * FROM class
          WHERE projeto_id = " . $project . " 
          
         ";
        $sql6 = "SELECT * FROM teacher
         INNER JOIN class ON class.teacher_id = teacher.teacher_id AND class.projeto_id = " . $project . " GROUP BY teacher.teacher_id
         
        ";

        $sql9 = "SELECT * FROM patrocinador_projeto
        WHERE id_projeto = " . $project . "
       ";

        $query1 =  $this->db->query($sql1);
        $query2 =  $this->db->query($sql2);
        $query3 =  $this->db->query($sql3);
        $query4 =  $this->db->query($sql4);
        $query5 =  $this->db->query($sql5);
        $query6 =  $this->db->query($sql6);
        $query7 =  $this->db->query($sql7);
        $query8 =  $this->db->query($sql8);
        $query9 =  $this->db->query($sql9);

        $nucleos = count($query1->result());
        $cidades = count($query2->result());
        $alunos = count($query3->result());
        $responsaveis = count($query4->result());
        $turmas = count($query5->result());
        $instrutores = count($query6->result());
        $patrocinadores = count($query9->result());

        $vetor_instrutores = $query6->result();
        $vetor_nucleos = $query1->result();
        $vetor_local = $query7->result();
        $vetor_patrocinadores = $query8->result();
        $data = array(
            'nucleos' => $nucleos,
            'cidades' => $cidades,
            'alunos' => $alunos,
            'responsaveis' => $responsaveis,
            'turmas' => $turmas,
            'instrutores' => $instrutores,
            'vetor_instrutores' => $vetor_instrutores,
            'vetor_nucleos' => $vetor_nucleos,
            'vetor_local' => $vetor_local,
            'vetor_patrocinadores' => $vetor_patrocinadores,
            'patrocinadores' => $patrocinadores
        );
        if ($project == "geral") {
            $data = array(
                'nucleos' => $this->db->count_all('nucleo'),
                'cidades' => $this->db->count_all('cidade'),
                'alunos' => $this->db->count_all('student'),
                'responsaveis' => $this->db->count_all('parent'),
                'turmas' => $this->db->count_all('class'),
                'instrutores' => $this->db->count_all('teacher')
            );
        }
        $json =  json_encode($data);
        echo $json;
    }

    function get_turma_nucleo_multiple()
    {
        $nucleos = $this->input->get('nucleos');

        $nucleos_array = explode(",", $nucleos);

        if ($nucleos != "null") {
            $where = "(";
            $cont = 0;

            foreach ($nucleos_array as $c) {
                if ($cont == 0) {
                    $where .= "id_nucleo='$c'";
                } else {
                    $where .= "OR id_nucleo='$c'";
                }
                $cont = $cont + 1;
            }
            $where .= ")";
        }
        $sql = "SELECT class_id,name,nucleo.nome_nucleo as nome_nucleo
        FROM class
        INNER JOIN nucleo ON class.id_nucleo = nucleo.id
        WHERE $where
       ";

        $query =  $this->db->query($sql);
        $data = $query->result();

        $json =  json_encode($data);
        echo $json;
    }

    function get_teacher_nucleo_multiple()
    {
        $nucleos = $this->input->post('nucleos');

        if ($nucleos != "null") {
            $where = "(";
            $cont = 0;

            foreach ($nucleos as $c) {
                if ($cont == 0) {
                    $where .= "class.id_nucleo='$c'";
                } else {
                    $where .= "OR class.id_nucleo='$c'";
                }
                $cont = $cont + 1;
            }
            $where .= ")";
        }
        $sql = "SELECT teacher.teacher_id as id,teacher.name as name
        FROM class
        INNER JOIN teacher ON class.teacher_id = teacher.teacher_id
        WHERE $where
        GROUP BY teacher.teacher_id
       ";

        $query =  $this->db->query($sql);
        $instrutores_selecionados = $query->result();
        $instrutores = array();
        $cont = 0;

        foreach ($instrutores_selecionados as $instrutor) {
            $instrutores[$cont]["nome_instrutor"] = $instrutor->name;
            $instrutores[$cont]["id_instrutor"] = $instrutor->id;
            $cont++;
        }

        echo json_encode($instrutores);
    }

    function get_teacher_projeto()
    {

        $id_projeto = $this->input->post("id_projeto");
        $instrutores_selecionados = $this->ajaxload->get_teacher_projeto($id_projeto);
        $cont = 0;
        $instrutores = array();

        foreach ($instrutores_selecionados as $instrutor) {
            $instrutores[$cont]["nome_instrutor"] = $instrutor->name;
            $instrutores[$cont]["id_instrutor"] = $instrutor->id;
            $cont++;
        }

        echo json_encode($instrutores);
    }

    function get_modalidade_projeto()
    {
        $id_projeto = $this->input->post("id_projeto");
        $modalidades_selecionadas = $this->ajaxload->get_modalidade_projeto($id_projeto);
        $cont = 0;
        $modalidades = array();

        foreach ($modalidades_selecionadas as $modalidade) {
            $modalidades[$cont]["name"] = $modalidade->name;
            $modalidades[$cont]["id"] = $modalidade->id;
            $cont++;
        }

        echo json_encode($modalidades);
    }


    function get_teacher_modalidade_multiple()
    {
        $modalidades = $this->input->post('modalidades');

        if ($modalidades != "null") {
            $where = "(";
            $cont = 0;

            foreach ($modalidades as $c) {
                if ($cont == 0) {
                    $where .= "class.modalidade='$c'";
                } else {
                    $where .= "OR class.modalidade='$c'";
                }
                $cont = $cont + 1;
            }
            $where .= ")";
        }

        $nucleos = $this->input->post('nucleos');

        if ($nucleos != "null") {
            $where .= " AND (";
            $cont = 0;
            foreach ($nucleos as $c) {
                if ($cont == 0) {
                    $where .= "class.id_nucleo='$c'";
                } else {
                    $where .= "OR class.id_nucleo='$c'";
                }
                $cont = $cont + 1;
            }
            $where .= ")";
        }

        $sql = "SELECT teacher.teacher_id as id,teacher.name as name
        FROM class
        INNER JOIN teacher ON class.teacher_id = teacher.teacher_id
        WHERE $where
        GROUP BY teacher.teacher_id
       ";

        $query =  $this->db->query($sql);
        $instrutores_selecionados = $query->result();
        $instrutores = array();
        $cont = 0;

        foreach ($instrutores_selecionados as $instrutor) {
            $instrutores[$cont]["nome_instrutor"] = $instrutor->name;
            $instrutores[$cont]["id_instrutor"] = $instrutor->id;
            $cont++;
        }

        echo json_encode($instrutores);
    }

    function get_teacher_modalidade_nucleo_multiple()
    {
        $modalidades = $this->input->post('modalidades');

        if ($modalidades != "null") {
            $where = "(";
            $cont = 0;

            foreach ($modalidades as $c) {
                if ($cont == 0) {
                    $where .= "class.modalidade='$c'";
                } else {
                    $where .= "OR class.modalidade='$c'";
                }
                $cont = $cont + 1;
            }
            $where .= ")";
        }
        $sql = "SELECT teacher.teacher_id as id,teacher.name as name
        FROM class
        INNER JOIN teacher ON class.teacher_id = teacher.teacher_id
        WHERE $where
        GROUP BY teacher.teacher_id
       ";

        $query =  $this->db->query($sql);
        $instrutores_selecionados = $query->result();
        $instrutores = array();
        $cont = 0;

        foreach ($instrutores_selecionados as $instrutor) {
            $instrutores[$cont]["nome_instrutor"] = $instrutor->name;
            $instrutores[$cont]["id_instrutor"] = $instrutor->id;
            $cont++;
        }

        echo json_encode($instrutores);
    }


    function get_turma_teacher_multiple()
    {
        $instrutores = $this->input->post('instrutores');

        if ($instrutores != "null") {
            $where = "(";
            $cont = 0;

            foreach ($instrutores as $c) {
                if ($cont == 0) {
                    $where .= "class.teacher_id='$c'";
                } else {
                    $where .= "OR class.teacher_id='$c'";
                }
                $cont = $cont + 1;
            }
            $where .= ")";
        }
        $sql = "SELECT class.class_id as id,class.name as name
        FROM class
        WHERE $where
       ";

        $query =  $this->db->query($sql);
        $turmas_selecionadas = $query->result();
        $turmas = array();
        $cont = 0;

        foreach ($turmas_selecionadas as $turm) {
            $turmas[$cont]["nome_turma"] = $turm->name;
            $turmas[$cont]["id_turma"] = $turm->id;
            $cont++;
        }

        echo json_encode($turmas);
    }



    function get_beneficiados()
    {
        if ($this->session->userdata('admin_login') != 1)
            redirect(site_url('login'), 'refresh');

        $id_projeto  = html_escape($this->input->post('id_projeto'));
        $nucleos = html_escape($this->input->post('id_nucleo'));
        $tipo_escola  = html_escape($this->input->post('tipo_escola'));
        $modalidades  = html_escape($this->input->post('modalidade_id'));
        $turmas  = html_escape($this->input->post('class_id'));
        $sexo  = html_escape($this->input->post('sexo'));
        $situacao  = html_escape($this->input->post('situacao'));
        $idade_inicial = html_escape($this->input->post('idade_inicial'));
        $idade_final  = html_escape($this->input->post('idade_final'));
        $escolas  = html_escape($this->input->post('escola_id'));
        $instrutores  = html_escape($this->input->post('instrutores'));

        $data_inicial = $this->input->post('data_inicial');
        $data_final = $this->input->post('data_final');

        /*
        echo "<br>";
        echo "id_projeto: " . $id_projeto;
        echo "<br>";
        echo "nucleos: " . var_dump($nucleos);
        echo "<br>";
        echo "tipo_escola: " . $tipo_escola;
        echo "<br>";
        echo "modalidades: " . var_dump($modalidades);
        echo "<br>";
        echo "turmas: " . var_dump($turmas);
        echo "<br>";
        echo "sexo: " . $sexo;
        echo "<br>";
        //echo "situacao: " . $situacao;
        //echo "<br>";
        echo "idade_inicial: " . $idade_inicial;
        echo "<br>";
        echo "idade_final: " . $idade_final;
        echo "<br>";
        echo "escolas: " . var_dump($escolas);
        echo "<br>";

        echo "instrutores: " . var_dump($instrutores);
        echo "<br>";
*/

        // ================================= Monta SQL =================================
        $where = "";

        // WHERE Nucleos
        if (!empty($nucleos) || $nucleos != null || $nucleos != "") {


            if ($where != "" || !empty($where)) {
                $where .= " AND (";
            } else {
                $where .= " (";
            }


            $contador = 1;

            foreach ($nucleos as $nuc) {
                if ($contador == 1) {
                    $where .= "student.id_nucleo = " . $nuc;
                } else {
                    $where .= " OR student.id_nucleo = " . $nuc;
                }
                $contador++;
            }

            $where .= ")";
        }

        // WHERE Instrutores
        if (!empty($instrutores) || $instrutores != null || $instrutores != "") {

            if ($where != "" || !empty($where)) {
                $where .= " AND (";
            } else {
                $where .= " (";
            }
            $contador = 1;

            foreach ($instrutores as $inst) {
                if ($contador == 1) {
                    $where .= "class.teacher_id = " . $inst;
                } else {
                    $where .= " OR class.teacher_id = " . $inst;
                }
                $contador++;
            }

            $where .= ")";
        }


        // WHERE Tipo de Escola
        if ($tipo_escola != "AMBOS") {
            if ($where != "" || !empty($where)) {
                $where .= " AND (escola.tipo_escola = '" . $tipo_escola . "'";
            } else {
                $where .= " (escola.tipo_escola = '" . $tipo_escola . "'";
            }
            $where .= ")";
        }

        // WHERE Modalidades
        if (!empty($modalidades) || $modalidades != null || $modalidades != "") {

            if ($where != "" || !empty($where)) {
                $where .= " AND (";
            } else {
                $where .= " (";
            }
            $contador = 1;

            foreach ($modalidades as $mod) {
                if ($contador == 1) {
                    $where .= "modalidade.id = " . $mod;
                } else {
                    $where .= " OR modalidade.id = " . $mod;
                }
                $contador++;
            }

            $where .= ")";
        }

        // WHERE Turmas
        if (!empty($turmas) || $turmas != null || $turmas != "") {

            if ($where != "" || !empty($where)) {
                $where .= " AND (";
            } else {
                $where .= " (";
            }
            $contador = 1;

            foreach ($turmas as $turm) {
                if ($contador == 1) {
                    $where .= "class.class_id = " . $turm;
                } else {
                    $where .= " OR class.class_id = " . $turm;
                }
                $contador++;
            }

            $where .= ")";
        }

        // WHERE Sexo
        if ($sexo != "AMBOS") {
            if ($where != "" || !empty($where)) {
                $where .= " AND (student.sex = '" . $sexo . "'";
            } else {
                $where .= " (student.sex = '" . $sexo . "'";
            }
            $where .= ")";
        }

        // WHERE Situação
        if ($situacao != "AMBOS") {
            if ($where != "" || !empty($where)) {
                $where .= " AND (student.situacao = '" . $situacao . "'";
            } else {
                $where .= " (student.situacao = '" . $situacao . "'";
            }

            $where .= ")";
        }

        // WHERE Idade
        if (!empty($idade_final) || $idade_final != null || $idade_final != "" || !empty($idade_inicial) || $idade_inicial != null || $idade_inicial != "") {

            if ($where != "" || !empty($where)) {
                $where .= " AND (YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(student.data_nascimento))) BETWEEN '" . $idade_inicial . "' AND '" . $idade_final . "')";
            } else {
                $where .= " (YEAR(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(student.data_nascimento))) BETWEEN '" . $idade_inicial . "' AND '" . $idade_final . "')";
            }
        }


        // WHERE Escolas
        if (!empty($escolas) || $escolas != null || $escolas != "") {

            if ($where != "" || !empty($where)) {
                $where .= " AND (";
            } else {
                $where .= " (";
            }
            $contador = 1;

            foreach ($escolas as $esc) {
                if ($contador == 1) {
                    $where .= "escola.id = " . $esc;
                } else {
                    $where .= " OR escola.id = " . $esc;
                }
                $contador++;
            }

            $where .= ")";
        }

        // WHERE Data
        if (!empty($data_inicial) || $data_inicial != null || $data_inicial != "" || !empty($data_final) || $data_final != null || $data_final != "") {

            if ($where != "" || !empty($where)) {
                $where .= " AND DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '" . $data_inicial . "' AND '" . $data_final . "'";
            } else {
                $where .= " DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '" . $data_inicial . "' AND '" . $data_final . "'";
            }
        }

        // WHERE Projeto
        if (!empty($id_projeto) || $id_projeto != null || $id_projeto != "") {


            if ($where != "" || !empty($where)) {
                $where .= " AND (class.projeto_id = '" . $id_projeto . "'";
            } else {
                $where .= " (class.projeto_id = '" . $id_projeto . "'";
            }
            $where .= ")";
        }

        //FROM student, class, modalidade, enroll 
        $sql = " SELECT student.name as nome, modalidade.modalidade as modalidade,class.name as turma, student.cidade, student.situacao, student.estado, student.rua, student.bairro, student.numero, student.complemento, student.phone as telefone, student.data_nascimento 
                FROM student 
                INNER JOIN enroll ON student.student_id = enroll.student_id
                INNER JOIN class ON enroll.class_id = class.class_id
                INNER JOIN modalidade ON class.modalidade = modalidade.id
                INNER JOIN escola ON student.id_escola = escola.id";

        if ($where != "" || !empty($where)) {
            $sql .= " WHERE " . $where;
        }
        $sql .= "  GROUP BY student.student_id
                order by student.name ASC";

        $beneficiados  =  $this->db->query($sql)->result();
        $recordsTotal  =  $this->db->query($sql)->num_rows();

        $data = array();
        if (!empty($beneficiados)) {
            foreach ($beneficiados as $row) {
                $nestedData['nome'] = $row->nome;
                $nestedData['modalidade'] = $row->modalidade;
                $nestedData['turma'] = $row->turma;
                $nestedData['endereco'] = $row->cidade . ' - ' . $row->estado . ', Rua ' . $row->rua . ', ' . $row->bairro . ', ' . $row->numero;
                $nestedData['telefone'] = $row->telefone;
                $nestedData['data_nascimento'] = date("d/m/Y", strtotime($row->data_nascimento));

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($recordsTotal),
            "recordsFiltered"    => intval($recordsTotal),
            "data"            => $data
        );

        echo json_encode($json_data);
    }
}
