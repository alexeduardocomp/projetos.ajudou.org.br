

<script type="text/javascript">

	(function( $ ) {
		$(function() {
			$(".cpf").mask('000.000.000-00');
			$('.tel').mask('(00) 00000-0000');
			$('.cep').mask('00000-000');
			$('.numeracao').mask('00.00');
			$('.nt').mask('00');
		});
	})(jQuery);
</script>


<style>
	select[readonly] {
		background: #eee;
		/*Simular campo inativo - Sugestão @GabrielRodrigues*/
		pointer-events: none;
		touch-action: none;
	}
</style>

<?php
$query = $this->db->get('projeto');
$projetos = $query->result();

$query = $this->db->get('nucleo');
$nucleos = $query->result();
?>

<?php
$edit_data		=	$this->db->get_where('enroll', array(
	'student_id' => $param2, 'class_id' => $param3, 'year' => $this->db->get_where('settings', array('type' => 'running_year'))->row()->description
))->result_array();

$turma		=	$this->db->get_where('class', array('class_id' => $param3))->row();

$matricula_turma		=	$this->db->get_where('enroll', array('class_id' => $param3, 'student_id' => $param2))->row();

foreach ($edit_data as $row) :
?>

	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-primary" data-collapsed="0">
				<div class="panel-heading">
					<div class="panel-title">
						<i class="entypo-plus-circled"></i>
						Editar aluno
					</div>
				</div>
				<div class="panel-body">

					<?php echo form_open(site_url('teacher/student/do_update/' . $row['student_id'] . '/' . $row['class_id']), array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data')); ?>
					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('photo'); ?></label>

						<div class="col-sm-5">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="<?php echo $this->crud_model->get_image_url('student', $row['student_id']); ?>" alt="...">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Selecione uma imagem</span>
										<span class="fileinput-exists">Mudar</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remover</a>
								</div>
							</div>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="name" style="text-transform:uppercase;" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->name; ?>" autofocus>
						</div>
					</div>
					<?php
					$query =
						$this->db->select("enroll.*,class.projeto_id as projeto_id,class.nome_nucleo as nome_nucleo,class.name as nome_turma,class.id_nucleo as id_nucleo")
						->from("enroll")
						->join('class', "enroll.class_id = class.class_id")
						->where('student_id', $row['student_id'])
						->order_by("date_added", "DESC");
					$query = $this->db->get();
					$ultima_matricula_aluno  = $query->row();
					//echo $ultima_matricula_aluno->enroll_id;

					$projeto  = $this->db->get_where('projeto', array('id' => $ultima_matricula_aluno->projeto_id))->row();



					$id_nuc = $ultima_matricula_aluno->id_nucleo;

					//	echo $id_nuc;
					?>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Projeto</label>
						<div class="col-sm-5">


							<input name="id_projeto" readonly class="form-control" id="pro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $projeto->nome; ?>">

						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Núcleo</label>
						<div class="col-sm-5">
							<input name="id_nucleo" readonly class="form-control" id="nuc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $ultima_matricula_aluno->nome_nucleo; ?>">

						</div>

					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Escola</label>
						<div class="col-sm-5">
							<select name="id_escola" class="form-control" id="esc" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
								<option style="display: none;" id="escolaatual" selected value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->id_escola; ?>"><?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->nome_escola; ?></option>
								<?php
								$query3 = $this->db->get('escola_nucleo');
								$escolas = $query3->result();

								foreach ($escolas as $esc) {
									if ($id_nuc == $esc->id_nucleo) {
										echo '<option value=' . $esc->id_escola . '>' . $esc->nome_escola . '</option>';
									}
								}
								?>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Turma atual</label>
						<div class="col-sm-5">
							<input name="class_id" readonly class="form-control" id="turm" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $ultima_matricula_aluno->nome_turma; ?>">

						</div>
					</div>

					<div class="form-group" style="display: none;">
						<label for="field-1" class="col-sm-3 control-label">Sessão</label>
						<div class="col-sm-5">
							<select name="section_id" class="form-control" id="seca" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" onchange="return get_class_sections(this.value)">
								<option disabled="" selected="">selecione um projeto</option>
							</select>
						</div>
					</div>

					<!--<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('class'); ?></label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control" name="class" disabled
								value="<?php echo $this->db->get_where('class', array('class_id' => $row['class_id']))->row()->name; ?>">
						</div>
					</div>-->

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('id'); ?></label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="student_code" value="<?php echo $this->db->get_where('student', array(
																									'student_id' => $row['student_id']
																								))->row()->student_code; ?>">
						</div>
					</div>

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Responsável</label>
                        
						<div class="col-sm-5">
							<select name="parent_id" class="form-control select2">
                              <option value=""><?php echo get_phrase('select'); ?></option>
                              <?php
								$parents = $this->db->get('parent')->result_array();
								$parent_id = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->parent_id;
								foreach ($parents as $row3) :
								?>
                                		<option value="<?php echo $row3['parent_id']; ?>"
                                        	<?php if ($row3['parent_id'] == $parent_id) echo 'selected'; ?>>
													<?php echo $row3['name']; ?>
                                                </option>
	                                <?php
								endforeach;
									?>
                          </select>
						</div> 
					</div>-->

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Telefone</label>

						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="phone" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->phone; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Telefone 2</label>

						<div class="col-sm-5">
							<input type="text" class="form-control tel" name="telefone_2" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->telefone_2; ?>" autofocus>
						</div>
					</div>

					<div class="form-group" style="display: none;">
						<label for="field-1" class="col-sm-3 control-label"><?php echo get_phrase('email') . '/' . get_phrase('username'); ?></label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="email" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->email; ?>">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Situação</label>
						<div class="col-sm-5">
							<select name="situacao" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>">
								<?php
								if ($this->db->get_where('student', array('student_id' => $row['student_id']))->row()->situacao == "ATIVO") {
									echo '<option value="ATIVO" selected>ATIVO</option>
											<option value="INATIVO">INATIVO</option>';
								} else {
									echo '<option value="ATIVO">ATIVO</option>
											<option value="INATIVO" selected>INATIVO</option>';
								} ?>
							</select>
						</div>

					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data nascimento</label>
						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_nascimento" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->data_nascimento; ?>" autofocus required id="data_nasc">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome do Pai</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome_pai" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->nome_pai; ?>" autofocus required>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Nome do Mãe</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="nome_mae" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->nome_mae; ?>" autofocus required>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CPF Aluno</label>
						<div class="col-sm-5">
							<input type="text"   class="form-control cpf" id="cpf_aluno" name="cpf_aluno" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->cpf_aluno; ?>" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CPF Responsável</label>
						<div class="col-sm-5">
							<input type="text" class="form-control cpf" name="cpf_responsavel" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->cpf_responsavel; ?>" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">RG Aluno</label>

						<div class="col-sm-5">
							<input type="text" class="form-control rg" id="rg_aluno" name="rg_aluno" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->rg_aluno; ?>" autofocus>
							
						</div>
					</div>



					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">RG Responsável</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="rg_responsavel" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->rg_responsavel; ?>" autofocus>
						</div>
					</div>



					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('birthday'); ?></label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control datepicker" name="birthday" 
								value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->birthday; ?>" 
									data-start-view="2">
						</div> 
					</div>-->

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Sexo</label>

						<div class="col-sm-5">
							<select name="sex" class="form-control selectboxit">
								<?php
								$gender = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->sex;
								?>
								<option value="MASCULINO" <?php if ($gender == 'MASCULINO') echo 'selected'; ?>>MASCULINO</option>
								<option value="FEMININO" <?php if ($gender == 'FEMININO') echo 'selected'; ?>>FEMININO</option>
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">CEP</label>

						<div class="col-sm-5">
							<input type="text" class="form-control cep" name="cep" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->cep; ?>" autofocus required placeholder="Busque pelo CEP">
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Estado</label>

						<div class="col-sm-5">
							<input type="text" id="est" class="form-control" name="estado" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->estado; ?>" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Cidade</label>

						<div class="col-sm-5">
							<input type="text" id="cid" class="form-control" name="cidade" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->cidade; ?>" autofocus required>
						</div>
					</div>



					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Rua</label>

						<div class="col-sm-5">
							<input type="text" id="rua" class="form-control" name="rua" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->rua; ?>" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Bairro</label>

						<div class="col-sm-5">
							<input type="text" id="bairro" class="form-control" name="bairro" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->bairro; ?>" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="numero" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->numero; ?>" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Complemento</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="complemento" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->complemento; ?>" autofocus>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Problemas de Saúde</label>
						<div class="col-sm-5">
							<select name="problema_saude" class="form-control" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" id="problema">
								<?php

								if ($this->db->get_where('student', array('student_id' => $row['student_id']))->row()->problema_saude == "SIM") {
									echo '<option selected value="SIM">SIM</option>
											<option value="NÃO">NÃO</option>';
								} else {
									echo '<option value="SIM">SIM</option>
											<option selected value="NÃO">NÃO</option>';
								}
								?>




							</select>
						</div>

					</div>


					<?php

					if ($this->db->get_where('student', array('student_id' => $row['student_id']))->row()->problema_saude == "SIM") {
						echo '<div class="form-group" id="descri" style="display: block;">
						<label for="field-1" class="col-sm-3 control-label">Descrição do problema</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="descricao_problema" data-validate="required" data-message-required="Valor requerido" value="' . $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->descricao_problema . '" autofocus required>
						</div>
					</div>';
					} else {
						echo '<div class="form-group" id="descri" style="display: none;">
						<label for="field-1" class="col-sm-3 control-label">Descrição do problema</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="descricao_problema" data-validate="required" data-message-required="Valor requerido" value="" autofocus required>
						</div>
					</div>';
					}

					?>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Uniforme</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="uniforme" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->uniforme; ?>" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número do tênis</label>

						<div class="col-sm-5">
							<input type="text" class="form-control nt" name="numero_tenis" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->numero_tenis; ?>" autofocus>
						</div>
					</div>




					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Número de pessoas no domicílio</label>

						<div class="col-sm-5">
							<input type="text" class="form-control" name="numero_pessoas_domicilio" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->numero_pessoas_domicilio; ?>" autofocus required maxlength="3" onkeypress="if (!isNaN(String.fromCharCode(window.event.keyCode))) return true; else return false;">
						</div>
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Principal responsável pelo sustento</label>

						<div class="col-sm-5">
							<select name="responsavel_sustento" class="form-control selectboxit">
								<?php

								$respons =  $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->responsavel_sustento;


								if ($respons == "O PRÓPRIO") {



									echo '                    
<option selected value="O PRÓPRIO">O PRÓPRIO</option>
<option value="PAI">PAI</option>
                              <option value="MÃE">MÃE</option>
                              <option value="CASAL">CASAL</option>
                              <option value="PARENTE">PARENTE</option>';
								}

								if ($respons == "PAI") {
									echo '                    
 <option value="O PRÓPRIO">O PRÓPRIO</option>
	<option value="PAI" selected>PAI</option>
                              <option value="MÃE">MÃE</option>
                              <option value="CASAL">CASAL</option>
                              <option value="PARENTE">PARENTE</option>';
								}

								if ($respons == "MÃE") {
									echo '                    
 <option value="O PRÓPRIO">O PRÓPRIO</option>
	<option value="PAI" >PAI</option>
                              <option value="MÃE" selected>MÃE</option>
                              <option value="CASAL">CASAL</option>
                              <option value="PARENTE">PARENTE</option>';
								}

								if ($respons == "CASAL") {
									echo '                    
 <option value="O PRÓPRIO">O PRÓPRIO</option>
	<option value="PAI" >PAI</option>
                              <option value="MÃE">MÃE</option>
                              <option value="CASAL" selected>CASAL</option>
                              <option value="PARENTE">PARENTE</option>';
								}

								if ($respons == "PARENTE") {
									echo '                    
 <option value="O PRÓPRIO">O PRÓPRIO</option>
	<option value="PAI" >PAI</option>
                              <option value="MÃE">MÃE</option>
                              <option value="CASAL">CASAL</option>
                              <option value="PARENTE" selected>PARENTE</option>';
								}


								?>


							</select>
						</div>
					</div>


					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Renda familiar</label>

						<div class="col-sm-5">
							<select name="renda_familiar" class="form-control selectboxit">
								<?php

								$renda =  $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->renda_familiar;


								if ($renda == "SEM RENDA") {

									echo '     <option value="SEM RENDA" selected>SEM RENDA</option>
<option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}


								if ($renda == "ATÉ R$ 500,00") {


									echo ' <option value="SEM RENDA" >SEM RENDA</option>
		<option selected value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}

								if ($renda == "R$ 501,00 ATÉ R$ 800,00") {
									echo ' <option value="SEM RENDA" >SEM RENDA</option>
	<option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option selected value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}

								if ($renda == "R$ 801,00 ATÉ R$ 1000,00") {

									echo ' <option value="SEM RENDA" >SEM RENDA</option>
	<option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option  value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option selected value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}

								if ($renda == "R$ 1001,00 ATÉ R$ 1500,00") {

									echo ' <option value="SEM RENDA" >SEM RENDA</option>
	<option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option  value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option selected value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}

								if ($renda == "R$ 1501,00 ATÉ R$ 2000,00") {

									echo ' <option value="SEM RENDA" >SEM RENDA</option>
	<option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option  value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option selected value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}

								if ($renda == "R$ 2001,00 ATÉ R$ 2500,00") {

									echo ' <option value="SEM RENDA" >SEM RENDA</option>
	<option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option  value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option selected value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}

								if ($renda == "MAIOR QUE R$ 2501,00") {

									echo ' <option value="SEM RENDA" >SEM RENDA</option>
<option value="ATÉ R$ 500,00">ATÉ R$ 500,00</option>
                              <option  value="R$ 501,00 ATÉ R$ 800,00">R$ 501,00 ATÉ R$ 800,00</option>
                              <option value="R$ 801,00 ATÉ R$ 1000,00">R$ 801,00 ATÉ R$ 1000,00</option>
                              <option value="R$ 1001,00 ATÉ R$ 1500,00">R$ 1001,00 ATÉ R$ 1500,00</option>
                               <option value="R$ 1501,00 ATÉ R$ 2000,00">R$ 1501,00 ATÉ R$ 2000,00</option>
                              <option value="R$ 2001,00 ATÉ R$ 2500,00">R$ 2001,00 ATÉ R$ 2500,00</option>
                              <option selected value="MAIOR QUE R$ 2501,00">MAIOR QUE R$ 2501,00</option>';
								}


								?>





							</select>
						</div>
					</div>


					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label">Tem parente que trabalha na patrocinadora</label>

						<div class="col-sm-5">
							<select name="parente_trabalha_patrocinadora" class="form-control selectboxit">

								<?php

								$parente = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->parente_trabalha_patrocinadora;

								if ($parente == "SIM") {
									echo ' <option value="SIM" selected>SIM</option>
                              <option value="NÃO">NÃO</option>   ';
								} else {

									echo ' <option value="SIM">SIM</option>
                              <option selected value="NÃO">NÃO</option>   ';
								}

								?>


							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data em que o aluno entrou no projeto</label>

						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_aluno_projeto" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php
																																																	echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->data_aluno_projeto; ?>" autofocus required>
						</div>
					</div>


					<div class="form-group">
						<label for="field-1" class="col-sm-3 control-label">Data de matrícula na <?php echo $turma->name; ?></label>

						<div class="col-sm-5">
							<input type="date" class="form-control" name="data_matricula_turma" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php
																																																	echo date('Y-m-d', $matricula_turma->date_added); ?>" autofocus required>



							<input type="hidden" class="form-control" name="enroll_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" value="<?php echo $matricula_turma->enroll_id; ?>">

						</div>
					</div>

					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('address'); ?></label>
                        
						<div class="col-sm-5">
							<input type="text" class="form-control" name="address" 
								value="<?php echo $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->address; ?>" >
						</div> 
					</div>-->



					<!--<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('dormitory'); ?></label>
                        
						<div class="col-sm-5">
							<select name="dormitory_id" class="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select'); ?></option>
	                              <?php
									$dorm_id = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->dormitory_id;
									$dormitories = $this->db->get('dormitory')->result_array();
									foreach ($dormitories as $row2) :
									?>
                              		<option value="<?php echo $row2['dormitory_id']; ?>"
                              			<?php if ($dorm_id == $row2['dormitory_id']) echo 'selected'; ?>><?php echo $row2['name']; ?></option>
                          		<?php endforeach; ?>
                          </select>
						</div> 
					</div>

					<div class="form-group">
						<label for="field-2" class="col-sm-3 control-label"><?php echo get_phrase('transport_route'); ?></label>
                        
						<div class="col-sm-5">
							<select name="transport_id" class="form-control selectboxit">
                              <option value=""><?php echo get_phrase('select'); ?></option>
	                              <?php
									$trans_id = $this->db->get_where('student', array('student_id' => $row['student_id']))->row()->transport_id;
									$transports = $this->db->get('transport')->result_array();
									foreach ($transports as $row2) :
									?>
                              		<option value="<?php echo $row2['transport_id']; ?>"
                              			<?php if ($trans_id == $row2['transport_id']) echo 'selected'; ?>><?php echo $row2['route_name']; ?></option>
                          		<?php endforeach; ?>
                          </select>
						</div> 
					</div>-->

					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-5">
							<button type="submit" class="btn btn-info" id="submit">Atualizar aluno</button>
						</div>
					</div>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>

<?php
endforeach;
?>



<script type="text/javascript">

	$("#pro").change(function() {

		var id_projeto = $("#pro").val();

		$.ajax('<?php echo site_url('admin/get_nucleo_projeto/'); ?>', {
			type: 'POST', // http method
			data: {
				id_projeto: id_projeto
			}, // data to submit
			success: function(data, status, xhr) {

				$("#nuc").empty();
				data = $.parseJSON(data);


				for (var i = 0; i < data.length; i++) {

					$("#nuc").append('<option value=' + data[i]['id_nucleo'] + '>' + data[i]['nome_nucleo'] + '</option>');
				}






				var id_nucleo = $("#nuc").val();


				$.ajax('<?php echo site_url('admin/get_escola_nucleo2/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: id_nucleo
					}, // data to submit
					success: function(data2, status, xhr) {

						$("#esc").empty();
						data2 = $.parseJSON(data2);



						if ((data2 == null) | (data2 == '')) {
							$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
						} else {

							for (var i = 0; i < data2.length; i++) {

								$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
							}

						}


					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});



				$.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: id_nucleo
					}, // data to submit
					success: function(data2, status, xhr) {

						$("#turm").empty();
						data2 = $.parseJSON(data2);



						if ((data2 == null) | (data2 == '')) {
							$("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada</option>');
						} else {

							for (var i = 0; i < data2.length; i++) {
								$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');

							}

						}




						var turma = $("#turm").val();

						$.ajax('<?php echo site_url('admin/get_secao_turma2/'); ?>', {
							type: 'POST', // http method
							data: {
								dados: turma
							}, // data to submit
							success: function(data, status, xhr) {



								$("#seca").empty();


								data = $.parseJSON(data);
								if ((data == null) | (data == '')) {
									$("#seca").append('<option value="" selected disabled>Nenhuma sessão encontrada</option>');
								} else {

									for (var i = 0; i < data.length; i++) {

										$("#seca").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_secao'] + '</option>');
									}

								}


							},
							error: function(jqXhr, textStatus, errorMessage) {
								alert("erro");
							}
						});


						var id_turma = $("#turm").val();

						$.ajax({
							url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
							success: function(response) {
								jQuery('#section_selector_holder').html(response);
							}
						});


					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert(errorMessage);
					}
				});







			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});


	});





	$("#nuc").change(function() {


		var id_nucleo = $("#nuc").val();


		$.ajax('<?php echo site_url('admin/get_escola_nucleo2/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo
			}, // data to submit
			success: function(data2, status, xhr) {



				$("#esc").empty();
				data2 = $.parseJSON(data2);

				if ((data2 == null) | (data2 == '')) {
					$("#esc").append('<option value="" selected disabled>Nenhuma escola encontrada</option>');
				} else {

					for (var i = 0; i < data2.length; i++) {

						$("#esc").append('<option value=' + data2[i]['id_escola'] + '>' + data2[i]['nome_escola'] + '</option>');
					}
				}

			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});



		$.ajax('<?php echo site_url('admin/get_turma_nucleo/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: id_nucleo
			}, // data to submit
			success: function(data2, status, xhr) {

				$("#turm").empty();
				data2 = $.parseJSON(data2);



				if ((data2 == null) | (data2 == '')) {
					$("#turm").append('<option value="" selected disabled>Nenhuma cidade encontrada</option>');
				} else {

					for (var i = 0; i < data2.length; i++) {


						$("#turm").append('<option value=' + data2[i]['id_turma'] + '>' + data2[i]['nome_turma'] + '</option>');

					}

				}


				var turma = $("#turm").val();

				$.ajax('<?php echo site_url('admin/get_secao_turma2/'); ?>', {
					type: 'POST', // http method
					data: {
						dados: turma
					}, // data to submit
					success: function(data, status, xhr) {



						$("#seca").empty();


						data = $.parseJSON(data);

						if ((data == null) | (data == '')) {
							$("#seca").append('<option value="" selected disabled>Nenhuma sessão encontrada</option>');
						} else {


							for (var i = 0; i < data.length; i++) {

								$("#seca").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_secao'] + '</option>');
							}

						}


					},
					error: function(jqXhr, textStatus, errorMessage) {
						alert("erro");
					}
				});


				var id_turma = $("#turm").val();

				$.ajax({
					url: '<?php echo site_url('admin/get_class_section/'); ?>' + id_turma,
					success: function(response) {
						jQuery('#section_selector_holder').html(response);
					}
				});


			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert(errorMessage);
			}
		});




	});



	// $( "#est" ).change(function() {

	//      var estado = $("#est").val();

	//      $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
	//     type: 'POST',  // http method
	//     data: { dados: estado },  // data to submit
	//     success: function (data, status, xhr) {



	//     		 $("#cid").empty();
	//     	 data = $.parseJSON(data);

	//     	  if((data == null)|(data == '')){
	//     	 	$("#cid").append('<option value="" selected disabled>Nenhuma cidade encontrada</option>');
	//     	 }else{

	//       for (var i = 0; i < data.length; i++) {

	//    $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
	// }

	// }


	//     },
	//     error: function (jqXhr, textStatus, errorMessage) {
	//            alert("erro");
	//     }
	// });

	// });



	$("#turm").change(function() {

		var turma = $("#turm").val();

		$.ajax('<?php echo site_url('admin/get_secao_turma2/'); ?>', {
			type: 'POST', // http method
			data: {
				dados: turma
			}, // data to submit
			success: function(data, status, xhr) {



				$("#seca").empty();


				data = $.parseJSON(data);

				if ((data == null) | (data == '')) {
					$("#seca").append('<option value="" selected disabled>Nenhuma sessão encontrada</option>');
				} else {


					for (var i = 0; i < data.length; i++) {

						$("#seca").append('<option value=' + data[i]['id'] + '>' + data[i]['nome_secao'] + '</option>');
					}

				}


			},
			error: function(jqXhr, textStatus, errorMessage) {
				alert("erro");
			}
		});

	});




	$("#problema").change(function() {

		var problema = $("#problema").val();


		if (problema == "SIM") {

			$("#descri").css('display', 'block');
		}

		if (problema == "NÃO") {
			$("#descri").css('display', 'none');
		}

	});



	// $(document).ready(function(){



	//   var estado = $("#est").val();
	//          var cidadeatual = $("#cidadeatual").val();




	//      $.ajax('<?php echo site_url('admin/get_cidade_estado/'); ?>', {
	//     type: 'POST',  // http method
	//     data: { dados: estado },  // data to submit
	//     success: function (data, status, xhr) {



	//          //$("#cid").empty();
	//        data = $.parseJSON(data);
	//         //alert(data);

	//       for (var i = 0; i < data.length; i++) {

	//         if(data[i]['id'] == cidadeatual){
	// $("#cid").append('<option selected value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
	//         }else{ 

	//    $("#cid").append('<option value='+data[i]['id']+'>'+data[i]['nome_cidade']+'</option>');
	// }
	// }


	//     },
	//     error: function (jqXhr, textStatus, errorMessage) {
	//            alert("erro");
	//     }
	// });

	// });



	$('#data_nasc').change(function(event) {


		var data = $('#data_nasc').val();

		var data_nova = new Date(data);



		var data_atual = new Date();



		var resultado = data_atual.getFullYear() - data_nova.getFullYear();



		if (resultado <= 2) {
			alert('Data de nascimento inválida!');
			$('#submit').attr('disabled', 'disabled');
		} else {
			$('#submit').removeAttr('disabled');
		}


	});
</script>

<script type="text/javascript">
	jQuery(window).ready(function() {
		var SPMaskBehavior = function(val) {
				return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
			},
			spOptions = {
				onKeyPress: function(val, e, field, options) {
					field.mask(SPMaskBehavior.apply({}, arguments), options);
				}
			};
		$('[name="cpf"]').mask('000.000.000-00', {
			reverse: true
		});
		$('[name="cell"]').mask(SPMaskBehavior, spOptions);
		$('[name="cep"]').mask('00000-000');

		$('[name="cep"]').blur(function() {

			//Nova variável "cep" somente com dígitos.
			var cep = $(this).val().replace(/\D/g, '');

			//Verifica se campo cep possui valor informado.
			if (cep != "") {
				//Expressão regular para validar o CEP.
				var validacep = /^[0-9]{8}$/;
				//Valida o formato do CEP.
				if (validacep.test(cep)) {

					//Preenche os campos com "..." enquanto consulta webservice.
					$("#rua").val("Procurando...");
					$("#bairro").val("Procurando...");
					$("#cid").val("Procurando...");
					$("#est").val("Procurando...");
					$("#uf").val("Procurando...");
					$("#estado").val("Procurando...");

					//Consulta o webservice viacep.com.br/
					$.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function(dados) {

						if (!("erro" in dados)) {

							//Atualiza os campos com os valores da consulta.
							$("#rua").val(dados.logradouro);
							$("#bairro").val(dados.bairro);
							$("#cid").val(dados.localidade);
							$("#uf").val(dados.uf);
							$("#est").val(dados.uf);


						} else {
							//CEP pesquisado não foi encontrado.
							limpa_formulario_cep();
							alert("CEP não encontrado.");
						}
					});
				} else {
					//cep é inválido.
					limpa_formulario_cep();
					alert("Formato de CEP inválido.");
				}
			} else {
				//cep sem valor, limpa formulário.
				limpa_formulario_cep();
			}
		});

		function limpa_formulario_cep() {
			// Limpa valores do formulário de cep.
			$("#rua").val("");
			$("#bairro").val("");
			$("#cid").val("");
			$("#est").val("");
			$("#uf").val("");
			$("#cep").val("");
		}
	});
</script>