<div class="row">
    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Adicionar plano de aula</h3>
            </div>
            <div class="panel-body">
                <?php echo form_open(site_url('teacher/class_routine/create'), array('class' => 'form-horizontal form-groups validate', 'target' => '_top')); ?>






                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-5">
                        <select name="class_id[]" class="form-control" id="class_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" multiple>
                            <option selected disabled>Selecione</option>
                            <?php

                            $id_teacher = $this->session->userdata('login_user_id');
                            $query3 = $this->db->where("teacher_id", $id_teacher)->get('class');
                            $turma = $query3->result();

                            $tama = count($turma);
                            for ($i = 0; $i < $tama; $i++) {

                                echo '<option value=' . $turma[$i]->class_id . '>' . $turma[$i]->name . '</option>';
                            }



                            ?>

                        </select>
                        <div style="margin-top:10px;">Segure a tecla Ctrl e selecione mais de uma Turma!</div>
                    </div>

                </div>







                <div id="section_subject_selection_holder"></div>

                <!--<div class="form-group">
                        <label class="col-sm-3 control-label">Dia</label>
                        <div class="col-sm-5">
                            <select name="day" class="form-control selectboxit" style="width:100%;">
                                <option value="sunday">Domingo</option>
                                <option value="monday">Segunda-feira</option>
                                <option value="tuesday">Terça-feira</option>
                                <option value="wednesday">Quarta-feira</option>
                                <option value="thursday">Quinta-feira</option>
                                <option value="friday">Sexta-feira</option>
                                <option value="saturday">Sábado</option>
                            </select>
                        </div>
                    </div>-->

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Atividade</label>
                    <div class="col-sm-5">
                        <select name="subject_id[]" class="form-control" id="subject_id" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" multiple>

                            <option selected disabled>Selecione</option>
                            <?php
                            $id_teacher2 = $this->session->userdata('login_user_id');
                            $query3 = $this->db->where("teacher_id", $id_teacher2)->get('subject');
                            $atividade = $query3->result();

                            $tama = count($atividade);
                            for ($i = 0; $i < $tama; $i++) {

                                echo '<option value=' . $atividade[$i]->subject_id . '>' . $atividade[$i]->name . '</option>';
                            }



                            ?>

                        </select>
                        <div style="margin-top:10px;">Segure a tecla Ctrl e selecione mais de uma Atividade!</div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Data</label>
                    <div class="col-sm-5">

                        <input type="text" id="data" class="form-control datepicker" name="data" data-format="dd-mm-yyyy" value="" placeholder="Selecione uma data" required="" data-validate="required" data-message-required="<?php echo get_phrase('value_required'); ?>" />
                    </div>
                </div>
                <!--<div class="form-group">
                        <label class="col-sm-3 control-label">Hora de começo</label>
                        <div class="col-sm-9">
                            <div class="col-md-3">
                                <select name="time_start" id= "starting_hour" class="form-control selectboxit">
                                    <option value="">Hora</option>
                                    <?php for ($i = 0; $i <= 12; $i++) : ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="time_start_min" id= "starting_minute" class="form-control selectboxit">
                                    <option value="">Minutos</option>
                                    <?php for ($i = 0; $i <= 11; $i++) : ?>
                                        <option value="<?php echo $i * 5; ?>"><?php echo $i * 5; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="starting_ampm" class="form-control selectboxit">
                                    <option value="1">am</option>
                                    <option value="2">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Hora de término</label>
                        <div class="col-sm-9">
                            <div class="col-md-3">
                                <select name="time_end" id= "ending_hour" class="form-control selectboxit">
                                    <option value="">Hora</option>
                                    <?php for ($i = 0; $i <= 12; $i++) : ?>
                                        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="time_end_min" id= "ending_minute" class="form-control selectboxit">
                                    <option value="">Minutos</option>  
                                    <?php for ($i = 0; $i <= 11; $i++) : ?>
                                        <option value="<?php echo $i * 5; ?>"><?php echo $i * 5; ?></option>
                                    <?php endfor; ?>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <select name="ending_ampm" class="form-control selectboxit">
                                    <option value="1">am</option>
                                    <option value="2">pm</option>
                                </select>
                            </div>
                        </div>
                    </div>-->
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" id="add_class_routine" class="btn btn-info">Adicionar plano de aula</button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>


    </div>
</div>


<script type="text/javascript">
    var class_id = '';
    var starting_hour = '';
    var starting_minute = '';
    var ending_hour = '';
    var ending_minute = '';
    jQuery(document).ready(function($) {
        //$('#add_class_routine').attr('disabled','disabled')
    });

    function get_class_section_subject(class_id) {
        $.ajax({
            url: '<?php echo site_url('teacher/get_class_section_subject/'); ?>' + class_id,
            success: function(response) {
                jQuery('#section_subject_selection_holder').html(response);
            }
        });
    }

    function check_validation() {
        console.log('class_id: ' + class_id + ' starting_hour:' + starting_hour + ' starting_minute: ' + starting_minute + ' ending_hour: ' + ending_hour + ' ending_minute: ' + ending_minute);
        if (class_id !== '' && starting_hour !== '' && starting_minute !== '' && ending_hour !== '' && ending_minute !== '') {
            $('#add_class_routine').removeAttr('disabled');
        }
    }
    $('#class_id').change(function() {
        class_id = $('#class_id').val();
        check_validation();
    });
    $('#starting_hour').change(function() {
        starting_hour = $('#starting_hour').val();
        check_validation();
    });
    $('#starting_minute').change(function() {
        starting_minute = $('#starting_minute').val();
        check_validation();
    });
    $('#ending_hour').change(function() {
        ending_hour = $('#ending_hour').val();
        check_validation();
    });
    $('#ending_minute').change(function() {
        ending_minute = $('#ending_minute').val();
        check_validation();
    });


    $('#data').change(function() {


        var data = $('#data').val();
        //data2 = String(data);
        //var date = new Date(data2);
        //alert(data2);

        split = data.split('-');
        novadata = split[1] + "/" + split[0] + "/" + split[2];
        data_input = new Date(novadata);


        const atual = new Date();
        atual.setHours(0);
        atual.setMinutes(0);
        atual.setMilliseconds(0);
        atual.setSeconds(0);


        var ano = data_input.getFullYear();




        /*if (data_input.getTime() === atual.getTime()) {
  
      $('#add_class_routine').removeAttr('disabled');
}
else if (data_input.getTime() > atual.getTime()) {

    // Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
    //alert(data_input.toString() + ' maior que ' + atual.toString());
   $('#add_class_routine').removeAttr('disabled');

}
else {

 $('#add_class_routine').attr('disabled', 'disabled');
      
    //alert(data_input.toString() + ' menor que ' + atual.toString());
}*/







    });
</script>