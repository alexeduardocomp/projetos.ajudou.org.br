<div class="row">
	<div class="col-md-12">
		<blockquote class="blockquote-blue">
			<p>
				<strong>Instruções para a exclusão de frequência</strong>
			</p>
			<p>
				<strong>Atenção!</strong>
				Antes de confirmar a exclusão da frequência, verifique se escolheu a turma e o dia corretos, pois esta ação não poderá ser desfeita.
				<br>
				Após confirmar a exclusão, o sistema apagará as frequência de todos os alunos na <strong>Turma e Data</strong> selecionados.
			</p>
		</blockquote>
	</div>
</div>
<?php echo form_open(site_url('teacher/apagar_frequencia_post/')); ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Apagar frequência</h3>
	</div>
	<div class="panel-body">
		<div class="row">

			<?php
			$query = $this->db->get('class');
			$id_teacher = $this->session->userdata('login_user_id');
			$classes = $this->db->where("teacher_id", $id_teacher)->get('class')->result_array();
			?>

			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Turma</label>
					<select class="form-control selectboxit" name="class_id" onchange="select_section(this.value)">
						<option value="">Selecione a turma</option>
						<?php foreach ($classes as $row) : ?>
							<option value="<?php echo $row['class_id']; ?>"><?php echo $row['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="col-md-3">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Data</label>
					<input type="text" class="form-control datepicker" id="data" name="timestamp" data-format="dd-mm-yyyy" value="Selecione uma data" />
				</div>
			</div>

			<div class="col-md-3" style="margin-top: 20px;">
				<button type="submit" class="btn btn-info" id="submit">Apagar frequência</button>
			</div>
		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$('#data').change(function() {

		var data = $('#data').val();
		//data2 = String(data);
		//var date = new Date(data2);
		//alert(data2);

		split = data.split('-');
		novadata = split[1] + "/" + split[0] + "/" + split[2];
		data_input = new Date(novadata);


		const atual = new Date();
		atual.setHours(0);
		atual.setMinutes(0);
		atual.setMilliseconds(0);
		atual.setSeconds(0);


		var ano = data_input.getFullYear();
		//alert(ano);

		if (ano < 1970) {

			alert('Valor de data não é permitido');
			$('#submit').attr('disabled', 'disabled');
		} else {

			if (data_input.getTime() === atual.getTime()) {
				//alert('As datas são iguais');
				$('#submit').removeAttr('disabled');
			} else if (data_input.getTime() > atual.getTime()) {

				// Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
				//alert(data_input.toString() + ' maior que ' + atual.toString());
				$('#submit').attr('disabled', 'disabled');

			} else {

				$('#submit').removeAttr('disabled');
				//alert(data_input.toString() + ' menor que ' + atual.toString());
			}

		}

	});
</script>