<?php
    $class_name    =   $this->db->get_where('class' , array('class_id' => $class_id))->row()->name;
    //$section_name  =   $this->db->get_where('section' , array('section_id' => $section_id))->row()->name;
    $system_name   =   $this->db->get_where('settings' , array('type'=>'system_name'))->row()->description;
    $running_year  =   $this->db->get_where('settings' , array('type'=>'running_year'))->row()->description;
?>


<div id="print">

    <script src="assets/js/jquery-1.11.0.min.js"></script>
    <style type="text/css">
        td {
            padding: 5px;
        }
    </style>

    <center>
        <img src="<?php echo base_url(); ?>uploads/logo.png" style="max-height : 60px;"><br>
        <h3 style="font-weight: 100;"><?php echo $system_name;?></h3>
        Rotina da Turma<br>
        <?php echo 'Turma ' . $class_name;?><br>

         <?php

        $datas = $_GET['dataini'];
        $data_fim = $_GET['datafim'];

        $dat = date($datas);

//         $diasemana = array('DOMINGO','SEGUNDA','TERÇA','QUARTA','QUINTA','SEXTA','SÁBADO');

// $dia_semana_numero = date('w',strtotime($dat));

// $dia_correto = $diasemana[$dia_semana_numero];

         ?>
    </center>
    <br>
    <table style="width:100%; border-collapse:collapse;border: 1px solid #eee; margin-top: 10px;" border="1">
        <tbody>
           
                 <?php

                        //$this->db->order_by("time_start", "asc");
                        //$this->db->where('day' , $day);
                        $this->db->where('class_id' , $class_id);
                        //$this->db->where('section_id' , $section_id);
                         $this->db->where("data_atividade BETWEEN '{$datas}' AND '{$data_fim}'");
                        
                         $this->db->order_by("data_atividade", "ASC");
                        $this->db->where('year' , $running_year);

            

                        $routines   =   $this->db->get('class_routine')->result_array();

                        $vetor = array();
                        $cont = 0;

                        foreach ($routines as $key) {
                         $vetor[$cont] = $key;
                         $cont++;
                        }

                        // var_dump($vetor);

                        
                           
                      
                  
                         $datas_selecionadas = $this->ajaxload->get_datas_periodo($class_id, $datas, $data_fim);

                        foreach ($datas_selecionadas as $dt) {
                    
                       echo '<tr>';
                        echo '<td style="padding-left:10px; width:10%; text-align:center;" ><b>';
                         $data = date('d-m-Y', strtotime($dt->data_atividade));
                        echo $data;
                        echo " : </b></td>";
                        echo '<td style="padding:20px;">';



                        for ($i=0; $i < count($vetor) ; $i++) { 

                         

                             if($vetor[$i]['data_atividade'] == $dt->data_atividade){

                                ?>

<div style="float:left; padding:8px; margin:5px; background-color:#ccc;">
                                <?php echo $this->crud_model->get_subject_name_by_id($vetor[$i]['subject_id']);?>
                                <?php
                                    if ($vetor[$i]['time_start_min'] == 0 && $vetor[$i]['time_end_min'] == 0) 
                                        echo '('.$vetor[$i]['time_start'].'-'.$vetor[$i]['time_end'].')';
                                    if ($vetor[$i]['time_start_min'] != 0 || $vetor[$i]['time_end_min'] != 0)
                                        echo '('.$vetor[$i]['time_start'].':'.$vetor[$i]['time_start_min'].'-'.$vetor[$i]['time_end'].':'.$vetor[$i]['time_end_min'].')';
                                ?>
                            </div>
                       
                         <?php

                         }else{
                            
                         } }?>
                    <?php

 echo "</td>";
                        echo '</tr>';
                     } ?>
                 
              
        </tbody>
   </table>

<br>

</div>


<script type="text/javascript">

    alert('Para imprimir pressione: CTRL + P');

    jQuery(document).ready(function($)
    {
        var elem = $('#print');
        PrintElem(elem);
        Popup(data);

    });

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var mywindow = window.open('', 'my div', 'height=400,width=600');
        mywindow.document.write('<html><head><title></title>');
        //mywindow.document.write('<link rel="stylesheet" href="assets/css/print.css" type="text/css" />');
        mywindow.document.write('</head><body >');
        //mywindow.document.write('<style>.print{border : 1px;}</style>');
        mywindow.document.write(data);
        mywindow.document.write('</body></html>');

        mywindow.document.close(); // necessary for IE >= 10
        mywindow.focus(); // necessary for IE >= 10

        mywindow.print();
        mywindow.close();

        return true;
    }
</script>