<hr />
<div class="row">
    <div class="col-md-12">
        <blockquote class="blockquote-blue">
            <p>
                <strong>Instruções para a Troca de Turma do Aluno</strong>
            </p>
            <p>
                <!--Promoting student from the present class to the next class will create an enrollment of that student to
                the next session. Make sure to select correct class options from the select menu before promoting.If you don't want
                to promote a student to the next class, please select that option. That will not promote the student to the next class
                but it will create an enrollment to the next session but in the same class.-->

                A troca de turma do aluno da turma atual para a próxima turma criará uma matrícula desse aluno para a outra turma. Certifique-se de selecionar as opções corretas da turma no menu de seleção antes de trocar.

            </p>
        </blockquote>
    </div>
</div>
<?php echo form_open(site_url('teacher/trocar_aluno_turma/promote')); ?>
<div class="row">
    <?php

    $id_teacher = $this->session->userdata('login_user_id');


    $query = $this
        ->db
        ->where("teacher_id", $id_teacher)->get('class');


    $classes = $query->result_array();


    $running_year_array             = explode("-", $running_year);
    $next_year_first_index          = $running_year_array[1];
    $next_year_second_index         = $running_year_array[1] + 1;
    $next_year                      = $next_year_first_index . "-" . $next_year_second_index;

    $query = $this
        ->db
        ->order_by('projeto.nome', 'ASC')
        ->get('projeto');
        $projetos = $query->result();

    ?>

    <div class="col-md-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Informações para troca de Turma</h3>
            </div>
            <div class="panel-body">
                <div class="row">

                    <input type="hidden" name="promotion_year" id="promotion_year" value="<?php echo $running_year ?>">

                    <div class="col-md-3" >
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Projeto</label>
                            <div class="col-sm-12">

                                <select name="pro" id="pro" class="form-control" required>
                                    <option value="">Selecione</option>
                                    <?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>

                                </select>
                            </div>
                        </div>                        
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Turma atual</label>
                            <div class="col-sm-12">

                                <select name="turm" id="turm" class="form-control" required>
                                    <option value="">Selecione</option>
                                    

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Aluno</label>
                            <div class="col-sm-12">
                                <select name="student" id="student" class="form-control" required>
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label for="field-1" class="control-label" style="text-align: left;">Data de Matrícula</label>
                                <input type="date" class="form-control" name="data_matricula" id="data_matricula" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" style="margin-top: 10px;">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Trocar para o Projeto</label>
                            <div class="col-sm-12">

                                <select name="pro2" id="pro2" class="form-control" required>
                                    <option value="">Selecione</option>
                                    <?php

									$tama = count($projetos);
											for ($i=0; $i < $tama ; $i++) { 
						
												echo '<option value='.$projetos[$i]->id.'>'.$projetos[$i]->nome.'</option>';

											}

									 ?>

                                </select>
                            </div>
                        </div>                        
                    </div>

                    <div class="col-md-3" style="margin-top: 10px;">
                        <div class="form-group">
                            <label for="field-1" class="col-sm-10 control-label">Trocar para a Turma</label>
                            <div class="col-sm-12">
                                <select name="promotion_to_class_id" id="promotion_to_class_id" class="form-control" required>
                                    <option value="">Selecione</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <center>
                    <button class="btn btn-info" type="submit" style="margin:20px; margin-top:40px;">
                        Trocar de Turma</button>
                </center>

            </div>
        </div>
    </div>

</div>

<?php echo form_close(); ?>

<script type="text/javascript">
    //SCRIPT


    // Change Projeto
    $("#pro").change(function() {

        $("#nuc").val("");
        $("#nuc").empty();
        $("#turm").val("");
        $("#turm").empty();

        var id_projeto = $("#pro").val(); 

        $.ajax('<?php echo site_url('teacher/get_turma_projeto_instrutor/'); ?>', {
            type: 'POST',  // http method
            data: { dados: id_projeto },  // data to submit
            success: function (data2, status, xhr) {
                $("#turm").empty();
                data2 = $.parseJSON(data2);
                var cont = 0;

                if((data2 == null)|(data2 == '')){
                    $("#turm").append('<option value="" selected disabled>Nenhuma turma encontrada para esse instruto,r no projeto selecionado</option>');
                }else{
                    for (var i = 0; i < data2.length; i++) {  
                        cont = cont + 1                  
                        $("#turm").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
                    }
                }
                if(cont == 1){
                    $("#student").val("");
                    $("#student").empty();

                    var class_id = $("#turm").val();

                    // alert(class_id);

                    $.ajax('<?php echo site_url('admin/get_students_turma/'); ?>', {
                        type: 'POST', // http method
                        data: {
                            class_id: class_id
                        }, // data to submit
                        success: function(data, status, xhr) {
                            data = $.parseJSON(data);
                            // alert(data);

                            for (var i = 0; i < data.length; i++) {
                                $("#student").append('<option value=' + data[i]['student_id'] + '>' + data[i]['name'] + '</option>');
                            }
                        }
                    });
                }
            }
        });
    });


    // Change Trocar para Projeto
    $("#pro2").change(function() {
        
        
        $("#promotion_to_class_id").empty();

        var id_projeto = $("#pro2").val(); 

        $.ajax('<?php echo site_url('teacher/get_turma_projeto_instrutor/'); ?>', {
            type: 'POST',  // http method
            data: { dados: id_projeto },  // data to submit
            success: function (data2, status, xhr) {
                $("#promotion_to_class_id").empty();
                data2 = $.parseJSON(data2);
                if((data2 == null)|(data2 == '')){
                    $("#promotion_to_class_id").append('<option value="" selected disabled>Nenhuma turma encontrada para esse instruto,r no projeto selecionado</option>');
                    
                    
                }else{
                    for (var i = 0; i < data2.length; i++) {                    
                        $("#promotion_to_class_id").append('<option value='+data2[i]['id_turma']+'>'+data2[i]['nome_turma']+'</option>');
                        
                    }
                }
                
            }
        });
    });

    // Change Núcleo
   /*  $("#nuc").change(function() {

        var nucleos = $("#nuc").val();
        var id_projeto = $("#pro").val();
        var id_turma = $("#turm").val();

        $('#assigned_to').empty();
        var clients = $('#clients_select').val();
        var groups = $('#groups_select').val();

        $("#turm").val("");
        $("#turm").empty();

        var url = "<?php echo site_url('admin/get_turma_nucleo_multiple'); ?>" + "?nucleos=" + nucleos + "";
        $.ajaxSetup({
            async: false
        });
        $.get(url, function(data) {
            const json = data;
            const obj = JSON.parse(json);
            $.each(obj, function(key, item) {
                var select = "<option value='" + item.class_id + "'>" + item.name + " - " + item.nome_nucleo + "</option>";
                $('#turm').append(select);
            });
        });
        $.ajaxSetup({
            async: true
        });

    }); */

    // Change Turma
    $("#turm").on('change', function() {
        // alert(1);
        $("#student").val("");
        $("#student").empty();

        var class_id = $("#turm").val();

        // alert(class_id);

        $.ajax('<?php echo site_url('admin/get_students_turma/'); ?>', {
            type: 'POST', // http method
            data: {
                class_id: class_id
            }, // data to submit
            success: function(data, status, xhr) {
                data = $.parseJSON(data);
                // alert(data);

                for (var i = 0; i < data.length; i++) {

                    //   alert(data[i]['name']);

                    $("#student").append('<option value=' + data[i]['student_id'] + '>' + data[i]['name'] + '</option>');
                }
            }
        });
    });
</script>