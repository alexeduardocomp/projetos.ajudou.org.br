<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>




<a href="<?php echo site_url('teacher/student_add'); ?>" class="btn btn-primary pull-right">
    <i class="entypo-plus-circled"></i>
    Adicionar novo aluno
</a>
<br><br><br>

<div class="row">
    <div class="col-md-12">

        <ul class="nav nav-tabs bordered">
            <li class="active">
                <a href="#home" data-toggle="tab">
                    <span class="visible-xs"><i class="entypo-users"></i></span>
                    <span class="hidden-xs">Todos os alunos</span>
                </a>
            </li>
            <?php
            // $query = $this->db->get_where('section' , array('class_id' => $class_id));
            // if ($query->num_rows() > 0):
            //     $sections = $query->result_array();
            //     foreach ($sections as $row):
            ?>
            <!--<li>
                <a href="#<?php echo $row['section_id']; ?>" data-toggle="tab">
                    <span class="visible-xs"><i class="entypo-user"></i></span>
                    <span class="hidden-xs">Seção <?php echo $row['name']; ?> ( <?php echo $row['nick_name']; ?> )</span>
                </a>
            </li>-->
            <?php //endforeach;
            ?>
            <?php //endif;
            ?>
        </ul>
                
        
        <div class="tab-content">
        <div class="" style="margin-top: 15px; margin-bottom: 10px;">
            <select id="running_year" name="running_year" class="form-control" style="width: 20%;">
                <?php $running_year = $this->db->get_where('settings', array('type' => 'running_year'))->row()->description; ?>
                <?php for ($i = 0; $i < 10; $i++) : ?>
                    <option value="<?php echo (2019 + $i); ?>-<?php echo (2019 + $i + 1); ?>" <?php if ($running_year == (2019 + $i) . '-' . (2019 + $i + 1)) echo 'selected'; ?>>
                        Ano : <?php echo (2019 + $i); ?>-<?php echo (2019 + $i + 1); ?>
                    </option>
                <?php endfor; ?>
            </select>
            <input type="hidden" id="id_turma" value="<?php echo $class_id; ?>">
        </div>
            <div id="tabela_alunos"  class="tab-pane active" id="home">

                <table  class="table table-bordered datatable display nowrap table-student">
                    <thead>
                        <tr>
                            <!--
                            <th width="80">
                                <div>Id</div>
                            </th>
                            -->
                            <th>
                                <div>Foto</div>
                            </th>
                            <th>
                                <div>Nome</div>
                            </th>
                            <th class="span3">
                                <div>Telefone</div>
                            </th>
                            <th>
                                <div><?php echo get_phrase('email') . '/ Nome de usuário'; ?></div>
                            </th>
                            <th>
                                <div>Projeto</div>
                            </th>
                            <th>
                                <div>Núcleo</div>
                            </th>
                            <th>
                                <div>Turma</div>
                            </th>
                            <th>
                                <div>Escola</div>
                            </th>
                            <th>
                                <div>Data de Matrícula</div>
                            </th>
                            <th>
                                <div>Opções</div>
                            </th>
                        </tr> 
                    </thead>
                    <tbody >
                        <?php
                        $sql = 'SELECT student.*,enroll.date_added, class.projeto_id, class.nome_projeto, class.nome_nucleo, class.name as nome_turma FROM `student`  
                                INNER JOIN enroll ON student.student_id = enroll.student_id 
                                INNER JOIN class ON enroll.class_id = class.class_id 
                                WHERE enroll.class_id ='.$class_id. ' AND
                                enroll.date_added <= "'.time().'" ORDER BY student.name ASC';
                        $query = $this->db->query($sql);

                        $students_selecionados = $query->result_array();
                       /*  $query =

                        $this->db->select("student.*,enroll.date_added, class.projeto_id, class.nome_projeto, class.nome_nucleo, class.name as nome_turma ")
                        ->from("student")
                        ->join("enroll", "student.student_id = enroll.student_id")
                        ->join("class", "enroll.class_id = class.class_id")
                        ->where('enroll.class_id', $class_id)
                        ->where('enroll.date_added <=', time())
                        ->order_by("student.name", "ASC");
                        $query = $this->db->get();
                        $query->result_array();
                        $students_selecionados  = $query->row(); */

                        $cont = 0;
                        $alunos = array();
                        
                        foreach ($students_selecionados as $student) {
                            //SCRIPT 
                            /* SELECT enroll.* FROM `enroll` WHERE enroll.class_id<> 102 AND '02/03/2020' >= '15/08/2021' AND enroll.student_id= 1474 AND  `date_added` <= '2021/08/16' */
                            $sql = "SELECT enroll.* FROM `enroll` WHERE enroll.class_id<>" . $class_id . " AND `date_added` >= '" . $student['date_added'] . "' AND enroll.student_id=" . $student['student_id'] . " AND  `date_added` <= '" . time() . "'";

                           
                            $query = $this->db->query($sql); 

                            if ($query->num_rows() < 1) {
                                $alunos[$cont]["student_id"] = $student['student_id'];
                                $alunos[$cont]["name"] = $student['name'];
                                $alunos[$cont]["phone"] = $student['phone'];
                                $alunos[$cont]["email"] = $student['email'];
                                $alunos[$cont]["nome_projeto"] = $student['nome_projeto'];
                                $alunos[$cont]["nome_nucleo"] = $student['nome_nucleo'];
                                $alunos[$cont]["nome_turma"] = $student['nome_turma'];
                                $alunos[$cont]["nome_escola"] = $student['nome_escola'];
                                $alunos[$cont]["date_added"] = $student['date_added'];
                                $cont++;
                            }
                        }
                        foreach ($alunos as $row) : ?>
                            <tr>
                                <!--
                                <td><?php echo $this->db->get_where('student', array(
                                        'student_id' => $row['student_id']
                                    ))->row()->student_code; ?>
                                </td>
                                -->
                                <td><img src="<?php echo $this->crud_model->get_image_url('student', $row['student_id']); ?>" class="img-circle" width="30" /></td>
                                <td>
                                    <?php
                                       echo $row['name'];
                                    ?>
                                </td>

                                <td>
                                    <?php
                                        echo $row['phone'];
                                    ?>
                                </td>

                                <td>
                                    <?php
                                        echo $row['email'];
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    echo  $row['nome_projeto'];
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    echo  $row['nome_nucleo'];
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    echo $row['nome_turma'];
                                    ?>
                                </td>

                                <td>
                                    <?php
                                    echo $row['nome_escola'];
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo date('d/m/Y', $row['date_added']);
                                    ?>
                                </td>
                                <td>

                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                            Ações <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                            <!-- STUDENT MARKSHEET LINK  -->
                                            <!--<li>
                                            <a href="<?php echo site_url('teacher/student_marksheet/' . $row['student_id']); ?>">
                                                <i class="entypo-chart-bar"></i>
                                                    Marcar presença
                                                </a>
                                        </li>-->


                                            <!-- STUDENT PROFILE LINK -->
                                            <li>
                                                <a href="<?php echo site_url('teacher/student_profile/' . $row['student_id']); ?>">
                                                    <i class="entypo-user"></i>
                                                    Perfil
                                                </a>
                                            </li>

                                            <!-- STUDENT EDITING LINK -->
                                            <li>
                                                <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_student_edit/' . $row['student_id']) . '/' . $class_id ?>');">
                                                    <i class="entypo-pencil"></i>
                                                    Editar
                                                </a>
                                            </li>
                                            <!--<li>
                                            <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/student_id/' . $row['student_id']); ?>');">
                                                <i class="entypo-vcard"></i>
                                                Gerar id
                                            </a>
                                        </li>-->

                                            <li class="divider"></li>
                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo site_url('teacher/delete_student/' . $row['student_id'] . '/' . $class_id); ?>');">
                                                    <i class="entypo-trash"></i>
                                                    Deletar
                                                </a>
                                            </li>

                                            <li>
                                                <a href="#" onclick="confirm_modal('<?php echo site_url('teacher/remover_aluno_turma/' . $row['student_id'] . '/' . $class_id); ?>');">
                                                    <i class="entypo-cancel-squared"></i>
                                                    Remover da turma
                                                </a>
                                            </li>
                                        </ul>
                                    </div>

                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
            <?php
            $query = $this->db->get_where('section', array('class_id' => $class_id));
            if ($query->num_rows() > 0) :
                $sections = $query->result_array();
                foreach ($sections as $row) :
            ?>
                    <div class="tab-pane" id="<?php echo $row['section_id']; ?>">

                        <table class="table table-bordered datatable">
                            <thead>
                                <tr>
                                    <th width="80">
                                        <div>Id</div>
                                    </th>
                                    <th width="80">
                                        <div>Foto</div>
                                    </th>
                                    <th>
                                        <div>Nome</div>
                                    </th>
                                    <th class="span3">
                                        <div>Telefone</div>
                                    </th>
                                    <th>
                                        <div><?php echo get_phrase('email') . '/ Nome de usuário'; ?></div>
                                    </th>
                                    <th>
                                        <div>Projeto</div>
                                    </th>
                                    <th>
                                        <div>Núcleo</div>
                                    </th>
                                    <th>
                                        <div>Turma</div>
                                    </th>
                                    <th>
                                        <div>Escola</div>
                                    </th>
                                    <th>
                                        <div>Opções</div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $students   =   $this->db->get_where('enroll', array(
                                    'class_id' => $class_id, 'section_id' => $row['section_id'], 'year' => $running_year
                                ))->result_array();
                                foreach ($students as $row) : ?>
                                    <tr>
                                        <td><?php echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->student_code; ?></td>
                                        <td><img src="<?php echo $this->crud_model->get_image_url('student', $row['student_id']); ?>" class="img-circle" width="30" /></td>
                                        <td>
                                            <?php
                                            echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->name;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->phone;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->email;
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->nome_projeto;
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->nome_nucleo;
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->nome_turma;
                                            ?>
                                        </td>

                                        <td>
                                            <?php
                                            echo $this->db->get_where('student', array(
                                                'student_id' => $row['student_id']
                                            ))->row()->nome_escola;
                                            ?>
                                        </td>

                                        <td>

                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                                                    Ações <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- STUDENT MARKSHEET LINK  -->
                                                    <!--<li>
                                            <a href="<?php echo site_url('teacher/student_marksheet/' . $row['student_id']); ?>">
                                                <i class="entypo-chart-bar"></i>
                                                   Marcar presença
                                                </a>
                                        </li>-->

                                                    <!-- STUDENT PROFILE LINK -->
                                                    <li>
                                                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_student_profile/' . $row['student_id']); ?>');">
                                                            <i class="entypo-user"></i>
                                                            Perfil
                                                        </a>
                                                    </li>
                                                    <!--<li>
                                            <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/student_id/' . $row['student_id']); ?>');">
                                                <i class="entypo-vcard"></i>
                                               Gerar id
                                            </a>
                                        </li>-->

                                                    <!-- STUDENT EDITING LINK -->
                                                    <li>
                                                        <a href="#" onclick="showAjaxModal('<?php echo site_url('modal/popup/modal_student_edit/' . $row['student_id'] . '/' . $class_id); ?>');">
                                                            <i class="entypo-pencil"></i>
                                                            Editar
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>

                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>


    </div>
</div> 


<script type="text/javascript">
    $( "#running_year" ).change(function() {  
        var running_year = $("#running_year").val();
        var id = $("#id_turma").val();
        var conteudo_tabela = document.getElementById('tabela_alunos');
        conteudo_tabela.innerHTML = "";

        $.ajax('<?php echo site_url('teacher/get_tabela_alunos_ano/'); ?>', {
            type: 'POST',  // http method
            data: { 
                running_year: running_year,
                id: id,                
            },  // data to submit
            success: function (response) {
                var tabela = response;
                var str = tabela.replace('"', '');
                var string = str.replace(/\\n/g, "")
                                .replace(/\\'/g, "\\'")
                                .replace(/\\"/g, '\\"')
                                .replace(/\\&/g, "\\&")
                                .replace(/\\r/g, "")
                                .replace(/\\/g, "")
                                .replace(/\\t/g, "\\t")
                                .replace(/\\b/g, "\\b")
                                .replace(/\\f/g, "\\f");
                conteudo_tabela.innerHTML = string  ;
            }
        });
    });


    jQuery(document).ready(function($) {    
        var rowCount = $('.table-student tr').length;
        console.log(rowCount);
        if(rowCount == 2){
            $('tbody tr').css('height', '150px');
        }
        $('.datatable').DataTable({
            columnDefs: [
            { orderable: false, targets: [0,9] }
            ],
            "oLanguage": {
                "sProcessing": "Aguarde enquanto os dados são carregados ...",
                "sLengthMenu": "Mostrar _MENU_ registros por pagina",
                "sZeroRecords": "Nenhum registro correspondente ao criterio encontrado",
                "sInfoEmpty": "Exibindo 0 a 0 de 0 registros",
                "sInfo": "Exibindo de _START_ a _END_ de _TOTAL_ registros",
                "sInfoFiltered": "",
                "sSearch": "Procurar",
                "oPaginate": {
                    "sFirst": "Primeiro",
                    "sPrevious": "Anterior",
                    "sNext": "Próximo",
                    "sLast": "Último"
                }
            },

            "scrollX": true,


            dom: "<'row'<'col-lg-4 cols1 cols'l><'#syllas.col-lg-4.cols2.cols'B><'col-lg-4 cols3 cols'f>r>t<'row'<'col-lg-6'i><'col-lg-6'p>>",
            buttons: [

                {
                    extend: 'copy',
                    text: 'Copiar'
                },
                {
                    extend: 'excel',
                    text: 'Excel'
                },
                {
                    extend: 'pdf',
                    text: 'PDF'
                },
                {
                    extend: 'csv',
                    text: 'CSV'
                }
            ]
        });
    });
</script>