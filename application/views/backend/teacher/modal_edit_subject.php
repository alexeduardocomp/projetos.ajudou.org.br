<?php 
$edit_data		=	$this->db->get_where('subject' , array('subject_id' => $param2) )->result_array();
foreach ( $edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" >
            		<i class="entypo-plus-circled"></i>
					Editar Atividade
            	</div>
            </div>
			<div class="panel-body">
                <?php echo form_open(site_url('teacher/subject/do_update/'.$row['subject_id']) , array('class' => 'form-horizontal form-groups-bordered validate','target'=>'_top'));?>
                <div class="form-group">
                    <label class="col-sm-3 control-label">Nome</label>
                    <div class="col-sm-5 controls">
                        <input type="text" class="form-control" name="name" value="<?php echo $row['name'];?>" required/>
                    </div>
                </div>
                <!--<div class="form-group">
                    <label class="col-sm-3 control-label">Turma</label>
                    <div class="col-sm-5 controls">
                        <select name="class_id" class="form-control" required>
                        <option value="" selected disabled>Selecione a turma</option>
                            <?php 

                               $id_teacher = $this->session->userdata('login_user_id');
                $classes = $this->db->where("teacher_id", $id_teacher)->get('class')->result_array();
                            //$classes = $this->db->get('class')->result_array();
                            foreach($classes as $row2):
                            ?>
                                <option value="<?php echo $row2['class_id'];?>"
                                    <?php if($row['class_id'] == $row2['class_id'])echo 'selected';?>>
                                        <?php echo $row2['name'];?>
                                            </option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>-->
                  <div class="form-group">
                                <label class="col-sm-3 control-label">Instrutor</label>
                                <div class="col-sm-5">
                                    <select name="teacher_id" class="form-control selectboxit" style="width:100%;">
                                    
                                        <?php
                                          $id_teacher = $this->session->userdata('login_user_id');
                                           $teachers = $this->db->where("teacher_id", $id_teacher)->get('teacher')->result_array();
                                        //$teachers = $this->db->get('teacher')->result_array();
                                        
                                        foreach($teachers as $row2):
                                        ?>
                                            <option value="<?php echo $row['teacher_id'];?>"><?php echo $row2['name'];?></option>
                                        <?php
                                        endforeach;
                                        ?>
                                    </select>
                                </div>
                            </div>

                            
                 <div class="form-group">
                        <label for="field-2" class="col-sm-3 control-label">Descrição</label>

                        <div class="col-sm-5">
                            <textarea name="descricao" class="form-control" rows="8" cols="80" value=""><?php echo $row['descricao'];?></textarea>
                        </div>
                    </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-info">Editar Atividade</button>
                    </div>
                 </div>
        		</form>
            </div>
        </div>
    </div>
</div>

<?php
endforeach;
?>



