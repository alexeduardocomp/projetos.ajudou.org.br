<div class="sidebar-menu">
    <header class="logo-env">
        <!-- logo -->
        <!-- <div class="logo" style="">
            <a href="<?php echo site_url('login'); ?>">
                <img src="<?php echo base_url('uploads/logo.png'); ?>"  style="max-height:30px;"/>
            </a>
        </div> --> 

        <!-- logo collapse icon -->
        <div class="sidebar-collapse" style="margin-top: 0px;">
            <a href="#" class="sidebar-collapse-icon" onclick="hide_brand()">
                <i class="entypo-menu"></i>
            </a>
        </div>
        <script type="text/javascript">
            function hide_brand() {
                $('#branding_element').toggle();
            }
        </script>

        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs">
            <a href="#" class="with-animation">
                <i class="entypo-menu"></i>
            </a>
        </div>
    </header>
    <ul id="main-menu" class="">
        <!-- add class "multiple-expanded" to allow multiple submenus to open -->
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

        <div style="text-align: -webkit-center;" id="branding_element">
            <img src="<?php echo base_url('uploads/logo.png'); ?>" style="max-height:60px;" />
            <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px; font-size:14px; line-height:20px; margin-top:15px;  font-weight: 300;margin-top: 10px;">
                <?php //echo $system_name;
                ?>
                SMA - Sistema de <br />Monitoramento e Avaliação
            </h4>
        </div>


        <!-- DASHBOARD -->
        <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?> " style="border-top:1px solid #232540;">
            <a href="<?php echo site_url($account_type . '/dashboard'); ?>">
                <i class="flaticon-home-2"></i>
                <span>Painel inicial</span>
            </a>
        </li>

        <!-- STUDENT -->
        <li class="<?php
                    if (
                        $page_name == 'student_add' ||
                        $page_name == 'student_information' ||
                        $page_name == 'student_marksheet' ||
                        $page_name == 'trocar_aluno_turma'
                    )
                        echo 'opened active has-sub';
                    ?> ">
            <a href="#">
                <i class="fa fa-group"></i>
                <span>Alunos</span>
            </a>
            <ul> 


                <!-- STUDENT ADMISSION -->
                <li class="<?php if ($page_name == 'student_add') echo 'active'; ?> ">
                    <a href="<?php echo site_url('teacher/student_add'); ?>">
                        <span><i class="entypo-dot"></i>Cadastrar Aluno</span>
                    </a>
                </li>

                <!-- STUDENT INFORMATION -->
                <li class="<?php if ($page_name == 'student_information' || $page_name == 'student_marksheet' || $page_name == 'student_profile') echo 'opened active'; ?> ">
                    <a href="#">
                        <span><i class="entypo-dot"></i>Informações dos Alunos</span>
                    </a>
                    <ul>
                    <?php
                        $id_teacher = $this->session->userdata('login_user_id');

                        $query =

                        $this->db->select("projeto.*")
                            ->from("projeto")
                            ->join("nucleo", "projeto.id = nucleo.id_projeto")
                            ->join("instrutor_nucleo", "instrutor_nucleo.id_nucleo = nucleo.id")
                            ->where('instrutor_nucleo.id_instrutor', $id_teacher)
                            ->order_by("projeto.nome", "ASC");
                        $query = $this->db->get();
                        $projetos = $query->result_array();
                        $nucleos = $this->db->get('nucleo')->result_array();
                        $classes = $this->db->where("teacher_id", $id_teacher)->get('class')->result_array();
                        foreach ($projetos as $pro){
                            echo "<li>  <a href='#'>
                            <span>";
                                    echo 'Projeto ';
                                    echo $pro['nome'];
                            echo "</span>
                            </a>";

                            echo "<ul>";

                            foreach ($nucleos as $nucleo) {
                                if ($pro['id'] == $nucleo['id_projeto']) {
                                    echo '<li><a href="#">';

                                    echo '<span>';
                                    echo $nucleo['nome_nucleo'];
                                    echo "</span></a>";
                                    echo "<ul>";
                                    foreach ($classes as $clas) {
                                        if ($nucleo['id'] == $clas['id_nucleo']) {
                                            echo '<li><a href="' . site_url("teacher/student_information/" . $clas["class_id"]) . '">';

                                            echo '<span>';
                                            echo $clas['name'];
                                            echo "</span></a></li>";
                                        }
                                    }
                                    echo "</ul>";
                                }
                            }
                            echo "</ul>";
                            echo "</li>";                                
                        }

                        ?>
                        <!-- <?php
                        $id_teacher = $this->session->userdata('login_user_id');
                        $classes = $this->db->where("teacher_id", $id_teacher)->get('class')->result_array();
                        foreach ($classes as $row) :
                        ?>
                            <li class="<?php if ($page_name == 'student_information' && $class_id == $row['class_id']) echo 'active'; ?>">
                                <a href="<?php echo site_url($account_type . '/student_information/' . $row['class_id']); ?>">
                                    <span><?php echo $row['name']; ?></span>
                                </a>
                            </li>
                        <?php endforeach; ?> -->
                    </ul>
                </li>

                <li class="<?php if ($page_name == 'trocar_aluno_turma') echo 'active'; ?> ">
                    <a href="<?php echo site_url('teacher/trocar_aluno_turma'); ?>">
                        <span><i class="entypo-dot"></i>Trocar Aluno de Turma</span>
                    </a>
                </li>

            </ul>
        </li>

        <!-- TEACHER -->
        <!--<li class="<?php if ($page_name == 'teacher') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/teacher_list'); ?>">
                <i class="entypo-users"></i>
                <span>Instrutores</span>
            </a>
        </li>-->


        <!-- SUBJECT -->
        <li class="<?php if ($page_name == 'subject') echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-docs"></i>
                <span>Atividades</span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'subject') echo 'active'; ?> ">
                    <a href="<?php echo site_url('teacher/subject/'); ?>">
                        <span><i class="entypo-dot"></i>
                            Gerenciar Atividades</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- CLASS ROUTINE -->
        <li class="<?php if (
                        $page_name == 'class_routine' ||
                        $page_name == 'class_routine_print_view'
                    )
                        echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-target"></i>
                <span>Plano de Aula</span>
            </a>
            <ul>

                <li class="<?php if ($page_name == 'class_routine_add') echo 'active'; ?> ">
                    <a href="<?php echo site_url('teacher/class_routine_add'); ?>">
                        <span><i class="flaticon-add-circular-button"></i>
                            Adicionar Plano de Aula</span>
                    </a>
                </li>

                <?php
                $id_teacher = $this->session->userdata('login_user_id');

                $query =

                $this->db->select("projeto.*")
                    ->from("projeto")
                    ->join("nucleo", "projeto.id = nucleo.id_projeto")
                    ->join("instrutor_nucleo", "instrutor_nucleo.id_nucleo = nucleo.id")
                    ->where('instrutor_nucleo.id_instrutor', $id_teacher)
                    ->order_by("projeto.nome", "ASC");
                $query = $this->db->get();
                $projetos = $query->result_array();
                $nucleos = $this->db->get('nucleo')->result_array();
                $classes = $this->db->where("teacher_id", $id_teacher)->get('class')->result_array();
                foreach ($projetos as $pro){
                    echo "<li>  <a href='#'>
                    <span>";
                            echo 'Projeto ';
                            echo $pro['nome'];
                    echo "</span>
                    </a>";

                    echo "<ul>";

                    foreach ($nucleos as $nucleo) {
                        if ($pro['id'] == $nucleo['id_projeto']) {
                            echo '<li><a href="#">';

                            echo '<span>';
                            echo $nucleo['nome_nucleo'];
                            echo "</span></a>";
                            echo "<ul>";
                            foreach ($classes as $clas) {
                                if ($nucleo['id'] == $clas['id_nucleo']) {
                                    echo '<li><a href="' . site_url("teacher/class_routine_view/" . $clas["class_id"]) . '">';

                                    echo '<span>';
                                    echo $clas['name'];
                                    echo "</span></a></li>";
                                }
                            }
                            echo "</ul>";
                        }
                    }
                    echo "</ul>";
                    echo "</li>";                                
                }

                ?>
            </ul>
        </li>

        <!-- STUDY MATERIAL -->
        <!--<li class="<?php if ($page_name == 'study_material') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/study_material'); ?>">
                <i class="entypo-book-open"></i>
                <span>Material de Estudo</span>
            </a>
        </li>-->

        <!-- ACADEMIC SYLLABUS -->
        <!--<li class="<?php if ($page_name == 'academic_syllabus') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/academic_syllabus'); ?>">
                <i class="entypo-doc"></i>
                <span>Programa Acadêmico</span>
            </a>
        </li>-->

        <!-- DAILY ATTENDANCE -->
        <li class="<?php if (
                        $page_name == 'manage_attendance' ||
                        $page_name == 'manage_attendance_view' ||
                        $page_name == 'attendance_report' ||
                        $page_name == 'attendance_report_view' ||
                        $page_name == 'apagar_frequencia'
                    )
                        echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-chart-area"></i>
                <span>Controle de Frequência</span>
            </a>
            <ul>
            <?php
                $id_teacher = $this->session->userdata('login_user_id');

                $query =

                $this->db->select("projeto.*")
                    ->from("projeto")
                    ->join("nucleo", "projeto.id = nucleo.id_projeto")
                    ->join("instrutor_nucleo", "instrutor_nucleo.id_nucleo = nucleo.id")
                    ->where('instrutor_nucleo.id_instrutor', $id_teacher)
                    ->order_by("projeto.nome", "ASC");
                $query = $this->db->get();
                $projetos = $query->result_array();
                $nucleos = $this->db->get('nucleo')->result_array();
                $classes = $this->db->where("teacher_id", $id_teacher)->get('class')->result_array();
                foreach ($projetos as $pro){
                    echo "<li>  <a href='#'>
                    <span>";
                            echo 'Projeto ';
                            echo $pro['nome'];
                    echo "</span>
                    </a>";

                    echo "<ul>";

                    foreach ($nucleos as $nucleo) {
                        if ($pro['id'] == $nucleo['id_projeto']) {
                            echo '<li><a href="#">';

                            echo '<span>';
                            echo $nucleo['nome_nucleo'];
                            echo "</span></a>";
                            echo "<ul>";
                            foreach ($classes as $clas) {
                                if ($nucleo['id'] == $clas['id_nucleo']) {
                                    echo '<li><a href="' . site_url("teacher/manage_attendance/" . $clas["class_id"]) . '">';

                                    echo '<span>';
                                    echo $clas['name'];
                                    echo "</span></a></li>";
                                }
                            }
                            echo "</ul>";
                        }
                    }
                    echo "</ul>";
                    echo "</li>";                                
                }

                ?>
                
            </ul>

            <ul>

                <li class="<?php if (($page_name == 'attendance_report' || $page_name == 'attendance_report_view')) echo 'active'; ?>">
                    <a href="<?php echo site_url('teacher/attendance_report'); ?>">
                        <span><i class="flaticon-graphic-1"></i>Relatório de Frequência</span>
                    </a>
                </li>

            </ul>
            <ul>
                <li class="<?php if (($page_name == 'apagar_frequencia')) echo 'active'; ?>">
                    <a href="<?php echo site_url('teacher/apagar_frequencia'); ?>">
                        <span><i class="entypo-cancel-circled"></i>Apagar frequência</span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- EXAMS -->
        <!-- 
        <li class="<?php if ($page_name == 'marks_manage' || $page_name == 'marks_manage_view' || $page_name == 'question_paper') echo 'opened active'; ?> ">
            <a href="#">
                <i class="entypo-graduation-cap"></i>
                <span><?php echo get_phrase('exam'); ?></span>
            </a>
            <ul>
                <li class="<?php if ($page_name == 'marks_manage' || $page_name == 'marks_manage_view') echo 'active'; ?> ">
                    <a href="<?php echo site_url($account_type . '/marks_manage'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('manage_marks'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'question_paper') echo 'active'; ?> ">
                    <a href="<?php echo site_url($account_type . '/question_paper'); ?>">
                        <span><i class="entypo-dot"></i> <?php echo get_phrase('question_paper'); ?></span>
                    </a>
                </li>
            </ul>
        </li>-->

        <!-- LIBRARY -->
        <!--
        <li class="<?php if ($page_name == 'book') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/book'); ?>">
                <i class="entypo-book"></i>
                <span><?php echo get_phrase('library'); ?></span>
            </a>
        </li>-->

        <!-- TRANSPORT -->
        <!--
        <li class="<?php if ($page_name == 'transport') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/transport'); ?>">
                <i class="entypo-location"></i>
                <span><?php echo get_phrase('transport'); ?></span>
            </a>
        </li>-->

        <!-- NOTICEBOARD -->
        <li class="<?php if ($page_name == 'noticeboard') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/noticeboard'); ?>">
                <i class="entypo-doc-text-inv"></i>
                <span>Quadro de Eventos</span>
            </a>
        </li>

        <!-- MESSAGE -->
        <!--<li class="<?php if ($page_name == 'message' || $page_name == 'group_message') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/message'); ?>">
                <i class="entypo-mail"></i>
                <span><?php echo get_phrase('message'); ?></span>
            </a>
        </li>-->

        <!-- ACCOUNT -->
        <li class="<?php if ($page_name == 'manage_profile') echo 'active'; ?> ">
            <a href="<?php echo site_url($account_type . '/manage_profile'); ?>">
                <i class="entypo-lock"></i>
                <span>Conta</span>
            </a> 
        </li>


        <div class="img_app" style="text-align:left; margin-left: 20px; margin-top: 20px;  bottom: 0;">
            <a href="http://appmarketing.com.br" target="_blank"><img src="<?php echo base_url('uploads/logo-app.png'); ?>" style="max-height:23px;" /></a>
            <h4 style="color: #a2a3b7;text-align: -webkit-center;margin-bottom: 25px;font-weight: 300;margin-top: 10px;">
            </h4>
        </div>

    </ul>

</div>