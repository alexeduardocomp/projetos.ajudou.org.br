<?php echo form_open(site_url('teacher/attendance_selector/')); ?>
<div class="panel panel-primary">
	<div class="panel-heading">
		<h3 class="panel-title">Frequência da</h3>
	</div>
	<div class="panel-body">
		<div class="row">
			<div class="col-md-3 col-sm-offset-2">
				<div class="form-group">
					<label class="control-label" style="margin-bottom: 5px;">Data</label>
					<input type="text" class="form-control datepicker" id="data" name="timestamp" data-format="dd-mm-yyyy" value="Selecione uma data" />
				</div>
			</div>

			<?php
			//$query = $this->db->get_where('section' , array('class_id' => $class_id,'teacher_id'=>$this->session->userdata('teacher_id')));

			$query = $this->db->get_where('section', array(
				'class_id' => $class_id
			));


			if ($query->num_rows() > 0) :
				$sections = $query->result_array();


			?>

				<div class="col-md-3" style="display: none;">
					<div class="form-group">
						<label class="control-label" style="margin-bottom: 5px;">Sessão</label>
						<select class="form-control selectboxit" name="section_id">
							<?php foreach ($sections as $row) : ?>
								<option value="<?php echo $row['section_id']; ?>"><?php echo $row['name']; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			<?php endif; ?>


			<input type="hidden" name="class_id" value="<?php echo $class_id; ?>">
			<input type="hidden" name="year" value="<?php echo $running_year; ?>">

			<div class="col-md-3" style="margin-top: 20px;">
				<button type="submit" class="btn btn-info" id="submit">Gerenciar frequência</button>
			</div>

		</div>
	</div>
</div>
<?php echo form_close(); ?>

<script type="text/javascript">
	$('#data').change(function() {

		var data = $('#data').val();
		//data2 = String(data);
		//var date = new Date(data2);
		//alert(data2);

		split = data.split('-');
		novadata = split[1] + "/" + split[0] + "/" + split[2];
		data_input = new Date(novadata);


		const atual = new Date();
		atual.setHours(0);
		atual.setMinutes(0);
		atual.setMilliseconds(0);
		atual.setSeconds(0);


		var ano = data_input.getFullYear();
		//alert(ano);

		if (ano < 1970) {

			alert('Valor de data não é permitido');
			$('#submit').attr('disabled', 'disabled');
		} else {

			if (data_input.getTime() === atual.getTime()) {
				//alert('As datas são iguais');
				$('#submit').removeAttr('disabled');
			} else if (data_input.getTime() > atual.getTime()) {

				// Se minha data informada for maior do que minha data atual não permito fazer a busca pelos alunos
				//alert(data_input.toString() + ' maior que ' + atual.toString());
				$('#submit').attr('disabled', 'disabled');

			} else {

				$('#submit').removeAttr('disabled');
				//alert(data_input.toString() + ' menor que ' + atual.toString());
			}

		}

	});
</script>