<?php

require_once __DIR__ . '/vendor/autoload.php';

error_reporting(1);
ini_set('display_errors', 1);

require_once 'phplot.php';

$data_inicial = $_GET['data_inicial'];
$data_final = $_GET['data_final'];

$projeto_id = $_GET['projeto'];
$nome_projeto = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->nome;
$tipo_projeto = $this->db->get_where('projeto', array('id' => $projeto_id))->row()->tipo_projeto;

$nucleo_id = $_GET['nucleo'];
$nome_nucleo = $this->db->get_where('nucleo', array('id' => $nucleo_id))->row()->nome_nucleo;

$this->db->select("count(*) as soma, problema_saude");
$this->db->join("enroll", "student.student_id = enroll.student_id");
$this->db->where("problema_saude<>", "");
$this->db->where("id_projeto", $projeto_id);
$this->db->where("id_nucleo", $nucleo_id);
$this->db->where("DATE_FORMAT(FROM_UNIXTIME(enroll.date_added), '%Y-%m-%d') between '$data_inicial' AND '$data_final'");
$this->db->group_by("problema_saude");
$dados = $this->db->get("student")->result();

$logo = "";
if ($tipo_projeto == "FEDERAL") {
  $logo =  '<td ROWSPAN="2" width="120" style="text-align:center;">
    <div><img src="' . base_url() . 'uploads/logos/federal.jpg " width="100" style="padding:5px 0px;"></div>
  </td>';
} elseif ($tipo_projeto == "ESTADUAL") {
  $logo = '<td ROWSPAN="2" width="120" style="text-align:center;">
    <div><img src="' . base_url() . 'uploads/logos/estadual.jpg " width="100" style="padding:5px 0px;"></div>
  </td>';
}

$cabecalho = '
  <table border="1" cellspacing="0" cellpadding="0" width="100%">
    <tr>
      <td width="170" style="text-align:center;">
        <div class="imagem"><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
      </td>
      <td style="text-align:center;">
      <div class="titulo" style="text-transform:uppercase;">GRÁFICO DE PROBLEMAS DE SAÚDE</div>
      </td>
      ' . $logo . '
     
    </tr>
    <tr>
      <td style="padding:10px 15px;" width="170">
        Críterios utilizados:
      </td>
      <td style="padding:10px 15px;">
        <table border="0" cellspacing="0" cellpadding="0" width="100%">
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Projeto: </b> ' . $nome_projeto . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Núcleo: </b> ' . $nome_nucleo . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data início: </b> ' . date("d/m/Y", strtotime($data_inicial)) . '</p>
              </div>
            </td>
          </tr>
          <tr>
            <td style="padding-bottom:5px;" width="300">
              <div>
                <p><b>Data fim: </b> ' . date("d/m/Y", strtotime($data_final)) . '</p>
              </div>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
  <br>';
$total = $cabecalho;

# The data labels aren't used directly by PHPlot. They are here for our
# reference, and we copy them to the legend below.
$data = array();

foreach ($dados as $d) {

  $data[] = array($d->problema_saude, $d->soma);
}

$plot = new PHPlot(800, 600);

$plot->SetPlotType('pie');
$plot->SetDataType('text-data-single');
$plot->SetDataValues($data);
$plot->SetShading(0);

$comAcentos = array('à', 'á', 'â', 'ã', 'ä', 'å', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ü', 'ú', 'ÿ', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'O', 'Ù', 'Ü', 'Ú');
$semAcentos = array('a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', '0', 'U', 'U', 'U');

foreach ($data as $row) {
  $row2 = implode(': ', $row);
  $legenda = str_replace($comAcentos, $semAcentos, $row2);
  $plot->SetLegend($legenda);
}


# Place the legend in the upper left corner:
$plot->SetLegendPixels(5, 5);

$plot->DrawGraph();
$total .= "<img src='" . $plot->EncodeImage() . "'>";


$tabela =
  '
<table border="1" cellspacing="0" cellpadding="0" width="100%">
  <tr>
    <th>Problemas de Saúde</th>
    <th>Quantidade</th>
  </tr>';

foreach ($dados as $dado) {

  $tabela .= " <tr>";
  $tabela .= "  <td>" . $dado->problema_saude . "</td>
      <td>" . $dado->soma . "</td>
    </tr>
  ";
}

$tabela .= "</table>";
$total .= $tabela;

$mpdf = new \Mpdf\Mpdf([
  'margin_top' => '0',
  'debug' => true,
  'setAutoTopMargin' => 'pad',
  'pagenumPrefix' => 'Página ',
  'pagenumSuffix' => ' - ',
  'nbpgPrefix' => ' de ',
  'nbpgSuffix' => ' páginas / '
]);

$mpdf->shrink_tables_to_fit = 1.4;
$mpdf->AddPage('P');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');

$mpdf->WriteHTML($total, \Mpdf\HTMLParserMode::HTML_BODY);
ob_clean();
$mpdf->Output();
