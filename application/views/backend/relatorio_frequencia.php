<?php

require_once __DIR__ . '/vendor/autoload.php';

$projeto = $_GET['projeto'];
$nucleo = $_GET['nucleo'];
$turma = $_GET['turma'];
$data = $_GET['data'];
$evento = $_GET['evento'];
//$data_final = $_GET['data_final'];

$nome_evento = $this->db->get_where('noticeboard', array('notice_id' => $evento))->row()->notice_title;

$nome_projeto = $this->db->get_where('projeto', array('id' => $projeto))->row()->nome;
$nome_turma = $this->db->get_where('class', array('class_id' => $turma))->row()->name;

$time_start = $this->db->get_where('class', array('class_id' => $turma))->row()->time_start;
$time_end = $this->db->get_where('class', array('class_id' => $turma))->row()->time_end;
$time_start_min = $this->db->get_where('class', array('class_id' => $turma))->row()->time_start_min;
$time_end_min = $this->db->get_where('class', array('class_id' => $turma))->row()->time_end_min;

$nome_nucleo = $this->db->get_where('nucleo', array('id' => $nucleo))->row()->nome_nucleo;
$id_instrutor = $this->db->get_where('class', array('class_id' => $turma))->row()->teacher_id;
$nome_instrutor = $this->db->get_where('teacher', array('teacher_id' => $id_instrutor))->row()->name;

$alunos = $this->ajaxload->get_alunos($turma);

// $html3 = $projeto . " / " . $nucleo . " / " . $turma . " / " . $data .' / '.$nome_projeto. ' / '.$nome_nucleo.' / '.$nome_turma;

//Eu ja pego os dadso que são enviados da página de relatorio
//falta fazer um controle melhor dos dados na página
//fazer o select e preencher os dados no pdf

$cabecalho = '
<table border="1" cellspacing="0" cellpadding="0" width="100%">
<tr>
<td width="170" style="text-align:center;">
<div class="imagem" ><img src="https://ajudou.org/wp-content/uploads/2019/07/logo-escalada.png" width="100" style="padding:5px 0px;"></div>
</td>
<td style="text-align:center;">
<div class="titulo" style="text-transform:uppercase;">LISTA DE PRESENÇA PADRÃO PARA PROJETOS - Eventos</div>
</td>
</tr>
<tr>
<td style="padding:10px 15px;" width="170">
Críterios utilizados:
</td>
<td style="padding:10px 15px;">
<table border="0" cellspacing="0" cellpadding="0" width="100%">
<tr><td style="padding-bottom:5px;" width="300"><div><p><b>Projeto: </b> ' . $nome_projeto . '</p></div></td> <td style="padding-bottom:5px;"><div><p><b>Evento: </b>' . $nome_evento . '</p></div></td><td><div><p><b>Horário: </b>' . $time_start . ':' . $time_start_min . ' às ' . $time_end . ':' . $time_end_min . ' </p></div></td></tr>
<tr><td style="padding-bottom:5px;"><div><p><b>Núcleo: </b> ' . $nome_nucleo . ' </p></div></td><td><div><p><b>Instrutor: </b>' . $nome_instrutor . '</p></div></td></tr>
<tr><td style="padding-bottom:5px;"><div><p><b>Turma: </b>' . $nome_turma . '</p></div></td><td><div><p><b>Data: </b>' . $data . ' </p></div></td></tr>

</table>
</td>
</tr>
</table>';

$tabela_cabeca = '<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<div class="relatorio" style="font-family: Roboto, sans-serif;"><table border="1" cellspacing="0" cellpadding="0" width="100%" style="margin-left:20px; margin-right:20px; text-align:center;">
<tr><td colspan="3" style="padding:5px; text-align:center; background:#5f727f; color:#fff; text-transform:uppercase;"><b>Relação de participantes</b></td></tr>
<tr><td style="padding:5px; width:5%;"><div><b>#</b></div></td>
<td style="padding:5px; width:55%;"><div><b>Nome dos presentes</b></div></td>
<td style="padding:5px; width:40%;"><div><b>Assinatura</b></div></td></tr>';
$cont = 1;
foreach ($alunos as $al) {
    $corpo_tabela = $corpo_tabela . '<tr><td>' . $cont . '</td><td style="text-align:left; padding-left:30px; padding-top:5px; padding-bottom:5px;">' . $al->name . '</td><td></td></tr>';
    $cont++;
}

$fim_tabela = '</table></div>';
$assinatura = "<div style='text-align:center; position:absolute; bottom:100px; left:30px;'>Assinatura do Profissional(is) responsável(is): ______________________________________________________________________</div>";

$tabela_completa = $tabela_cabeca . $corpo_tabela . $fim_tabela . $assinatura;

$mpdf = new \Mpdf\Mpdf([
    'margin_top' => '-5',
    'setAutoTopMargin' => 'pad',
    'pagenumPrefix' => 'Página ',
    'pagenumSuffix' => ' - ',
    'nbpgPrefix' => ' de ',
    'nbpgSuffix' => ' páginas / '
]);
$mpdf->SetHTMLHeader($cabecalho);
$mpdf->AddPage('L');
$mpdf->setFooter('{PAGENO}{nbpg}{DATE j-m-Y}');
$mpdf->WriteHTML($tabela_completa, \Mpdf\HTMLParserMode::HTML_BODY);
$mpdf->Output();
